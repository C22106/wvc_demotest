package destinations.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class FooterLinksPage_IOS extends FooterLinksPage {

	public static final Logger log = Logger.getLogger(FooterLinksPage_IOS.class);

	public FooterLinksPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		ourCompanyLink = By.xpath("//ul[@class='wyn-footer__sublinks']//a[text()='Our Company']");
		aboutWyndhamTab = By.xpath("//span[text()='About Wyndham']");
		socialResponsibilities = By.xpath("//span[text()='Social Responsibility']");
		privacyNoticeMailAdd = By
				.xpath("//b[contains(.,'Nevada Residents')]//following::a[contains(.,'Privacy@wyn.com')]");		
		privacySettingsVal = By
                .xpath("//h1[contains(.,'Interest Based Advertising Policies') or contains(.,'Opt Out')]");
        footerBrandLogos = By.xpath("//div[@class='wyn-footer__mobile-brand-links wyn-footer__links ']//a");
        footerLegalLinks = By.xpath("//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li");
       
        footerPrivacyNotice = By.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[2]");
        footerPrivacySettings = By.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[3]");
        footerTimeshare = By.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[4]");
        footerCopyright = By.xpath("//div[contains(@class,'body-2 text-white') and contains(.,'© Wyndham Destinations 2021. All Rights Reserved.')]");
        footerWynLogo = By.xpath("//div[@class='cell']/a/img");
        buttonCareers = By.xpath("//a[@class='subtitle-3' and contains(.,'Careers')]");
        buttonContactUs = By.xpath("//a[@class='subtitle-3' and contains(.,'Contact')]");
        footerWynPages = By.xpath("//ul[@class= 'menu vertical emptyAccordion']/li");
        footerDoNotSell = By.xpath("(//a[contains(text(),'Do Not Sell')])[2]");
	}

	/**
	 * 
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public By newsMediaLink = By.xpath("//span[text()='News & Media']");
	private By footerTermsOfUse = By.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[1]");
    private By footerAboutWyn = By.xpath("//div[@class='footer']//a[contains(.,'About Wyndham')]");
    private By footerNewsMedia = By.xpath("//div[@class='footer']//a[contains(.,'News & Media')]");
    private By footersocialResp = By.xpath("//div[@class='footer']//a[contains(.,'Social Responsibility')]");
    private By footerourCompany = By.xpath("//div[@class='footer']//a[contains(.,'Our Company')]");

	@Override
	/*
	 * Method: footerLinksValidation Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void footerLinksValidation() {
		List<WebElement> legalList = driver.findElements(footerWynPages);
		getElementInView(footerWynLogo);
		Assert.assertTrue(verifyObjectDisplayed(footerWynLogo));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerLinksValidation", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of Quick links: " + legalList.size());

			for (int i = 0; i < legalList.size(); i++) {
				// List<WebElement> legalList1 =
				// driver.findElements(footerWynPages);

				if (i == 0) {
					//clickElementBy(buttonContactUs);
					scrollDownForElementJSBy(buttonContactUs);
					clickIOSElement("Contact Us", 1, buttonContactUs);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateContact();
					navigateBack();
				} else if (i == 1) {
					//clickElementBy(buttonCareers);
					//clickElementJSWithWait(buttonCareers);
					scrollDownForElementJSBy(buttonCareers);
					clickIOSElement("Careers", 1, buttonCareers);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateCareers();
				} else {
					break;
				}

			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerLinksValidation", Status.FAIL,
					"Wyndham Quick link is not present in the Footer");
		}
	}

	/*
	 * Method: validateCareers Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */
	@Override
	public void validateCareers() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilObjectVisible(driver, ourCareersHeader, 120);

		if (driver.getTitle().toUpperCase().contains("Travel + Leisure Careers")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCareers", Status.PASS,
					"WYNDHAM DESTINATIONS CAREERS Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCareers", Status.FAIL,
					"WYNDHAM DESTINATIONS CAREERS Page could not be Navigated");
		}

	}

	/*
	 * Method: validateContact Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */
	@Override
	public void validateContact() {

		waitUntilObjectVisible(driver, ourContactUsHeader, 120);

		if (driver.getTitle().toUpperCase().contains("CONTACT US")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateContact", Status.PASS, "News Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateContact", Status.FAIL,
					"News Page could not be Navigated");
		}

	}

	/*
	 * Method: footerPrivacySection Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	@Override
	public void footerPrivacySection() {
		List<WebElement> legalList = driver.findElements(footerLegalLinks);
		Assert.assertTrue(verifyObjectDisplayed(footerCopyright));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of privacy links: " + legalList.size());

			for (int i = 0; i < legalList.size(); i++) {

				if (i == 0) {
					//clickElementBy(footerTermsOfUse);
					scrollDownForElementJSBy(footerTermsOfUse);
					clickIOSElement("Terms Of Use", 1, footerTermsOfUse);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateTermsOfUse();
					navigateBack();
				} else if (i == 1) {
					//clickElementBy(footerPrivacyNotice);
					scrollDownForElementJSBy(footerPrivacyNotice);
					clickIOSElement("Privacy Notice", 1, footerPrivacyNotice);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validatePrivacyNotice();
					navigateBack();
				} else if (i == 2) {
					//clickElementBy(footerPrivacySettings);
					scrollDownForElementJSBy(footerPrivacySettings);
					clickIOSElement("Privacy Settings", 1, footerPrivacySettings);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validatePrivacySettings();
				} else if (i == 3) {
					//clickElementBy(footerTimeshare);
					scrollDownForElementJSBy(footerTimeshare);
					clickIOSElement("Proudly Supports Timeshare.com", 1, footerTimeshare);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateSupportTimeshare();
				}
				
				scrollDownForElementJSBy(footerCopyright);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Wyndham Brand Logos Present in the not present in the Footer");
		}
		scrollDownForElementJSBy(footerDoNotSell);
		if (verifyObjectDisplayed(footerDoNotSell)) {
			clickIOSElement("Do Not Sell My", 1, footerDoNotSell);
			verifyObjectDisplayed(optOutHeader);
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Footer do not sell is present ");
			navigateBack();

		} else {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Footer do not sell is not present ");
		}

	}

	/*
	 * Method: validateTermsOfUse Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */
	@Override
	public void validateTermsOfUse() {
		waitUntilObjectVisible(driver, termsOfUseHeader, 120);
		getElementInView(termsOfUseHeader);
		if (verifyObjectDisplayed(termsOfUseHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.PASS,
					"Terms Of Use Page Navigated");

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.FAIL,
					"Terms Of Use Page could not be Navigated");
		}

	}

	/*
	 * Method: validatePrivacyNotice Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validatePrivacyNotice() {

		waitUntilObjectVisible(driver, privacyNoticeHeader, 120);
		getElementInView(privacyNoticeHeader);
		if (verifyObjectDisplayed(privacyNoticeHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.PASS,
					"Privacy Notice Navigated");

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.FAIL,
					" Privacy Notice could not be Navigated");
		}
	}

	/*
	 * Method: validatePrivacySettings Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validatePrivacySettings() {
		String url = testData.get("URL");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if(tabs.size() == 2)
		{
			waitUntilObjectVisible(driver, privacySettingsVal, 120);
			getElementInView(privacySettingsVal);
			if (verifyObjectDisplayed(privacySettingsVal)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
						"Privacy Settings Page Navigated");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
						"Privacy Settings Page could not be Navigated");
			}
		}
		
		navigateToURL(url);
		pageCheck();
	}

	/*
	 * Method: validateSupportTimeshare Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateSupportTimeshare() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String url = testData.get("URL");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		//driver.switchTo().window(tabs.get(1));
		if(tabs.size() == 2)
		{
			if (driver.getTitle().toUpperCase().contains("TIMESHARE")) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
						"timeshare Page Navigated");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
						"timeshare could not be Navigated");
			}
		}
		navigateToURL(url);
		pageCheck();
	}

	@SuppressWarnings("rawtypes")
	public void footerSocialIcons() {

		String URL = driver.getCurrentUrl();
		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;
				String imagePath = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/facebooklogo.png";
					} else {
						imagePath = "PUBLIC:/automation_images/facebooklogo_ipad.png";
					}

					break;
				case 1:
					strSiteName = "TWITTER";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/twitterlogo.png";
					} else {
						imagePath = "PUBLIC:/automation_images/twitterlogo_ipad.png";
					}

					break;
				case 2:
					strSiteName = "INSTAGRAM";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/instalogo.png";
					} else {
						imagePath = "PUBLIC:/automation_images/instalogo_ipad.png";
					}

					break;
				case 3:
					strSiteName = "YOUTUBE";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/youtubelogo.png";
					} else {
						imagePath = "PUBLIC:/automation_images/youtubelogo_ipad.png";
					}

					break;
				case 4:
					strSiteName = "LINKED";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/linkedinlogo.png";
					} else {
						imagePath = "PUBLIC:/automation_images/linkedinlogo_ipad.png";
					}

					break;

				default:
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				/*
				 * //clickElementJS(socialIconsList.get(i));
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * ArrayList<String> tabs2 = new
				 * ArrayList<String>(driver.getWindowHandles());
				 * driver.switchTo().window(tabs2.get(1));
				 * waitForSometime(tcConfig.getConfig().get("LongWait"));
				 */
				try {
					clickIOSImage(imagePath, socialIconsList.get(i));
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Error message is " + ex.getMessage());
				}

				if (driver.getCurrentUrl().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.get(URL);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	public void footerWynLinks() {

		if (verifyObjectDisplayed(footerWynPages)) {
			String URL = driver.getCurrentUrl();
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
					"Wyndham Destinations Sub Links Present in the Footer,Total: " + wynLinksList.size());

			driver.findElement(aboutWyndhamTab).click();
			if (verifyObjectDisplayed(footerWyndhamHotels)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Wyndham Hotels & Resort");

				clickElementBy(footerWyndhamHotels);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (driver.getCurrentUrl().toUpperCase().contains("WYNDHAMHOTELS")) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
							"Wyndham Hotel & Resort Associated Link Opened");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
							"Wyndham Hotel & Resort Associated Link opening error");

				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}
			driver.get(URL);
			scrollDownForElementJSBy(footer);
			driver.findElement(newsMediaLink).click();
			if (verifyObjectDisplayed(footerFactSheetLink)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Wyndham Destinations Fact Sheet Link Present in footer");
				clickElementBy(footerFactSheetLink);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(factSheetHeader)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
							"User Navigated to Fact Sheet Page");

					if (verifyObjectDisplayed(factSheetLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"View Fact Sheet Link Present in the content");
						/*
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "View Fact Sheet Link Present in the content");
						 * 
						 * clickIOSElement("Click here", 1, factSheetLink);
						 * 
						 * if(((RemoteWebDriver)
						 * driver).getCapabilities().getCapability("model").
						 * toString().contains("iPad")){ clickIOSElement(
						 * "Click here", 1, factSheetLink); }else{
						 * clickElementBy(factSheetLink); }
						 * 
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * ArrayList<String> tabs2 = new
						 * ArrayList<String>(driver.getWindowHandles());
						 * 
						 * if (browserName.equalsIgnoreCase("IE")) {
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * }
						 * 
						 * String strPdf =
						 * driver.findElement(factSheetPdf).getAttribute("type")
						 * ; String strURL = driver.getCurrentUrl(); if
						 * (strPdf.toUpperCase().contains("PDF") ||
						 * strURL.toUpperCase().contains("PDF")) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Associated PDF opened");
						 * 
						 * 
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get(
						 * "LowWait")); driver.switchTo().window(tabs2.get(0));
						 * 
						 * driver.navigate().back(); // driver.get(URL);
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * 
						 * if (verifyObjectDisplayed(footerWynLogo)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated Back and Wyndham Logo present in the footer"
						 * ); clickElementBy(footerWynLogo);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * if (verifyObjectDisplayed(footer)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated to homepage");
						 * 
						 * scrollDownForElementJSBy(footer);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to homepage failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to Wyn Destination Failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Fact Sheet Associated PDF opening problem"); }
						 * 
						 */} else if (verifyObjectDisplayed(factSheetUpLink)) {
						/*
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "View Fact Sheet Link Present in the content");
						 * String strCurrURL = driver.getCurrentUrl();
						 * scrollDownForElementJSBy(factSheetUpLink);
						 * scrollUpByPixel(150); clickIOSElement("Click here",
						 * 1, factSheetLink);
						 * 
						 * if(((RemoteWebDriver)
						 * driver).getCapabilities().getCapability("model").
						 * toString().contains("iPad")){ clickIOSElement(
						 * "Click here", 1, factSheetLink); }else{
						 * clickElementBy(factSheetUpLink); }
						 * 
						 * 
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * ArrayList<String> tabs2 = new
						 * ArrayList<String>(driver.getWindowHandles());
						 * driver.switchTo().window(tabs2.get(1)); if
						 * (browserName.equalsIgnoreCase("IE")) {
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * } String strPdf = ""; String flag = ""; if
						 * (browserName.equalsIgnoreCase("firefox")) { strPdf =
						 * driver.findElement(factSheetPdf).getAttribute("class"
						 * ); } else if (((RemoteWebDriver)
						 * driver).getCapabilities().getCapability("model").
						 * toString() .contains("iPhone")) {
						 * switchToContext(driver, "VISUAL"); Map<String,
						 * Object> params = new HashMap<>();
						 * params.put("content",
						 * "PUBLIC:automation_images/factsheetiphone.png");
						 * params.put("context", "body"); flag = (String)
						 * ((JavascriptExecutor)
						 * driver).executeScript("mobile:image:find", params);
						 * 
						 * }
						 * 
						 * else { strPdf = driver.getCurrentUrl(); } if
						 * (strPdf.toUpperCase().contains("PDF") ||
						 * flag.equalsIgnoreCase("true")) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Associated PDF opened");
						 * 
						 * // driver.close();
						 * 
						 * driver.get(strCurrURL);
						 * waitForSometime(tcConfig.getConfig().get(
						 * "LowWait")); driver.switchTo().window(tabs2.get(0));
						 * 
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * if (((RemoteWebDriver)
						 * driver).getCapabilities().getCapability("model").
						 * toString() .contains("iPhone")) { //
						 * driver.get(strCurrURL); switchToContext(driver,
						 * "NATIVE_APP");
						 * 
						 * try { // driver.findElement(By.xpath(
						 * "//*[@label=\"Settings\"]")).click();
						 * driver.findElement(By.xpath(
						 * "//XCUIElementTypeButton[@label=\"Back\"]")).click();
						 * } catch (Exception e) {
						 * 
						 * System.out.println("no element found"); }
						 * switchToContext(driver, "WEBVIEW"); } else {
						 * driver.get(strCurrURL); }
						 * scrollDownForElementJSBy(factSheetUpLink);
						 * scrollUpByPixel(150); if
						 * (verifyObjectDisplayed(footerWynLogo)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated Back and Wyndham Logo present in the footer"
						 * ); clickElementBy(footerWynLogo);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * if (verifyObjectDisplayed(footer)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated to homepage");
						 * 
						 * scrollDownForElementJSBy(footer);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to homepage failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to Wyn Destination Failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Fact Sheet Associated PDF opening problem"); }
						 * 
						 */} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"View Fact Sheet Link not Present in the content");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
							"Navigation to fact sheet page failed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Destinations Fact Sheet Link not Present in footer");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	protected By footerBrandLogos_iPad = By.xpath("//div[@class='wyn-footer__brands wyn-l-padding-xlarge--bottom']//a");

	public void footerBrands() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(footerBrandLogos) || verifyObjectDisplayed(footerBrandLogos_iPad)) {
			List<WebElement> brandsList;
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				brandsList = driver.findElements(footerBrandLogos_iPad);
			} else {
				brandsList = driver.findElements(footerBrandLogos);
			}

			scrollDownForElementJSWb(brandsList.get(brandsList.size() - 1));
			tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total: " + brandsList.size());

			clickIOSElement("REWARDS", 1, brandsList.get(brandsList.size() - 1));
			// clickElementJS(brandsList.get(brandsList.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			// updated as of 1023
			/*
			 * ArrayList<String> tabs2 = new
			 * ArrayList<String>(driver.getWindowHandles());
			 * driver.switchTo().window(tabs2.get(1));
			 */
			if (browserName.equalsIgnoreCase("IE")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			if (driver.getTitle().toUpperCase().contains("REWARDS")) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.PASS,
						"Wyndham Rewards Page Opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// driver.close();
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				// driver.switchTo().window(tabs2.get(0));

				waitUntilObjectVisible(driver, footer, 120);
				if (verifyObjectDisplayed(footer)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.PASS,
							"Navigated Back to homepage");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.FAIL,
							"Homepage Navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.FAIL,
						"Wyndham Rewards Page not Opened");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.FAIL,
					"Wyndham Brands Logo not present in footer");
		}
	}

	public void footerTermsOfUse() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(0))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
					"Terms Of Use Link present in the footer");

			clickElementWb(legalList.get(0));
			waitUntilObjectVisible(driver, termsOfUseHeader, 120);

			if (verifyObjectDisplayed(termsOfUseHeader)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
						"Terms Of Use Page Navigated");

				if (verifyObjectDisplayed(termsOfUseBreadcrumb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Terms Of Use Breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Terms Of Use Breadcrumb not present");
				}

				if (verifyObjectDisplayed(aboutWyndhamTab) && verifyObjectDisplayed(socialResponsibilities)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"About Wyndham and Social Responsibilities Tab present");

					clickElementBy(aboutWyndhamTab);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(ourCompanyLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"User has the option to navigate to Our Company page in the Secondary Navigation");
						clickElementBy(aboutWyndhamTab);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Our Company Link is not present in the Secondary Navigation");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"About Wyndham and Social Responsibilities Tab not present");
				}

				if (verifyObjectDisplayed(termsOfUseContent)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Content present in the Terms of Use page");
					if (verifyObjectDisplayed(termsOfUseMail)) {
						scrollDownForElementJSBy(termsOfUseMail);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"Copyright Violation Email Link present");

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Copyright violation email link not present");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Content not present in the Terms of Use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
						"Terms Of Use Page could not be Navigated");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
					"Terms Of Use Link not present in the footer");
		}

	}

	public void footerPrivacySettings() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);
		scrollDownForElementJSWb(legalList.get(2));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(2))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
					"Privacy Settings Link present in the Footer");
			String URL = driver.getCurrentUrl();
			clickIOSElement("Privacy Setting", 1, legalList.get(2));
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (verifyObjectDisplayed(privacySettingsVal)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
						"Navigated to Privacy Settings related page");
				driver.get(URL);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				legalList = driver.findElements(footerLegalLinks);
				scrollDownForElementJSWb(legalList.get(2));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(legalList.get(2))) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
							"Navigated Back to Terms of Use page");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
							"Unable to navigate to Terms of use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
						"Unable to navigate to Privacy Settings page");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
					"Privacy Settings Link not present in the Footer");
		}

	}

	@Override
	public void footerPrivacyNotice() {
		String URL = driver.getCurrentUrl();
		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(1))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
					"Privacy Notice Link present in the Footer");

			// clickElementWb(legalList.get(1));
			clickIOSElement("Privacy Notice", 1, legalList.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String URL2 = driver.getCurrentUrl();
			if (verifyObjectDisplayed(privacyNotice)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
						"Privacy Notice page navigated and header present");
				if (verifyObjectDisplayed(privacyNoticeBrdcrmb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice breadcrumb not present");
				}

				if (verifyObjectDisplayed(privacyNoticeMaterialUpdate)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Material Update Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Material Update Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeIntroduction)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Intrdouction Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Intrdouction Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeCookies)) {
					scrollDownForElementJSBy(privacyNoticeCookies);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Cookies link displayed");

					// clickElementBy(privacyNoticeCookies);
					clickIOSElement("CLICK HERE", 1, privacyNoticeCookies);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					/*
					 * ArrayList<String> tabs2 = new
					 * ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(tabs2.get(1)); if
					 * (browserName.equalsIgnoreCase("IE")) {
					 * waitForSometime(tcConfig.getConfig().get("MedWait"));
					 * waitForSometime(tcConfig.getConfig().get("LongWait")); }
					 */

					waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (verifyObjectDisplayed(privacySettingsVal)) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Cookies page navigated");
						// driver.close();
						driver.get(URL2);
						/*
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * driver.switchTo().window(tabs2.get(0));
						 */
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Cookies page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Cookies link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeGAnalytics)) {
					/*
					 * scrollDownForElementJSBy(privacyNoticeGAnalytics);
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * scrollUpByPixel(90);
					 */

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Google Analytics link displayed");
					scrollDownForElementJSBy(privacyNoticeGAnalytics);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					// clickElementBy(privacyNoticeGAnalytics);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickIOSElement("http://tools.google.com/dlpage", 1, privacyNoticeGAnalytics);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					/*
					 * ArrayList<String> tabs2 = new
					 * ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(tabs2.get(1));
					 */
					// waitForSometime(tcConfig.getConfig().get("LongWait"));

					/*
					 * if (browserName.equalsIgnoreCase("IE")) {
					 * waitForSometime(tcConfig.getConfig().get("MedWait")); }
					 * waitUntilObjectVisible(driver, privacySettingsVal, 120);
					 */
					if (driver.getCurrentUrl().toUpperCase().contains("GOOGLE")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Google Analytics page navigated");
						// driver.close();
						driver.get(URL2);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.switchTo().window(tabs2.get(0));
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 */

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Google Analytics page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Google Analytics link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeSignalTrack) && verifyObjectDisplayed(privacyNoticeAds)) {
					scrollDownForElementJSBy(privacyNoticeSignalTrack);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Signal Track section displayed and ADS link displayed");
					// clickElementBy(privacyNoticeAds);
					scrollDownForElementJSBy(privacyNoticeAds);
					clickElementJS(privacyNoticeAds);
					clickIOSElement("www.aboutads.info", 1, privacyNoticeAds);
					/*
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * ArrayList<String> tabs2 = new
					 * ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(tabs2.get(1));
					 * 
					 * if (browserName.equalsIgnoreCase("IE")) {
					 * waitForSometime(tcConfig.getConfig().get("MedWait")); }
					 */
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("YOURADCHOICES.COM")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"DAA page navigated");
						// driver.close();
						driver.get(URL2);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.switchTo().window(tabs2.get(0));
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 */

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"DAA page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Signal Track section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeNwAdv)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Network Adv link displayed");
					// clickElementBy(privacyNoticeNwAdv);
					scrollDownForElementJSBy(privacyNoticeNwAdv);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					scrollUpByPixel(150);
					clickIOSElement("www.netw", 1, privacyNoticeNwAdv);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					/*
					 * ArrayList<String> tabs2 = new
					 * ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(tabs2.get(1));
					 * waitForSometime(tcConfig.getConfig().get("LongWait"));
					 * 
					 * if (browserName.equalsIgnoreCase("IE")) {
					 * waitForSometime(tcConfig.getConfig().get("MedWait")); }
					 */
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("NETWORK")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Network Adv page navigated");
						// driver.close();
						driver.get(URL2);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.switchTo().window(tabs2.get(0));
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 */

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Network Adv page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Network Adv link not displayed");
				}

				/*
				 * if (verifyObjectDisplayed(privacyNoticeWhatInfo)) {
				 * 
				 * scrollDownForElementJSBy(privacyNoticeWhatInfo);
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * scrollUpByPixel(150);
				 * tcConfig.updateTestReporter("FooterLinksPage",
				 * "footerPrivacyNotice", Status.PASS,
				 * "What Information Is Collected and How It Is Collected? Section displayed"
				 * ); } else { tcConfig.updateTestReporter("FooterLinksPage",
				 * "footerPrivacyNotice", Status.FAIL,
				 * "What Information Is Collected and How It Is Collected? Section not displayed"
				 * ); }
				 */
				if (verifyObjectDisplayed(privacyNoticeMailAdd)) {

					scrollDownForElementJSBy(privacyNoticeMailAdd);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Mail link displayed");

					driver.switchTo().activeElement().sendKeys(Keys.HOME);

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Mail link not displayed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
						"Privacy Notice page navigation unsuccessful");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
					"Privacy Notice Link not present in the Footer");
		}
	}
}
