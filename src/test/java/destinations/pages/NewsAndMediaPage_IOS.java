package destinations.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NewsAndMediaPage_IOS extends NewsAndMediaPage {

	public static final Logger log = Logger.getLogger(NewsAndMediaPage_IOS.class);

	public NewsAndMediaPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public void pressReleaseValidations() throws ParseException {

		waitUntilElementVisibleBy(driver, newsAndMediaTab, 120);

		if (verifyObjectDisplayed(newsAndMediaTab)) {
			tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
					"News And Media Tab present in the Global Navigation of the homepage");
			if (browserName.equalsIgnoreCase("firefox")) {
				Actions a = new Actions(driver);
				a.moveToElement(driver.findElement(newsAndMediaTab)).click().build().perform();
			} else {
				clickElementBy(newsAndMediaTab);
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(pressReleaseTab)) {
				tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
						"Press Release Present under News and Media tab");

				clickElementBy(pressReleaseTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, headerPressRelease, 120);

				if (verifyObjectDisplayed(headerPressRelease)) {
					tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
							"Press Release Page Opened");

					if (verifyObjectDisplayed(breadCrumbPress)) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Breadcrumb Navigation Present");

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Breadcrumb Navigation error");
					}

					List<WebElement> dateSortCheck = driver.findElements(pressRelaseDate);

					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy");
					Date d2 = null;
					Boolean b3 = null;
					for (int i = 0; i < dateSortCheck.size(); i++) {

						Date d1 = sdf.parse(dateSortCheck.get(i).getText());

						if (i >= 1) {
							Boolean b1 = d1.before(d2);
							Boolean b2 = d1.equals(d2);
							if (b1 == true || b2 == true) {
								b3 = true;
								d2 = d1;
							} else {
								b3 = false;
								break;
							}
						} else {
							d2 = d1;
						}

					}

					if (b3 == true) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Date is sorted in Newest to Oldest Manner By Default");
					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Date is not sorted in Newest to oldest manner");
					}

					if (verifyObjectDisplayed(sortSelect)) {

						new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (oldest first)");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						List<WebElement> dateSortCheck1 = driver.findElements(pressRelaseDate);

						SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd,yyyy");
						Date d3 = null;
						Boolean b6 = null;
						for (int i = 0; i < dateSortCheck1.size(); i++) {

							Date d4 = sdf1.parse(dateSortCheck1.get(i).getText());

							if (i >= 1) {
								Boolean b4 = d4.after(d3);
								Boolean b5 = d4.equals(d3);
								if (b4 == true || b5 == true) {
									b6 = true;
									d3 = d4;
								} else {
									b6 = false;
									break;
								}
							} else {
								d3 = d4;
							}

						}

						if (b6 == true) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Date is sorted in Ascending order");
						} else {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Date is not sorted in Ascending order");
						}

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Sorting Option Not Available");
					}

					new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (newest first)");

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					List<WebElement> numberOfArticles = driver.findElements(pressReleaseArticles);
					String strfirstNewsHeader = numberOfArticles.get(0).getText().toUpperCase();
					if (verifyElementDisplayed(numberOfArticles.get(0))) {

						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Articles present in Press Release Page, Header of first Article: "
										+ numberOfArticles.get(0).getText());

						try {
							List<WebElement> readMore = driver.findElements(readMoreCTA);
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Read More CTA present, total: " + readMore.size());

						} catch (Exception e) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Read More CTA not present");
						}

						// scrollDownByPixel(40);
						String URL = driver.getCurrentUrl();
						scrollDownForElementJSWb(numberOfArticles.get(0));
						numberOfArticles.get(0).click();
						try {
							clickIOSElement("Read More", 1, numberOfArticles.get(0));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						waitForSometime(tcConfig.getConfig().get("LongWait"));

						if (driver.findElement(clickedArticleHeader).getText().toUpperCase()
								.contains(strfirstNewsHeader)) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"First Article Associated page opened");

							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Error in opening first header associated page");
						}
						driver.get(URL);
						waitUntilElementVisibleBy(driver, headerPressRelease, 120);
						if (verifyObjectDisplayed(headerPressRelease)) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Navigated Back to Press Release Page");

							if (verifyObjectDisplayed(loadMoreButton)) {
								scrollDownForElementJSBy(loadMoreButton);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								scrollUpByPixel(200);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								List<WebElement> numberOfArticlesB4 = driver.findElements(pressReleaseArticles);
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
										"Load More Button Present and Number of Articles before clicking: "
												+ numberOfArticlesB4.size());

								clickElementBy(loadMoreButton);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								List<WebElement> numberOfArticlesAfter = driver.findElements(pressReleaseArticles);

								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
										"Number of Articles After Clicking: " + numberOfArticlesAfter.size());

							} else {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
										"Load More Button Not Present on Homepage");
							}

						} else {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Press Release Navigation Failed");
						}
					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Articles not present in Press Release Page");
					}

				} else {
					tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
							"Could not open press release page");
				}

			} else {
				tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
						"Press Release not Present under News and Media tab");
			}

		} else {
			tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
					"Unable to find News and Media Tab");
		}

	}

}
