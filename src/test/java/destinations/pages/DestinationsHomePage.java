package destinations.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class DestinationsHomePage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(DestinationsHomePage.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public DestinationsHomePage(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected String url;

	// New Xpath for Destination Site
	protected By destinationLogo = By.xpath("//a[img[contains(@src,'logo')] and parent::div[contains(@class,'logo')]]");

	protected By destinationHeroImage = By.xpath("//div[@class='wyn-hero__image']");
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");

	protected By vacationTab = By.xpath("//div[@class='wyn-top-nav wyn-top-nav--divider']//a[contains(.,'Book')]");
	protected By vacationSubMenu = By.xpath("//div[@class='wyn-header__expand-widget-desktop']//h3");
	protected By hotelsAndResortsLogo = By.xpath("//img[@alt='Wyndham Hotels & Resorts']");
	protected By popUp = By.xpath("//div[@id='email-popup-content-wrapper']//button//span");
	protected By vacationRentalsLogo = By.xpath("//img[@title='Wyndham Vacation Rentals']");
	protected By resortLogo = By.xpath("//img[@id='header-logo' and @class='max-width hide-xs']");

	protected By searchIconActive = By.xpath("//button[@class='wyn-js-search-input__toggle is-active']");

	protected By carouselArrowNext = By
			.xpath("//div[@class='wyn-l-wrapper--edge']//button[@class='slick-arrow slick-next']");
	protected By carouselArrowPrev = By
			.xpath("//div[@class='wyn-l-wrapper--edge']//button[@class='slick-arrow slick-prev']");
	protected By exploreWyndham = By.xpath("//h3//span[contains(.,'EXPLORE WYNDHAM')]");
	protected By container = By.xpath("//div[@class='wyn-card-content__container']");
	protected By containerTitle = By.xpath(
			"//div[@class='wyn-card-content__container']//div[@class='wyn-type-title-1 wyn-l-margin-small--bottom']");
	protected By containerImage = By.xpath("//div[@class='wyn-card-content__container']//img");
	protected By containerLocation = By.xpath("//div[@class='wyn-card-content__container']//span[@class='strong']");
	protected By containerCard = By.xpath("//div[@class='wyn-js-expand-card wyn-expand-card is-active']");
	protected By cardLearnMore = By
			.xpath("//div[@class='wyn-js-expand-card wyn-expand-card is-active']//a[contains(.,'Learn')]");
	protected By cardClose = By.xpath(
			"//div[@class='wyn-js-expand-card wyn-expand-card is-active']//a[@class='wyn-js-expand-card__close wyn-expand-card__close']");

	protected By weBelieveData = By.xpath(
			"//div[@class='wyn-content-block-stats']//div[@class='wyn-content-block-stats_data wyn-type-display-3']");
	/*
	 * protected By RCIExchange = By.xpath(
	 * "//div//span[contains(.,'RCI EXCHANGES')]"); protected By rciReadMore =
	 * By.xpath(
	 * "//span[contains(.,'RCI EXCHANGES')]/ancestor::div[@class='wyn-l-padding-medium--top wyn-l-flex']//a"
	 * );
	 */

	protected By RCIExchange = By.xpath("//h2[contains(.,'Open up your vacation')]");
	protected By rciReadMore = By.xpath(
			"//h2[contains(.,'Open up your vacation')]/ancestor::div[@class='column wyn-content-block__content']//a");
	protected By wyndhamVacationrentals = By.xpath("//span[contains(.,'WYNDHAM VACATION RENTALS')]");
	protected By ourInvestorsHeader = By.xpath("//h2[contains(.,'Our Investors')]");
	protected By ourInvestorslearnMore = By
			.xpath("//h2[contains(.,'Our Investors')]/ancestor::div[@class='column wyn-content-block__content']//a");
	protected By newsSeeMore = By.xpath("//div[@class='latestarticles']//div[@class='columns wyn-type--center']//a");
	protected By pressReleaseHeader = By.xpath("//span[contains(.,'Press Release')]");
	protected By latestArticleHeader = By.xpath("//div[@class='latestarticles']//article//h3/a");
	protected By latestArticleReadMore = By.xpath("//div[@class='latestarticles']//article/a");
	protected By clickedArticleHeader = By.xpath("//h1");

	protected By socialIconsFooter = By.xpath("//div[@class='column is-narrow wyn-footer__social']");
	protected By totalSocialIcons = By
			.xpath("//div[@class='column is-narrow wyn-footer__social']//div[@class='column']");
	protected By ourLeadershipFooter = By.xpath("//div[@class='wyn-footer']//a[contains(.,'Our Leadership')]");

	protected By corporateSocialRespHeader = By.xpath("//li[@class='has-submenu']//a[contains(.,'Corporate')]");
	protected By workforceDiversityHeader = By
			.xpath("//div[@class='wyn-fly-out__container']//a[contains(.,'Workforce Diversity')]");

	protected By ieSupportBanner = By.xpath("//div[@class='wyn-ie-alert-message__panel']");
	protected By closeieSupport = By.xpath("//a[@id='alertClose']");

	protected By panoramaHeader = By.xpath("//span[contains(text(),'PANORAMA')]");
	protected By panoramaReadMore = By
			.xpath("//span[contains(text(),'PANORAMA')]//ancestor::div[@class='column wyn-content-block__content']//a");

	public void searchField() {

		waitUntilObjectVisible(driver, searchIcon, 120);

		if (verifyObjectDisplayed(ieSupportBanner)) {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
					"IE Support Banner Present");
			try {
				clickElementBy(closeieSupport);
			} catch (Exception e) {
				System.out.println("Close Error");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
					"IE Support Banner not Present");
		}

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer)) {

				// fieldDataEnter(searchContainer, testData.get("strSearch"));

				driver.findElement(searchContainer).click();
				driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				;
				tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.PASS,
						"Search Keyword Entered");

				clickElementBy(searchButton);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	public void validateLogo() {
		url = tcConfig.getTestData().get("URL");
		assertTrue(verifyObjectDisplayed(destinationLogo), "Header Destination logo is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateLogo", Status.PASS,
				"Header Destination logo is displayed");

		assertTrue(getElementAttribute(destinationLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header Destination logo url not correct");
		tcConfig.updateTestReporter("DestinationHomePage", "validateLogo", Status.PASS,
				"Header Wyndham logo is present");
	}

	protected By bookYourVacation = By
			.xpath("//div[contains(@class,'preMenu')]//a[contains(text(),'Book Your Vacation')]");
	protected By exchangeWithRCI = By
			.xpath("//div[contains(@class,'preMenu')]//a[contains(text(),'Exchange with RCI')]");
	protected By contactUs = By.xpath("//div[contains(@class,'preMenu')]//a[contains(text(),'Contact Us')]");

	public void validateHomeHeaderMenu() {

		assertTrue(verifyObjectDisplayed(bookYourVacation), "Book Your Vacation menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Book Your Vacation menu item is present");
		navigateValidateAndReturn(bookYourVacation);

		assertTrue(verifyObjectDisplayed(exchangeWithRCI), "Exchange With RCI menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Exchange With RCI menu item is present");
		navigateValidateAndReturn(exchangeWithRCI);

		assertTrue(verifyObjectDisplayed(contactUs), "Contact Us menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Contact Us menu item is present");
		getElementInView(contactUs);
		navigateValidateAndReturnToPage(contactUs);

		assertTrue(verifyObjectDisplayed(career), "Career menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Career menu item is present");
	}

	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href").trim();
		clickElementBy(link);
		pageCheck();
		waitForSometime("5");
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() > 0) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");

	public void navigateValidateAndReturnToPage(By link) {

		String href = getElementAttribute(link, "href");
		clickElementBy(link);
		pageCheck();
		String title = driver.getTitle().trim();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : "
						+ getCurrentURL().trim());
		tcConfig.updateTestReporter("DestinationHomePage", "navigateValidateAndReturnToContactUs", Status.PASS,
				"Page : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	protected By ourCompany = By.xpath("//li/a[contains(text(),'Our Company')]");
	protected By ourBrand = By.xpath("//li/a[contains(text(),'Our Brands')]");
	protected By vacationClub = By.xpath("//li/a[contains(text(),'Why Vacation Clubs')]");
	protected By wyndhamCares = By.xpath("//li/a[contains(text(),'Wyndham Cares')]");
	protected By news = By.xpath("//li/a[contains(text(),'News')]");
	protected By career = By.xpath("//li/a[contains(text(),'Careers')]");

	public void validateHeaderMenu() {

		assertTrue(verifyObjectDisplayed(ourCompany), "Our Company menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Our Company menu item is present");
		navigateValidateAndReturnToPage(ourCompany);

		assertTrue(verifyObjectDisplayed(ourBrand), "Our Brand menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Our Brand menu item is present");
		navigateValidateAndReturnToPage(ourBrand);

		assertTrue(verifyObjectDisplayed(vacationClub), "Vacation Clubs menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Vacation Clubs menu item is present");
		navigateValidateAndReturnToPage(vacationClub);

		assertTrue(verifyObjectDisplayed(wyndhamCares), "Wyndham Cares menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Wyndham Cares menu item is present");
		navigateValidateAndReturnToPage(wyndhamCares);

		assertTrue(verifyObjectDisplayed(news), "News menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"News menu item is present");
		navigateValidateAndReturnToPage(news);

		// navigateValidateAndReturn(career);

	}

	protected By clubWyndhamLogo = By.xpath("//img[@class='logo' and @alt='Wyndham']");
	protected By worldMarkWyndhamLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
	protected By margaritavilleLogo = By
			.xpath("//div[contains(@class,'MGVCHeader show-for-large')]//img[contains(@src,'logo')]");
	protected By shellVacationLogo = By.xpath("//img[@alt='Shell Vacations Club Logo']");

	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		hoverOnElement(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]"));

		assertTrue(getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//..//ul/li")).size() == count,
				"List size doesn't match, expected count=" + count + " | actual count="
						+ getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//..//ul/li")).size());

		assertTrue(
				verifyObjectDisplayed(
						By.xpath("//ul[contains(@class,'align-top menu')]/li/a[contains(text(),'" + subMenu + "')]")),
				headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

		tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
				headerMenuItem + " menu item's sub menu item " + subMenu + " is present");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(
				By.xpath("//ul[contains(@class,'align-top menu')]/li/a[contains(text(),'" + subMenu + "')]"));
		String href = getElementAttribute(
				By.xpath("//ul[contains(@class,'align-top menu')]/li/a[contains(text(),'" + subMenu + "')]"), "href")
						.trim();

		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			switch (subMenu) {
			case "Club Wyndham":
				waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Club Wyndham page navigation is Successful");
				break;
			case "WorldMark by Wyndham":
				// waitUntilElementLocated(driver, worldMarkWyndhamLogo, 240);
				Assert.assertTrue(/*
									 * verifyObjectDisplayed(worldMarkWyndhamLogo)
									 */
						getCurrentURL().contains("https://www.worldmarktheclub.com/"), "Worldmark page is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Worldmark page navigation is Successful");
				break;
			case "Margaritaville Vacation Club":
				waitUntilElementLocated(driver, margaritavilleLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo), "Margaritaville Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Margaritaville page navigation is Successful");
				break;
			case "Shell Vacation Clubs  ":
				waitUntilElementLocated(driver, shellVacationLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Shell Vacation page navigation is Successful");
				break;
			}
			navigateToMainTab(allTabs);
			pageCheck();

		} else {
			switch (subMenu) {
			case "Club Wyndham":
				waitUntilElementLocated(driver, clubWyndhamLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Club Wyndham page navigation is Successful");
				break;
			case "WorldMark by Wyndham":
				// waitUntilElementLocated(driver, worldMarkWyndhamLogo, 240);
				Assert.assertTrue(getCurrentURL().contains(href), "Worldmark page is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Worldmark page navigation is Successful");
				break;
			case "Margaritaville Vacation Club":
				waitUntilElementLocated(driver, margaritavilleLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo), "Margaritaville Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Margaritaville page navigation is Successful");
				break;
			case "Shell Vacation Clubs  ":
				waitUntilElementLocated(driver, shellVacationLogo, 240);
				Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
				tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						"Shell Vacation page navigation is Successful");
				break;
			}
			navigateBack();
			pageCheck();
		}

	}

	protected By bannerImg = By.xpath("//div[contains(@class,'image-quote-banner')]");
	protected By bennerTitle = By
			.xpath("//div[contains(@class,'image-quote-banner')]//div[contains(@class,'title-1')]");
	protected By ourCompanyHeader = By
			.xpath("(//div[contains(@class,'contentSlice')]//div[contains(@class,'title-1')])[1]");
	protected By ourCompanyBodyContent = By
			.xpath("(//div[contains(@class,'contentSlice')]//div[contains(@class,'body-1')])[1]");

	public void validateBanner() {

		driver.get(url);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(bannerImg), "Banner image is absent");
		tcConfig.updateTestReporter("DestinationHomePage", "validateBanner", Status.PASS,
				"Homepage Banner image is present");

		/*
		 * assertTrue(verifyObjectDisplayed(bennerTitle),
		 * "Banner title is absent");
		 * assertTrue(getElementText(bennerTitle).trim().length() > 0,
		 * "Home page Banner title is blank");
		 */

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(ourCompanyHeader);
		assertTrue(verifyObjectDisplayed(ourCompanyHeader), "Company header is absent");
		tcConfig.updateTestReporter("DestinationHomePage", "validateBanner", Status.PASS, "Company Header is present");

		assertTrue(verifyObjectDisplayed(ourCompanyBodyContent), "Company Content is absent");
		assertTrue(getElementText(ourCompanyBodyContent).trim().length() > 0, "Company Title is blank");

	}

	protected By brandHeader = By.xpath("//div[contains(text(),'Our Brands')]");
	protected By brandImage = By.xpath("//div[contains(@class,'cardComponent')]//a/img");
	protected By brandName = By.xpath("//div[contains(@class,'cardComponent')]//div[contains(@class,'subtitle-2')]");
	protected By brandDescription = By
			.xpath("//div[contains(@class,'cardComponent')]//div[contains(@class,'body-1')]/p");
	protected By learnMoreCTA = By
			.xpath("//div[contains(@class,'cardComponent')]//div[contains(@class,'body-1-link')]");

	public void validateBrand() {
		assertTrue(verifyObjectDisplayed(brandHeader), "Our Brand Header is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand header present");

		List<WebElement> brandImg = getList(brandImage);
		List<WebElement> brandNme = getList(brandName);
		List<WebElement> brandDes = getList(brandDescription);
		List<WebElement> brandCTA = getList(learnMoreCTA);

		if (brandImg.size() == brandNme.size()) {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand Name present");
			if (brandImg.size() == brandDes.size()) {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
						"Our Brand Description present");
				if (brandImg.size() == brandCTA.size()) {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
							"Our Brand Learn More CTA present");
				} else {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
							"Our Brand Learn More CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
						"Our Brand Description present");
			}
		} else {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL, "Our Brand Name present");
		}

		for (int i = 0; i < brandImg.size(); i++) {
			brandNme = getList(brandName);
			brandCTA = getList(learnMoreCTA);
			String brandName = getElementText(brandNme.get(i));
			clickElementBy(brandCTA.get(i));
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				waitForSometime("5");
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				waitForSometime("2");
				navigateToMainTab(allTabs);
				pageCheck();

			} else {
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				navigateBack();
				pageCheck();
			}

		}
	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it.");
				 * report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components",
							"brokenLinkValidation", Status.FAIL, "[Response Code: " + respCode + "] [Response message: "
									+ respMessage + "] [URL:" + url + "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

	protected By whyVacationClubsImage = By
			.xpath("//div[contains(.,'Why Vacation Clubs')]/div[@gtm_component]//div[@class='cardBanner']//img");
	protected By whyVacationClubsTitle = By.xpath(
			"//div[contains(.,'Why Vacation Clubs')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1']");
	protected By whyVacationClubsBody = By.xpath(
			"//div[contains(.,'Why Vacation Clubs')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')]");
	protected By btnLearnMorewhyVacayClubs = By
			.xpath("//div[contains(.,'Why Vacation Clubs')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a");

	public void validateWhyVacationClubs() {

		getElementInView(whyVacationClubsImage);
		assertTrue(verifyObjectDisplayed(whyVacationClubsImage), "Why Vacation Clubs image is absent");
		tcConfig.updateTestReporter("DestinationsHomePage", "validateWhyVacationClubs", Status.PASS,
				"Why Vacation Clubs Image is present");
		assertTrue(verifyObjectDisplayed(whyVacationClubsTitle), "Why Vacation Clubs title is absent");
		assertTrue(getElementText(whyVacationClubsTitle).trim().length() > 0, "Why Vacation Clubs title is blank");
		String title = getElementText(whyVacationClubsTitle).trim();

		assertTrue(verifyObjectDisplayed(whyVacationClubsBody), "Why Vacation Clubs body is absent");
		assertTrue(getElementText(whyVacationClubsBody).trim().length() > 0, "Why Vacation Clubs body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorewhyVacayClubs), "Why Vacation Clubs Learn more button is absent");

		String href = getElementAttribute(btnLearnMorewhyVacayClubs, "href");
		clickElementBy(btnLearnMorewhyVacayClubs);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("DestinationsHomePage", "validateWhyVacationClubs", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	protected By vacationReadyImage = By.xpath(
			"//div[contains(.,'Vacation Ready')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//img");
	protected By vacationReadyTitle = By.xpath(
			"//div[contains(.,'Vacation Ready')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//div[contains(@class,'title-1')]");
	protected By vacationReadyBody = By.xpath(
			"//div[contains(.,'Vacation Ready')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//div[contains(@class,'body-1')]");
	protected By btnLearnMoreVacationReady = By.xpath(
			"//div[contains(.,'Vacation Ready')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//a");

	public void validateVacationReady() {

		getElementInView(vacationReadyImage);
		assertTrue(verifyObjectDisplayed(vacationReadyImage), "Vacation Ready image is absent");
		tcConfig.updateTestReporter("DestinationsHomePage", "validateVacationReady", Status.PASS,
				"Vacation Ready Image is present");
		assertTrue(verifyObjectDisplayed(vacationReadyTitle), "Vacation Ready title is absent");
		assertTrue(getElementText(vacationReadyTitle).trim().length() > 0, "Vacation Ready title is blank");
		String title = getElementText(vacationReadyTitle).trim();

		assertTrue(verifyObjectDisplayed(vacationReadyBody), "Vacation Ready body is absent");
		assertTrue(getElementText(vacationReadyBody).trim().length() > 0, "Vacation Ready body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreVacationReady), "Vacation Ready Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreVacationReady, "href");
		clickElementBy(btnLearnMoreVacationReady);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String getURL = driver.getCurrentUrl().trim();
			assertTrue(getURL.equalsIgnoreCase(href), "Navigation to learn more link not correct");
			tcConfig.updateTestReporter("DestinationsHomePage", "validateWhyVacationClubs", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String getURL = driver.getCurrentUrl().trim();
			assertTrue(getURL.equalsIgnoreCase(href), "Navigation to learn more link not correct");
			tcConfig.updateTestReporter("DestinationsHomePage", "validateWhyVacationClubs", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();

		}

	}

	protected By wyndhamCaresImage = By.xpath(
			"//div[contains(.,'Wyndham Cares')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//img");
	protected By WyndhamCaresTitle = By.xpath(
			"//div[contains(.,'Wyndham Cares')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//div[contains(@class,'title-1')]");
	protected By wyndhamCaresBody = By.xpath(
			"//div[contains(.,'Wyndham Cares')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//div[contains(@class,'body-1')]");
	protected By btnLearnMoreWyndhamCares = By.xpath(
			"//div[contains(.,'Wyndham Cares')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//a");

	public void validateWyndhamCares() {

		getElementInView(wyndhamCaresImage);
		assertTrue(verifyObjectDisplayed(wyndhamCaresImage), "Wyndham Cares image is absent");
		tcConfig.updateTestReporter("DestinationsHomePage", "validateWyndhamCares", Status.PASS,
				"Wyndham Cares Image is present");
		assertTrue(verifyObjectDisplayed(WyndhamCaresTitle), "Wyndham Cares title is absent");
		assertTrue(getElementText(WyndhamCaresTitle).trim().length() > 0, "Wyndham Cares title is blank");
		String title = getElementText(WyndhamCaresTitle).trim();

		assertTrue(verifyObjectDisplayed(wyndhamCaresBody), "Wyndham Cares body is absent");
		assertTrue(getElementText(wyndhamCaresBody).trim().length() > 0, "Wyndham Cares body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreWyndhamCares), "Wyndham Cares Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreWyndhamCares, "href");
		clickElementBy(btnLearnMoreWyndhamCares);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");
			navigateToMainTab(allTabs);
			pageCheck();

		} else {
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("DestinationsHomePage", "validateWyndhamCares", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	protected By aboutUsHeader = By.xpath("//h1[contains(text(),'About Us')]");
	protected By brandLinksAboutUs = By.xpath("//div[contains(@class,'body-1')]/p/a");

	public void navigateToOurCompany() {
		if (verifyObjectDisplayed(ourCompany)) {
			clickElementJS(ourCompany);
			waitUntilElementVisibleBy(driver, aboutUsHeader, 120);
		}
	}

	public void validateAboutUs() {
		String currentURL = "";
		String title = "";
		getElementInView(aboutUsHeader);
		assertTrue(verifyObjectDisplayed(aboutUsHeader), "About US Header is absent");
		tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
				getElementText(aboutUsHeader).trim() + " " + "is displayed");

		List<WebElement> links = getList(brandLinksAboutUs);
		for (int i = 0; i < links.size() - 1; i++) {
			String brandName = getElementText(links.get(i)).trim();
			String href = getElementAttribute(links.get(i), "href");
			if (brandName.contains("Margaritaville")) {
				href = "https://mgvc.wyndhamdestinations.com/";
			}
			clickElementBy(links.get(i));
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() > 1) {
				newTabNavigations(allTabs);
				currentURL = driver.getCurrentUrl().trim();
				title = driver.getTitle().trim();
				switch (brandName) {
				case "Club Wyndham":
					Assert.assertTrue(title.contains(brandName), "Club Wyndham Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "Worldmark® by Wyndham":
					Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Worldmark Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club® by Wyndham":
					Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Margaritaville Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Wyndham Destinations vacation clubs":
					Assert.assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
							"Vacation Club Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Vacation Club page navigation is Successful");
					break;
				}
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				currentURL = driver.getCurrentUrl().trim();
				title = driver.getTitle().trim();
				switch (brandName) {
				case "Club Wyndham":
					Assert.assertTrue(title.contains(brandName), "Club Wyndham Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "Worldmark® by Wyndham":
					Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Worldmark Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club® by Wyndham":
					Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Margaritaville Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Wyndham Destinations vacation clubs":
					Assert.assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
							"Vacation Club Page is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateAboutUs", Status.PASS,
							"Vacation Club page navigation is Successful");
					break;
				}
				waitForSometime("3");
				navigateBack();
				pageCheck();
			}

		}
	}

	public void bookYourVacationValidations() {

		if (verifyObjectDisplayed(vacationTab)) {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
					"Vacation Tab present");

			driver.findElement(vacationTab).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> listTab = driver.findElements(vacationSubMenu);

			if (listTab.size() == 3) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2." + listTab.get(1).getText()
								+ " 3." + listTab.get(2).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(hotelsAndResortsLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Hotel Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Hotel and Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Hotel Tab not present");
				}

				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

				//
				if (verifyElementDisplayed(listTab.get(2))) {
					clickElementWb(listTab.get(2));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}

			} else if (listTab.size() == 2) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2."
								+ listTab.get(1).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						try {
							driver.switchTo().alert().accept();
						} catch (Exception e) {
							System.out.println("Alert Not Present");
						}

						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}
				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

			} else if (listTab.size() == 0) {
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (verifyObjectDisplayed(resortLogo)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Book Your Resort Associated Hotel site is navigated");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Failed in navigating to Wyndham Resort site");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

				waitUntilObjectVisible(driver, vacationTab, 120);

				if (verifyObjectDisplayed(vacationTab)) {

					// driver.findElement(vacationTab).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Navigated back to homepage");
				} else {

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Homepage Navigation Failed");

				}

			} else {

				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
						"Sub Booking Menu Not Present");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
					"Vacation Tab is not present");
		}

	}

	public void homepageCarouselFunctionality() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollDownForElementJSBy(exploreWyndham);

		waitUntilObjectVisible(driver, exploreWyndham, 120);

		scrollUpByPixel(100);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(exploreWyndham)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
					"Explore Wyndham Destinations Content Block present in homepage");

			List<WebElement> containerList = driver.findElements(container);

			if (containerList.size() < 5) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
						"Explore Wyndham Destinations Content Block present but there are less than 5 cards so no Carousel");
			} else {
				waitUntilObjectVisible(driver, carouselArrowNext, 120);
				if (verifyObjectDisplayed(carouselArrowNext)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
							"Caraousel Next Arrow Present");

					for (int i = 0; i < 6; i++) {

						if (verifyObjectDisplayed(carouselArrowNext)) {
							driver.findElement(carouselArrowNext).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else if (verifyObjectDisplayed(carouselArrowPrev)) {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.PASS, "No more containers, caraousel Prev Arrow Present");
							break;
						} else {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.FAIL, "No more containers, caraousel Prev Arrow not Present");
							break;
						}
					}

					if (verifyObjectDisplayed(carouselArrowPrev)) {
						for (int j = 0; j < 6; j++) {
							if (verifyObjectDisplayed(carouselArrowPrev)) {
								driver.findElement(carouselArrowPrev).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));

							} else if (verifyObjectDisplayed(carouselArrowNext)) {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.PASS, "Navigated Back to the first Container");
								break;
							} else {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.FAIL, "Failed to navigate back to first container");
								break;
							}
						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.FAIL, "Failed to navigate back to first container");
					}

					// List<WebElement> containerList =
					// driver.findElements(container);
					List<WebElement> containerTitleList = driver.findElements(containerTitle);
					List<WebElement> containerImageList = driver.findElements(containerImage);
					List<WebElement> containerLocationList = driver.findElements(containerLocation);

					int intContainer = containerList.size();
					String strfirstTitle = containerTitleList.get(0).getText().toUpperCase();
					String strfirstTitlefinal = strfirstTitle.split(" ")[0];
					System.out.println(strfirstTitlefinal);
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
							"Total Number of Container Present: " + intContainer);

					if (verifyElementDisplayed(containerList.get(0))
							&& verifyElementDisplayed(containerTitleList.get(0))
							&& verifyElementDisplayed(containerImageList.get(0))) {

						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.PASS,
								"First Container image present, with Title: " + containerTitleList.get(0).getText()
										+ " and Location: " + containerLocationList.get(0).getText());

						clickElementJS(containerImageList.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(cardLearnMore)) {

							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.PASS, "First Container Card Opened and Learn More button present");

							clickElementBy(cardLearnMore);
							waitForSometime(tcConfig.getConfig().get("LongWait"));

							ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
							driver.switchTo().window(tabs2.get(1));
							System.out.println(driver.getTitle().toUpperCase());
							By pageTitle = By.xpath("//title[contains(.,'" + strfirstTitle + "')]");
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.getTitle().toUpperCase().contains(strfirstTitlefinal)) {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.PASS, "Associated Page Opened");
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.close();
								waitForSometime(tcConfig.getConfig().get("MedWait"));

								driver.switchTo().window(tabs2.get(0));
								waitUntilObjectVisible(driver, cardClose, 120);
								if (verifyObjectDisplayed(cardClose)) {
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.PASS, "Navigated Back to homepage");
									scrollUpByPixel(100);
									clickElementBy(cardClose);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.PASS, "Container Card Closed");

								} else {
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.FAIL, "Container Card could not be Closed");
								}

							} else {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.FAIL, "Unable to open associated page");
							}

						} else {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.FAIL, "Container Card could not be Opened");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.FAIL, "Error in getting First Container details");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.FAIL,
							"Carousel is not present");
				}

			}
		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.FAIL,
					"Explore Wyndham Destinations Content Block is not present in homepage");
		}

	}

	public void homepageWeBelieveVal() {
		List<WebElement> weBelieveDataList = driver.findElements(weBelieveData);
		String strDestinations = weBelieveDataList.get(0).getText();
		String strCountries = weBelieveDataList.get(1).getText();
		String strAssociates = weBelieveDataList.get(2).getText();

		waitUntilElementVisibleWb(driver, weBelieveDataList.get(0), "120");

		scrollDownForElementJSWb(weBelieveDataList.get(0));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(weBelieveDataList.get(0))) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageWeBelieveVal", Status.PASS,
					"Wyndham Destination We Believe Block Present on Homepage with Destinations: " + strDestinations
							+ " Countries: " + strCountries + " and Associate: " + strAssociates);

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageWeBelieveVal", Status.FAIL,
					"Error in getting we believe data");
		}

	}

	public void homepageRCIExchange() {
		waitUntilElementVisibleBy(driver, RCIExchange, 120);

		scrollDownForElementJSBy(RCIExchange);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(100);

		if (verifyObjectDisplayed(RCIExchange)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
					"RCI Exchange Block Present on the homepage");

			clickElementBy(rciReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			if (browserName.equalsIgnoreCase("CHROME") || browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("EDGE")) {
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (driver.getTitle().toUpperCase().contains("RCI")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
							"RCI Exchange related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
							"RCI Exchange related page not opened");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().window(tabs2.get(0));
				waitUntilObjectVisible(driver, RCIExchange, 120);
				if (verifyObjectDisplayed(RCIExchange)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
							"Navigated Back to homepage");
				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
							"Error in getting back to homepage");
				}

			} else if (browserName.equalsIgnoreCase("IE")) {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String currentUrl = driver.getCurrentUrl();
				System.out.println(currentUrl);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// String currentTitle= driver.getTitle();

				if (currentUrl.toUpperCase().contains("RCI")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
							"RCI Exchange related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
							"RCI Exchange related page not opened");
				}

				if (tabs2.size() > 2) {
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(tabs2.get(2));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.close();

				} else if (tabs2.size() <= 2) {
					driver.close();
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().window(tabs2.get(0));
				waitUntilObjectVisible(driver, RCIExchange, 120);
				if (verifyObjectDisplayed(RCIExchange)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
							"Navigated Back to homepage");
				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
							"Error in getting back to homepage");
				}

			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
					"RCI Exchange block is not present on homepage");
		}

	}

	public void homepageRentals() {
		waitUntilElementVisibleBy(driver, wyndhamVacationrentals, 120);

		// scrollDownForElementJSBy(wyndhamVacationrentals);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(wyndhamVacationrentals)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRentals", Status.PASS,
					"Vacation Rentals Block Present on the homepage");

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRentals", Status.FAIL,
					"Vacation Rentals block is not present on homepage");
		}

	}

	public void homepageOurInvestors() {
		waitUntilElementVisibleBy(driver, ourInvestorsHeader, 120);

		scrollDownForElementJSBy(ourInvestorsHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollUpByPixel(100);

		if (verifyObjectDisplayed(ourInvestorsHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
					"Our Investors Block Present on the homepage");

			clickElementBy(ourInvestorslearnMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (browserName.equalsIgnoreCase("CHROME") || browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("edge") || browserName.equalsIgnoreCase("")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getTitle().toUpperCase().contains("INVESTOR")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
							"Our Investors related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Our Investors related page not opened");
				}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().window(tabs2.get(0));
				waitUntilObjectVisible(driver, ourInvestorsHeader, 120);
				if (verifyObjectDisplayed(ourInvestorsHeader)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
							"Navigated Back to homepage");
				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Error in getting back to homepage");
				}

			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
					"Our Investors block is not present on homepage");
		}

	}

	public void homepagePanorama() {
		waitUntilElementVisibleBy(driver, panoramaHeader, 120);

		scrollDownForElementJSBy(panoramaHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollUpByPixel(100);

		if (verifyObjectDisplayed(panoramaHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
					"Panorama Block Present on the homepage");

			clickElementBy(panoramaReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (browserName.equalsIgnoreCase("CHROME") || browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("edge") || browserName.equalsIgnoreCase("")) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (driver.getTitle().toUpperCase().contains("PANORAMA")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
							"Panorama related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Panorama related page not opened");
				}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().window(tabs2.get(0));
				waitUntilObjectVisible(driver, panoramaHeader, 120);
				if (verifyObjectDisplayed(panoramaHeader)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
							"Navigated Back to homepage");
				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Error in getting back to homepage");
				}

			} /*
				 * else if (browserName.equalsIgnoreCase("IE")) {
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * try { if (isAlertPresent()) {
				 * driver.switchTo().alert().accept(); }
				 * 
				 * } catch (Exception e) {
				 * 
				 * }
				 * 
				 * String currentUrl = driver.getCurrentUrl();
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * if (currentUrl.toUpperCase().contains("INVESTOR")) {
				 * tcConfig.updateTestReporter("DestinationsHomePage",
				 * "homepageOurInvestors", Status.PASS,
				 * "Our Investors related page opened");
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
				 * "homepageOurInvestors", Status.FAIL,
				 * "Our Investors related page not opened"); } if (tabs2.size()
				 * > 2) { driver.close();
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.switchTo().window(tabs2.get(2));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.close();
				 * 
				 * } else if (tabs2.size() <= 2) { driver.close(); }
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * driver.switchTo().window(tabs2.get(0));
				 * waitUntilObjectVisible(driver, ourInvestorsHeader, 120); if
				 * (verifyObjectDisplayed(ourInvestorsHeader)) {
				 * tcConfig.updateTestReporter("DestinationsHomePage",
				 * "homepageOurInvestors", Status.PASS,
				 * "Navigated Back to homepage"); } else {
				 * tcConfig.updateTestReporter("DestinationsHomePage",
				 * "homepageOurInvestors", Status.FAIL,
				 * "Error in getting back to homepage"); }
				 * 
				 * }
				 */

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.FAIL,
					"Panorama block is not present on homepage");
		}

	}

	/*
	 * public void homepageLatestNews() { waitUntilElementVisibleBy(driver,
	 * latestNews, 120);
	 * 
	 * scrollDownForElementJSBy(latestNews);
	 * 
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * scrollUpByPixel(200);
	 * 
	 * if (verifyObjectDisplayed(latestNews)) {
	 * 
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "Latest News Block Present on the homepage");
	 * 
	 * clickElementBy(newsSeeMore);
	 * waitForSometime(tcConfig.getConfig().get("LongWait"));
	 * 
	 * if (verifyObjectDisplayed(pressReleaseHeader)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "Press Release Page Opened after clicking see more news");
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * driver.navigate().back();
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilObjectVisible(driver, latestNews, 120); if
	 * (verifyObjectDisplayed(latestNews)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "Navigated Back to homepage"); } else {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.FAIL, "Error in getting back to homepage"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homepageLatestNews", Status.FAIL,
	 * "Error: Press Release page is not opened"); }
	 * 
	 * List<WebElement> newsArticlesList =
	 * driver.findElements(latestArticleHeader); String strfirstNewsHeader =
	 * newsArticlesList.get(0).getText().toUpperCase();
	 * scrollDownForElementJSWb(newsArticlesList.get(0));
	 * 
	 * if (verifyElementDisplayed(newsArticlesList.get(0))) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "Latest News Articles Displayed, Total Article: " +
	 * newsArticlesList.size()); List<WebElement> newsReadMoreList =
	 * driver.findElements(latestArticleReadMore); if
	 * (verifyElementDisplayed(newsReadMoreList.get(0))) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "First article Header: " + strfirstNewsHeader);
	 * 
	 * clickElementWb(newsReadMoreList.get(0));
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (driver.findElement(clickedArticleHeader).getText().toUpperCase().
	 * contains(strfirstNewsHeader)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "First Article Associated page opened");
	 * 
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.navigate().back();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitUntilObjectVisible(driver, latestNews, 120); if
	 * (verifyObjectDisplayed(latestNews)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.PASS, "Navigated Back to homepage"); } else {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews",
	 * Status.FAIL, "Error in getting back to homepage"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homepageLatestNews", Status.FAIL,
	 * "First Article Associated page not opened"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homepageLatestNews", Status.FAIL, "Read More CTA not present"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homepageLatestNews", Status.FAIL, "Article Not Present"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homepageLatestNews", Status.FAIL,
	 * "Latest News block is not present on homepage"); }
	 * 
	 * }
	 */

	protected By latestNews = By.xpath("//div[@class='text-primary title-1' and contains(.,'Latest News')]");
	protected By buttonReadMore = By.xpath(
			"//div[contains(@class,'large-up-3')]/div[contains(@data-ishidden,'false')]/div/div/a/div[contains(@class,'body-1-link')]");
	protected By labelLatestNewsHeader = By.xpath(
			"//div[contains(@class,'large-up-3')]/div[contains(@data-ishidden,'false')]/div/div/a/div[contains(@class,'subtitle-3 bold')]");
	protected By breadcrumbsPressRelease = By.xpath("//ul[@class='breadcrumbs']/li[contains(.,'Press')]");
	protected By buttonViewMore = By
			.xpath("//a[contains(@class,'button showAll secondary') and contains(.,'View More')]");
	protected By buttonViewLess = By
			.xpath("//a[contains(@class,'button showLess secondary') and contains(.,'View less')]");
	protected By labelAllLatestNewsHeader = By
			.xpath("//div[contains(@class,'cardIndex align-center-middle ')]/div/a/div[@class='subtitle-3 bold']");

	public void homepageLatestNews() {
		waitUntilElementVisibleBy(driver, latestNews, 120);

		scrollDownForElementJSBy(latestNews);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(latestNews)) {
			getElementInView(latestNews);
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
					"Latest News Block Present on the homepage");

			List<WebElement> newsArticlesHeader = driver.findElements(labelLatestNewsHeader);
			getElementInView(buttonViewMore);
			clickElementBy(buttonViewMore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(getObject(buttonViewLess).isEnabled(), "View Less button is present");
			List<WebElement> allNewsArticlesHeader = driver.findElements(labelAllLatestNewsHeader);
			if (allNewsArticlesHeader.size() > newsArticlesHeader.size()) {

				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
						"View more button is clicked in Latest News Block on the homepage");

				for (int i = 0; i < /*allNewsArticlesHeader.size()*/3; i++) {
					List<WebElement> allNewsArticlesHeader1 = driver.findElements(labelAllLatestNewsHeader);
					getElementInView(latestNews);
					String strNewsHeader = allNewsArticlesHeader1.get(i).getText().toUpperCase();
					clickElementBy(allNewsArticlesHeader1.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// Assert.assertTrue(verifyObjectDisplayed(breadcrumbsPressRelease),
					// "Breadcurmbs is not present in the navigated page");
					Assert.assertTrue(
							driver.findElement(clickedArticleHeader).getText().toUpperCase().contains(strNewsHeader),
							"Header not matched");
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
							"Successfully navigate to " + (i + 1) + " news article");
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(buttonViewMore);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
						"View more button is unable to click in Latest News Block on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
					"Latest News block is not present on homepage");
		}
	}

	/*
	 * public void homePageValidations() {
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver, destinationHeroImage, 16); if
	 * (verifyObjectDisplayed(destinationHeroImage)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS,
	 * "Destinations HoemPage Displayed, Home Banner Present"); if
	 * (verifyObjectDisplayed(vacationTab)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Vacation Tab present");
	 * 
	 * driver.findElement(vacationTab).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * List<WebElement> list = driver.findElements(vacationSubMenu);
	 * 
	 * if (list.size() == 3) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Sub Booking Menu Present as: " +
	 * list.get(0).getText() + " " + list.get(1).getText() + " " +
	 * list.get(2).getText());
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(vacationTab).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Sub Booking Menu Not Present"); }
	 * 
	 * if (verifyObjectDisplayed(searchIcon)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Search icon present");
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * clickElementBy(searchIcon); // driver.findElement(searchIcon).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * if (verifyObjectDisplayed(searchContainer)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Search Container Present");
	 * 
	 * clickElementBy(searchIconActive);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Search Container not Present"); }
	 * 
	 * scrollDownForElementJSBy(carouselArrowNext); scrollUpByPixel(500);
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (verifyObjectDisplayed(carouselArrowNext)) {
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Caraousel Next Arrow Present");
	 * 
	 * driver.findElement(carouselArrowNext).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * if (verifyObjectDisplayed(carouselArrowPrev)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "No New Resort, Clicking Prev");
	 * driver.findElement(carouselArrowPrev).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
	 * driver.findElement(carouselArrowNext).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Error in finding Carousel"); }
	 * 
	 * scrollDownForElementJSBy(socialIconsFooter); scrollUpByPixel(250);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); if
	 * (verifyObjectDisplayed(socialIconsFooter)) { List<WebElement> listSocial
	 * = driver.findElements(totalSocialIcons);
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS,
	 * "Social Icons Present, total social icons: " + listSocial.size()); } else
	 * { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Social Icons Not Present"); }
	 * 
	 * if (verifyObjectDisplayed(ourLeadershipFooter)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS, "Our Leadership present in footer" );
	 * driver.findElement(ourLeadershipFooter).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Our Leadershio Not Present"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL, "Search icon not present"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL,
	 * "Error in reaching Destinations homepage");
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * public void csrInHeader() { if
	 * (verifyObjectDisplayed(destinationHeroImage)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.PASS,
	 * "Destinations HoemPage Displayed, Home Banner Present");
	 * 
	 * if (verifyObjectDisplayed(corporateSocialRespHeader)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "csrInHeader",
	 * Status.PASS, "Corporate Social Resp present in header");
	 * driver.findElement(corporateSocialRespHeader).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); if
	 * (verifyObjectDisplayed(workforceDiversityHeader)) {
	 * tcConfig.updateTestReporter("DestinationsHomePage", "csrInHeader",
	 * Status.PASS, "workforceDiversityHeader present in header");
	 * driver.findElement(workforceDiversityHeader).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "csrInHeader", Status.FAIL,
	 * "workforceDiversityHeader not present in header"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL,
	 * "Corporate Social Resp not present in header"); }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage",
	 * "homePageValidations", Status.FAIL,
	 * "Destinations HoemPage Displayed, Home Banner Present"); } }
	 * 
	 * 
	 * 
	 */

	protected By searchContainer = By.xpath("//div[@id='searchInput']//input[contains(@class,'searchQuery')]");
	protected By searchIcon = By.xpath("//li[contains(@class,'searchOpen')]//img");
	protected By searchButton = By.xpath("//div[@id='searchInput']//input[contains(@class,'siteSearch')]");
	protected By searchTitle = By.xpath("//div[contains(@class,'search-title')]");
	protected By resultNumber = By.xpath("//div[contains(@class,'resultNumber')]");
	protected By resultList = By.xpath("//ul[contains(@class,'searchResults')]/li//div[@class='searchItemTitle']");
	protected By paginationTab = By.xpath("//ul[contains(@class,'pagination')]");
	protected By paginationNext = By.xpath("//li[@class='pagination-next']");
	protected By firstPageList = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']");
	protected By searchListLink = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']/..");

	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		assertTrue(verifyObjectDisplayed(searchIcon), "Search Icon is not Displayed");
		clickElementBy(searchIcon);
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		fieldDataEnter(searchContainer, searchValue);
		clickElementBy(searchButton);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}

	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchTitle, 120);

		if (verifyObjectDisplayed(searchTitle)) {
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsFor = getElementText(resultNumber);

			String strResults = strResultsFor.split(" ")[0];

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchFunctionality", Status.PASS,
					"Total Results: " + strResults + " for" + strResultsFor);

			List<WebElement> listOfTotalResults = driver.findElements(resultList);
			List<WebElement> listFirstResults = driver.findElements(firstPageList);
			List<WebElement> listCTA = driver.findElements(searchListLink);
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"No Of Results on first page " + listFirstResults.size());

			if (listOfTotalResults.size() >= 10) {
				if (verifyObjectDisplayed(paginationTab)) {
					tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
							"Pagination Available on the page");
				}
			} else {
				System.out.println("Less than 10 results hence no pagination");
			}

			String firstTitle = listFirstResults.get(0).getText();
			String href = getElementAttribute(listCTA.get(0), "href").trim().replace(".html", "");

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"First Result title " + firstTitle);

			clickElementBy(listFirstResults.get(0));
			pageCheck();
			assertTrue(getCurrentURL().trim().contains(href), "Navigations not successfull");

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to first Search related page " + firstTitle);

			driver.navigate().back();

			pageCheck();

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}
}
