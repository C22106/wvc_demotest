package destinations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OurLeadershipsPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(OurLeadershipsPage.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public OurLeadershipsPage(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By ourLeadershipHeader = By.xpath("//div[@class='columns']//h1[contains(.,'Our Leadership')]");
	protected By profileName = By.xpath("//div[@class='profile']//h2");
	protected By profileImage = By.xpath("//div[@class='profile']//img");
	protected By profileDesig = By.xpath("//div[@class='profile']//strong");
	protected By profileDesc = By.xpath("//div[@class='profile']//p");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");

	public void ourLeadershipValidation() {
		if (verifyObjectDisplayed(ourLeadershipHeader)) {
			tcConfig.updateTestReporter("OurLeadershipsPage", "ourLeadershipValidation", Status.PASS,
					"ourLeadershipHeader present");
			// driver.findElement(ourLeadershipHeader).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> nameList = driver.findElements(profileName);
			List<WebElement> imageList = driver.findElements(profileImage);
			List<WebElement> designList = driver.findElements(profileDesig);
			List<WebElement> descList = driver.findElements(profileDesc);

			int size = nameList.size();

			for (int i = 0; i < size; i++) {
				tcConfig.updateTestReporter("OurLeadershipsPage", "ourLeadershipValidation", Status.PASS,
						"Name: " + nameList.get(i).getText() + " Designation: " + designList.get(i).getText());

				if (verifyElementDisplayed(imageList.get(i))) {
					tcConfig.updateTestReporter("OurLeadershipsPage", "ourLeadershipValidation", Status.PASS,
							"Image Present");
				} else {
					tcConfig.updateTestReporter("OurLeadershipsPage", "ourLeadershipValidation", Status.FAIL,
							"Image not Present");
				}

				for (int j = i + 1; j < size; j++) {
					scrollDownForElementJSWb(nameList.get(i + 1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(80);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			}

			if (verifyObjectDisplayed(homeBreadcrumb)) {
				driver.switchTo().activeElement().sendKeys(Keys.HOME);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("ourLeadershipHeader", "ourLeadershipValidation", Status.PASS,
						"BreadCrumb present, clicking Home");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(homeBreadcrumb).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("ourLeadershipHeader", "ourLeadershipValidation", Status.FAIL,
						"BreadCrumb not present");
			}

		} else {
			tcConfig.updateTestReporter("ourLeadershipHeader", "ourLeadershipValidation", Status.FAIL,
					"ourLeadershipHeader not present ");
		}

	}

}
