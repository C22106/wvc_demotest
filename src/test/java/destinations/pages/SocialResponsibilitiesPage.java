package destinations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SocialResponsibilitiesPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(SocialResponsibilitiesPage.class);

	public SocialResponsibilitiesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By socialResp = By.xpath("//div[@class='column wyn-header__navigation']//a[contains(.,'Social')]");

	protected By environmentSust = By
			.xpath("//div[@class='column wyn-header__navigation']//a[contains(.,'Environment')]");
	protected By environmentHeader = By.xpath("//h1[contains(.,'Environment')]");
	protected By environmentBreadCrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Env')]");

	protected By wynBanners = By.xpath("//div[@class='wyn-banner']");

	protected By coreBlock = By.xpath("//div[@class='columns wyn-action-blocks']//a[contains(.,'Core')]");
	protected By coreReadMore = By.xpath("//div[@class='columns wyn-action-blocks'][2]//a[contains(.,'Read')]");
	protected By coreHeader = By.xpath("//h1[contains(.,'Core')]");
	protected By coreBreadCrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Core')]");
	protected By coreActionBlockHeaders = By.xpath("//div[@class='columns wyn-action-blocks']//h2");

	protected By sustainBlock = By.xpath("//div[@class='columns wyn-action-blocks']//a[contains(.,'Sustain')]");
	protected By ourSustainHeader = By.xpath("//h1[contains(.,'Our Sustain')]");

	protected By msgFromMichealAB = By.xpath("//div[@class='columns wyn-action-blocks']//a[contains(.,'Message')]");
	protected By msgMichealABImage = By.xpath("//div[@class='columns wyn-action-blocks']//img");
	protected By msgMichealdescription = By.xpath("//div[@class='columns wyn-action-blocks']//p[contains(.,' ')]");
	protected By msgMichealReadMore = By.xpath("//div[@class='columns wyn-action-blocks']//a[contains(.,'Read')]");
	protected By msgMichealHeader = By.xpath("//h1[contains(.,'Message')]");
	protected By msgMichealProfileBlock = By.xpath("//div[@class='profile']");

	protected By philanthrophyTab = By.xpath("//div[@class='column wyn-header__navigation']//a[contains(.,'Philan')]");
	protected By philanthrophyHeader = By.xpath("//h1[contains(.,'Philan')]");
	protected By philanContent = By.xpath("//div[@class='richtext']");

	protected By applicationForDonationLink = By
			.xpath("//div[@class='richtext']//a[contains(.,'Application for Donations')]");
	protected By aplnDonationHeader = By.xpath("//h1[contains(.,'Application')]");
	protected By applnDonationContant = By.xpath("//div[@class='richtext']");
	protected By applyNowLink = By.xpath("//div[@class='richtext']//a[contains(.,'Apply Now')]");

	protected By diversityAndInclusionTab = By
			.xpath("//div[@class='wyn-js-main-nav wyn-main-nav ']//a[contains(.,'Diversity')]");
	protected By diversityHeader = By.xpath("//h1[contains(.,'Diversity')]");
	protected By diversityContent = By.xpath("//div[@class='richtext']");
	protected By diveristyActionBlocks = By.xpath("//h2/a");

	protected By supplierHeader = By.xpath("//h1[contains(.,'Supplier')]");
	protected By supplierContent = By.xpath("//div[@class='richtext']");
	protected By supplierLink = By.xpath("//div[@class='richtext']//a[contains(.,'click here')]");
	protected By supplierMail = By.xpath("//div[@class='richtext']//a[contains(.,'@wyn.com')]");
	protected By supplierBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Supplier')]");

	protected By msgFromPresident = By
			.xpath("//div[@class='column wyn-header__navigation']//a[contains(.,'Message from the President')]");
	protected By msgFromPresidentHeader = By.xpath("//h1[contains(.,'Message from the President')]");
	protected By msgFromPresidentContent = By.xpath("//div[@class='richtext']");

	protected By workforceDivTab = By
			.xpath("//div[@class='wyn-js-main-nav wyn-main-nav ']//a[contains(.,'Workforce Diversity')]");
	protected By workforceHeader = By.xpath("//h1[contains(.,'Workforce Diversity')]");
	protected By workforceDivContent = By.xpath("//div[@class='richtext']");

	public void coreStrategiesValidations() {

		if (verifyObjectDisplayed(socialResp)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.PASS,
					"Social Resposibilities Tab Present in the homepage");

			clickElementBy(socialResp);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(environmentSust)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.PASS,
						"Environment and Sustainability link present in the sub menu");

				clickElementBy(environmentSust);

				waitUntilObjectVisible(driver, environmentHeader, 120);
				if (verifyObjectDisplayed(environmentHeader)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.PASS,
							"Navigated to Environment and Sustainability Page");

					if (verifyObjectDisplayed(wynBanners)) {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
								Status.PASS, "Banner Present");
					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
								Status.FAIL, "Banner not present");
					}

					if (verifyObjectDisplayed(coreBlock) && verifyObjectDisplayed(sustainBlock)) {

						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
								Status.PASS,
								"Our Sustainability and Core Strategies Action Blocks present in the Env and Sustain. page");
						scrollDownByPixel(75);

						if (verifyObjectDisplayed(coreReadMore)) {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
									Status.PASS, "Read More CTA present for Core Strategies");

							clickElementBy(coreReadMore);

							waitUntilObjectVisible(driver, coreHeader, 120);

							if (verifyObjectDisplayed(coreHeader)) {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
										Status.PASS, "Navigated to Core Strategies page");
								if (verifyObjectDisplayed(wynBanners)) {
									if (verifyObjectDisplayed(coreBreadCrumb)) {
										tcConfig.updateTestReporter("SocialResponsibilitiesPage",
												"coreStrategiesValidations", Status.PASS,
												"Core Strategies Header, Banner and Breadcrumb navigation present");

										List<WebElement> actionBlocksList = driver.findElements(coreActionBlockHeaders);
										tcConfig.updateTestReporter("SocialResponsibilitiesPage",
												"coreStrategiesValidations", Status.PASS,
												"Static Action Blocks present in the Core Strategies page: Total: "
														+ actionBlocksList.size());

										if (verifyObjectDisplayed(environmentBreadCrumb)) {
											tcConfig.updateTestReporter("SocialResponsibilitiesPage",
													"coreStrategiesValidations", Status.PASS,
													"Navigating Back to Environment and Sustainability page through breadcrumb");
											clickElementJSWithWait(environmentBreadCrumb);
											waitUntilElementVisibleBy(driver, environmentHeader, 120);

											if (verifyObjectDisplayed(environmentHeader)) {
												tcConfig.updateTestReporter("SocialResponsibilitiesPage",
														"coreStrategiesValidations", Status.PASS,
														"Navigated back to the environment and sustainability page");
											} else {
												tcConfig.updateTestReporter("SocialResponsibilitiesPage",
														"coreStrategiesValidations", Status.FAIL,
														"Env and Sust. page navigation failed");
											}

										} else {
											tcConfig.updateTestReporter("SocialResponsibilitiesPage",
													"coreStrategiesValidations", Status.FAIL,
													"Env and Sust not present in the breadcrumb");
										}

									} else {
										tcConfig.updateTestReporter("SocialResponsibilitiesPage",
												"coreStrategiesValidations", Status.FAIL,
												"Core Strategies Breadcrumb not present");
									}

								} else {
									tcConfig.updateTestReporter("SocialResponsibilitiesPage",
											"coreStrategiesValidations", Status.FAIL,
											"Core Strategies Banner not present");
								}

							} else {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
										Status.FAIL, "Core Strategies Navigation failed");

							}

						} else {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
									Status.FAIL, "Read More CTA not present in the Action Block");
						}

					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations",
								Status.FAIL, "Action Blocks not present in the page");
					}

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.FAIL,
							"Env. and Sust. navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.FAIL,
						"Env. and Sust. link not present in the Sub menu");
			}

		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "coreStrategiesValidations", Status.FAIL,
					"Social Responsibilities Tab not present in the Main Nav");
		}

	}

	public void ourSustainablilityValidations() {
		if (verifyObjectDisplayed(sustainBlock)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "ourSustainablilityValidations", Status.PASS,
					"Our Sustainability Block present on the page");

			clickElementBy(sustainBlock);
			waitUntilElementVisibleBy(driver, ourSustainHeader, 120);
			if (verifyObjectDisplayed(ourSustainHeader)) {
				if (verifyObjectDisplayed(wynBanners)) {

					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "ourSustainablilityValidations",
							Status.PASS, "Our Sustainability Navigation Successful and Header present");

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "ourSustainablilityValidations",
							Status.FAIL, "Our Sustainability Header not present");
				}
			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "ourSustainablilityValidations", Status.FAIL,
						"Our Sustainability Navigation UnSuccessful");
			}

		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "ourSustainablilityValidations", Status.FAIL,
					"Our Sustainability block not present");
		}

	}

	public void messageFromMichaelValidations() {

		if (verifyObjectDisplayed(msgFromMichealAB)) {
			scrollDownForElementJSBy(msgFromMichealAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(100);

			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.PASS,
					"Message From Michael Action Block and Header Present");
			if (verifyObjectDisplayed(msgMichealABImage)) {
				if (verifyObjectDisplayed(msgMichealReadMore)) {
					if (verifyObjectDisplayed(msgMichealdescription)) {

						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
								Status.PASS,
								"Message From Michael Action Block Image, Description and ReadMore CTA present");

						clickElementBy(msgFromMichealAB);

						waitUntilElementVisibleBy(driver, msgMichealHeader, 120);

						if (verifyObjectDisplayed(msgMichealHeader)) {
							if (verifyObjectDisplayed(wynBanners)) {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage",
										"messageFromMichaelValidations", Status.PASS,
										"Navigated to Message from Michael page and Banner present");
							} else {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage",
										"messageFromMichaelValidations", Status.FAIL, "Banner not present");
							}
							if (verifyObjectDisplayed(msgMichealProfileBlock)) {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage",
										"messageFromMichaelValidations", Status.PASS,
										"Message From Michael Profile Block content present");
							} else {
								tcConfig.updateTestReporter("SocialResponsibilitiesPage",
										"messageFromMichaelValidations", Status.FAIL,
										"Message From Michael Profile Block content not present");
							}

						} else {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
									Status.FAIL, "Navigation to Msg from Michael unsuccessful");
						}

					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
								Status.FAIL, "Message From Michael Description not present");
					}
				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
							Status.FAIL, "Message From Michael Read More CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.FAIL,
						"Message From Michael Image not present");
			}

		} else {
			// updated as of 1024, not present in the page
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.PASS,
					"Message From Michael Action Block and Header not Present");
		}

	}

	public void philanthrophyValidations() {
		if (verifyObjectDisplayed(socialResp)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.PASS,
					"Social Resposibilities Tab Present");

			clickElementBy(socialResp);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(philanthrophyTab)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.PASS,
						"Philanthrophy link present in the sub menu");

				clickElementBy(philanthrophyTab);

				waitUntilObjectVisible(driver, philanthrophyHeader, 120);
				if (verifyObjectDisplayed(philanthrophyHeader)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.PASS,
							"Philanthrophy Navigation Successful");

					if (verifyObjectDisplayed(philanContent)) {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations",
								Status.PASS, "Philanthrophy Content Present");
					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations",
								Status.FAIL, "Philanthrophy Content not Present");
					}

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.FAIL,
							"Philanthrophy Navigation UnSuccessful");
				}

			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.FAIL,
						"Philanthrophy link not present in the sub menu");
			}

		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "philanthrophyValidations", Status.FAIL,
					"Social Resposibilities Tab not Present");
		}
	}

	public void applicationForDonationVal() {

		if (verifyObjectDisplayed(applicationForDonationLink)) {
			scrollDownForElementJSBy(applicationForDonationLink);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);

			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.PASS,
					"Application For Donations Link present in the Philanthrophy content");

			clickElementBy(applicationForDonationLink);

			waitUntilElementVisibleBy(driver, aplnDonationHeader, 120);
			if (verifyObjectDisplayed(aplnDonationHeader)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.PASS,
						"Application for Donation Navigation Successful");

				if (verifyObjectDisplayed(applnDonationContant)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.PASS,
							"Application for Donation Content Present");

					if (verifyObjectDisplayed(applyNowLink)) {
						scrollDownForElementJSBy(applyNowLink);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);

						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink",
								Status.PASS, "Apply Now link present in the Application for donation page");
					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink",
								Status.FAIL, "Apply Now link not present in the Application for donation page");
					}

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.FAIL,
							"Application for Donation Content not Present");
				}

			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.FAIL,
						"Application for Donation Navigation UnSuccessful");
			}

		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "applicationForDonationLink", Status.FAIL,
					"Application For Donations Link not present in the Philanthrophy content");
		}

	}

	public void diversityAndInclusionValidations() {

		if (verifyObjectDisplayed(socialResp)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations", Status.PASS,
					"Social Resposibilities Tab Present in the homepage");

			clickElementBy(socialResp);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(diversityAndInclusionTab)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
						Status.PASS, "Diversity and Inclusion tab present in the Sub menu");

				clickElementBy(diversityAndInclusionTab);

				waitUntilObjectVisible(driver, diversityHeader, 120);
				if (verifyObjectDisplayed(diversityHeader)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
							Status.PASS, "Navigated to Diversity and Inclusion Page");

					if (verifyObjectDisplayed(wynBanners)) {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
								Status.PASS, "Banner Present");
					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
								Status.FAIL, "Banner not present");
					}

					if (verifyObjectDisplayed(diversityContent)) {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
								Status.PASS, "Diversity related Content present in the page");

						if (verifyObjectDisplayed(diveristyActionBlocks)) {
							List<WebElement> diversityABList = driver.findElements(diveristyActionBlocks);
							tcConfig.updateTestReporter("SocialResponsibilitiesPage",
									"diversityAndInclusionValidations", Status.PASS,
									"Diversity Action Blocks Present: 1." + diversityABList.get(0).getText() + " 2."
											+ diversityABList.get(1).getText() + " 3."
											+ diversityABList.get(2).getText());

						} else {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage",
									"diversityAndInclusionValidations", Status.FAIL,
									"No Action Blocks in the Diversity page");
						}

					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
								Status.FAIL, "Diversity related Content not present in the page");
					}

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
							Status.FAIL, "Navigation to Diversity and Inclusion Page failed");
				}
			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations",
						Status.FAIL, "Diversity and Inclusion tab not present in the Sub menu");
			}
		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "diversityAndInclusionValidations", Status.FAIL,
					"Social Responsibility tab not present in the homepage");
		}

	}

	public void supplierDiversityValidations() {

		List<WebElement> diversityABList = driver.findElements(diveristyActionBlocks);
		scrollDownForElementJSWb(diversityABList.get(2));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(diversityABList.get(2))) {

			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations", Status.PASS,
					"Supplier Diversity Header Present");

			clickElementJS(diversityABList.get(2));

			waitUntilElementVisibleBy(driver, supplierHeader, 120);
			if (verifyObjectDisplayed(supplierHeader)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations", Status.PASS,
						"Navigated to Supplier Diversity Page");

				if (verifyObjectDisplayed(wynBanners)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.PASS, "Banner present on the page");
				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.FAIL, "Banner is not present on the page");
				}

				if (verifyObjectDisplayed(supplierBreadcrumb)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.PASS, "Supplier Diversity breadcrumb Present");
				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.FAIL, "Supplier Diversity breadcrumb not Present");
				}

				if (verifyObjectDisplayed(supplierContent)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.PASS, "Supplier Diversity Content Present");
//					if (verifyObjectDisplayed(supplierLink)) {
//						scrollDownForElementJSBy(supplierLink);
//						waitForSometime(tcConfig.getConfig().get("LowWait"));
//						scrollUpByPixel(150);
//						waitForSometime(tcConfig.getConfig().get("LowWait"));
//						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
//								Status.PASS, "Supplier Details Link present");
//						clickElementBy(supplierLink);
//						waitForSometime(tcConfig.getConfig().get("LowWait"));
//						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
//						driver.switchTo().window(tabs2.get(1));
//						waitForSometime(tcConfig.getConfig().get("LongWait"));
//
//						if (driver.getTitle().toUpperCase().contains("SUPPLIER")) {
//
//							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
//									Status.PASS, "Navigated to Supplier Details Page");
//
//							driver.close();
//							waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//							driver.switchTo().window(tabs2.get(0));
//							waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//							waitUntilElementVisibleBy(driver, supplierHeader, 120);
//
//						} else {
//							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
//									Status.FAIL, "Navigation to Supplier Details Page failed");
//						}
//
//					} else {
//						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
//								Status.FAIL, "Supplier Details Link not present");
//					}

					/*
					 * if (verifyObjectDisplayed(supplierMail)) {
					 * scrollDownForElementJSBy(supplierMail);
					 * waitForSometime(tcConfig.getConfig().get("LowWait")); scrollUpByPixel(150);
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * tcConfig.updateTestReporter("SocialResponsibilitiesPage",
					 * "supplierDiversityValidations", Status.PASS,
					 * "Suppliers Mail address present");
					 * 
					 * } else { tcConfig.updateTestReporter("SocialResponsibilitiesPage",
					 * "supplierDiversityValidations", Status.FAIL,
					 * "Suppliers Mail address not present"); }
					 */

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations",
							Status.FAIL, "Supplier Diversity Content not Present");
				}

			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations", Status.FAIL,
						"Supplier Diversity navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "supplierDiversityValidations", Status.FAIL,
					"Supplier action block header error");
		}

	}

	public void msgFromPresidentVal() {

		if (verifyObjectDisplayed(socialResp)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.PASS,
					"Social Resposibilities Tab Present in the homepage");

			clickElementBy(socialResp);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(msgFromPresident)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.PASS,
						"Message From President present in the submenu");

				clickElementBy(msgFromPresident);

				waitUntilObjectVisible(driver, msgFromPresidentHeader, 120);
				if (verifyObjectDisplayed(msgFromPresidentHeader) && verifyObjectDisplayed(msgFromPresidentContent)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.PASS,
							"Navigated to Message from President Page and Content present");

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.FAIL,
							"Navigation to Msg from President page failed");
				}
			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.FAIL,
						"Msg from President not present in submenu");
			}
		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "msgFromPresidentVal", Status.FAIL,
					"Social Responsibilities not present in the page");
		}

	}

	public void workForceDiversity() {

		if (verifyObjectDisplayed(socialResp)) {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.PASS,
					"Social Resposibilities Tab Present in the homepage");

			clickElementBy(socialResp);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(workforceDivTab)) {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.PASS,
						"Workforce Diversity present in the submenu");

				clickElementBy(workforceDivTab);

				waitUntilObjectVisible(driver, workforceHeader, 120);
				if (verifyObjectDisplayed(workforceHeader) && verifyObjectDisplayed(workforceDivContent)) {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.PASS,
							"Navigated to Workforce Diversity Page and Content present");

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.FAIL,
							"Navigation to Workforce Diversity page failed");
				}
			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.FAIL,
						"Workforce Diversity not present in submenu");
			}
		} else {
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "workForceDiversity", Status.FAIL,
					"Social Responsibilities not present in the page");
		}

	}

	public void clickSocialResponsibilty() {
		clickElementBy(socialResp);
	}
}