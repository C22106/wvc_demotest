package destinations.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class FooterLinksPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(FooterLinksPage.class);

	public FooterLinksPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By footer = By.xpath("//div[@class='cell medium-2']");
	protected By footerWynPages = By.xpath("//ul[@class= 'menu expanded']/li/ul/li/a/span");

	protected By footerPrivacyNotice = By.xpath("(//ul[@class='quickLinks menu float-right']/li)[2]");
	protected By footerPrivacySettings = By.xpath("(//ul[@class='quickLinks menu float-right']/li)[3]");
	protected By footerTimeshare = By.xpath("(//ul[@class='quickLinks menu float-right']/li)[4]");
	protected By footerDoNotSell = By.xpath("//div[@class='cell auto']//div/a");
	protected By footerWyndhamHotels = By.xpath("//div[@class='footer']//a[contains(.,'Wyndham Hotels & Resorts')]");
	protected By footerFactSheetLink = By
			.xpath("//div[@class='footer']//a[contains(.,'Wyndham Destinations Overview')]");
	protected By factSheetPdf = By.xpath("//embed | //div[@id='viewer']"); // type-application/pdf
	protected By factSheetHeader = By.xpath("//h1[contains(.,'Wyndham Destinations Overview')]");
	protected By factSheetLink = By.xpath("//a[contains(.,'View')]");
	protected By factSheetUpLink = By.xpath("//p//a[contains(.,'Click here')]");
	protected By aboutWyndhamTab = By.xpath("//a[@title='About Wyndham']");
	protected By ourCompanyLink = By.xpath("(//a[contains(.,'Our Company')])[1] | (//a[contains(.,'Our Company')])[2]");
	protected By ourBrands = By.xpath("(//a[contains(.,'Our Brand')])[1] | (//a[contains(.,'Our Brand')])[2]");
	protected By socialResponsibilities = By.xpath("//a[@title='Social Responsibility']");
	protected By termsOfUseContent = By.xpath("//div[@class='richtext']");
	protected By termsOfUseMail = By.xpath("//div[@class='richtext']//a[contains(.,'Copyright')]");
	protected By optOutHeader = By.xpath("//div[@class='text-primary title-1' and contains(.,'Opt')]");
	protected By privacyNotice = By.xpath("//h1[contains(.,'Privacy Notice')]");
	protected By privacyNoticeBrdcrmb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Privacy Notice')]");
	protected By privacyNoticeMaterialUpdate = By.xpath("//b[contains(.,'Material Updates to this Privacy Notice')]");
	protected By privacyNoticeIntroduction = By.xpath("//b[contains(.,'Introduction to this Privacy Notice')]");
	protected By privacyNoticeWhatInfo = By
			.xpath("//b[contains(.,'What Information Is Collected and How It Is Collected?')]");
	protected By privacyNoticeCookies = By.xpath("//a[contains(.,'CLICK HERE')]");
	protected By privacyNoticeGAnalytics = By.xpath("//a[contains(.,'http://tools.google.com/dlpage/gaoptout?hl=en')]");
	protected By privacyNoticeSignalTrack = By.xpath("//b[contains(.,'Do Not Track Signals')]");
	protected By privacyNoticeAds = By.xpath("//b/following-sibling::a[contains(.,'www.aboutads.info')]");
	protected By privacyNoticeNwAdv = By.xpath("//b/following-sibling::a[contains(.,'www.networkadvertising.org')]");
	protected By privacyNoticeMailAdd = By.xpath("//a[contains(.,'Privacy@wyn.com')]");

	// saket
	protected By footerLegalLinks = By.xpath("//ul[@class='quickLinks menu float-right']/li");
	protected By footerCopyright = By.xpath(
			"//ul[@class='menu expanded body-2']//li[contains(.,'© Wyndham Destinations 2021. All Rights Reserved.')]");
	protected By footerWynLogo = By.xpath("//div[@class='cell medium-2']/a");
	protected By termsOfUseHeader = By.xpath("//div[contains(@class,'text-primary title-1') and contains(.,'Terms')]");
	protected By breadcrumbAboutUs = By.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'Our Company')]");
	protected By breadcrumbOurBrands = By.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'Our Brands')]");
	protected By breadcrumbWhyVacationClubs = By
			.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'Why Vacation Clubs')]");
	protected By breadcrumbWyndhamCares = By.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'Wyndham Cares')]");
	protected By breadcrumbNews = By.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'News')]");
	protected By breadcrumbContactUs = By.xpath("//ul[@class='breadcrumbs']//li//a[contains(.,'Contact Us')]");
	protected By headerPR = By.xpath("//h3[@class='text-black title-1']");
	protected By headerMVC = By.xpath("//h2[@class='modal-title' and contains(.,'Important')]");
	protected By headerCW = By.xpath("//li[@role='menuitem']/a/img");
	protected By headerWorldMark = By.xpath("(//tr[@style='vertical-align:top;']/td/img)[2]");
	protected By headerShell = By.xpath("//h2[contains(.,'Get')]");
	protected By timeshareHeader = By.xpath("(//div[@class='title-1 textSpacing' and contains(.,'Discover')])[1]");
	protected By termsOfUseBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Terms')]");
	protected By privacyBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Privacy')]");
	protected By privacySettingsVal = By.xpath("//h1[contains(.,'Policies')]");
	protected By privacyNoticeHeader = By.xpath("//div[@class='text-primary title-1' and contains(.,'Privacy')]");

	protected By ourCompanyHeader = By.xpath("//h1[@class='text-primary title-1' and contains(.,'About Us')]");
	protected By ourBrandsHeader = By.xpath("//h1[@class='text-primary title-1' and contains(.,'Our Brands')]");
	protected By ourWhyVacationsHeader = By
			.xpath("//h1[@class='text-primary title-1' and contains(.,'Why Vacation Clubs')]");
	protected By ourWYNCareHeader = By.xpath("//div[@class='text-primary title-1' and contains(.,'Wyndham')]");
	protected By ourNewsHeader = By.xpath("//div[contains(text(),'Press')]");
	protected By ourCareersHeader = By.xpath(
			"//h1[@style='text-align:center;' and contains(.,'LET')] | //h2[@style='text-align:left;' and contains(.,'Career')]");
	protected By ourContactUsHeader = By.xpath("//div[@class='text-primary title-1' and contains(.,'Contact Us')]");

	protected By buttonOurCompany = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[1]");
	protected By buttonOurBrands = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[2]");
	protected By buttonWhyVacations = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[3]");
	protected By buttonWYNCare = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[4]");
	protected By buttonNews = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[5]");
	protected By buttonCareers = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[7]");
	protected By buttonContactUs = By.xpath("(//ul[@class= 'menu expanded']/li/ul/li/a/span)[6]");

	protected By footerSocialIcons = By
			.xpath("//div[contains(@class,'show-for-large')]//div[contains(@class,'socialMedia')]//img");
	protected By footerBrandLogos = By.xpath(
			"//div[contains(@class,'show-for-large')]//ul[contains(@class,'menu align-center-middle expanded')]/li//a/img");
	protected By footerBrandLink = By.xpath(
			"//div[contains(@class,'show-for-large')]//ul[contains(@class,'menu align-center-middle expanded')]/li//a");
	protected By footerBrandOrder = By.xpath(
			"//div[contains(@class,'show-for-large')]//ul[contains(@class,'menu align-center-middle expanded')]/li//a/img");

	public void footerPresence() {

		waitUntilObjectVisible(driver, footer, 120);
		if (verifyObjectDisplayed(footer)) {

			scrollDownForElementJSBy(footer);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// scrollDownByPixel(200);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("FooterLinksPage", "footerPresence", Status.PASS,
					"Footer is present in the page");
			if (verifyObjectDisplayed(footerWynLogo)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPresence", Status.PASS,
						"Wyndham Logo is present in Footer");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPresence", Status.FAIL,
						"Wyndham Logo is not present in Footer");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPresence", Status.FAIL,
					"Footer is not present in the page");
		}

	}

	public void footerSocialIcons() {

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					break;
				case 1:
					strSiteName = "TWITTER";
					break;
				case 2:
					strSiteName = "INSTAGRAM";
					break;
				case 3:
					strSiteName = "YOUTUBE";
					break;
				case 4:
					strSiteName = "LINKEDIN";
					break;

				default:
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				clickElementJS(socialIconsList.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	public void footerWynLinks() {

		if (verifyObjectDisplayed(footerWynPages)) {
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
					"Wyndham Destinations Sub Links Present in the Footer,Total: " + wynLinksList.size());

			if (verifyObjectDisplayed(footerWyndhamHotels)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Wyndham Hotels & Resort");

				clickElementBy(footerWyndhamHotels);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}

				if (tabs2.size() == 2) {
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains("WYNDHAM HOTELS")) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"Wyndham Hotel & Resort Associated Link Opened");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.switchTo().window(tabs2.get(0));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"Wyndham Hotel & Resort Associated Link opening error");

					}
				} else {
					if (driver.getTitle().toUpperCase().contains("WYNDHAM HOTELS")) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"Wyndham Hotel & Resort Associated Link Opened");

						driver.navigate().back();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"Wyndham Hotel & Resort Associated Link opening error");

					}

				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}

			if (verifyObjectDisplayed(footerFactSheetLink)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Wyndham Destinations Fact Sheet Link Present in footer");
				clickElementBy(footerFactSheetLink);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(factSheetHeader)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
							"User Navigated to Fact Sheet Page");

					if (verifyObjectDisplayed(factSheetLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"View Fact Sheet Link Present in the content");

						clickElementBy(factSheetLink);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs2.get(1));
						if (browserName.equalsIgnoreCase("IE")) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
						}

						String strPdf = driver.findElement(factSheetPdf).getAttribute("type");
						if (strPdf.toUpperCase().contains("PDF")) {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
									"Associated PDF opened");

							driver.close();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().window(tabs2.get(0));
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							if (verifyObjectDisplayed(footerWynLogo)) {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
										"Navigated Back and Wyndham Logo present in the footer");
								clickElementBy(footerWynLogo);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								if (verifyObjectDisplayed(footer)) {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
											"Navigated to homepage");

									scrollDownForElementJSBy(footer);
									waitForSometime(tcConfig.getConfig().get("LowWait"));

								} else {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
											"Navigation to homepage failed");
								}

							} else {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
										"Navigation to Wyn Destination Failed");
							}

						} else {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
									"Fact Sheet Associated PDF opening problem");
						}

					} else if (verifyObjectDisplayed(factSheetUpLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"View Fact Sheet Link Present in the content");

						/*
						 * scrollDownForElementJSBy(factSheetUpLink);
						 * scrollUpByPixel(150);
						 */
						getElementInView(factSheetUpLink);
						clickElementBy(factSheetUpLink);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs2.get(1));
						if (browserName.equalsIgnoreCase("IE")) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
						}
						String strPdf = "";
						if (browserName.equalsIgnoreCase("firefox")) {
							strPdf = driver.findElement(factSheetPdf).getAttribute("class");
						} else {
							strPdf = driver.findElement(factSheetPdf).getAttribute("type");
						}
						if (strPdf.toUpperCase().contains("PDF")) {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
									"Associated PDF opened");

							driver.close();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().window(tabs2.get(0));
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							if (verifyObjectDisplayed(footerWynLogo)) {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
										"Navigated Back and Wyndham Logo present in the footer");
								clickElementBy(footerWynLogo);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								if (verifyObjectDisplayed(footer)) {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
											"Navigated to homepage");

									scrollDownForElementJSBy(footer);
									waitForSometime(tcConfig.getConfig().get("LowWait"));

								} else {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
											"Navigation to homepage failed");
								}

							} else {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
										"Navigation to Wyn Destination Failed");
							}

						} else {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
									"Fact Sheet Associated PDF opening problem");
						}

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"View Fact Sheet Link not Present in the content");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
							"Navigation to fact sheet page failed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Destinations Fact Sheet Link not Present in footer");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	public void footerTermsOfUse() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(0))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
					"Terms Of Use Link present in the footer");

			clickElementWb(legalList.get(0));
			waitUntilObjectVisible(driver, termsOfUseHeader, 120);

			if (verifyObjectDisplayed(termsOfUseHeader)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
						"Terms Of Use Page Navigated");

				if (verifyObjectDisplayed(termsOfUseBreadcrumb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Terms Of Use Breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Terms Of Use Breadcrumb not present");
				}

				if (verifyObjectDisplayed(aboutWyndhamTab) && verifyObjectDisplayed(socialResponsibilities)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"About Wyndham and Social Responsibilities Tab present");

					clickElementBy(aboutWyndhamTab);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(ourCompanyLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"User has the option to navigate to Our Company page in the Secondary Navigation");
						clickElementBy(aboutWyndhamTab);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Our Company Link is not present in the Secondary Navigation");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"About Wyndham and Social Responsibilities Tab not present");
				}

				if (verifyObjectDisplayed(termsOfUseContent)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Content present in the Terms of Use page");
					if (verifyObjectDisplayed(termsOfUseMail)) {
						scrollDownForElementJSBy(termsOfUseMail);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"Copyright Violation Email Link present");

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Copyright violation email link not present");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Content not present in the Terms of Use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
						"Terms Of Use Page could not be Navigated");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
					"Terms Of Use Link not present in the footer");
		}

	}

	public void footerPrivacySettings() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		if (legalList.get(3).getText().toLowerCase().contains("setting")) {
			scrollDownForElementJSWb(legalList.get(3));
		} else if (legalList.get(2).getText().toLowerCase().contains("setting")) {
			scrollDownForElementJSWb(legalList.get(2));
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(2))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
					"Privacy Settings Link present in the Footer");

			if (legalList.get(3).getText().toLowerCase().contains("setting")) {
				clickElementWb(legalList.get(3));
			} else if (legalList.get(2).getText().toLowerCase().contains("setting")) {
				clickElementWb(legalList.get(2));
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());

			driver.switchTo().window(tabs2.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (browserName.equalsIgnoreCase("IE")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			if (verifyObjectDisplayed(privacySettingsVal)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
						"Navigated to Privacy Settings related page");

				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(tabs2.get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(legalList.get(2))) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
							"Navigated Back to Terms of Use page");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
							"Unable to navigate to Terms of use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
						"Unable to navigate to Privacy Settings page");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
					"Privacy Settings Link not present in the Footer");
		}

	}

	public void footerPrivacyNotice() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(1))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
					"Privacy Notice Link present in the Footer");

			clickElementWb(legalList.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(privacyNotice)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
						"Privacy Notice page navigated and header present");
				if (verifyObjectDisplayed(privacyNoticeBrdcrmb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice breadcrumb not present");
				}

				if (verifyObjectDisplayed(privacyNoticeMaterialUpdate)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Material Update Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Material Update Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeIntroduction)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Intrdouction Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Intrdouction Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeCookies)) {
					scrollDownForElementJSBy(privacyNoticeCookies);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Cookies link displayed");
					clickElementBy(privacyNoticeCookies);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (verifyObjectDisplayed(privacySettingsVal)) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Cookies page navigated");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().window(tabs2.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Cookies page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Cookies link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeGAnalytics)) {
					/*
					 * scrollDownForElementJSBy(privacyNoticeGAnalytics);
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * scrollUpByPixel(90);
					 */

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Google Analytics link displayed");
					clickElementBy(privacyNoticeGAnalytics);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("GOOGLE ANALYTICS")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Google Analytics page navigated");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().window(tabs2.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Google Analytics page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Google Analytics link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeSignalTrack) && verifyObjectDisplayed(privacyNoticeAds)) {
					scrollDownForElementJSBy(privacyNoticeSignalTrack);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Signal Track section displayed and ADS link displayed");
					clickElementBy(privacyNoticeAds);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("YOURADCHOICES.COM")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"DAA page navigated");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.switchTo().window(tabs2.get(0));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"DAA page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Signal Track section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeNwAdv)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Network Adv link displayed");
					clickElementBy(privacyNoticeNwAdv);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("NETWORK")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Network Adv page navigated");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.switchTo().window(tabs2.get(0));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Network Adv page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Network Adv link not displayed");
				}

				/*
				 * if (verifyObjectDisplayed(privacyNoticeWhatInfo)) {
				 * 
				 * scrollDownForElementJSBy(privacyNoticeWhatInfo);
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * scrollUpByPixel(150);
				 * tcConfig.updateTestReporter("FooterLinksPage",
				 * "footerPrivacyNotice", Status.PASS,
				 * "What Information Is Collected and How It Is Collected? Section displayed"
				 * ); } else { tcConfig.updateTestReporter("FooterLinksPage",
				 * "footerPrivacyNotice", Status.FAIL,
				 * "What Information Is Collected and How It Is Collected? Section not displayed"
				 * ); }
				 */
				if (verifyObjectDisplayed(privacyNoticeMailAdd)) {

					scrollDownForElementJSBy(privacyNoticeMailAdd);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					scrollUpByPixel(150);
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Mail link displayed");

					driver.switchTo().activeElement().sendKeys(Keys.HOME);

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Mail link not displayed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
						"Privacy Notice page navigation unsuccessful");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
					"Privacy Notice Link not present in the Footer");
		}
	}

	public void footerBrandsOrder() {
		// updated as of 1023
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(footerBrandLogos)) {
			List<WebElement> brandsList = driver.findElements(footerBrandOrder);
			tcConfig.updateTestReporter("FooterLinksPage", "footerBrandsOrder", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total: " + brandsList.size());

			for (int i = 0; i < brandsList.size(); i++) {

				String strBrandNameExcel = testData.get("Brand " + (i + 1)).toLowerCase();
				String strBrandTextImg = brandsList.get(i).getAttribute("aria-label").toLowerCase();

				if (strBrandTextImg.contains(strBrandNameExcel)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerBrandsOrder", Status.PASS,
							"Brand Number: " + (i + 1) + " is " + strBrandTextImg.toUpperCase());
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					/*
					 * if (i == brandsList.size() - 1) {
					 * tcConfig.updateTestReporter("FooterLinksPage",
					 * "footerBrandsOrder", Status.PASS,
					 * "All The Brands are displayed in the required order"); }
					 * else { tcConfig.updateTestReporter("FooterLinksPage",
					 * "footerBrandsOrder", Status.FAIL,
					 * "All The Brands are not displayed in the required order"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerBrandsOrder", Status.FAIL,
							"The Brands are not displayed in the required order");
				}

			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerBrandsOrder", Status.FAIL,
					"Wyndham Brands Logo not present in footer");
		}

	}

	public void footerBrands() throws Exception {
		String href = "";
		String strBrandTextImg = "";
		String currentURL = "";
		if (verifyObjectDisplayed(footerBrandLogos)) {
			List<WebElement> brandsList = driver.findElements(footerBrandLogos);
			List<WebElement> brandsLinkList = driver.findElements(footerBrandLink);
			tcConfig.updateTestReporter("FooterLinksPage", "footerBrands", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total: " + brandsList.size());
			for (int i = 0; i < brandsList.size(); i++) {
				href = getElementAttribute(brandsLinkList.get(i), "href").trim();
				strBrandTextImg = brandsList.get(i).getAttribute("aria-label").trim();
				if (strBrandTextImg.contains("Margaritaville")) {
					href = "https://mgvc.wyndhamdestinations.com/";
				}
				getElementInView(brandsList.get(i));
				clickElementWithJS(brandsList.get(i));
				pageCheck();
				ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
				if (allTabs.size() > 0) {
					newTabNavigations(allTabs);
					currentURL = driver.getCurrentUrl().trim();
					switch (strBrandTextImg) {
					case "Presidential Reserve":
						Assert.assertTrue(currentURL.equalsIgnoreCase(href),
								"Presidential Reserve Page is not Displayed");
						tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
								"Club Wyndham page navigation is Successful");
						break;
					case "Margaritaville Vacation Club by Wyndham":
						Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Margaritaville Page is not Displayed");
						tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
								"Margaritaville page navigation is Successful");
						break;
					case "Club Wyndham":
						Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Club Wyndham Page is not Displayed");
						tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
								"Club Wyndham page navigation is Successful");
						break;
					case "Worldmark by Wyndham":
						Assert.assertTrue(currentURL.equalsIgnoreCase(href), "Worldmark Page is not Displayed");
						tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
								"Worldmark page navigation is Successful");
						break;
					case "Shell Vacations Club":
						Assert.assertTrue(driver.getTitle().trim().contains(strBrandTextImg),
								"Shell Vacation Page is not Displayed");
						tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
								"Shell Vacation page navigation is Successful");
						break;
					}
					navigateToMainTab(allTabs);
					pageCheck();

				}
			}

		}
	}

	/*
	 * Method: footerPrivacySection Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void footerPrivacySection() {
		List<WebElement> legalList = driver.findElements(footerLegalLinks);
		getElementInView(footerCopyright);
		Assert.assertTrue(verifyObjectDisplayed(footerCopyright));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of privacy links: " + legalList.size());

			for (int i = 0; i < legalList.size(); i++) {
				if (i == 0) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Terms of Use", 1, legalList.get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateTermsOfUse();
					} else {
						clickElementBy(legalList.get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateTermsOfUse();
					}

				} else if (i == 1) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Privacy Notice", 1, footerPrivacyNotice);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validatePrivacyNotice();
					} else {
						clickElementBy(footerPrivacyNotice);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validatePrivacyNotice();
					}

				} else if (i == 2) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Privacy Settings", 1, footerPrivacySettings);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validatePrivacySettings();
					} else {
						clickElementBy(footerPrivacySettings);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validatePrivacySettings();
					}

				} else if (i == 3) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Proudly Supports Timeshare.com", 1, footerTimeshare);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateSupportTimeshare();
					} else {
						clickElementBy(footerTimeshare);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateSupportTimeshare();
					}

				}
				sendKeyboardKeys(Keys.END);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Wyndham Brand Logos Present in the not present in the Footer");
		}

		if (verifyObjectDisplayed(footerDoNotSell)) {
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				clickIOSElement("Do Not Sell My", 1, footerDoNotSell);
				verifyObjectDisplayed(optOutHeader);
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
						"Footer do not sell is present ");
				driver.get(testData.get("URL"));
				pageCheck();
			} else {
				clickElementBy(footerDoNotSell);
				ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				verifyObjectDisplayed(optOutHeader);
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
						"Footer do not sell is present ");
				driver.close();
				driver.switchTo().window(tabs.get(0));
			}

		} else {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Footer do not sell is not present ");
		}

	}

	/*
	 * Method: validateTermsOfUse Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateTermsOfUse() {
		waitUntilObjectVisible(driver, termsOfUseHeader, 120);

		if (verifyObjectDisplayed(termsOfUseHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.PASS,
					"Terms Of Use Page Navigated");

			if (verifyObjectDisplayed(termsOfUseBreadcrumb)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.PASS,
						"Terms Of Use Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.FAIL,
						"Terms Of Use Breadcrumb not present");
			}

			if (verifyObjectDisplayed(ourCompanyLink) && verifyObjectDisplayed(ourBrands)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.PASS,
						"About Wyndham and Social Responsibilities Tab present");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.FAIL,
						"Terms Of Use Breadcrumb not present");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.FAIL,
					"Terms Of Use Page could not be Navigated");
		}

	}

	/*
	 * Method: validatePrivacyNotice Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validatePrivacyNotice() {

		waitUntilObjectVisible(driver, privacyNoticeHeader, 120);

		if (verifyObjectDisplayed(privacyNoticeHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.PASS,
					"Privacy Notice Navigated");

			if (verifyObjectDisplayed(privacyBreadcrumb)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.PASS,
						"Privacy Notice Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.FAIL,
						"Privacy Notice Breadcrumb not present");
			}

			if (verifyObjectDisplayed(ourCompanyLink) && verifyObjectDisplayed(ourBrands)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.PASS,
						"About Wyndham and Social Responsibilities Tab present");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.FAIL,
						"Privacy Notice Breadcrumb not present");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.FAIL,
					" Privacy Notice could not be Navigated");
		}
	}

	/*
	 * Method: validatePrivacySettings Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validatePrivacySettings() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, privacySettingsVal, 120);

		if (verifyObjectDisplayed(privacySettingsVal)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
					"Privacy Settings Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
					"Privacy Settings Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, privacyNoticeHeader, 120);
	}

	/*
	 * Method: validateSupportTimeshare Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateSupportTimeshare() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

		if (driver.getTitle().toUpperCase().contains("TIMESHARE")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
					"timeshare Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
					"timeshare could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, privacyNoticeHeader, 120);
	}

	/*
	 * Method: footerBrandLogo Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */

	public void footerBrandLogo() {
		List<WebElement> brandLogos = driver.findElements(footerBrandLogos);
		getElementInView(footerBrandLogos);
		Assert.assertTrue(verifyObjectDisplayed(footerCopyright));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of Brand logos: " + brandLogos.size());

			for (int i = 0; i < brandLogos.size(); i++) {
				if (i == 0) {
					clickElementBy(brandLogos.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validatePR();
				} else if (i == 1) {
					clickElementBy(brandLogos.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateMVC();
				} else if (i == 2) {
					clickElementBy(brandLogos.get(2));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateCW();
				} else if (i == 3) {
					clickElementBy(brandLogos.get(3));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateWorldMark();
				} else if (i == 4) {
					clickElementBy(brandLogos.get(4));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// validateShellVacationClub();
				}
				sendKeyboardKeys(Keys.END);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Wyndham Brand Logos Present in the not present in the Footer");
		}

	}

	/*
	 * Method: validatePR Description: Validtions : Date: Feb/2021 Author: Saket
	 * Sharma Changes By:
	 */

	public void validatePR() {

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, headerPR, 120);

		if (driver.getTitle().toUpperCase().contains("OWNER RESOURCES")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePR", Status.PASS, "PR Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePR", Status.FAIL, "PR Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, optOutHeader, 120);
	}

	/*
	 * Method: validateMVC Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */

	public void validateMVC() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, headerMVC, 120);

		if (driver.getTitle().toUpperCase().contains("MARGARITAVILLE")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateMVC", Status.PASS, "MARGARITAVILLE Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateMVC", Status.FAIL,
					"MARGARITAVILLE Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, optOutHeader, 120);

	}

	/*
	 * Method: validateCW Description: Validtions : Date: Feb/2021 Author: Saket
	 * Sharma Changes By:
	 */

	public void validateCW() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, headerCW, 120);

		if (driver.getTitle().toUpperCase().contains("CLUB WYNDHAM")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCW", Status.PASS, "CLUB WYNDHAM Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCW", Status.FAIL,
					"CLUB WYNDHAM Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, optOutHeader, 120);

	}

	/*
	 * Method: validateWorldMark Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateWorldMark() {

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, headerWorldMark, 120);

		if (driver.getTitle().toUpperCase().contains("WORLDMARK")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateWorldMark", Status.PASS,
					"WORLDMARK Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateWorldMark", Status.FAIL,
					"WORLDMARK Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, optOutHeader, 120);
	}

	/*
	 * Method: validateShellVacationClub Description: Validtions : Date:
	 * Feb/2021 Author: Saket Sharma Changes By:
	 */

	public void validateShellVacationClub() {

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, headerShell, 120);

		if (driver.getTitle().toUpperCase().contains("SHELL VACATIONS")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateShellVacationClub", Status.PASS,
					"validateShellVacationClub Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateShellVacationClub", Status.FAIL,
					"validateShellVacationClub Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilObjectVisible(driver, optOutHeader, 120);
	}

	/*
	 * Method: footerLinksValidation Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void footerLinksValidation() {
		List<WebElement> legalList = driver.findElements(footerWynPages);
		getElementInView(footerWynLogo);
		Assert.assertTrue(verifyObjectDisplayed(footerWynLogo));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerLinksValidation", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of Quick links: " + legalList.size());

			for (int i = 0; i < legalList.size(); i++) {
				// List<WebElement> legalList1 =
				// driver.findElements(footerWynPages);

				if (i == 0) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Our Company", 1, buttonOurCompany);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateOurCompany();
					} else {
						clickElementBy(buttonOurCompany);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateOurCompany();
					}

				} else if (i == 1) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Our Brands", 1, buttonOurBrands);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateOurBrands();
					} else {
						clickElementJSWithWait(buttonOurBrands);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateOurBrands();
					}

				} else if (i == 2) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Why Vacation Clubs", 1, buttonWhyVacations);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateWhyVacationClubs();
					} else {
						clickElementJSWithWait(buttonWhyVacations);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateWhyVacationClubs();
					}

				} else if (i == 3) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Wyndham Cares", 1, buttonWYNCare);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateWyndhamCares();
					} else {
						clickElementJSWithWait(buttonWYNCare);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateWyndhamCares();
					}

				} else if (i == 4) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("News", 1, buttonNews);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateNews();
					} else {
						clickElementJSWithWait(buttonNews);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateNews();
					}

				} else if (i == 5) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Contact Us", 1, buttonContactUs);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateContact();
					} else {
						clickElementJSWithWait(buttonContactUs);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateContact();
					}

				} else if (i == 6) {
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						clickIOSElement("Careers", 1, buttonCareers);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateCareers();
					} else {
						clickElementJSWithWait(buttonCareers);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						validateCareers();
					}

				} else {
					break;
				}
				sendKeyboardKeys(Keys.END);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerLinksValidation", Status.FAIL,
					"Wyndham Quick link is not present in the Footer");
		}
	}

	/*
	 * Method: validateOurCompany Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateOurCompany() {

		waitUntilObjectVisible(driver, ourCompanyHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbAboutUs));
		if (driver.getTitle().toUpperCase().contains("OUR COMPANY")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurCompany", Status.PASS,
					"OurCompany Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurCompany", Status.FAIL,
					"OurCompany Page could not be Navigated");
		}
		driver.navigate().back();
	}

	/*
	 * Method: validateOurBrands Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateOurBrands() {

		waitUntilObjectVisible(driver, ourBrandsHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbOurBrands));

		if (driver.getTitle().toUpperCase().contains("OUR BRANDS")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurBrands", Status.PASS,
					"OurBrands Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurBrands", Status.FAIL,
					"OurBrands Page could not be Navigated");
		}
		driver.navigate().back();
	}

	/*
	 * Method: validateWhyVacationClubs Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateWhyVacationClubs() {

		waitUntilObjectVisible(driver, ourWhyVacationsHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbWhyVacationClubs));

		if (driver.getTitle().toUpperCase().contains("WHY VACATION CLUBS")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurBrands", Status.PASS,
					"WHY VACATION CLUBS Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateOurBrands", Status.FAIL,
					"WHY VACATION CLUBS Page could not be Navigated");
		}
		driver.navigate().back();
	}

	/*
	 * Method: validateWyndhamCares Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	public void validateWyndhamCares() {

		waitUntilObjectVisible(driver, ourWYNCareHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbWyndhamCares));

		if (driver.getTitle().toUpperCase().contains("SAFE TIMESHARE EXIT OPTIONS")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateWyndhamCares", Status.PASS,
					"WYNDHAM CARES Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateWyndhamCares", Status.FAIL,
					"WYNDHAM CARES Page could not be Navigated");
		}
		driver.navigate().back();
	}

	/*
	 * Method: validateNews Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */

	public void validateNews() {

		waitUntilObjectVisible(driver, ourNewsHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbNews));

		if (driver.getTitle().toUpperCase().contains("NEWS")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateNews", Status.PASS, "News Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateNews", Status.FAIL,
					"News Page could not be Navigated");
		}
		driver.navigate().back();
	}

	/*
	 * Method: validateCareers Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */

	public void validateCareers() {

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (browserName.equalsIgnoreCase("IE")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitUntilObjectVisible(driver, ourCareersHeader, 120);

		if (driver.getTitle().trim().contains("Travel + Leisure Careers")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCareers", Status.PASS,
					"WYNDHAM DESTINATIONS CAREERS Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateCareers", Status.FAIL,
					"WYNDHAM DESTINATIONS CAREERS Page could not be Navigated");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(tabs.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: validateContact Description: Validtions : Date: Feb/2021 Author:
	 * Saket Sharma Changes By:
	 */

	public void validateContact() {

		waitUntilObjectVisible(driver, ourContactUsHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(breadcrumbContactUs));

		if (driver.getTitle().toUpperCase().contains("CONTACT US")) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateContact", Status.PASS, "News Page Navigated");
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateContact", Status.FAIL,
					"News Page could not be Navigated");
		}
		driver.navigate().back();
	}

}
