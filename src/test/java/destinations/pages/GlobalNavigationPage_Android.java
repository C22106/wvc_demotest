package destinations.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GlobalNavigationPage_Android extends GlobalNavigationPage {

	public static final Logger log = Logger.getLogger(GlobalNavigationPage_Android.class);

	public GlobalNavigationPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		headerLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']//img[@class='is-mobile']");
	}

	public void globalNavValidations() {

		if (verifyObjectDisplayed(headerLogo)) {

			tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
					"Wyndham Logo is present in the Global Nav");

			if (verifyObjectDisplayed(headerClubs)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Clubs tab present in the global nav");

				clickElementBy(headerClubs);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().toUpperCase().contains("CLUBWYNDHAM")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Club site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham club site");
				}

				driver.get(testData.get("URL"));
				openHamburgerMenu();

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Clubs tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerExchanges)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Exchanges tab present in the global nav");

				clickElementBy(headerExchanges);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (driver.getCurrentUrl().toUpperCase().contains("RCI")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Exchanges: RCI site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Exchanges site");
				}

				driver.get(testData.get("URL"));
				openHamburgerMenu();

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Exchanges tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerRentals)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Rentals tab present in the global nav");

				clickElementBy(headerRentals);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains("RENTAL")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Rentals site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Rentals site");
				}

				driver.get(testData.get("URL"));
				openHamburgerMenu();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Rentals tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerInvestors)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Investors tab present in the global nav");

				clickElementBy(headerInvestors);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(tabs2.get(1));

				if (driver.getCurrentUrl().toUpperCase().contains("INVESTOR")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Investors site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Investors site");
				}

				driver.get(testData.get("URL"));
				openHamburgerMenu();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Investors tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerCareers)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Careers tab present in the global nav");

				clickElementBy(headerCareers);
				/*
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); ArrayList<String> tabs2
				 * = new ArrayList<String>(driver.getWindowHandles());
				 * driver.switchTo().window(tabs2.get(1));
				 */
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				// waitUntilElementVisibleBy(driver, locator, timeOutInSeconds);

				if (driver.getCurrentUrl().toUpperCase().contains("CAREERS")) {
					tcConfig.updateTestReporterWithoutScreenshot("GlobalNavigationPage", "globalNavValidations",
							Status.PASS, "Wyndham Careers site opened");
					// driver.navigate().back();
					driver.get(testData.get("URL"));
					openHamburgerMenu();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Careers site");
				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Careers tab not present in the global nav");
			}

			if (verifyObjectDisplayed(contactUsTab)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Contact Us Tab present in global navigation");

				clickElementBy(contactUsTab);
				waitUntilElementVisibleBy(driver, contactUsHeader, 120);
				if (verifyObjectDisplayed(contactUsHeader)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Navigated To Contact Us Page and Header is present");

					driver.get(testData.get("URL"));
					openHamburgerMenu();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Contact Us Page navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Contact Us tab not present in the global nav ");
			}

			waitUntilElementVisibleBy(driver, headerMainMenu, 120);
			List<WebElement> mainMenuList = driver.findElements(headerMainMenu);

			for (int i = 0; i < 3; i++) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						mainMenuList.get(i).getText() + " tab is present in the global nav");
				clickElementJS(mainMenuList.get(i));

				List<WebElement> subMenuList = driver.findElements(headerSubMenu);
				for (int j = 0; j < subMenuList.size(); j++) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							subMenuList.get(j).getText() + " is present in the " + mainMenuList.get(i).getText()
									+ "  menu");

				}

			}

			if (verifyObjectDisplayed(modernSlaveryHdr)) {
				clickElementJS(modernSlaveryHdr);
				waitUntilElementVisibleBy(driver, modernSlavery, 120);
				if (verifyObjectDisplayed(modernSlavery)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Navigated to Modern Slavery Statement Page and Header is present");
					// waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Unable to navigate to Modern Slavery Statement page");
				}
			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Modern Slavery not present in Global Nav");
			}

		} else {
			tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
					"Wyndham Logo is not present in the Global Nav");
		}

	}

	public void contactUsValidations() {
		waitUntilElementVisibleBy(driver, contactUsTab, 120);

		if (verifyObjectDisplayed(contactUsTab)) {
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
					"Contact Us Tab present in global navigation");

			clickElementBy(contactUsTab);
			waitUntilElementVisibleBy(driver, contactUsHeader, 120);
			if (verifyObjectDisplayed(contactUsHeader)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
						"Navigated To Contact Us Page and Header is present");
				if (verifyObjectDisplayed(contactUsBreadCrumb)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Bread Crumb Navigation Present");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Bread Crumb Navigation not Present");
				}

				if (verifyObjectDisplayed(contactUsContent)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Content Present");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Content not Present");
				}

				if (verifyObjectDisplayed(wynHotelsResortLink)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Wyndham Hotels & Resorts Link Present with contact: "
									+ driver.findElement(wynHotelsResortContact).getText());

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String url = driver.getCurrentUrl();
					clickElementJSWithWait(wynHotelsResortLink);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (driver.getCurrentUrl().toUpperCase().contains("WYNDHAMHOTELS")) {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								"Wyndhm Hotel & Resorts Link opened");
						// driver.navigate().back();
						driver.get(url);
						waitUntilElementVisibleBy(driver, contactUsHeader, 120);

						if (verifyObjectDisplayed(wynRewardsLink)) {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
									"Wyndham Rewards Link Present in contact us page");
							url = driver.getCurrentUrl();
							clickElementJSWithWait(wynRewardsLink);
							waitForSometime(tcConfig.getConfig().get("MedWait"));

							if (driver.getCurrentUrl().toUpperCase().contains("JOIN")) {
								tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
										"Wyndhm Rewards Link opened");
								driver.get(url);

								waitUntilElementVisibleBy(driver, contactUsHeader, 120);

							} else {
								tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
										"Wyndhm Rewards Link did not opened");
							}
						} else {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
									"Wyndhm Rewards Link not present");
						}
					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Wyndhm Hotel & Resorts Link not opened");
					}

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Wyndhm Hotel & Resorts Link not present");
				}

				List<WebElement> mainSectionList = driver.findElements(contactUsSection);
				for (int i = 0; i < mainSectionList.size(); i++) {

					scrollDownForElementJSWb(mainSectionList.get(i));
					scrollUpByPixel(200);

					if (verifyElementDisplayed(mainSectionList.get(i))) {

						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								getElementText(mainSectionList.get(i)) + " Section displayed");

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								mainSectionList.size() + " Section not displayed");
					}

				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
						"Could not navigate to Contact Us page");
			}

		} else {
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
					"Contact Us tab not present");
		}

	}
}