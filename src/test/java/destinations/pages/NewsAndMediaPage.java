package destinations.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NewsAndMediaPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(NewsAndMediaPage.class);

	public NewsAndMediaPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	protected By newsAndMediaTab = By.xpath("//a[@title='News & Media']");
	protected By pressReleaseTab = By.xpath("//div[@class='wyn-fly-out']//a[contains(.,'Press Releases')]");
	protected By ourAwardsTab = By.xpath("//div[@class='wyn-fly-out']//a[contains(.,'Awards')]");
	protected By mediaContacts = By.xpath("//div[@class='wyn-fly-out']//a[contains(.,'Media Contacts')]");

	protected By headerPressRelease = By.xpath("//span[contains(.,'Press Release')]");
	protected By breadCrumbPress = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Press')]");
	protected By loadMoreButton = By.xpath("//button[contains(.,'Load More')]");
	protected By pressRelaseDate = By.xpath("//article/div/span");
	protected By pressReleaseArticles = By.xpath("//article/h3/a");
	protected By readMoreCTA = By.xpath("//article/a[contains(.,'Read More')]");
	protected By clickedArticleHeader = By.xpath("//h1");
	protected By sortSelect = By.xpath("//select[@aria-label='Sort By']");
	// Date Ascending
	// Date Descending

	protected By headerOurAwards = By.xpath("//h1[contains(.,'Awards')]");
	protected By brandHeaders = By.xpath("//div[@class='richtext']//h3");
	protected By awardList = By.xpath("//div[@class='richtext']//ul/li");
	protected By breadCrumbAwards = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Awards')]");
	protected By homeClick = By.xpath("//a[contains(.,'Home')]");

	protected By headerMediaContacts = By.xpath("//h1[contains(.,'Media Contacts')]");
	protected By contactEmail = By.xpath("//p/a[contains(.,'.com')]");

	public void pressReleaseValidations() throws ParseException {

		waitUntilElementVisibleBy(driver, newsAndMediaTab, 120);

		if (verifyObjectDisplayed(newsAndMediaTab)) {
			tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
					"News And Media Tab present in the Global Navigation of the homepage");
			if (browserName.equalsIgnoreCase("firefox")) {
				Actions a = new Actions(driver);
				a.moveToElement(driver.findElement(newsAndMediaTab)).click().build().perform();
			} else {
				clickElementBy(newsAndMediaTab);
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(pressReleaseTab)) {
				tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
						"Press Release Present under News and Media tab");

				clickElementBy(pressReleaseTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, headerPressRelease, 120);

				if (verifyObjectDisplayed(headerPressRelease)) {
					tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
							"Press Release Page Opened");

					if (verifyObjectDisplayed(breadCrumbPress)) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Breadcrumb Navigation Present");

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Breadcrumb Navigation error");
					}

					List<WebElement> dateSortCheck = driver.findElements(pressRelaseDate);

					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy");
					Date d2 = null;
					Boolean b3 = null;
					for (int i = 0; i < dateSortCheck.size(); i++) {

						Date d1 = sdf.parse(dateSortCheck.get(i).getText());

						if (i >= 1) {
							Boolean b1 = d1.before(d2);
							Boolean b2 = d1.equals(d2);
							if (b1 == true || b2 == true) {
								b3 = true;
								d2 = d1;
							} else {
								b3 = false;
								break;
							}
						} else {
							d2 = d1;
						}

					}

					if (b3 == true) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Date is sorted in Newest to Oldest Manner By Default");
					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Date is not sorted in Newest to oldest manner");
					}

					if (verifyObjectDisplayed(sortSelect)) {

						new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (oldest first)");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						List<WebElement> dateSortCheck1 = driver.findElements(pressRelaseDate);

						SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd,yyyy");
						Date d3 = null;
						Boolean b6 = null;
						for (int i = 0; i < dateSortCheck1.size(); i++) {

							Date d4 = sdf1.parse(dateSortCheck1.get(i).getText());

							if (i >= 1) {
								Boolean b4 = d4.after(d3);
								Boolean b5 = d4.equals(d3);
								if (b4 == true || b5 == true) {
									b6 = true;
									d3 = d4;
								} else {
									b6 = false;
									break;
								}
							} else {
								d3 = d4;
							}

						}

						if (b6 == true) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Date is sorted in Ascending order");
						} else {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Date is not sorted in Ascending order");
						}

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Sorting Option Not Available");
					}

					new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (newest first)");

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					List<WebElement> numberOfArticles = driver.findElements(pressReleaseArticles);
					String strfirstNewsHeader = numberOfArticles.get(0).getText().toUpperCase();
					if (verifyElementDisplayed(numberOfArticles.get(0))) {

						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
								"Articles present in Press Release Page, Header of first Article: "
										+ numberOfArticles.get(0).getText());

						try {
							List<WebElement> readMore = driver.findElements(readMoreCTA);
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Read More CTA present, total: " + readMore.size());

						} catch (Exception e) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Read More CTA not present");
						}

						numberOfArticles.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LongWait"));

						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs2.get(1));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (browserName.equalsIgnoreCase("CHROME") || browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("edge")) {

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(clickedArticleHeader).getText().toUpperCase()
									.contains(strfirstNewsHeader)) {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
										"First Article Associated page opened");

								waitForSometime(tcConfig.getConfig().get("LowWait"));

							} else {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
										"Error in opening first header associated page");
							}
							driver.close();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.switchTo().window(tabs2.get(0));
							waitUntilElementVisibleBy(driver, headerPressRelease, 120);
							if (verifyObjectDisplayed(headerPressRelease)) {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
										"Navigated Back to Press Release Page");

								if (verifyObjectDisplayed(loadMoreButton)) {
									scrollDownForElementJSBy(loadMoreButton);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									scrollUpByPixel(200);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									List<WebElement> numberOfArticlesB4 = driver.findElements(pressReleaseArticles);
									tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
											Status.PASS,
											"Load More Button Present and Number of Articles before clicking: "
													+ numberOfArticlesB4.size());

									clickElementBy(loadMoreButton);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									List<WebElement> numberOfArticlesAfter = driver.findElements(pressReleaseArticles);

									tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
											Status.PASS,
											"Number of Articles After Clicking: " + numberOfArticlesAfter.size());

								} else {
									tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
											Status.FAIL, "Load More Button Not Present on Homepage");
								}

							} else {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
										"Press Release Navigation Failed");
							}

						} else if (browserName.equalsIgnoreCase("IE")) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(clickedArticleHeader).getText().toUpperCase()
									.contains(strfirstNewsHeader)) {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
										"First Article Associated page opened");

								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.close();
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								driver.switchTo().window(tabs2.get(0));

								if (verifyObjectDisplayed(headerPressRelease)) {
									tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
											Status.PASS, "Navigated Back to Press Release Page");

									if (verifyObjectDisplayed(loadMoreButton)) {
										scrollDownForElementJSBy(loadMoreButton);
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										scrollUpByPixel(200);
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										List<WebElement> numberOfArticlesB4 = driver.findElements(pressReleaseArticles);
										tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
												Status.PASS,
												"Load More Button Present and Number of Articles before clicking: "
														+ numberOfArticlesB4.size());

										clickElementBy(loadMoreButton);
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										List<WebElement> numberOfArticlesAfter = driver
												.findElements(pressReleaseArticles);

										tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
												Status.PASS,
												"Number of Articles After Clicking: " + numberOfArticlesAfter.size());

									} else {
										tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
												Status.FAIL, "Load More Button Not Present on Homepage");
									}

								} else {
									tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations",
											Status.FAIL, "Press Release Navigation Failed");
								}

							} else {
								tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
										"Error in opening first header associated page");
							}
						}

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Articles not present in Press Release Page");
					}

				} else {
					tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
							"Could not open press release page");
				}

			} else {
				tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
						"Press Release not Present under News and Media tab");
			}

		} else {
			tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
					"Unable to find News and Media Tab");
		}

	}

	public void ourAwardsValidations() {
		scrollDownForElementJSBy(newsAndMediaTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(newsAndMediaTab)) {
			tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
					"News And Media Tab present in the Press Release Page");

			if (browserName.equalsIgnoreCase("firefox")) {
				Actions a = new Actions(driver);
				a.moveToElement(driver.findElement(newsAndMediaTab)).click().build().perform();
			} else {
				clickElementBy(newsAndMediaTab);
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(ourAwardsTab)) {
				tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
						"Our Awards Tab present in the News and Media Tab");

				clickElementBy(ourAwardsTab);
				waitUntilObjectVisible(driver, headerOurAwards, 120);

				if (verifyObjectDisplayed(headerOurAwards)) {
					tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
							"Navigated to Our Awards page");

					List<WebElement> brandList = driver.findElements(brandHeaders);
					List<WebElement> noAwardList = driver.findElements(awardList);
					tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
							noAwardList.size() + " Awards are seperated under " + brandList.size() + " Brands");

					for (int i = 0; i < brandList.size(); i++) {
						scrollDownForElementJSWb(brandList.get(i));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(100);

						tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
								"Brand No. " + (i + 1) + " " + brandList.get(i).getText());

					}

					scrollDownForElementJSBy(breadCrumbAwards);

					if (verifyObjectDisplayed(breadCrumbAwards)) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.PASS,
								"Breadcrumb Present");
						scrollUpByPixel(200);

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.FAIL,
								"Breadcrumb not Present");
					}

				} else {
					tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.FAIL,
							"Our Awards Navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.FAIL,
						"Our Awards Tab not present in News and Media tab");
			}

		} else {
			tcConfig.updateTestReporter("NewsAndMediaPage", "ourAwardsValidations", Status.FAIL,
					"News and Media tab not present");
		}

	}

	public void mediaContactValidations() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(newsAndMediaTab)) {
			tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.PASS,
					"News And Media Tab present in the Press Release Page");

			clickElementBy(newsAndMediaTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(mediaContacts)) {
				tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.PASS,
						"Media Contacts Tab present in the News and Media Tab");

				clickElementBy(mediaContacts);
				waitUntilObjectVisible(driver, headerMediaContacts, 120);

				if (verifyObjectDisplayed(headerMediaContacts)) {
					tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.PASS,
							"Navigated to Media Contacts page");

					if (verifyObjectDisplayed(contactEmail)) {
						tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.PASS,
								"Contacting Email Address present as: " + driver.findElement(contactEmail).getText());

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.FAIL,
								"Contacting Email Address not present");
					}

				} else {
					tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.FAIL,
							"Media Contact Navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.FAIL,
						"Media Contact Tab not present in News and Media tab");
			}

		} else {
			tcConfig.updateTestReporter("NewsAndMediaPage", "mediaContactValidations", Status.FAIL,
					"News and Media tab not present");
		}

	}

}
