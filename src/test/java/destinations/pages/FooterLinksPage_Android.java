package destinations.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class FooterLinksPage_Android extends FooterLinksPage {

	public static final Logger log = Logger.getLogger(FooterLinksPage_Android.class);

	public FooterLinksPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		privacySettingsVal = By
				.xpath("//h1[contains(.,'Interest Based Advertising Policies') or contains(.,'Opt Out')]");
		footerBrandLogos = By.xpath("//div[@class='wyn-footer__mobile-brand-links wyn-footer__links ']//a");
		privacySettingsVal = By
				.xpath("//h1[contains(.,'Interest Based Advertising Policies') or contains(.,'Opt Out')]");
		footerBrandLogos = By.xpath("//div[@class='wyn-footer__mobile-brand-links wyn-footer__links ']//a");
		footerLegalLinks = By
				.xpath("//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li");

		footerPrivacyNotice = By
				.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[2]");
		footerPrivacySettings = By
				.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[3]");
		footerTimeshare = By
				.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[4]");
		footerCopyright = By.xpath(
				"//div[contains(@class,'body-2 text-white') and contains(.,'© Wyndham Destinations 2021. All Rights Reserved.')]");
		footerWynLogo = By.xpath("//div[@class='cell']/a/img");
		buttonCareers = By.xpath("//a[@class='subtitle-3' and contains(.,'Careers')]");
		buttonContactUs = By.xpath("//a[@class='subtitle-3' and contains(.,'Contact')]");
		footerWynPages = By.xpath("//ul[@class= 'menu vertical emptyAccordion']/li");
	}

	protected By footerTermsOfUse = By
			.xpath("(//div[contains(@class,'align-center quickLinks')]//ul[@class='menu align-center']/li)[1]");
	protected By footerAboutWyn = By.xpath("//div[@class='footer']//a[contains(.,'About Wyndham')]");
	protected By footerNewsMedia = By.xpath("//div[@class='footer']//a[contains(.,'News & Media')]");
	protected By footersocialResp = By.xpath("//div[@class='footer']//a[contains(.,'Social Responsibility')]");
	protected By footerourCompany = By.xpath("//div[@class='footer']//a[contains(.,'Our Company')]");

	/*
	 * Method: footerPrivacySection Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */
	@Override
	public void footerPrivacySection() {
		List<WebElement> legalList = driver.findElements(footerLegalLinks);
		Assert.assertTrue(verifyObjectDisplayed(footerCopyright));
		if (verifyObjectDisplayed(footerWynLogo)) {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total number of privacy links: " + legalList.size());

			for (int i = 0; i < legalList.size(); i++) {

				if (i == 0) {
					scrollDownForElementJSBy(footerTermsOfUse);
					clickIOSElement("Terms of Use", 1, footerTermsOfUse);
					clickElementJSWithWait(footerTermsOfUse);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateTermsOfUse();
					navigateBack();
				} else if (i == 1) {
					scrollDownForElementJSBy(footerPrivacyNotice);
					clickElementJSWithWait(footerPrivacyNotice);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validatePrivacyNotice();
					navigateBack();
				} else if (i == 2) {
					scrollDownForElementJSBy(footerPrivacySettings);
					clickElementJSWithWait(footerPrivacySettings);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validatePrivacySettings();
				} else if (i == 3) {
					scrollDownForElementJSBy(footerTimeshare);
					clickElementJSWithWait(footerTimeshare);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					validateSupportTimeshare();
				}
				getElementInView(footerCopyright);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Wyndham Brand Logos Present in the not present in the Footer");
		}

		scrollDownForElementJSBy(footerDoNotSell);
		if (verifyObjectDisplayed(footerDoNotSell)) {
			clickElementJSWithWait(footerDoNotSell);
			verifyObjectDisplayed(optOutHeader);
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.PASS,
					"Footer do not sell is present ");

		} else {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySection", Status.FAIL,
					"Footer do not sell is not present ");
		}

	}

	/*
	 * Method: validateTermsOfUse Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */
	@Override
	public void validateTermsOfUse() {
		waitUntilObjectVisible(driver, termsOfUseHeader, 120);
		getElementInView(termsOfUseHeader);
		if (verifyObjectDisplayed(termsOfUseHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.PASS,
					"Terms Of Use Page Navigated");

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validateTermsOfUse", Status.FAIL,
					"Terms Of Use Page could not be Navigated");
		}

	}

	/*
	 * Method: validatePrivacyNotice Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */
	@Override
	public void validatePrivacyNotice() {

		waitUntilObjectVisible(driver, privacyNoticeHeader, 120);
		getElementInView(privacyNoticeHeader);
		if (verifyObjectDisplayed(privacyNoticeHeader)) {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.PASS,
					"Privacy Notice Navigated");

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacyNotice", Status.FAIL,
					" Privacy Notice could not be Navigated");
		}
	}

	/*
	 * Method: validatePrivacySettings Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */
	@Override
	public void validatePrivacySettings() {
		String url = testData.get("URL");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

		if (tabs.size() == 2) {
			waitUntilObjectVisible(driver, privacySettingsVal, 120);
			getElementInView(privacySettingsVal);
			if (verifyObjectDisplayed(privacySettingsVal)) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
						"Privacy Settings Page Navigated");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
						"Privacy Settings Page could not be Navigated");
			}
		}
		navigateToURL(url);
		pageCheck();

	}

	/*
	 * Method: validateSupportTimeshare Description: Validtions : Date: Feb/2021
	 * Author: Saket Sharma Changes By:
	 */

	@Override
	public void validateSupportTimeshare() {
		String url = testData.get("URL");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			if (driver.getTitle().toUpperCase().contains("TIMESHARE")) {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.PASS,
						"timeshare Page Navigated");
			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "validatePrivacySettings", Status.FAIL,
						"timeshare could not be Navigated");
			}
		}
		navigateToURL(url);
		pageCheck();

	}

	public void footerSocialIcons() {
		String strURL = driver.getCurrentUrl();

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());
			int LoopCnt = socialIconsList.size();
			for (int i = 0; i < LoopCnt; i++) {
				driver.get(strURL);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				scrollDownForElementJSBy(footer);
				List<WebElement> socialIconsListNew = driver.findElements(footerSocialIcons);
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					break;
				case 1:
					strSiteName = "TWITTER";
					break;
				case 2:
					strSiteName = "INSTAGRAM";
					break;
				case 3:
					strSiteName = "YOUTUBE";
					break;
				case 4:
					strSiteName = "LINKED";
					break;

				default:
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				clickElementJS(socialIconsListNew.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// ArrayList<String> tabs2 = new
				// ArrayList<String>(driver.getWindowHandles());
				/* driver.switchTo().window(tabs2.get(1)); */
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				/*
				 * driver.close();
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * driver.switchTo().window(tabs2.get(0));
				 */

			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}
		driver.get(strURL);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(footer);

	}

	public void footerWynLinks() {

		if (verifyObjectDisplayed(footerWynPages)) {
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
					"Wyndham Destinations Sub Links Present in the Footer,Total: " + wynLinksList.size());
			waitUntilObjectVisible(driver, footerAboutWyn, 120);
			clickElementBy(footerAboutWyn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(footerWyndhamHotels)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Wyndham Hotels & Resort");
				String strCurrentURL = driver.getCurrentUrl();
				clickElementBy(footerWyndhamHotels);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}

				if (tabs2.size() == 2) {
					// driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains("WYNDHAM HOTELS")) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"Wyndham Hotel & Resort Associated Link Opened");
						// driver.close();
						// waitForSometime(tcConfig.getConfig().get("MedWait"));
						// driver.switchTo().window(tabs2.get(0));
						driver.get(strCurrentURL);
						scrollDownForElementJSBy(footer);
						// waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitForSometime(tcConfig.getConfig().get("LongWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"Wyndham Hotel & Resort Associated Link opening error");

					}
				} else {
					if (driver.getTitle().toUpperCase().contains("WYNDHAM HOTELS")) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"Wyndham Hotel & Resort Associated Link Opened");

						driver.get(strCurrentURL);
						scrollDownForElementJSBy(footer);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"Wyndham Hotel & Resort Associated Link opening error");

					}

				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}
			String strcurURL = driver.getCurrentUrl();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownForElementJSBy(footer);
			clickElementBy(footerNewsMedia);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(footerFactSheetLink)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
						"Wyndham Destinations Fact Sheet Link Present in footer");
				clickElementBy(footerFactSheetLink);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(factSheetHeader)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
							"User Navigated to Fact Sheet Page");

					if (verifyObjectDisplayed(factSheetLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"View Fact Sheet Link Present in the content");

						clickElementBy(factSheetLink);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						// driver.switchTo().window(tabs2.get(1));*/
						if (browserName.equalsIgnoreCase("IE")) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
						}

						String strPdf = driver.findElement(factSheetPdf).getAttribute("type");
						if (strPdf.toUpperCase().contains("PDF")) {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
									"Associated PDF opened");

							/*
							 * driver.close();
							 * waitForSometime(tcConfig.getConfig().get(
							 * "LowWait"));
							 * driver.switchTo().window(tabs2.get(0));
							 */
							driver.get(strcurURL);
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							if (verifyObjectDisplayed(footerWynLogo)) {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
										"Navigated Back and Wyndham Logo present in the footer");
								clickElementBy(footerWynLogo);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								if (verifyObjectDisplayed(footer)) {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
											"Navigated to homepage");

									scrollDownForElementJSBy(footer);
									waitForSometime(tcConfig.getConfig().get("LowWait"));

								} else {
									tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
											"Navigation to homepage failed");
								}

							} else {
								tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
										"Navigation to Wyn Destination Failed");
							}

						} else {
							tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
									"Fact Sheet Associated PDF opening problem");
						}

					} else if (verifyObjectDisplayed(factSheetUpLink)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.PASS,
								"View Fact Sheet Link Present in the content");
						String strCurrURL = driver.getCurrentUrl();
						scrollDownForElementJSBy(factSheetUpLink);
						scrollUpByPixel(150);
						clickElementBy(factSheetUpLink);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						/* driver.switchTo().window(tabs2.get(1)); */
						if (browserName.equalsIgnoreCase("IE")) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
						}
						String strPdf = "";
						/*
						 * if(browserName.equalsIgnoreCase("firefox")) { strPdf
						 * =
						 * driver.findElement(factSheetPdf).getAttribute("class"
						 * ); } else { strPdf =
						 * driver.findElement(factSheetPdf).getAttribute("type")
						 * ; } if (strPdf.toUpperCase().contains("PDF")) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Associated PDF opened");
						 * 
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * driver.switchTo().window(tabs2.get(0));
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * driver.get(strCurrURL);
						 * scrollDownForElementJSBy(factSheetUpLink);
						 * scrollUpByPixel(150); if
						 * (verifyObjectDisplayed(footerWynLogo)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated Back and Wyndham Logo present in the footer"
						 * ); clickElementBy(footerWynLogo);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * if (verifyObjectDisplayed(footer)) {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.PASS,
						 * "Navigated to homepage");
						 * 
						 * scrollDownForElementJSBy(footer);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to homepage failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Navigation to Wyn Destination Failed"); }
						 * 
						 * } else {
						 * tcConfig.updateTestReporter("FooterLinksPage",
						 * "footerWynLinks", Status.FAIL,
						 * "Fact Sheet Associated PDF opening problem"); }
						 */

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
								"View Fact Sheet Link not Present in the content");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
							"Navigation to fact sheet page failed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
						"Wyndham Destinations Fact Sheet Link not Present in footer");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	public void footerTermsOfUse() {

		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(0))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
					"Terms Of Use Link present in the footer");

			clickElementWb(legalList.get(0));
			waitUntilObjectVisible(driver, termsOfUseHeader, 120);

			if (verifyObjectDisplayed(termsOfUseHeader)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
						"Terms Of Use Page Navigated");

				if (verifyObjectDisplayed(termsOfUseBreadcrumb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Terms Of Use Breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Terms Of Use Breadcrumb not present");
				}

				if (verifyObjectDisplayed(footerAboutWyn) && verifyObjectDisplayed(footersocialResp)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"About Wyndham and Social Responsibilities Tab present");

					clickElementBy(footerAboutWyn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(footerourCompany)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"User has the option to navigate to Our Company page in the Secondary Navigation");
						clickElementBy(footerAboutWyn);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Our Company Link is not present in the Secondary Navigation");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"About Wyndham and Social Responsibilities Tab not present");
				}

				if (verifyObjectDisplayed(termsOfUseContent)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
							"Content present in the Terms of Use page");
					if (verifyObjectDisplayed(termsOfUseMail)) {
						scrollDownForElementJSBy(termsOfUseMail);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.PASS,
								"Copyright Violation Email Link present");

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
								"Copyright violation email link not present");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
							"Content not present in the Terms of Use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
						"Terms Of Use Page could not be Navigated");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerTermsOfUse", Status.FAIL,
					"Terms Of Use Link not present in the footer");
		}

	}

	public void footerPrivacySettings() {
		String strUrl = driver.getCurrentUrl();
		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(2));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(2))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
					"Privacy Settings Link present in the Footer");

			clickElementWb(legalList.get(2));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());

			// driver.switchTo().window(tabs2.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (browserName.equalsIgnoreCase("IE")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			if (verifyObjectDisplayed(privacySettingsVal)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
						"Navigated to Privacy Settings related page");

				driver.get(strUrl);
				List<WebElement> legalListNew = driver.findElements(footerLegalLinks);

				scrollDownForElementJSWb(legalListNew.get(2));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollDownByPixel(100);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(legalListNew.get(2))) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.PASS,
							"Navigated Back to Terms of Use page");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
							"Unable to navigate to Terms of use page");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
						"Unable to navigate to Privacy Settings page");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacySettings", Status.FAIL,
					"Privacy Settings Link not present in the Footer");
		}

	}

	@Override
	public void footerPrivacyNotice() {
		String strUrl = driver.getCurrentUrl();
		List<WebElement> legalList = driver.findElements(footerLegalLinks);

		scrollDownForElementJSWb(legalList.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownByPixel(100);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(legalList.get(1))) {

			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
					"Privacy Notice Link present in the Footer");

			clickElementWb(legalList.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(privacyNotice)) {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
						"Privacy Notice page navigated and header present");
				if (verifyObjectDisplayed(privacyNoticeBrdcrmb)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice breadcrumb present");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice breadcrumb not present");
				}

				if (verifyObjectDisplayed(privacyNoticeMaterialUpdate)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Material Update Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Material Update Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeIntroduction)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Intrdouction Section displayed");
				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Intrdouction Section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeCookies)) {
					String newURL = driver.getCurrentUrl();
					scrollDownForElementJSBy(privacyNoticeCookies);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Cookies link displayed");
					clickElementBy(privacyNoticeCookies);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					// waitForSometime(tcConfig.getConfig().get("LongWait"));
					waitUntilObjectVisible(driver, privacySettingsVal, 120);// need
																			// changes
																			// from
																			// utsav
					if (verifyObjectDisplayed(privacySettingsVal)) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Cookies page navigated");
						driver.get(newURL);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollDownForElementJSBy(privacyNoticeCookies);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Cookies page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Cookies link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeGAnalytics)) {
					/*
					 * scrollDownForElementJSBy(privacyNoticeGAnalytics);
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * scrollUpByPixel(90);
					 */

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Google Analytics link displayed");
					String stringURL = driver.getCurrentUrl();
					clickElementBy(privacyNoticeGAnalytics);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getCurrentUrl().toUpperCase().contains("GOOGLE")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Google Analytics page navigated");
						driver.get(stringURL);
						scrollDownForElementJSBy(privacyNoticeSignalTrack);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Google Analytics page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Google Analytics link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeSignalTrack) && verifyObjectDisplayed(privacyNoticeAds)) {
					scrollDownForElementJSBy(privacyNoticeSignalTrack);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Signal Track section displayed and ADS link displayed");
					String strNewURL = driver.getCurrentUrl();
					clickElementBy(privacyNoticeAds);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("YOURADCHOICES.COM")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"DAA page navigated");
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * driver.switchTo().window(tabs2.get(0));
						 */
						driver.get(strNewURL);
						scrollDownForElementJSBy(privacyNoticeNwAdv);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"DAA page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Signal Track section not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeNwAdv)) {

					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Network Adv link displayed");
					strUrl = driver.getCurrentUrl();
					clickElementBy(privacyNoticeNwAdv);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					// waitUntilObjectVisible(driver, privacySettingsVal, 120);
					if (driver.getTitle().toUpperCase().contains("NETWORK")) {

						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
								"Network Adv page navigated");
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * driver.switchTo().window(tabs2.get(0));
						 */
						driver.get(strUrl);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollDownForElementJSBy(privacyNoticeMailAdd);

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
								"Network Adv page navigation failed");
					}

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Network Adv link not displayed");
				}

				if (verifyObjectDisplayed(privacyNoticeMailAdd)) {

					scrollDownForElementJSBy(privacyNoticeMailAdd);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.PASS,
							"Privacy Notice Mail link displayed");

					// driver.switchTo().activeElement().sendKeys(Keys.HOME);

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
							"Privacy Notice Mail link not displayed");
				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
						"Privacy Notice page navigation unsuccessful");
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerPrivacyNotice", Status.FAIL,
					"Privacy Notice Link not present in the Footer");
		}
	}

}
