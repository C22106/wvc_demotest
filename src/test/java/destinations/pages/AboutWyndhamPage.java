package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class AboutWyndhamPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(AboutWyndhamPage.class);

	public AboutWyndhamPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By aboutWyndhamTab = By
			.xpath("//div[@class='wyn-js-main-nav wyn-main-nav ']//a[contains(.,'About Wyndham')]");
	protected By ourCompanyPageLink = By
			.xpath("//div[@class='wyn-js-main-nav wyn-main-nav ']//a[contains(.,'Our Company')]");
	protected By ourCompanyHeader = By.xpath("//h1[contains(.,'Our Company')]");
	protected By ourCompanyBreadcrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Our Company')]");
	protected By BeliefsABImage = By.xpath("//div[@class='columns wyn-action-blocks']//img[@alt='Beliefs & Values']");
	protected By BeliefsABHeader = By
			.xpath("//div[@class='columns wyn-action-blocks']//a[contains(.,'Beliefs and Values')]");
	protected By BeliefsABDescription = By.xpath("//div[@class='columns wyn-action-blocks'][1]//p[contains(.,' ')]");
	protected By BeliefsABReadMore = By.xpath("//div[@class='columns wyn-action-blocks'][1]//a[contains(.,'Read')]");
	protected By BeliefsBanner = By.xpath("//div[@class='wyn-banner']//img");
	protected By BeliefsHeader = By.xpath("//h1[contains(.,'Beliefs')]");
	protected By BeliefsBreadcrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Beliefs')]");
	protected By BeliefsContent = By.xpath("//div[@class='richtext']");

	public void beliefsAndValues() {

		waitUntilObjectVisible(driver, aboutWyndhamTab, 120);

		if (verifyObjectDisplayed(aboutWyndhamTab)) {

			tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
					"About Wyndham Tab Present in the homepage");

			clickElementBy(aboutWyndhamTab);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(ourCompanyPageLink)) {
				tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
						"Our Company Link present in the sub menu");

				clickElementBy(ourCompanyPageLink);

				waitUntilObjectVisible(driver, ourCompanyHeader, 120);

				if (verifyObjectDisplayed(ourCompanyHeader)) {
					tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
							"User is navigated to Our Company page");

					scrollDownByPixel(75);

					if (verifyObjectDisplayed(BeliefsABHeader)) {
						tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
								"Beliefs and Values Header Present in the Action Block");

						if (verifyObjectDisplayed(BeliefsABImage) && verifyObjectDisplayed(BeliefsABDescription)
								&& verifyObjectDisplayed(BeliefsABReadMore)) {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
									"Beliefs and Values Image, Description and Read More CTA present");

						} else {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
									"Beliefs and Values Image or Description or Read More CTA not present");
						}

						clickElementBy(BeliefsABHeader);
						waitUntilObjectVisible(driver, BeliefsHeader, 120);

						if (verifyObjectDisplayed(BeliefsHeader)) {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
									"Beliefs and Values Page navigations successful and Header Present");

							if (verifyObjectDisplayed(BeliefsBanner)) {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
										"Beliefs and Values Page Banner present");
							} else {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
										"Beliefs and Values Page Banner not present");
							}

							if (verifyObjectDisplayed(BeliefsBreadcrumb) && verifyObjectDisplayed(BeliefsContent)) {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
										"Beliefs and Values Breadcrumb and Content Present");
							} else {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
										"Beliefs and Values Breadcrumb or Content not Present");
							}

						} else {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
									"Beliefs and Values Page navigations unsuccessful and Header not Present");
						}

					} else {
						tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
								"Beliefs and Values Header not Present in the Action Block");
					}

				} else {
					tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
							"User is not navigated to Our Company page");
				}

			} else {
				tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
						"Our Company Link not present in the sub menu");
			}

		} else {
			tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
					"About Wyndham Tab is not present in the homepage");
		}

	}

}
