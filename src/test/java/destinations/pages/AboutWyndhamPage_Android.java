package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class AboutWyndhamPage_Android extends AboutWyndhamPage {

	public static final Logger log = Logger.getLogger(AboutWyndhamPage_Android.class);

	public AboutWyndhamPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public void beliefsAndValues() {

		waitUntilObjectVisible(driver, aboutWyndhamTab, 120);

		if (verifyObjectDisplayed(aboutWyndhamTab)) {

			tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
					"About Wyndham Tab Present in the homepage");

			clickElementBy(aboutWyndhamTab);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(ourCompanyPageLink)) {
				tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
						"Our Company Link present in the sub menu");

				clickElementBy(ourCompanyPageLink);

				waitUntilObjectVisible(driver, ourCompanyHeader, 120);

				if (verifyObjectDisplayed(ourCompanyHeader)) {
					tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
							"User is navigated to Our Company page");

					scrollDownByPixel(75);

					if (verifyObjectDisplayed(BeliefsABHeader)) {
						tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
								"Beliefs and Values Header Present in the Action Block");

						if (verifyObjectDisplayed(BeliefsABImage) && verifyObjectDisplayed(BeliefsABDescription)
								&& verifyObjectDisplayed(BeliefsABReadMore)) {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
									"Beliefs and Values Image, Description and Read More CTA present");

						} else {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
									"Beliefs and Values Image or Description or Read More CTA not present");
						}

						clickElementBy(BeliefsABHeader);
						waitUntilObjectVisible(driver, BeliefsHeader, 120);

						if (verifyObjectDisplayed(BeliefsHeader)) {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
									"Beliefs and Values Page navigations successful and Header Present");

							if (verifyObjectDisplayed(BeliefsBanner)) {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
										"Beliefs and Values Page Banner present");
							} else {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
										"Beliefs and Values Page Banner not present");
							}

							if (verifyObjectDisplayed(BeliefsBreadcrumb) && verifyObjectDisplayed(BeliefsContent)) {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.PASS,
										"Beliefs and Values Breadcrumb and Content Present");
							} else {
								tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
										"Beliefs and Values Breadcrumb or Content not Present");
							}

						} else {
							tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
									"Beliefs and Values Page navigations unsuccessful and Header not Present");
						}

					} else {
						tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
								"Beliefs and Values Header not Present in the Action Block");
					}

				} else {
					tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
							"User is not navigated to Our Company page");
				}

			} else {
				tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
						"Our Company Link not present in the sub menu");
			}

		} else {
			tcConfig.updateTestReporter("AboutWyndhamPage", "BeliefsAndValues", Status.FAIL,
					"About Wyndham Tab is not present in the homepage");
		}

	}
}