package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class DestinationsLoginPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(DestinationsLoginPage.class);

	public DestinationsLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By destinationHeroImage = By.xpath("//div[@class='image-quote-banner']");
	protected By acceptButton = By.xpath("//button[contains(.,'Close')]");
	protected By wyndhamLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By ieSupportBanner = By.xpath("//div[@class='wyn-ie-alert-message__panel']");
	protected By closeieSupport = By.xpath("//button[contains(.,'Got It')]");

	public void launchApplication(){
		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");

		waitUntilElementVisibleBy(driver, destinationHeroImage, 120);

		if (verifyObjectDisplayed(destinationHeroImage)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			}
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
					"Wyndham Destinations Navigation Successful");

			try {
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementBy(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

			if (verifyObjectDisplayed(closeieSupport)) {
				tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
						"Cookies Banner Present");

				clickElementBy(closeieSupport);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.INFO,
						"Cookies not Present");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
					"Wyndham Destinations Navigation Unsuccessful");
		}

	}

	public void navigateToHomepage(String strHome) {

		if (strHome.equalsIgnoreCase("logo")) {
			waitUntilElementVisibleBy(driver, wyndhamLogo, 120);
			tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementBy(wyndhamLogo);

			waitUntilObjectVisible(driver, destinationHeroImage, 120);

			if (verifyObjectDisplayed(destinationHeroImage)) {

				tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Destinations Navigation Successful");

			} else {
				tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Destinations Navigation Unsuccessful");
			}

		} else if (strHome.equalsIgnoreCase("breadcrumb")) {
			waitUntilElementVisibleBy(driver, homeBreadcrumb, 120);
			tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementJSWithWait(homeBreadcrumb);

			waitUntilObjectVisible(driver, destinationHeroImage, 120);

			if (verifyObjectDisplayed(destinationHeroImage)) {

				tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Destinations Navigation Successful");

			} else {
				tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Destinations Navigation Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "navigateToHomepage", Status.FAIL,
					"Unknown keyword entered, Failed to navigate to homepage");

		}

	}

}
