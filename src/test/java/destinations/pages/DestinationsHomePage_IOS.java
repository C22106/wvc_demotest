package destinations.pages;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class DestinationsHomePage_IOS extends DestinationsHomePage {

	public static final Logger log = Logger.getLogger(DestinationsHomePage_IOS.class.getName());
	protected By hamburgerMenu = By.xpath("//div[@id='nav-icon4']/span[2]");

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public DestinationsHomePage_IOS(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		vacationTab = By.xpath("//a[text()='Book Your Vacation']");
		resortLogo = By.xpath("//img[@id='header-logo' and @class='max-width hide-gt-xs']");
		destinationLogo = By.xpath("(//a[img[contains(@src,'logo')]])[2]");
		ourCompany = By.xpath("//li/a[@class='dropMenuItem']/span[contains(text(),'Our Company')]");
		clubWyndhamLogo = By.xpath("//img[@class='mobile-logo' and @alt='Wyndham']");
		worldMarkWyndhamLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
		margaritavilleLogo = By.xpath("//img[@alt='Margaritaville Logo']");
		shellVacationLogo = By.xpath("//img[@alt='Shell Vacations Club Logo']");
		searchContainer = By.xpath("//div[contains(@class,'wdHeader mobileNav')]//input[contains(@class,'searchQuery')]");
		searchButton = By.xpath("(//div[@class='input-group-button']//input[contains(@class,'siteSearch')])[2]");

	}
	
	@Override
	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		//fieldDataEnter(searchContainer, searchValue);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Search", searchValue);
		clickIOSElement("Search", 1, searchButton);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}
	
	protected By moreResultsLink = By.xpath("//a[contains(text(),'More Results')]");
	@Override
	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchTitle, 120);

		if (verifyObjectDisplayed(searchTitle)) {
			tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsFor = getElementText(resultNumber);

			String strResults = strResultsFor.split(" ")[0];

			tcConfig.updateTestReporter("DestinationsHomePage", "searchFunctionality", Status.PASS,
					"Total Results: " + strResults + " for" + strResultsFor);

			List<WebElement> listOfTotalResults = driver.findElements(resultList);
			List<WebElement> listFirstResults = driver.findElements(firstPageList);
			List<WebElement> listCTA = driver.findElements(searchListLink);
			tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.PASS,
					"No Of Results on first page " + listFirstResults.size());

			if (listOfTotalResults.size() >= 10) {
				if (verifyObjectDisplayed(moreResultsLink)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.PASS,
							"More Results Available on the page");
				}
			} else {
				System.out.println("Less than 10 results hence no More Results link");
			}

			String firstTitle = listFirstResults.get(0).getText();
			String href = getElementAttribute(listCTA.get(0), "href").trim().replace(".html", "");

			tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.PASS,
					"First Result title " + firstTitle);

			clickIOSElement(firstTitle, 1, listFirstResults.get(0));
			pageCheck();
			assertTrue(getCurrentURL().trim().contains(href), "Navigations not successfull");

			tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to first Search related page " + firstTitle);

			driver.navigate().back();

			pageCheck();

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

	@Override
	public void homepageLatestNews() {
		waitUntilElementVisibleBy(driver, latestNews, 120);

		scrollDownForElementJSBy(latestNews);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(latestNews)) {
			getElementInView(latestNews);
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
					"Latest News Block Present on the homepage");

			List<WebElement> newsArticlesHeader = driver.findElements(labelLatestNewsHeader);
			getElementInView(buttonViewMore);
			clickElementBy(buttonViewMore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(getObject(buttonViewLess).isEnabled(), "View Less button is present");
			List<WebElement> allNewsArticlesHeader = driver.findElements(labelAllLatestNewsHeader);
			if (allNewsArticlesHeader.size() > newsArticlesHeader.size()) {

				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
						"View more button is clicked in Latest News Block on the homepage");

				for (int i = 0; i < allNewsArticlesHeader.size(); i++) {
					List<WebElement> allNewsArticlesHeader1 = driver.findElements(labelAllLatestNewsHeader);
					getElementInView(latestNews);
					String strNewsHeader = allNewsArticlesHeader1.get(i).getText().toUpperCase();
					clickElementBy(allNewsArticlesHeader1.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// Assert.assertTrue(verifyObjectDisplayed(breadcrumbsPressRelease),
					// "Breadcurmbs is not present in the navigated page");
					Assert.assertTrue(
							driver.findElement(clickedArticleHeader).getText().toUpperCase().contains(strNewsHeader),
							"Header not matched");
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
							"Successfully navigate to " + (i + 1) + " news article");
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(buttonViewMore);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
						"View more button is unable to click in Latest News Block on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
					"Latest News block is not present on homepage");
		}
	}

	@Override
	public void validateBrand() {
		String strCurrentURL = driver.getCurrentUrl().trim();
		assertTrue(verifyObjectDisplayed(brandHeader), "Our Brand Header is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand header present");

		List<WebElement> brandImg = getList(brandImage);
		List<WebElement> brandNme = getList(brandName);
		List<WebElement> brandDes = getList(brandDescription);
		List<WebElement> brandCTA = getList(learnMoreCTA);

		if (brandImg.size() == brandNme.size()) {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand Name present");
			if (brandImg.size() == brandDes.size()) {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
						"Our Brand Description present");
				if (brandImg.size() == brandCTA.size()) {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
							"Our Brand Learn More CTA present");
				} else {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
							"Our Brand Learn More CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
						"Our Brand Description present");
			}
		} else {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL, "Our Brand Name present");
		}

		for (int i = 0; i < brandImg.size(); i++) {
			brandNme = getList(brandName);
			brandCTA = getList(learnMoreCTA);
			String brandName = getElementText(brandNme.get(i));
			getElementInView(brandCTA.get(i));
			if ((i == 0 || i == 1) && ((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
					.contains("iPad")) {
				clickIOSElement("Learn More", 1, brandCTA.get(i));
			} else if ((i == 2 || i == 3) && ((RemoteWebDriver) driver).getCapabilities().getCapability("model")
					.toString().contains("iPad")) {
				clickIOSElement("Learn More", 2, brandCTA.get(i));
			} else {
				clickElementByScriptExecutor("Learn More", 1);
			}
			// pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				// newTabNavigations(allTabs);
				waitForSometime("5");
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				waitForSometime("2");
				navigateToURL(strCurrentURL);
				pageCheck();

			} else {
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				waitForSometime("2");
				navigateToURL(strCurrentURL);
				pageCheck();
			}

		}
	}

	private void clickHamburgerMenu() {
		clickElementBy(hamburgerMenu);
		assertTrue(verifyObjectDisplayed(ourCompany), "Hamburger Menu not clicked successfully");

	}

	@Override
	public void validateLogo() {
		url = tcConfig.getTestData().get("URL");
		assertTrue(verifyObjectDisplayed(destinationLogo), "Header Destination logo is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateLogo", Status.PASS,
				"Header Destination logo is displayed");

		assertTrue(getElementAttribute(destinationLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header Destination logo url not correct");
		tcConfig.updateTestReporter("DestinationHomePage", "validateLogo", Status.PASS,
				"Header Wyndham logo is present");
	}

	@Override
	public void validateHomeHeaderMenu() {

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(contactUs), "Contact Us menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Contact Us menu item is present");
		getElementInView(contactUs);
		navigateValidateAndReturnToPage(contactUs);

		assertTrue(verifyObjectDisplayed(career), "Career menu item is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
				"Career menu item is present");
		clickElementBy(career);
		pageCheck();
		String getTitle = driver.getCurrentUrl().trim();
		if (getTitle.contains("careers")) {
			tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.PASS,
					"Career Page is present");
		} else {
			tcConfig.updateTestReporter("DestinationHomePage", "validateHeaderMenu", Status.FAIL,
					"Career menu item is not present");
		}
		navigateBack();
		pageCheck();

	}

	@Override
	public void homepagePanorama() {
		waitUntilElementVisibleBy(driver, panoramaHeader, 120);

		scrollDownForElementJSBy(panoramaHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollUpByPixel(100);

		if (verifyObjectDisplayed(panoramaHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
					"Panorama Block Present on the homepage");

			clickElementBy(panoramaReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			if (tabs2.size() == 2)

			{
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getTitle().toUpperCase().contains("PANORAMA")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
							"Panorama related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.switchTo().window(tabs2.get(0));
					waitUntilObjectVisible(driver, panoramaHeader, 120);
					if (verifyObjectDisplayed(panoramaHeader)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
								"Navigated Back to homepage");
					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
								"Error in getting back to homepage");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Panorama related page not opened");
				}

			} else {
				if (driver.getTitle().toUpperCase().contains("PANORAMA")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
							"Panorama related page opened");
					driver.navigate().to(testData.get("URL"));
					waitUntilObjectVisible(driver, panoramaHeader, 120);
					if (verifyObjectDisplayed(panoramaHeader)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
								"Navigated Back to homepage");
					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
								"Error in getting back to homepage");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Panorama related page not opened");
				}
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.FAIL,
					"Panorama block is not present on homepage");
		}

	}

	protected By resortLogoIpad = By.xpath("//img[@id='header-logo' and @class='max-width hide-xs']");
	protected By destinationHamburger = By.xpath("(//div[contains(@class,'wdHeader')]//span)[2]");

	public void openHamburgerMenu() {

		waitUntilObjectVisible(driver, destinationHamburger, 120);
		if (verifyObjectDisplayed(destinationHamburger)) {

			try {
				driver.findElement(destinationHamburger).click();
				tcConfig.updateTestReporter("DestinationsHomePage_Mobile", "openHamburgerMenu", Status.PASS,
						"Hamburger is displayed");
			} catch (Exception ex) {
				tcConfig.updateTestReporter("DestinationsHomePage_Mobile", "openHamburgerMenu", Status.FAIL,
						"Hamburger clicking is failed due to " + ex.getMessage());

			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage_Mobile", "openHamburgerMenu", Status.FAIL,
					"Hamburger not visible ");
		}
	}

	public void searchField() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(searchContainer)) {

			// fieldDataEnter(searchContainer, testData.get("strSearch"));

			driver.findElement(searchContainer).click();
			driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.PASS, "Search Keyword Entered");

			clickElementBy(searchButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search container");
		}
	}

	public void bookYourVacationValidations() {

		if (verifyObjectDisplayed(vacationTab)) {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
					"Vacation Tab present");

			if (((RemoteWebDriver) driver).getCapabilities().getPlatform().name().equalsIgnoreCase("ios")) {
				switchToContext(driver, "VISUAL");
				Map<String, Object> params = new HashMap<>();
				params.put("content", "Book Your Vacation");
				if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPhone")) {
					params.put("index", 2);
				} else {
					params.put("index", 1);
				}
				params.put("threshold", 80);
				((RemoteWebDriver) driver).executeScript("mobile:text:select", params);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				switchToContext(driver, "WEBVIEW");
			} else {
				driver.findElement(vacationTab).click();
				// driver.findElement("//div[@class='wyn-top-nav
				// wyn-top-nav--divider']//a[contains(text(),'Book Your
				// Vacation')]").click();
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * TouchAction touchAction = new
			 * TouchAction((PerformsTouchActions)driver);
			 * touchAction.tap((TapOptions) driver.findElement(vacationTab));
			 * //driver.findElement(vacationTab).click();
			 * 
			 * //
			 * System.out.println(driver.findElement(vacationTab).getLocation())
			 * ; // driver.executeScript('arguments[0].click();',we);
			 * 
			 * Map<String, Object> params = new HashMap<>();
			 * params.put("location", "20%,27%,20%,65%"); Object res =
			 * ((RemoteWebDriver) driver).executeScript("mobile:touch:tap",
			 * params);
			 * 
			 * Actions builder = new Actions(driver);
			 * builder.moveToElement(driver.findElement(vacationTab), 20,
			 * 513).click().build().perform();
			 * 
			 * 
			 * driver.findElement(vacationTab).sendKeys(Keys.ENTER);
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * action.clickAndHold(driver.findElement(vacationTab)).build().
			 * perform(); Actions action=new Actions(driver);
			 * action.doubleClick(driver.findElement(vacationTab));
			 * 
			 * 
			 * //longPress(driver.findElement(vacationTab)).moveTo(vacationTab).
			 * release(). perform(); TouchAction action = new
			 * TouchAction((PerformsTouchActions) driver); int
			 * x=driver.findElement(vacationTab).getLocation().getX(); int
			 * y=driver.findElement(vacationTab).getLocation().getY();
			 * 
			 * int h=driver.findElement(vacationTab).getSize().getHeight(); int
			 * w=driver.findElement(vacationTab).getSize().getWidth();
			 * 
			 * WebElement dot=driver.findElement(vacationTab);
			 * 
			 * 
			 * action.tap(x+(w/2), y+(h/2)).perform();
			 * //action.contextClick(driver.findElement(vacationTab)).build().
			 * perform();
			 */ List<WebElement> listTab = driver.findElements(vacationSubMenu);

			if (listTab.size() == 3) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2." + listTab.get(1).getText()
								+ " 3." + listTab.get(2).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(hotelsAndResortsLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Hotel Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Hotel and Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Hotel Tab not present");
				}

				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

				//
				if (verifyElementDisplayed(listTab.get(2))) {
					clickElementWb(listTab.get(2));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}

			} else if (listTab.size() == 2) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2."
								+ listTab.get(1).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						try {
							driver.switchTo().alert().accept();
						} catch (Exception e) {
							System.out.println("Alert Not Present");
						}

						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}
				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

			} else if (listTab.size() == 0) {
				if (verifyObjectDisplayed(resortLogo) || verifyObjectDisplayed(resortLogoIpad)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Book Your Resort Associated Hotel site is navigated");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Failed in navigating to Wyndham Resort site");
				}

				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPhone")) {
					openHamburgerMenu();
				}

				if (verifyObjectDisplayed(vacationTab)) {

					// driver.findElement(vacationTab).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Navigated back to homepage");
				} else {

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Homepage Navigation Failed");

				}

			} else {

				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
						"Sub Booking Menu Not Present");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
					"Vacation Tab is not present");
		}

	}

	public void homepageRCIExchange() {
		String URL = driver.getCurrentUrl();
		waitUntilElementVisibleBy(driver, RCIExchange, 120);

		scrollDownForElementJSBy(RCIExchange);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(100);

		if (verifyObjectDisplayed(RCIExchange)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
					"RCI Exchange Block Present on the homepage");

			clickIOSElement("Learn More", 1, rciReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (driver.getTitle().toUpperCase().contains("RCI")) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
						"RCI Exchange related page opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
						"RCI Exchange related page not opened");
			}

			driver.get(URL);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, RCIExchange, 120);
			if (verifyObjectDisplayed(RCIExchange)) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
						"Navigated Back to homepage");
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
						"Error in getting back to homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
					"RCI Exchange block is not present on homepage");
		}

	}

	public void homepageOurInvestors() {
		String URL = driver.getCurrentUrl();
		waitUntilElementVisibleBy(driver, ourInvestorsHeader, 120);

		scrollDownForElementJSBy(ourInvestorsHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		// scrollUpByPixel(50);
		scrollDownByPixel(100);

		if (verifyObjectDisplayed(ourInvestorsHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
					"Our Investors Block Present on the homepage");

			clickIOSElement("Learn More", 1, ourInvestorslearnMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (driver.getTitle().toUpperCase().contains("INVESTOR")) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
						"Our Investors related page opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
						"Our Investors related page not opened");
			}
			driver.get(URL);
			if (verifyObjectDisplayed(ourInvestorsHeader)) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
						"Navigated Back to homepage");
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
						"Error in getting back to homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
					"Our Investors block is not present on homepage");
		}

	}
}
