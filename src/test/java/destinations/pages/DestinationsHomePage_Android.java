package destinations.pages;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class DestinationsHomePage_Android extends DestinationsHomePage {

	public static final Logger log = Logger.getLogger(DestinationsHomePage_Android.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public DestinationsHomePage_Android(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		resortLogo = By.xpath("//img[@id='header-logo' and @class='max-width hide-gt-xs']");
		ourInvestorsHeader = By.xpath("//h2[contains(.,'Our Investors')]");
		rciReadMore = By.xpath(
				"//h2[contains(.,'Open up your vacation')]/ancestor::div[@class='column wyn-content-block__content']//a");
		clubWyndhamLogo = By.xpath("//img[@class='mobile-logo' and @alt='Wyndham']");
		worldMarkWyndhamLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
		margaritavilleLogo = By.xpath("//img[@alt='Margaritaville Logo']");
		shellVacationLogo = By.xpath("//img[@alt='Shell Vacations Club Logo']");
		searchContainer = By.xpath("//div[contains(@class,'wdHeader mobileNav')]//input[contains(@class,'searchQuery')]");
		searchButton = By.xpath("(//div[@class='input-group-button']//input[contains(@class,'siteSearch')])[2]");
		

	}

	@Override
	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		fieldDataEnter(searchContainer, searchValue);
		clickElementBy(searchButton);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}

	/*
	 * protected By destinationHamburger =
	 * By.xpath("//button[contains(@class,'hamburger')]");
	 * 
	 * @Override public void openHamburgerMenu() {
	 * 
	 * waitUntilObjectVisible(driver, destinationHamburger, 120); if
	 * (verifyObjectDisplayed(destinationHamburger)) {
	 * 
	 * try { driver.findElement(destinationHamburger).click();
	 * tcConfig.updateTestReporter("DestinationsHomePage_Mobile",
	 * "openHamburgerMenu", Status.PASS, "Hamburger is displayed"); } catch
	 * (Exception ex) {
	 * tcConfig.updateTestReporter("DestinationsHomePage_Mobile",
	 * "openHamburgerMenu", Status.FAIL, "Hamburger clicking is failed due to "
	 * + ex.getMessage());
	 * 
	 * }
	 * 
	 * } else { tcConfig.updateTestReporter("DestinationsHomePage_Mobile",
	 * "openHamburgerMenu", Status.FAIL, "Hamburger not visible "); } }
	 */

	@Override
	public void homepageLatestNews() {
		waitUntilElementVisibleBy(driver, latestNews, 120);

		scrollDownForElementJSBy(latestNews);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(latestNews)) {
			getElementInView(latestNews);
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
					"Latest News Block Present on the homepage");

			List<WebElement> newsArticlesHeader = driver.findElements(labelLatestNewsHeader);
			getElementInView(buttonViewMore);
			clickElementBy(buttonViewMore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(getObject(buttonViewLess).isEnabled(), "View Less button is present");
			List<WebElement> allNewsArticlesHeader = driver.findElements(labelAllLatestNewsHeader);
			if (allNewsArticlesHeader.size() > newsArticlesHeader.size()) {

				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
						"View more button is clicked in Latest News Block on the homepage");

				for (int i = 0; i < allNewsArticlesHeader.size(); i++) {
					List<WebElement> allNewsArticlesHeader1 = driver.findElements(labelAllLatestNewsHeader);
					getElementInView(latestNews);
					String strNewsHeader = allNewsArticlesHeader1.get(i).getText().toUpperCase();
					clickElementBy(allNewsArticlesHeader1.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// Assert.assertTrue(verifyObjectDisplayed(breadcrumbsPressRelease),
					// "Breadcurmbs is not present in the navigated page");
					Assert.assertTrue(
							driver.findElement(clickedArticleHeader).getText().toUpperCase().contains(strNewsHeader),
							"Header not matched");
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.PASS,
							"Successfully navigate to " + (i + 1) + " news article");
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(buttonViewMore);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
						"View more button is unable to click in Latest News Block on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageLatestNews", Status.FAIL,
					"Latest News block is not present on homepage");
		}
	}

	@Override
	public void validateBrand() {
		String strCurrentURL = driver.getCurrentUrl().trim();
		assertTrue(verifyObjectDisplayed(brandHeader), "Our Brand Header is not present");
		tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand header present");

		List<WebElement> brandImg = getList(brandImage);
		List<WebElement> brandNme = getList(brandName);
		List<WebElement> brandDes = getList(brandDescription);
		List<WebElement> brandCTA = getList(learnMoreCTA);

		if (brandImg.size() == brandNme.size()) {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS, "Our Brand Name present");
			if (brandImg.size() == brandDes.size()) {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
						"Our Brand Description present");
				if (brandImg.size() == brandCTA.size()) {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.PASS,
							"Our Brand Learn More CTA present");
				} else {
					tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
							"Our Brand Learn More CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL,
						"Our Brand Description present");
			}
		} else {
			tcConfig.updateTestReporter("DestinationHomePage", "validateBrand", Status.FAIL, "Our Brand Name present");
		}

		for (int i = 0; i < brandImg.size(); i++) {
			brandNme = getList(brandName);
			brandCTA = getList(learnMoreCTA);
			String brandName = getElementText(brandNme.get(i));
			getElementInView(brandCTA.get(i));
			clickElementBy(brandCTA.get(i));
			// pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				// newTabNavigations(allTabs);
				waitForSometime("5");
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				waitForSometime("2");
				navigateToURL(strCurrentURL);
				pageCheck();

			} else {
				switch (brandName) {
				case "Club Wyndham":
					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Club Wyndham page navigation is Successful");
					break;
				case "WorldMark by Wyndham":
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilObjectVisible(driver, worldMarkWyndhamLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(worldMarkWyndhamLogo), "Worldmark Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Worldmark page navigation is Successful");
					break;
				case "Margaritaville Vacation Club":
					waitUntilObjectVisible(driver, margaritavilleLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(margaritavilleLogo),
							"Margaritaville Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Margaritaville page navigation is Successful");
					break;
				case "Shell Vacation Club":
					waitUntilObjectVisible(driver, shellVacationLogo, 240);
					Assert.assertTrue(verifyObjectDisplayed(shellVacationLogo), "Shell Vacation Logo is not Displayed");
					tcConfig.updateTestReporter("DestinationsHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							"Shell Vacation page navigation is Successful");
					break;
				}
				waitForSometime("2");
				navigateToURL(strCurrentURL);
				
				pageCheck();
			}

		}
	}

	public void searchField() {

		if (verifyObjectDisplayed(searchContainer)) {
			driver.findElement(searchContainer).click();
			driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.PASS, "Search Keyword Entered");

			clickElementBy(searchButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search container");
		}
	}

	@Override
	public void homepagePanorama() {
		waitUntilElementVisibleBy(driver, panoramaHeader, 120);

		scrollDownForElementJSBy(panoramaHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollUpByPixel(100);

		if (verifyObjectDisplayed(panoramaHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
					"Panorama Block Present on the homepage");

			clickElementBy(panoramaReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			if (tabs2.size() == 2)

			{
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getTitle().toUpperCase().contains("PANORAMA")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
							"Panorama related page opened");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.switchTo().window(tabs2.get(0));
					waitUntilObjectVisible(driver, panoramaHeader, 120);
					if (verifyObjectDisplayed(panoramaHeader)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
								"Navigated Back to homepage");
					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
								"Error in getting back to homepage");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Panorama related page not opened");
				}

			} else {
				if (driver.getTitle().toUpperCase().contains("PANORAMA")) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.PASS,
							"Panorama related page opened");
					driver.navigate().to(testData.get("URL"));
					waitUntilObjectVisible(driver, panoramaHeader, 120);
					if (verifyObjectDisplayed(panoramaHeader)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
								"Navigated Back to homepage");
					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
								"Error in getting back to homepage");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
							"Panorama related page not opened");
				}
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepagePanorama", Status.FAIL,
					"Panorama block is not present on homepage");
		}

	}

	public void bookYourVacationValidations() {
		String strCurrentURL = driver.getCurrentUrl();
		if (verifyObjectDisplayed(vacationTab)) {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
					"Vacation Tab present");

			driver.findElement(vacationTab).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> listTab = driver.findElements(vacationSubMenu);

			if (listTab.size() == 3) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2." + listTab.get(1).getText()
								+ " 3." + listTab.get(2).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					/*
					 * ArrayList<String> tabs2 = new
					 * ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(tabs2.get(1));
					 */
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(hotelsAndResortsLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Hotel Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * 
						 * driver.switchTo().window(tabs2.get(0));
						 */
						driver.get(strCurrentURL);
						openHamburgerMenu();

						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Hotel and Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Hotel Tab not present");
				}

				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					// ArrayList<String> tabs2 = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * 
						 * driver.switchTo().window(tabs2.get(0));
						 */

						driver.get(strCurrentURL);
						openHamburgerMenu();
						waitUntilObjectVisible(driver, vacationTab, 120);
						if (verifyObjectDisplayed(vacationTab)) {

							// driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

				//
				if (verifyElementDisplayed(listTab.get(2))) {
					clickElementWb(listTab.get(2));
					// ArrayList<String> tabs2 = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * 
						 * driver.switchTo().window(tabs2.get(0));
						 */
						driver.get(strCurrentURL);
						openHamburgerMenu();
						waitUntilObjectVisible(driver, vacationTab, 120);

						if (verifyObjectDisplayed(vacationTab)) {

							driver.findElement(vacationTab).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.PASS, "Navigated back to homepage");
						} else {

							tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations",
									Status.FAIL, "Homepage Navigation Failed");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}

			} else if (listTab.size() == 2) {
				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
						"Sub Booking Menu Present as: 1." + listTab.get(0).getText() + " 2."
								+ listTab.get(1).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(listTab.get(0))) {
					clickElementWb(listTab.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// ArrayList<String> tabs2 = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));

					if (browserName.equalsIgnoreCase("IE")) {
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						try {
							driver.switchTo().alert().accept();
						} catch (Exception e) {
							System.out.println("Alert Not Present");
						}

						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (verifyObjectDisplayed(resortLogo)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Resort Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Resort site");
					}

					// driver.close();
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					//
					// driver.switchTo().window(tabs2.get(0));
					driver.get(strCurrentURL);
					openHamburgerMenu();
					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Resort Tab not present");
				}
				if (verifyElementDisplayed(listTab.get(1))) {
					clickElementWb(listTab.get(1));
					// ArrayList<String> tabs2 = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (verifyObjectDisplayed(vacationRentalsLogo) || verifyObjectDisplayed(popUp)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Book Your Vacation Rentals Associated Hotel site is navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Failed in navigating to Wyndham Vacation Rentals site");
					}

					/*
					 * driver.close();
					 * waitForSometime(tcConfig.getConfig().get("MedWait"));
					 * 
					 * driver.switchTo().window(tabs2.get(0));
					 */
					driver.get(strCurrentURL);
					openHamburgerMenu();

					waitUntilObjectVisible(driver, vacationTab, 120);

					if (verifyObjectDisplayed(vacationTab)) {

						// driver.findElement(vacationTab).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
								"Navigated back to homepage");
					} else {

						tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
								"Homepage Navigation Failed");

					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Book Your Vacation Rentals Tab not present");
				}

			} else if (listTab.size() == 0) {
				// ArrayList<String> tabs2 = new
				// ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (verifyObjectDisplayed(resortLogo)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Book Your Resort Associated Hotel site is navigated");
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Failed in navigating to Wyndham Resort site");
				}

				driver.get(strCurrentURL);
				openHamburgerMenu();

				waitUntilObjectVisible(driver, vacationTab, 120);

				if (verifyObjectDisplayed(vacationTab)) {

					// driver.findElement(vacationTab).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.PASS,
							"Navigated back to homepage");
				} else {

					tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
							"Homepage Navigation Failed");

				}

			} else {

				tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
						"Sub Booking Menu Not Present");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "bookYourVacationValidations", Status.FAIL,
					"Vacation Tab is not present");
		}

	}

	public void homepageCarouselFunctionality() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollDownForElementJSBy(exploreWyndham);

		waitUntilObjectVisible(driver, exploreWyndham, 120);

		scrollUpByPixel(100);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(exploreWyndham)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
					"Explore Wyndham Destinations Content Block present in homepage");

			List<WebElement> containerList = driver.findElements(container);

			if (containerList.size() < 5) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
						"Explore Wyndham Destinations Content Block present but there are less than 5 cards so no Carousel");
			} else {
				waitUntilObjectVisible(driver, carouselArrowNext, 120);
				if (verifyObjectDisplayed(carouselArrowNext)) {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
							"Caraousel Next Arrow Present");

					for (int i = 0; i < 6; i++) {

						if (verifyObjectDisplayed(carouselArrowNext)) {
							driver.findElement(carouselArrowNext).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else if (verifyObjectDisplayed(carouselArrowPrev)) {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.PASS, "No more containers, caraousel Prev Arrow Present");
							break;
						} else {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.FAIL, "No more containers, caraousel Prev Arrow not Present");
							break;
						}
					}

					if (verifyObjectDisplayed(carouselArrowPrev)) {
						for (int j = 0; j < 6; j++) {
							if (verifyObjectDisplayed(carouselArrowPrev)) {
								driver.findElement(carouselArrowPrev).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));

							} else if (verifyObjectDisplayed(carouselArrowNext)) {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.PASS, "Navigated Back to the first Container");
								break;
							} else {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.FAIL, "Failed to navigate back to first container");
								break;
							}
						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.FAIL, "Failed to navigate back to first container");
					}

					// List<WebElement> containerList =
					// driver.findElements(container);
					List<WebElement> containerTitleList = driver.findElements(containerTitle);
					List<WebElement> containerImageList = driver.findElements(containerImage);
					List<WebElement> containerLocationList = driver.findElements(containerLocation);

					int intContainer = containerList.size();
					String strfirstTitle = containerTitleList.get(0).getText().toUpperCase();
					String strfirstTitlefinal = strfirstTitle.split(" ")[0];
					System.out.println(strfirstTitlefinal);
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.PASS,
							"Total Number of Container Present: " + intContainer);

					if (verifyElementDisplayed(containerList.get(0))
							&& verifyElementDisplayed(containerTitleList.get(0))
							&& verifyElementDisplayed(containerImageList.get(0))) {

						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.PASS,
								"First Container image present, with Title: " + containerTitleList.get(0).getText()
										+ " and Location: " + containerLocationList.get(0).getText());

						clickElementJS(containerImageList.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(cardLearnMore)) {

							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.PASS, "First Container Card Opened and Learn More button present");

							clickElementBy(cardLearnMore);
							waitForSometime(tcConfig.getConfig().get("LongWait"));

							ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
							driver.switchTo().window(tabs2.get(1));
							System.out.println(driver.getTitle().toUpperCase());
							By pageTitle = By.xpath("//title[contains(.,'" + strfirstTitle + "')]");
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.getTitle().toUpperCase().contains(strfirstTitlefinal)) {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.PASS, "Associated Page Opened");
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.close();
								waitForSometime(tcConfig.getConfig().get("MedWait"));

								driver.switchTo().window(tabs2.get(0));
								waitUntilObjectVisible(driver, cardClose, 120);
								if (verifyObjectDisplayed(cardClose)) {
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.PASS, "Navigated Back to homepage");
									scrollUpByPixel(100);
									clickElementBy(cardClose);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.PASS, "Container Card Closed");

								} else {
									tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
											Status.FAIL, "Container Card could not be Closed");
								}

							} else {
								tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
										Status.FAIL, "Unable to open associated page");
							}

						} else {
							tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
									Status.FAIL, "Container Card could not be Opened");

						}

					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality",
								Status.FAIL, "Error in getting First Container details");
					}

				} else {
					tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.FAIL,
							"Carousel is not present");
				}

			}
		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageCarouselFunctionality", Status.FAIL,
					"Explore Wyndham Destinations Content Block is not present in homepage");
		}

	}

	public void homepageWeBelieveVal() {
		List<WebElement> weBelieveDataList = driver.findElements(weBelieveData);
		String strDestinations = weBelieveDataList.get(0).getText();
		String strCountries = weBelieveDataList.get(1).getText();
		String strAssociates = weBelieveDataList.get(2).getText();

		waitUntilElementVisibleWb(driver, weBelieveDataList.get(0), "120");

		scrollDownForElementJSWb(weBelieveDataList.get(0));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(weBelieveDataList.get(0))) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageWeBelieveVal", Status.PASS,
					"Wyndham Destination We Believe Block Present on Homepage with Destinations: " + strDestinations
							+ " Countries: " + strCountries + " and Associate: " + strAssociates);

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageWeBelieveVal", Status.FAIL,
					"Error in getting we believe data");
		}

	}

	public void homepageRCIExchange() {
		waitUntilElementVisibleBy(driver, RCIExchange, 120);
		String currentURL = driver.getCurrentUrl();
		scrollDownForElementJSBy(RCIExchange);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(100);

		if (verifyObjectDisplayed(RCIExchange)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
					"RCI Exchange Block Present on the homepage");

			clickElementBy(rciReadMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			// driver.switchTo().window(tabs2.get(1));

			// if (browserName.equalsIgnoreCase("CHROME") ||
			// browserName.equalsIgnoreCase("firefox")||
			// browserName.equalsIgnoreCase("EDGE")) {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (driver.getCurrentUrl().toUpperCase().contains("RCI")) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
						"RCI Exchange related page opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
						"RCI Exchange related page not opened");
			}

			driver.get(currentURL);
			scrollDownForElementJSBy(RCIExchange);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(RCIExchange)) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.PASS,
						"Navigated Back to homepage");
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
						"Error in getting back to homepage");
			}

			// }

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRCIExchange", Status.FAIL,
					"RCI Exchange block is not present on homepage");
		}

	}

	public void homepageRentals() {
		waitUntilElementVisibleBy(driver, wyndhamVacationrentals, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(wyndhamVacationrentals)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRentals", Status.PASS,
					"Vacation Rentals Block Present on the homepage");

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageRentals", Status.FAIL,
					"Vacation Rentals block is not present on homepage");
		}
	}

	public void homepageOurInvestors() {
		waitUntilElementVisibleBy(driver, ourInvestorsHeader, 120);
		String CurrentUrl = driver.getCurrentUrl();
		scrollDownForElementJSBy(ourInvestorsHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollUpByPixel(100);

		if (verifyObjectDisplayed(ourInvestorsHeader)) {

			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
					"Our Investors Block Present on the homepage");

			clickElementBy(ourInvestorslearnMore);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			// driver.switchTo().window(tabs2.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// if (browserName.equalsIgnoreCase("CHROME") ||
			// browserName.equalsIgnoreCase("firefox")||
			// browserName.equalsIgnoreCase("edge")) {
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.getCurrentUrl().toUpperCase().contains("INVESTOR")) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
						"Our Investors related page opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
						"Our Investors related page not opened");
			}
			driver.get(CurrentUrl);
			waitUntilObjectVisible(driver, ourInvestorsHeader, 120);
			scrollDownForElementJSBy(ourInvestorsHeader);
			if (verifyObjectDisplayed(ourInvestorsHeader)) {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.PASS,
						"Navigated Back to homepage");
			} else {
				tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
						"Error in getting back to homepage");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsHomePage", "homepageOurInvestors", Status.FAIL,
					"Our Investors block is not present on homepage");
		}

	}

}