package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class DestinationsLoginPage_IOS extends DestinationsLoginPage {

	public static final Logger log = Logger.getLogger(DestinationsLoginPage_IOS.class);

	public DestinationsLoginPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
