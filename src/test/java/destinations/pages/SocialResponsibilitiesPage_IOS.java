package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class SocialResponsibilitiesPage_IOS extends SocialResponsibilitiesPage {

	public static final Logger log = Logger.getLogger(SocialResponsibilitiesPage_IOS.class);

	public SocialResponsibilitiesPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		applicationForDonationLink = By.xpath("//ul//a[contains(.,'Application for Donations')]");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

}