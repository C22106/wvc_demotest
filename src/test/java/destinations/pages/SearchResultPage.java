package destinations.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SearchResultPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(SearchResultPage.class);

	public SearchResultPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * login in to salepoint app with given credentials
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By searchPage = By.xpath("//span[contains(.,'Search Results')]");
	protected By noResults = By
			.xpath("//span[@class='wyn-type-title-1' and contains(.,'Currently there is not a match')]");
	protected By searchResultsTitle = By.xpath("//span[@class='wyn-type-title-1' and contains(.,'results for')]");
	protected By resultsName = By.xpath("//a[@class='wyn-content-article-excerpt__title']");
	protected By paginationTab = By.xpath("//a[@class='page-link']");
	protected By sortSelect = By.xpath("//select[@aria-label='Sort by']"); // text //Date (oldest first)
	protected By dateCheck = By.xpath("//span[@class='wyn-type-body-2 wyn-type--bold wyn-l-margin-small--bottom']");
	protected By resultClickHeader = By.xpath("//div[@class='title-1']");

	public void searchFunctionality() {
		String newsHeader = "";

		waitUntilObjectVisible(driver, searchPage, 120);

		if (verifyObjectDisplayed(searchPage)) {
			tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
					"Navigated to search results page");

			if (verifyObjectDisplayed(sortSelect)) {
				String strTotalResult = driver.findElement(searchResultsTitle).getText();
				String[] strResult = strTotalResult.split("of");
				strTotalResult = strResult[strResult.length - 1];
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						"Number of Results: " + strTotalResult + " " + testData.get("strSearch"));

				if (verifyObjectDisplayed(paginationTab)) {
					List<WebElement> paginationsTab = driver.findElements(paginationTab);
					tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
							"Pagination Available");

					clickElementWb(paginationsTab.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult2 = driver.findElement(searchResultsTitle).getText();
					if (strTotalResult2.contains("11") && verifyObjectDisplayed(resultsName)) {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"Navigated to 2nd page and Results are present");

					} else {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"2nd page navigation failed");
					}

					clickElementWb(paginationsTab.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult1 = driver.findElement(searchResultsTitle).getText();
					if (strTotalResult1.contains("10") && verifyObjectDisplayed(resultsName)) {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"Navigated to 1st Page");

					} else {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"1st page navigation failed");
					}

				} else {

					tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
							"The keyword you searched does not contain more than 10 results hence pagination not available");
				}

				if (verifyObjectDisplayed(sortSelect)) {
					List<WebElement> strDate = driver.findElements(dateCheck);
					String strFirstDate = strDate.get(0).getText();
					tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
							"Date is sorted by default by Newest First");

					new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (oldest first)");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> strSortedDate = driver.findElements(dateCheck);
					String strSortedFirstDate = strSortedDate.get(0).getText();

					if (!strFirstDate.equals(strSortedFirstDate)) {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"Date is sorted in Oldest First Manner");
					} else {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.FAIL,
								"Sorting Failed");
					}

				} else {
					tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.FAIL,
							"Sorting Option Not available");
				}
				new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (newest first)");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> strResultHeader = driver.findElements(resultsName);
				String strfirstHeader = strResultHeader.get(0).getText();
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						"First Element Header Title is: " + strfirstHeader);

				clickElementWb(strResultHeader.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
				if (tabs.size() == 2) {
					driver.switchTo().window(tabs.get(1));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					newsHeader = driver.findElement(resultClickHeader).getText();
					if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"Successfully Navigated to associated page with header: " + newsHeader);
						driver.close();
						driver.switchTo().window(tabs.get(0));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.FAIL,
								"Could not navigate to associated page");
					}
				} else {
					newsHeader = driver.findElement(By.xpath("//div[@class='column']//h1")).getText();
					if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
								"Successfully Navigated to associated page with header: " + newsHeader);
					} else {
						tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.FAIL,
								"Could not navigate to associated page");
					}
					driver.navigate().back();
				}

			} else if (verifyObjectDisplayed(noResults)) {
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						driver.findElement(noResults).getText() + " " + testData.get("strSearch")
								+ " ,Please search with other keyword");
			}

		} else {
			tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.FAIL,
					"Search Results not displayed");
		}

	}

}
