package destinations.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GlobalNavigationPage extends DestinationsBasePage {

	public static final Logger log = Logger.getLogger(GlobalNavigationPage.class);

	public GlobalNavigationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By headerLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']//img[@class='is-desktop']");
	protected By headerClubs = By.xpath("//div[@class='header']//a[contains(.,'Clubs')]"); // club
	protected By headerExchanges = By.xpath("//div[@class='header']//a[contains(.,'Exchanges')]"); // rci
	protected By headerRentals = By.xpath("(//div[@class='header']//li//a[contains(.,'Book Your Vacation')])[1]"); // rentals
	protected By headerInvestors = By.xpath("//div[@class='header']//li//a[contains(.,'Investors')]"); // investors
	protected By headerCareers = By.xpath("//div[@class='header']//li//a[contains(.,'Careers')]"); // careers
	protected By headerContactUs = By.xpath("//div[@class='header']//li//a[contains(.,'Contact Us')]");
	protected By headerMainMenu = By.xpath("//li[@class='has-submenu']/a");
	protected By headerSubMenu = By.xpath("//li[@class='has-submenu is-active']//a[@class='wyn-fly-out__headline']");
	protected By modernSlavery = By.xpath("//h1[contains(.,'Modern')]");
	protected By modernSlaveryHdr = By.xpath("//li/a[contains(.,'Modern Slavery')]");

	protected By contactUsTab = By.xpath("//div[contains(@class,'preMenu')]//a[contains(text(),'Contact Us')]");
	protected By contactUsHeader = By.xpath("//div[contains(text(),'Contact Us')]");
	protected By contactUsBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Contact Us')]");
	protected By contactUsContent = By.xpath("//h5");
	protected By wynHotelsResortLink = By.xpath("//h5[contains(.,'Wyndham Hotels & Resorts')]");
	protected By wynHotelsResortContact = By
			.xpath("//h5[contains(.,'Wyndham Hotels & Resorts')]/..//a[contains(text(),'-')]");
	protected By wynRewardsLink = By.xpath("//p/a[contains(.,'WyndhamRewards.com')]");
	protected By contactUsMainLinks = By.xpath("//div[contains(@class,'body-1')]/p/a[contains(@href,'https')]");
	protected By firstLinkSection = By.xpath("//h2[contains(.,'Contact Wyndham Destinations')]");
	protected By secondLinkSection = By.xpath("//a[@id='WDNI']/parent::h2");
	protected By thirdLinkSection = By.xpath("//h3[contains(.,'Contact Wyndham Destinations')]");
	protected By contactUsSection = By.xpath("//h5");

	public void globalNavValidations() {

		if (verifyObjectDisplayed(headerLogo)) {

			tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
					"Wyndham Logo is present in the Global Nav");

			if (verifyObjectDisplayed(headerClubs)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Clubs tab present in the global nav");

				clickElementBy(headerClubs);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getTitle().toUpperCase().contains("CLUB WYNDHAM")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Club site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham club site");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Clubs tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerExchanges)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Exchanges tab present in the global nav");

				clickElementBy(headerExchanges);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				if (driver.getTitle().toUpperCase().contains("RCI")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Exchanges: RCI site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Exchanges site");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Exchanges tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerRentals)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Rentals tab present in the global nav");

				clickElementBy(headerRentals);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				if (driver.getTitle().toUpperCase().contains("EXTRA HOLIDAYS")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Rentals site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Rentals site");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Rentals tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerInvestors)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Investors tab present in the global nav");

				clickElementBy(headerInvestors);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				if (driver.getTitle().toUpperCase().contains("INVESTOR")) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Wyndham Investors site opened");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Investors site");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Investors tab not present in the global nav");
			}

			if (verifyObjectDisplayed(headerCareers)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Careers tab present in the global nav");

				clickElementBy(headerCareers);
				/*
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * ArrayList<String> tabs2 = new
				 * ArrayList<String>(driver.getWindowHandles());
				 * driver.switchTo().window(tabs2.get(1));
				 */
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				// waitUntilElementVisibleBy(driver, locator, timeOutInSeconds);
				if (browserName.equalsIgnoreCase("IE")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				if (driver.getTitle().toUpperCase().contains("CAREERS")) {
					tcConfig.updateTestReporterWithoutScreenshot("GlobalNavigationPage", "globalNavValidations",
							Status.PASS, "Wyndham Careers site opened");
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Failed to open Wyndham Careers site");
				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Careers tab not present in the global nav");
			}

			if (verifyObjectDisplayed(contactUsTab)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						"Contact Us Tab present in global navigation");

				clickElementBy(contactUsTab);
				waitUntilElementVisibleBy(driver, contactUsHeader, 120);
				if (verifyObjectDisplayed(contactUsHeader)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Navigated To Contact Us Page and Header is present");

					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Contact Us Page navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Contact Us tab not present in the global nav ");
			}

			waitUntilElementVisibleBy(driver, headerMainMenu, 120);
			List<WebElement> mainMenuList = driver.findElements(headerMainMenu);

			for (int i = 0; i < 3; i++) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
						mainMenuList.get(i).getText() + " tab is present in the global nav");
				clickElementJS(mainMenuList.get(i));

				List<WebElement> subMenuList = driver.findElements(headerSubMenu);
				for (int j = 0; j < subMenuList.size(); j++) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							subMenuList.get(j).getText() + " is present in the " + mainMenuList.get(i).getText()
									+ "  menu");

				}

			}

			if (verifyObjectDisplayed(modernSlaveryHdr)) {
				clickElementJS(modernSlaveryHdr);
				waitUntilElementVisibleBy(driver, modernSlavery, 120);
				if (verifyObjectDisplayed(modernSlavery)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.PASS,
							"Navigated to Modern Slavery Statement Page and Header is present");
					// waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
							"Unable to navigate to Modern Slavery Statement page");
				}
			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
						"Modern Slavery not present in Global Nav");
			}

		} else {
			tcConfig.updateTestReporter("GlobalNavigationPage", "globalNavValidations", Status.FAIL,
					"Wyndham Logo is not present in the Global Nav");
		}

	}

	public void contactUsValidations() {
		waitUntilElementVisibleBy(driver, contactUsTab, 120);

		if (verifyObjectDisplayed(contactUsTab)) {
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
					"Contact Us Tab present in global navigation");

			clickElementBy(contactUsTab);
			String URL = driver.getCurrentUrl();
			waitUntilElementVisibleBy(driver, contactUsHeader, 120);
			if (verifyObjectDisplayed(contactUsHeader)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
						"Navigated To Contact Us Page and Header is present");
				if (verifyObjectDisplayed(contactUsBreadCrumb)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Bread Crumb Navigation Present");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Bread Crumb Navigation not Present");
				}

				if (verifyObjectDisplayed(contactUsContent)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Content Present");

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Content not Present");
				}

				if (verifyObjectDisplayed(wynHotelsResortLink)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Wyndham Hotels & Resorts Link Present with contact: "
									+ driver.findElement(wynHotelsResortContact).getText());
					getElementInView(wynHotelsResortLink);

					if (verifyObjectDisplayed(wynRewardsLink)) {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								"Wyndham Rewards Link Present in contact us page");

						clickElementJSWithWait(wynRewardsLink);
						pageCheck();
						ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs1.get(1));

						if (driver.getTitle().toUpperCase().contains("JOIN")) {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
									"Wyndhm Rewards Link opened");
							// driver.navigate().back();
							driver.close();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().window(tabs1.get(0));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							waitUntilElementVisibleBy(driver, contactUsHeader, 120);

						} else {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
									"Wyndhm Rewards Link did not opened");
						}
					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Wyndhm Rewards Link not present");
					}

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Wyndhm Hotel & Resorts Link not present");
				}

				List<WebElement> mainSectionList = driver.findElements(contactUsSection);
				for (int i = 0; i < mainSectionList.size(); i++) {

					getElementInView(mainSectionList.get(i));

					if (verifyElementDisplayed(mainSectionList.get(i))) {

						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								getElementText(mainSectionList.get(i)) + " Section displayed");

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								mainSectionList.size() + " Section not displayed");
					}

				}

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
						"Could not navigate to Contact Us page");
			}

		} else {
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
					"Contact Us tab not present");
		}

	}

}
