package destinations.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class WorkForceDiversityPage_Android extends WorkForceDiversityPage {

	public static final Logger log = Logger.getLogger(WorkForceDiversityPage_Android.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public WorkForceDiversityPage_Android(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void workforceDiversityValidation() {
		if (verifyObjectDisplayed(workforceDiversityHeader)) {
			tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.PASS,
					"workforceDiversityHeader present");
			if (verifyObjectDisplayed(workforceDiversityContent)) {
				tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.PASS,
						"workforceDiversity content present");
			} else {
				tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.FAIL,
						"workforceDiversity content not present");
			}

			if (verifyObjectDisplayed(careerLink)) {

				String winHandleBefore = driver.getWindowHandle();

				tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.PASS,
						"Career Link present in content");
				driver.findElement(careerLink).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
				// System.out.println("No. of tabs: " + tabs.size());

				if (tabs.size() > 1) {
					for (String winHandle : driver.getWindowHandles()) {
						driver.switchTo().window(winHandle);
					}

					waitUntilElementVisibleBy(driver, careerHeader, 20);

					if (verifyObjectDisplayed(careerHeader)) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation",
								Status.PASS, "Career Page Navigation Successful and career header present");

					} else {
						tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation",
								Status.FAIL, "Career Page Navigation UnSuccessful");
					}
					driver.close();

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(winHandleBefore);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(wdLogo);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(destinationHeroImage)) {
						tcConfig.updateTestReporter("DestinationsHomePage", "homePageValidations", Status.PASS,
								"Destinations HoemPage Displayed, Home Banner Present");
					} else {
						tcConfig.updateTestReporter("DestinationsHomePage", "homePageValidations", Status.FAIL,
								"Destinations HoemPage Displayed, Home Banner Present");
					}
				} else {
					tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.FAIL,
							"New Tab Opening Failed");
				}

			} else {
				tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.FAIL,
						"Career Link not present");
			}

		} else {
			tcConfig.updateTestReporter("WorkForceDiversityPage", "workforceDiversityValidation", Status.FAIL,
					"workforceDiversityHeader not present");
		}

	}
}
