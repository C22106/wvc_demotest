package destinations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GlobalNavigationPage_IOS extends GlobalNavigationPage {

	public static final Logger log = Logger.getLogger(GlobalNavigationPage_IOS.class);

	public GlobalNavigationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		headerLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']//img[@class='is-mobile']");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	
	public void contactUsValidations() {
		waitUntilElementVisibleBy(driver, contactUsTab, 120);

		if (verifyObjectDisplayed(contactUsTab)) {
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
					"Contact Us Tab present in global navigation");

			clickElementBy(contactUsTab);

			getElementInView(wynHotelsResortLink);

			if (verifyObjectDisplayed(wynHotelsResortLink)) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
						"Wyndham Hotels & Resorts Link Present with contact: "
								+ driver.findElement(wynHotelsResortContact).getText());

				if (verifyObjectDisplayed(wynRewardsLink)) {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
							"Wyndham Rewards Link Present in contact us page");

					clickElementJSWithWait(wynRewardsLink);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					String winHandlesBefore = driver.getWindowHandle();

					for (String winHandle : driver.getWindowHandles()) {
						driver.switchTo().window(winHandle);

					}

					if (driver.getTitle().toUpperCase().contains("JOIN")) {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								"Wyndhm Rewards Link opened");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.close();
						driver.switchTo().window(winHandlesBefore);

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Wyndhm Rewards Link did not opened");
					}

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Wyndhm Hotel & Resorts Link not opened");
				}

				List<WebElement> mainSectionList = driver.findElements(contactUsSection);
				for (int i = 0; i < mainSectionList.size(); i++) {

					scrollDownForElementJSWb(mainSectionList.get(i));
					scrollUpByPixel(200);

					if (verifyElementDisplayed(mainSectionList.get(i))) {

						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								getElementText(mainSectionList.get(i)) + " Section displayed");

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								mainSectionList.size() + " Section not displayed");
					}

				}
				/*if (verifyObjectDisplayed(contactUsMainLinks)) {
					List<WebElement> mainLinksList = driver.findElements(contactUsMainLinks);

					if (verifyElementDisplayed(mainLinksList.get(0))) {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								mainLinksList.get(0).getText() + " link present");

						clickElementJS(mainLinksList.get(0));

						if (verifyObjectDisplayed(firstLinkSection)) {
							scrollUpByPixel(100);
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
									driver.findElement(firstLinkSection).getText() + " Section displayed");

						} else {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
									"Section not displayed");
						}

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Main Link not present");
					}

					if (verifyElementDisplayed(mainLinksList.get(1))) {
						scrollDownForElementJSWb(mainLinksList.get(1));
						scrollUpByPixel(150);

						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								mainLinksList.get(1).getText() + " link present");

						clickElementJS(mainLinksList.get(1));

						if (verifyObjectDisplayed(secondLinkSection)) {
							scrollUpByPixel(150);
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
									driver.findElement(secondLinkSection).getText() + " Section displayed");

						} else {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
									"Section not displayed");
						}
					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Main Link not present");
					}

					if (verifyElementDisplayed(mainLinksList.get(2))) {
						scrollDownForElementJSWb(mainLinksList.get(2));
						scrollUpByPixel(150);

						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
								mainLinksList.get(2).getText() + " link present");

						clickElementJS(mainLinksList.get(2));

						if (verifyObjectDisplayed(thirdLinkSection)) {
							scrollUpByPixel(150);
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.PASS,
									driver.findElement(thirdLinkSection).getText() + " Section displayed");

						} else {
							tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
									"Section not displayed");
						}

					} else {
						tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
								"Main Link not present");
					}

				} else {
					tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
							"Main Links not present");
				}*/
				navigateBack();

			} else {
				tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
						"Could not navigate to Contact Us page");
			}

		}else{
			tcConfig.updateTestReporter("GlobalNavigationPage", "contactUsValidations", Status.FAIL,
					"Contact Us Tab not present in global navigation");
		}
	}
}
