package destinations.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import destinations.pages.AboutWyndhamPage;
import destinations.pages.AboutWyndhamPage_IOS;
import destinations.pages.DestinationsHomePage;
import destinations.pages.DestinationsHomePage_IOS;
import destinations.pages.DestinationsLoginPage;
import destinations.pages.DestinationsLoginPage_IOS;
import destinations.pages.FooterLinksPage;
import destinations.pages.FooterLinksPage_IOS;
import destinations.pages.GlobalNavigationPage;
import destinations.pages.GlobalNavigationPage_IOS;
import destinations.pages.NewsAndMediaPage;
import destinations.pages.NewsAndMediaPage_IOS;
import destinations.pages.SearchResultPage;
import destinations.pages.SearchResultPage_IOS;
import destinations.pages.SocialResponsibilitiesPage;
import destinations.pages.SocialResponsibilitiesPage_IOS;

public class DestinationsScripts_MobileIOS extends TestBase {

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_001_WD_SearchFunctionality � Description: Destinations Search
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "destinations", "mobile", "ios", "regression" })

	public void TC_001_WD_SearchFunctionality(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage_IOS(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage_IOS(tcconfig);

		try {

			login.launchApplication();
			homePage.openHamburgerMenu();
			homePage.validateSearchFunctionality();
			homePage.searchResultsValidatins();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_003_WD_HomepageCarouselFunctionality� Description: Destinations
	 * homepage carousel Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Homepage", "" })

	public void TC_003_WD_HomepageBrandContentValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		DestinationsLoginPage login = new DestinationsLoginPage_IOS(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage_IOS(tcconfig);

		try {

			login.launchApplication();
			homePage.validateBrand();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "legal", "regression" })
	public void TC_007_WD_LegalLinksValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage_IOS(tcconfig);
		FooterLinksPage footer = new FooterLinksPage_IOS(tcconfig);

		try {

			login.launchApplication();
			footer.footerPrivacySection();
			footer.footerLinksValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}