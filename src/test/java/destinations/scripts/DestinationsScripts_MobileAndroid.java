package destinations.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import destinations.pages.AboutWyndhamPage;
import destinations.pages.AboutWyndhamPage_Android;
import destinations.pages.DestinationsHomePage;
import destinations.pages.DestinationsHomePage_Android;
import destinations.pages.DestinationsLoginPage;
import destinations.pages.DestinationsLoginPage_Android;
import destinations.pages.FooterLinksPage;
import destinations.pages.FooterLinksPage_Android;
import destinations.pages.GlobalNavigationPage;
import destinations.pages.GlobalNavigationPage_Android;
import destinations.pages.NewsAndMediaPage;
import destinations.pages.NewsAndMediaPage_Android;
import destinations.pages.SearchResultPage;
import destinations.pages.SearchResultPage_Android;
import destinations.pages.SocialResponsibilitiesPage;
import destinations.pages.SocialResponsibilitiesPage_Android;

public class DestinationsScripts_MobileAndroid extends TestBase {

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_001_WD_SearchFunctionality � Description: Destinations Search
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "destinations", "mobile", "android", "regression" })

	public void TC_001_WD_SearchFunctionality(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		DestinationsLoginPage login = new DestinationsLoginPage_Android(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage_Android(tcconfig);

		try {
			login.launchApplication();
			homePage.openHamburgerMenu();
			homePage.validateSearchFunctionality();
			homePage.searchResultsValidatins();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_003_WD_HomepageCarouselFunctionality� Description: Destinations
	 * homepage carousel Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Homepage", "regression" })

	public void TC_003_WD_HomepageBrandContentValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage_Android(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();
			homePage.validateBrand();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_005_WD_NewsAndMediaPagesValidations� Description: Destinations News
	 * And Media Pages Content and Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "NewsMedia", "" })

	public void TC_005_WD_latestNewsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage_Android(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage_Android(tcconfig);
		try {

			login.launchApplication();
			homePage.homepageLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData", groups = { "legal", "wip" })
	public void TC_007_WD_LegalLinksValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage_Android(tcconfig);
		FooterLinksPage footer = new FooterLinksPage_Android(tcconfig);

		try {

			login.launchApplication();

			footer.footerPrivacySection();
			footer.footerLinksValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}