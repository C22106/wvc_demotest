package destinations.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import destinations.pages.AboutWyndhamPage;
import destinations.pages.DestinationsHomePage;
import destinations.pages.DestinationsLoginPage;
import destinations.pages.FooterLinksPage;
import destinations.pages.GlobalNavigationPage;
import destinations.pages.NewsAndMediaPage;
import destinations.pages.SearchResultPage;
import destinations.pages.SocialResponsibilitiesPage;

public class DestinationsScripts_Web extends TestBase {

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_001_WD_SearchFunctionality � Description: Destinations Search
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Search", "regression", "CB" })

	public void TC_001_WD_SearchFunctionality(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);

		try {

			login.launchApplication();
			homePage.validateSearchFunctionality();
			homePage.searchResultsValidatins();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_002_WD_BookYourVacationValidation � Description: Destinations Book
	 * Your Vacation Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Header", "regression", "CB" })

	public void TC_002_WD_ValidateHeadersSubHeaders(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);
		GlobalNavigationPage globalNav = new GlobalNavigationPage(tcconfig);

		try {

			login.launchApplication();
			homePage.validateLogo();
			homePage.validateHomeHeaderMenu();
			globalNav.contactUsValidations();
			homePage.validateHeaderMenu();
			homePage.validateHomeHeaderSubMenu("Our Brands", "WorldMark by Wyndham", 4);
			homePage.validateHomeHeaderSubMenu("Our Brands", "Margaritaville Vacation Club", 4);
			homePage.validateHomeHeaderSubMenu("Our Brands", "Shell Vacation Clubs  ", 4);
			homePage.validateHomeHeaderSubMenu("Our Brands", "Club Wyndham", 4);
			homePage.validateBanner();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_003_WD_HomepageCarouselFunctionality� Description: Destinations
	 * homepage carousel Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "HomepageBrand", "regression", "CB" })

	public void TC_003_WD_HomepageBrandContentValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);

		try {

			login.launchApplication();
			homePage.validateBrand();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "brokenlink", "brokenlink" })

	public void tc04_Destinations_Broken_Link_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);
			homePage.launchApplicationforBrokenLinkValidation();
			homePage.brokenLinkValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_005_WD_NewsAndMediaPagesValidations� Description: Destinations News
	 * And Media Pages Content and Functionality
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "NewsMedia", "regression", "" })

	public void TC_005_WD_latestNewsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);
		try {

			login.launchApplication();
			homePage.homepageLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_006_WD_FooterValidations� Description: Destinations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Footer", "regression", "CB" })

	public void TC_006_WD_SocialMediaIconValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		FooterLinksPage footer = new FooterLinksPage(tcconfig);

		try {

			login.launchApplication();
			footer.footerSocialIcons();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_007_WD_LegalLinksValidations� Description: Destinations Footer Legal
	 * Links Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "LegalLinks", "regression", "CB" })

	public void TC_007_WD_LegalLinksValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		FooterLinksPage footer = new FooterLinksPage(tcconfig);

		try {

			login.launchApplication();
			//footer.footerPrivacySection();
			footer.footerLinksValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_008_WD_AboutWyndhamValidations� Description: About Wyndham Pages
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "Testimonials", "regression" })

	public void TC_008_WD_HomepageTestimonialsValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);

		try {

			login.launchApplication();
			homePage.validateWhyVacationClubs();
			homePage.validateVacationReady();
			homePage.validateWyndhamCares();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_009_WD_SocialResposibilitiesValidations� Description: Corporate Social
	 * Resp. Env Sust and Philanthrophy Pages Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "AboutUs", "regression" })

	public void TC_009_WD_AboutUsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		DestinationsHomePage homePage = new DestinationsHomePage(tcconfig);

		try {

			login.launchApplication();
			homePage.navigateToOurCompany();
			homePage.validateAboutUs();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_013_WD_FooterBrandOrderValidation� Description: Brand Sequence
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "FooterBrandOrder", "regression", "CB" })

	public void TC_010_WD_FooterBrandOrderValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		DestinationsLoginPage login = new DestinationsLoginPage(tcconfig);
		FooterLinksPage footerLinks = new FooterLinksPage(tcconfig);

		try {

			login.launchApplication();
			footerLinks.footerBrandsOrder();
			footerLinks.footerBrands();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			cleanUp();
		}

	}

}