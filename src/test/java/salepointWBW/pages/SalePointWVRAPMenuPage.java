package salepointWBW.pages;
/*package com.wvo.UIScripts.SalePoint.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalePointWVRAPMenuPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalePointWVRAPMenuPage.class);

	public SalePointWVRAPMenuPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;
	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(id = "wbwcommissions_salesMan")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(xpath = "//a[contains(.,'Edit')]")
	WebElement editPri;

	@FindBy(id = "saveContract")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	@FindBy(xpath = "//span[contains(text(), 'Contracts')]")
	WebElement contracts;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(xpath = "//input[@name='driversLicense']")
	WebElement driversLicense;

	@FindBy(xpath = "//input[@name='firstName']")
	WebElement firstName;

	@FindBy(xpath = "//input[@name='lastName']")
	WebElement lastName;

	@FindBy(xpath = "//input[@name='address1']")
	WebElement address1;

	@FindBy(xpath = "//input[@name='city']")
	WebElement city;

	@FindBy(xpath = "//input[@name='postalCode']")
	WebElement postalCode;

	@FindBy(xpath = "//input[@name='homePhone']")
	WebElement homePhone;

	@FindBy(xpath = "//input[@name='workPhone']")
	WebElement workPhone;

	@FindBy(xpath = "//input[@name='cellPhone']")
	WebElement cellPhone;

	@FindBy(xpath = "//input[@name='emailAddress']")
	WebElement emailAddress;

	@FindBy(xpath = "//input[@name='employee']")
	WebElement employee;

	By driverLicns = By.xpath("//input[@name='driversLicense']");

	By tourIdEnter = By.name("tourID");

	By nextClick = By.xpath("//input[@value='Next']");

	By txtDownPayment = By.xpath("//input[@name='contractDownPymt']");
	By txtPurPricePayment = By.xpath("//td[@id='netPurchasePriceTd']");
	By efName = By.xpath("//input[@name='efName']");
	By emName = By.xpath("//input[@name='emName']");
	By erName = By.xpath("//input[@name='erName']");

	public void sp_wvrap_MemberAdd() throws Exception {

		waitUntilElementVisibleIE(driver, driverLicns, 20);

		if (verifyElementDisplayed(driversLicense)) {

			driversLicense.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driversLicense.sendKeys(testData.get("driversLicense"));

			firstName.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			firstName.sendKeys(testData.get("firstName"));

			lastName.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			lastName.sendKeys(testData.get("lastName"));

			address1.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			address1.sendKeys(testData.get("address1"));

			new Select(driver.findElement(By.name("country"))).selectByValue(testData.get("country"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select(driver.findElement(By.name("stateAbbreviation"))).selectByValue(testData.get("state"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			city.click();
			city.sendKeys(testData.get("city"));

			postalCode.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			postalCode.sendKeys(testData.get("postalCode"));
			homePhone.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			homePhone.sendKeys(testData.get("homePhone"));
			workPhone.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			workPhone.sendKeys(testData.get("workPhone"));
			cellPhone.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			cellPhone.sendKeys(testData.get("cellPhone"));
			emailAddress.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			emailAddress.sendKeys(testData.get("emailAddress"));

			if (verifyElementDisplayed(employee)) {
				clickElementJS(employee);
			}

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_wvrap_MemberAdd", Status.PASS,
					"New Primary Owner Details added successfully");

			clickElementJS(next1);

		} else if (!verifyElementDisplayed(driversLicense)) {
			log.info("Primary Owner already present good to go");
		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_wvrap_MemberAdd", Status.FAIL,
					"Some Issue in page loading");

		}

	}

	
	 * public void sp_Ex_ContractFlow() throws Exception {
	 * 
	 * 
	 * driver.navigate().to(testData.get("MenuURL"));
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (verifyElementDisplayed(tourID)) {
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.PASS, "Experience Page Navigation Succesful"); tourID.click();
	 * tourID.sendKeys(testData.get("strTourId"));
	 * 
	 * new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(
	 * testData.get("strWaveTime"));
	 * 
	 * JavascriptExecutor executor = (JavascriptExecutor) driver;
	 * executor.executeScript("arguments[0].click();", next1);
	 * 
	 * } else {
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.FAIL, "Experience Page Navigation Error" );
	 * 
	 * }
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * sp_wvrap_MemberAdd();
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (verifyElementDisplayed(deleteSec)) { List<WebElement> list =
	 * driver.findElements(By.xpath("//a[contains(.,'Delete')]"));
	 * 
	 * list.get(0).click(); waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.switchTo().alert().accept();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * } else { log.info("No Secondary owner good to go"); }
	 * 
	 * JavascriptExecutor executor = (JavascriptExecutor) driver;
	 * executor.executeScript("arguments[0].click();", next1);
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement> list1
	 * = driver.findElements(By.name("paymentType"));
	 * 
	 * WebElement element = driver.findElement(By.id("gbqfd"));
	 * 
	 * executor.executeScript("arguments[0].click();", list1.get(1));
	 * 
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); // 20,000 Credits -
	 * $2,980.00
	 * 
	 * List<WebElement> list2 = driver.findElements(By.name("explID"));
	 * 
	 * 
	 * list2.get(1).click();
	 * 
	 * 
	 * new Select(driver.findElement(By.name("explID"))).selectByVisibleText(
	 * testData.get("strPoints"));
	 * 
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * executor.executeScript("arguments[0].click();", next1);
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * executor.executeScript("arguments[0].click();", next1);
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * waitUntilElementVisibleIE(driver, saleper, 10);
	 * 
	 * 
	 * 
	 * saleper.click(); saleper.sendKeys(testData.get("strSalePer"));
	 * 
	 * executor.executeScript("arguments[0].click();", next1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * waitUntilElementVisibleIE(driver, savecontract, 10);
	 * 
	 * if (verifyElementDisplayed(savecontract)) {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.PASS, "Contract Review Page Displayed and Save Button Clicked");
	 * 
	 * executor.executeScript("arguments[0].click();", savecontract); } else {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.FAIL, "Contract Save Page Navigation Error");
	 * 
	 * } waitUntilElementVisibleIE(driver, generate, 10);
	 * 
	 * if (verifyElementDisplayed(generate)) {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.PASS,
	 * "Contract Review Page Displayed and Generate Button Clicked");
	 * executor.executeScript("arguments[0].click();", generate); } else {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_ContractFlow",
	 * Status.FAIL, "Generate contract Page Navigation Error"); } //
	 * FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]
	 * 
	 * // conNo.getText();
	 * 
	 * if (verifyElementDisplayed(conNo)) {
	 * 
	 * // testData.put("strContract", conNo.getText()); strContractNo =
	 * conNo.getText();
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_ex_contractFlow",
	 * Status.PASS, "Contract Created with number: " + conNo.getText()); } else {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_ex_contractFlow",
	 * Status.FAIL, "Contract Creation failed "); }
	 * 
	 * if (verifyElementDisplayed(memberNo)) {
	 * 
	 * strMemberNo = memberNo.getText().split("\\:")[1].trim();
	 * log.info("MemberNo " + strMemberNo);
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_ex_contractFlow",
	 * Status.PASS, "Member No: " + strMemberNo);
	 * 
	 * } else {
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_ex_contractFlow",
	 * Status.FAIL, "Member number could not be retrieved"); }
	 * 
	 * }
	 
	@FindBy(xpath = "//input[@value='Experience']")
	WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By editOwner = By.xpath("//a[contains(.,'Edit')]");

	By paymentClick = By.name("paymentType");

	By salePerson = By.id("wbwcommissions_salesMan");

	By generateClick = By.xpath("//input[@name='print']");

	By saveContractClick = By.id("saveContract");

	By memberNoCheck = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By summaryHeader = By.xpath("//tr[@class='page_title']//div");

	By errorLogOut = By.xpath("//input[@value='Logout']");
	
	 * public void sp_Ex_contractSearch() throws Exception {
	 * 
	 * driver.navigate().to(testData.get("contractMgmt"));
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (verifyElementDisplayed(experienceClick)) {
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_contractSearch",
	 * Status.PASS, "Contract Management Page Displayed");
	 * 
	 * JavascriptExecutor executor = (JavascriptExecutor) driver;
	 * executor.executeScript("arguments[0].click();", experienceClick);
	 * 
	 * next1.click();
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * if (verifyElementDisplayed(driver.findElement(By.xpath(
	 * "//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {
	 * 
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_contractSearch",
	 * Status.PASS, "Member No Verified and is: " + strMemberNo); WebElement wb1
	 * = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo +
	 * "')]"));
	 * 
	 * wb1.click();
	 * 
	 * } else { tcConfig.updateTestReporter("SalePointWVRAPMenuPage",
	 * "sp_Ex_contractSearch", Status.FAIL, "Member No Not Verified");
	 * 
	 * }
	 * 
	 * waitUntilElementVisibleIE(driver, contractSummary, 15);
	 * 
	 * if (verifyElementDisplayed(contractSummary)) {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_contractSearch",
	 * Status.PASS, "Summary Page Navigation Successful and Verified");
	 * 
	 * driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
	 * 
	 * if (verifyElementDisplayed(finalizeButton)) {
	 * 
	 * finalizeButton.click(); waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.switchTo().alert().accept();
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * List<WebElement> list2 =
	 * driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));
	 * 
	 * if (list2.get(0).getText().contains("Finalize")) {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_contractSearch",
	 * Status.PASS, "Contract Finalized"); } else {
	 * tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_Ex_contractSearch",
	 * Status.FAIL, "Contract did notr finalize");
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } else { tcConfig.updateTestReporter("SalePointWVRAPMenuPage",
	 * "sp_Ex_contractSearch", Status.FAIL, "Summary Page Navigation Failed" ); }
	 * 
	 * } else { tcConfig.updateTestReporter("SalePointWVRAPMenuPage",
	 * "sp_Ex_contractSearch", Status.FAIL,
	 * "Contract Management Page Not Displayed"); }
	 * 
	 * }
	 

	public void sp_WM_ContractFlow() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 20);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.PASS,
					"WorldMark Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.FAIL,
					"WorldMark Page Navigation Error");

		}

		sp_wvrap_MemberAdd();

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, 20);

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			clickElementJSWithWait(list.get(0));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, paymentClick, 20);

		List<WebElement> list1 = driver.findElements(By.name("paymentType"));

		clickElementJS(list1.get(1));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, 20);

		String downPayText = driver.findElement(txtPurPricePayment).getText().trim();
		downPayText = downPayText.replace("$", "");
		driver.findElement(txtDownPayment).click();
		driver.findElement(txtDownPayment).clear();
		driver.findElement(txtDownPayment).sendKeys(downPayText);
		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyElementDisplayed(saleper)) {

			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			log.info("Good To Go");
		}

		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitUntilElementVisibleIE(driver, saveContractClick, 20);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		waitUntilElementVisibleIE(driver, generateClick, 20);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJS(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}
		// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

		// conNo.getText();

		waitUntilElementVisibleIE(driver, memberNoCheck, 20);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.PASS,
					"Contract Created with number: " + strContractNo);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_ContractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_EX_contract() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 20);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.PASS,
					"WorldMark Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"WorldMark Page Navigation Error");

		}

		sp_wvrap_MemberAdd();

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, 20);

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			clickElementJSWithWait(list.get(0));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, paymentClick, 20);

		List<WebElement> list1 = driver.findElements(By.name("paymentType"));

		clickElementJS(list1.get(1));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		new Select(driver.findElement(By.name("explID"))).selectByVisibleText("20000 Credits - $2600.00");

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextClick);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextVal, 20);

		
		 * String downPayText =driver.findElement(txtPurPricePayment).getText().trim();
		 * downPayText=downPayText.replace("$", "");
		 * driver.findElement(txtDownPayment).click();
		 * driver.findElement(txtDownPayment).clear();
		 * driver.findElement(txtDownPayment).sendKeys(downPayText);
		 * clickElementJS(nextVal);
		 

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(efName)) {
			driver.findElement(efName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(emName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(erName).sendKeys(testData.get("strSalePer"));
		} else {
			log.info("Error");
		}

		clickElementJS(nextVal);
		
		 * waitForSometime(tcConfig.getConfig().get("LongWait"));
		 * 
		 * if (verifyElementDisplayed(saleper)) {
		 * 
		 * driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		 * clickElementJS(next1);
		 * 
		 * waitForSometime(tcConfig.getConfig().get("MedWait")); } else {
		 * log.info("Good To Go"); }
		 

		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitUntilElementVisibleIE(driver, saveContractClick, 20);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		waitUntilElementVisibleIE(driver, generateClick, 20);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJS(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}
		// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

		// conNo.getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(errorLogOut)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"Error in generating contract");

			clickElementJS(errorLogOut);

		} else {
			log.info("");
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, 20);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.PASS,
					"Contract Created with number: " + strContractNo);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_EX_ContractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_WW_contract() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 20);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.PASS,
					"WorldMark Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.FAIL,
					"WorldMark Page Navigation Error");

		}

		sp_wvrap_MemberAdd();

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, 20);

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			clickElementJSWithWait(list.get(0));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, paymentClick, 20);

		List<WebElement> list1 = driver.findElements(By.name("paymentType"));

		clickElementJS(list1.get(1));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, 20);

		String downPayText = driver.findElement(txtPurPricePayment).getText().trim();
		downPayText = downPayText.replace("$", "");
		driver.findElement(txtDownPayment).click();
		driver.findElement(txtDownPayment).clear();
		driver.findElement(txtDownPayment).sendKeys(downPayText);
		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyElementDisplayed(saleper)) {

			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			log.info("Good To Go");
		}

		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitUntilElementVisibleIE(driver, saveContractClick, 20);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		waitUntilElementVisibleIE(driver, generateClick, 20);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJS(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}
		// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

		// conNo.getText();

		waitUntilElementVisibleIE(driver, memberNoCheck, 20);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.PASS,
					"Contract Created with number: " + strContractNo);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WW_ContractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_WM_contractSearch() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, worldMarkSelect, 20);

		if (verifyElementDisplayed(worldmarkClick)) {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.PASS,
					"Contract Management Page Displayed");

			Calendar cal = Calendar.getInstance();
			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date today = cal.getTime();
			// cal.MONTH;

			int monthVal = (cal.get(Calendar.MONTH));

			// String strMonth= String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select(driver.findElement(By.name("beginDateDay"))).selectByValue(strDate);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// new
			// Select(driver.findElement(By.name("endDateDay"))).selectByValue(strDate);

			clickElementJS(worldmarkClick);

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), 20);

			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryHeader, 20);

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					Alert alert = driver.switchTo().alert();
					alert.accept();

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), 20);

					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WM_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	@FindBy(xpath = "//input[@id='member_number']")
	WebElement Owner_member1;

	@FindBy(xpath = "//input[@name='trade']")
	WebElement Add_contract;

	@FindBy(xpath = "//input[@id='member_number']")
	WebElement Owner_member2;

	@FindBy(xpath = "//input[@name='process']")
	WebElement Process_Combine;

	@FindBy(xpath = "//select[@name='loanPACId']")
	WebElement Payment_method_combine;

	private By pacRate = By.xpath("//input[@name='pacRate']");

	private By noPacRate = By.xpath("//input[@name='noPacRate']");

	public void sp_WVRAP_CombineContract() {

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Owner_member1, 20);
		if (verifyElementDisplayed(Owner_member1)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
					"Navigated to Contract Processing - Combine Contract page successfully");

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, Owner_member1, 20);
			if (verifyElementDisplayed(Owner_member1)) {
				Owner_member1.click();
				Owner_member1.sendKeys(testData.get("Owner_Member_1"));

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"owner number 1 Entered");

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Error while entering owner number 1");
			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, Add_contract, 20);
			if (verifyElementDisplayed(Add_contract)) {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Navigated to Contract Processing - Combine Information page Successfully ");
				Add_contract.click();

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Error while clicking on Add contract button ");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, Owner_member2, 20);
			if (verifyElementDisplayed(Owner_member2)) {
				Owner_member2.click();
				Owner_member2.sendKeys(testData.get("Owner_Member_2"));

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"owner number 2 Entered");

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Error while entering owner number 2");
			}

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, Process_Combine, 20);
			if (verifyElementDisplayed(Process_Combine)) {
				Process_Combine.click();
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Process combine button clicked");

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Error while clicking on process combine button");
			}
			// clickElementJS(nextVal);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, nextClick, 20);

			if (verifyElementDisplayed(deleteSec)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> list = driver.findElements(deleteOwner);

				clickElementJSWithWait(list.get(0));

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

					Alert alert = driver.switchTo().alert();
					alert.accept();
				} catch (Exception e) {
					log.info("No Alert Prersent");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("No Secondary owner good to go");
			}

			if (verifyElementDisplayed(editPri)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> list = driver.findElements(editOwner);

				clickElementJSWithWait(list.get(0));

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleIE(driver, driverLicns, 20);

				if (verifyElementDisplayed(driversLicense)) {

					driversLicense.click();
					driversLicense.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driversLicense.sendKeys(testData.get("driversLicense"));

					postalCode.click();
					postalCode.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					postalCode.sendKeys(testData.get("postalCode"));
					homePhone.click();
					homePhone.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					homePhone.sendKeys(testData.get("homePhone"));
					workPhone.click();
					workPhone.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					workPhone.sendKeys(testData.get("workPhone"));
					cellPhone.click();
					cellPhone.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					cellPhone.sendKeys(testData.get("cellPhone"));
					emailAddress.click();
					emailAddress.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					emailAddress.sendKeys(testData.get("emailAddress"));

				} else {
					tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
							"Form Fillup Failed");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Edit option not available");

			}

			clickElementJS(nextClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleIE(driver, pacRate, 10);

			if (verifyObjectDisplayed(pacRate)) {
				driver.findElement(pacRate).click();
				driver.findElement(pacRate).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(pacRate).sendKeys("120");

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(noPacRate).click();
				driver.findElement(noPacRate).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(noPacRate).sendKeys("120");

			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Pac Rate not displayed");
			}

			clickElementJS(nextClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(savecontract)) {
				log.info(" ");

			} else if (verifyObjectDisplayed(nextClick)) {
				waitUntilElementVisibleIE(driver, nextClick, 10);

				clickElementJS(nextClick);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleIE(driver, nextClick, 10);
				clickElementJS(nextClick);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				log.info("Something went wrong");
			}

			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitUntilElementVisibleIE(driver, saveContractClick, 20);

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Contract Save Page Navigation Error");

			}
			waitUntilElementVisibleIE(driver, generateClick, 20);

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				clickElementJS(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Generate contract Page Navigation Error");
			}
			// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

			// conNo.getText();

			waitUntilElementVisibleIE(driver, memberNoCheck, 20);

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Contract Created with number: " + strContractNo);
			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				log.info("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
						"Member number could not be retrieved");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WVRAP_CombineContract", Status.FAIL,
					"Navigation to Contract Processing - Combine Contract page failed");

		}

	}

	@FindBy(xpath = "//input[@name='memberNo']")
	WebElement Member_search;

	@FindBy(xpath = "//input[@value='Search']")
	WebElement search_btn;

	@FindBy(xpath = "//input[@name='tsc']")
	WebElement TSC_number;

	// input[@name='umName']

	private By umName = By.xpath("//input[@name='umName']");
	private By uaName = By.xpath("//input[@name='uaName']");
	private By uhName = By.xpath("//input[@name='uhName']");
	private By ugName = By.xpath("//input[@name='ugName']");
	private By uspresenter = By.xpath("//input[@name='uspresenter']");

	public void sp_WVRAP_UpgradeContact() {
		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, Member_search, 20);
		if (verifyElementDisplayed(Member_search)) {
			Member_search.click();
			Member_search.sendKeys(testData.get("WVRAP_Member"));
			search_btn.click();
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.PASS,
					"Member number Entered");

			waitUntilElementVisibleIE(driver, TSC_number, 20);
			if (verifyElementDisplayed(TSC_number)) {
				TSC_number.click();
				TSC_number.sendKeys(testData.get("strTourId"));
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact",
						Status.PASS, "TSC Entered");
			} else {
				tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact",
						Status.FAIL, "Error while entering TSC Number");

			}

			clickElementJS(nextVal);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.FAIL,
					"Upgrade-Purchase Information  Navigation Failed");
		}

		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, umName, 20);

		if (verifyObjectDisplayed(umName)) {

			driver.findElement(umName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(uaName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(uhName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(ugName).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(uspresenter).sendKeys(testData.get("strSalePer"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saveContractClick, 50);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, generateClick, 50);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJSWithWait(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRAPMenuPage", "sp_WVRAP_UpgradeContact", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

}
*/