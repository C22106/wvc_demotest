package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWSummaryPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWSummaryPage.class);

	public WBWSummaryPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Summary')]")
	protected WebElement summaryHeader;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Summary')]")
	protected WebElement summaryHeaderDiv;

	@FindBy(name = "saveContract")
	protected WebElement saveContractCTA;

	@FindBy(name = "print")
	protected WebElement printCTA;

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	protected WebElement contractNumber;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNumber;

	@FindBy(xpath = "//input[@value='Finalize']")
	protected WebElement finalizeCTA;

	@FindBy(xpath = "//input[@value='Void']")
	protected WebElement voidCTA;

	@FindBy(xpath = "//select[@name='signatureDate']")
	protected WebElement signatureDate;

	@FindBy(xpath = "//a[contains(.,'Contract documents')]")
	protected List<WebElement> splitContractLink;

	@FindBy(xpath = "//td[contains(.,'Finance Information')]//following-sibling::td/input[@value='Edit']")
	protected WebElement editFinanceInformation;

	String strContractNo = "";
	String strMemberNo = "";

	/*
	 * Method: summaryPageNavigation Description:Summary Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void summaryPageNavigation() throws Exception {
		waitUntilElementVisible(driver, summaryHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(summaryHeader) || verifyElementDisplayed(summaryHeaderDiv),
				"Summary page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Summary Page");

	}

	/*
	 * Method: validatePaymentType Description:Validate Payment Type Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void validatePaymentType() {
		String paymentXpath = "//td[contains(.,'" + testData.get("PaymentBy") + "')]";
		By paymentType = driver.findElement(By.xpath(paymentXpath));
		if (verifyObjectDisplayed(paymentType)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"PaymentType Correctly Displayed");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Payment Type inCorrectly Displayed");
		}
	}

	/*
	 * Method: validatePaymentCard Description:Validate Payment Card Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void validatePaymentCard() {
		String paymentXpath = "//td[contains(.,'" + testData.get("CCType") + "')]";
		By paymentType = driver.findElement(By.xpath(paymentXpath));
		if (verifyObjectDisplayed(paymentType)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Payment Card Correctly Displayed");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Payment Card inCorrectly Displayed");
		}
	}

	/*
	 * Method: saveContract Description:Save Contract Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void saveContract() throws Exception {
		waitUntilElementVisibleIE(driver, saveContractCTA, "ExplicitMedWait");
		if (verifyElementDisplayed(saveContractCTA)) {
			tcConfig.updateTestReporter("WBWSummaryPage", "clickSave", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(saveContractCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			try {
				new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}
		} else {
			tcConfig.updateTestReporter("WBWSummaryPage", "clickSave", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
	}

	/*
	 * Method: clickPrintCTA Description:Click Print CTA Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void clickPrintCTA() {
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisible(driver, printCTA, "ExplicitLongWait");

		if (verifyElementDisplayed(printCTA)) {
			clickElementJSWithWait(printCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed((By.id("justification")))) {
				Select sel = new Select(driver.findElement(By.id("justification")));
				sel.selectByValue("System Issue");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(printCTA);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {
			log.info("Print element not found");
		}

	}

	/*
	 * Method: captureContractNumber Description:Captur Contract Number Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void captureContractNumber() {
		waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitLongWait");

		if (verifyElementDisplayed(contractNumber)) {

			testData.put("strContractNumber", contractNumber.getText());
			strContractNo = contractNumber.getText();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + contractNumber.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

	}

	/*
	 * Method: captureMemberNumber Description:Capture Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void captureMemberNumber() {

		if (verifyElementDisplayed(memberNumber)) {

			strMemberNo = memberNumber.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);
			testData.put("strMemberNumber", strMemberNo);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: finalizeContract Description:Finalize Contract Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void finalizeContract() {
		if (verifyElementDisplayed(finalizeCTA)) {
			String strMemberNo = testData.get("strMemberNumber");
			clickElementJSWithWait(finalizeCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"),
					"ExplicitLongWait");
			WebElement updatedStatus = driver.findElement(
					By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]/../../td[contains(.,'Finalize')]"));

			if (verifyElementDisplayed(updatedStatus)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Contract Finalized");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract was not Finalized");

			}
		}

	}
	
	


	/*
	 * Method: voidContract Description:Void Contract Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void voidContract() {
		if (verifyElementDisplayed(voidCTA)) {
			String strMemberNo = testData.get("strMemberNumber");
			clickElementJSWithWait(voidCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"),
					"ExplicitLongWait");
			WebElement updatedStatus = driver
					.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]/../../td[contains(.,'Void')]"));

			if (verifyElementDisplayed(updatedStatus)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Contract Voided");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract was not Voided");

			}
		}

	}

	/*
	 * Method: selectSignature Description:Select Signature Date Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectSignature() {
		if (verifyObjectDisplayed(signatureDate)) {
			clickElementJSWithWait(signatureDate);
			sendKeyboardKeys(Keys.ARROW_DOWN);
			sendKeyboardKeys(Keys.ENTER);

		}
	}

	/*
	 * Method: selectSplitTransferor Description:Select Split Transferor Date
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void selectSplitTransferor() {
		waitUntilElementVisible(driver, splitContractLink.get(0), "ExplicitLongWait");
		splitContractLink.get(0).click();

	}

	/*
	 * Method: clickEditFinanceInfo Description:Edit Finance Info Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickEditFinanceInfo() {
		if (verifyElementDisplayed(editFinanceInformation)) {

			clickElementJSWithWait(editFinanceInformation);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		}

	}

}
