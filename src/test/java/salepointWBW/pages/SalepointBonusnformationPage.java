package salepointWBW.pages;
/*package com.wvo.UIScripts.SalePoint.pages;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.SealedObject;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalepointBonusnformationPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalepointBonusnformationPage.class);

	public SalepointBonusnformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	@FindBy(xpath = "//select[@name = 'bonuspoints']")
	WebElement selectBonusPoints;
	
	@FindBy(xpath = "//input[@value = 'Next']")
	WebElement buttonNext;

	@FindBy(xpath = "//div[contains(.,'Contract Processing - Go! Points')]")
	WebElement headerGoPoints;
	public void continueBonus(){
		waitUntilElementVisibleIE(driver, headerGoPoints, 120);
		if (verifyElementDisplayed(selectBonusPoints)) {
			String Points = testData.get("strPoints");
			new Select(driver.findElement(By.xpath("//select[@name = 'bonuspoints']"))).selectByVisibleText(Points);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			buttonNext.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("SalepointBonusnformationPage", "continueBonus",
					Status.PASS, "Bonus Information Page Displayed and clicekd on next button");
		}else if (!verifyElementDisplayed(selectBonusPoints)) {
			buttonNext.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("SalepointBonusnformationPage", "continueBonus",
					Status.PASS, "Bonus Information Page Displayed and clicekd on next button");
		}else{
			tcConfig.updateTestReporter("SalepointBonusnformationPage", "continueBonus",
					Status.FAIL, "Failed on Go Point Page");
		}
	}
}*/