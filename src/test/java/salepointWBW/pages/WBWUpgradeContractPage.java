package salepointWBW.pages;

import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWUpgradeContractPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWUpgradeContractPage.class);

	public WBWUpgradeContractPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "memberNo")
	protected WebElement memberNumber;

	@FindBy(name = "tsc")
	protected WebElement tscCode;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//input[@name='search']")
	protected WebElement searchButton;

	/*
	 * Method: enterMemberNumber Description:enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		if (verifyElementDisplayed(memberNumber)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member Number Field Navigation Successful");
			memberNumber.click();
			memberNumber.sendKeys(testData.get("strMemberNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member Number Field Not Present ");
		}
	}

	/*
	 * Method: clickSearchButton Description:Click Search Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickSearchButton() {
		clickElementJS(searchButton);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (ExpectedConditions.alertIsPresent() != null) {
			try {
				driver.switchTo().alert().accept();

			} catch (Exception e) {
				log.info(e);
			}
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: enterTSCCodeOnly Description:enter TSC Code Only Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterTSCCodeOnly() {
		waitUntilElementVisibleIE(driver, tscCode, "ExplicitLongWait");
		if (verifyElementDisplayed(tscCode)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "TSC Code Field Displayed");
			tscCode.clear();
			tscCode.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			try {
				driver.switchTo().alert().accept();

			} catch (Exception e) {
				log.info(e);
			}
		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "TSC Code Field not displayed");
		}
	}

}
