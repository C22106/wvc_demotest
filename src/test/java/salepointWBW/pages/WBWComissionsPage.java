package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWComissionsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWComissionsPage.class);

	public WBWComissionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Commissions')]")
	protected WebElement comissionsHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(name = "salesMan")
	protected WebElement salesPerson;

	/*
	 * Method: comissionNavigation Description:Comission Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void comissionNavigation() throws Exception {
		waitUntilElementVisible(driver, comissionsHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(comissionsHeader), "Comissions page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Comissions Page");

	}

	/*
	 * Method: enterSalePerson Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterSalePerson() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		salesPerson.clear();
		salesPerson.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}
}