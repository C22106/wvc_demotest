package salepointWBW.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointResaleContractSelection extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointResaleContractSelection.class);
	public static String point;
	public static String strContractNo;
	public static String strMemberNo;

	public SalePointResaleContractSelection(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;
	protected By creditCardcType = By.xpath("//select[contains(@name,'cc_payment_type')]");
	protected By creditCardcMonth = By.xpath("//select[contains(@name,'cc_payment_exp_month')]");

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(xpath = "//input[@name='salesman']")
	WebElement saleper;
	protected By printOption = By.xpath("//select[@id = 'justification']");
	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(xpath = "//input[@value='Save and Print']")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	WebElement conNo;
	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//div[contains(text(),'Payment Gateway')]/../../input")
	WebElement paymentGatewayForm;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	@FindBy(xpath = "//input[@id='customerNumber']")
	WebElement customerNo;

	@FindBy(xpath = "//input[@name='CalculateAllFigures']")
	WebElement calculateBtn;

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//input[@name='down_payment_amount0']")
	WebElement paymentAmount1;

	@FindBy(xpath = "//input[@name='down_payment_amount1']")
	WebElement paymentAmount2;

	@FindBy(xpath = "//input[@name='cc_payment_member_name']")
	WebElement ccName;

	@FindBy(xpath = "//input[@name='cc_payment_account_number']")
	WebElement CCAccountNo;

	@FindBy(xpath = "//input[@name='bank_name']")
	WebElement bank_name;

	@FindBy(xpath = "//input[@name='bank_city']")
	WebElement bank_city;
	@FindBy(xpath = "//input[@name='bank_postal_code']")
	WebElement bank_postal_code;
	@FindBy(xpath = "//input[@name='bank_account_number']")
	WebElement bank_account_number;
	@FindBy(xpath = "//input[@name='account_holder_name']")
	WebElement account_holder_name;
	@FindBy(xpath = "//input[@name='bank_routing_number']")
	WebElement bank_routing_number;
	@FindBy(xpath = "//input[@name='thrid_party_closing_fee']")
	WebElement thrid_party_closing_fee;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	WebElement udiSelect;

	@FindBy(xpath = "//input[@name='sales_types' and @value='lots']")
	WebElement lotsSelect;

	@FindBy(id = "Print")
	WebElement print;

	@FindBy(id = "newPoints")
	WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	WebElement calculate;

	By nextClick = By.xpath("//input[@value='Next']");

	By customerNoInput = By.id("customerNumber");

	By saleTypeSelect = By.xpath("//input[@name='fvl_saleType' and @value='M']");

	By deleteCheck = By.xpath("//a[contains(.,'Delete')]");

	By udiClick = By.xpath("//input[@name='sales_types' and @value='UDI']");

	By lotsClick = By.xpath("//input[@name='sales_types' and @value='lots']");

	By newPointsCheck = By.id("newPoints");

	By calculateClick = By.xpath("//input[@value='Calculate']");

	By salePersonSelect = By.xpath("//input[@name='salesman']");

	By saveClick = By.xpath("//input[@value='Save and Print']");

	By printClick = By.id("Print");

	By memberNoCheck = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");

	By ssnSelect = By.xpath("//input[@name='ssn']");

	By genderSelect = By.xpath("//input[@name='sex' and @value='M']");
	public By ExpDate = By.xpath("//tr[@class='section_title']/../tr/td[4]");

	By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");
	By ccAutoPaySelect = By.xpath("//td[contains(.,'CC Auto Pay')]/input[@type='radio']");
	By noSelect = By.xpath("//td[contains(.,'No')]/input[@type='radio']");
	By achAutoPaySelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	By achSelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	By headerContractProcessOwner = By.xpath("//div[contains(text(),'Contract Processing - Owners')]");

	By headerCustInfo = By.xpath("//div[contains(text(),'Contract Processing - Customer Information')]");

	By headerContractSummary = By.xpath("//div[contains(text(),'Contract Summary')]");

	By headerChangeStatus = By.xpath("//div[contains(text(),'View Contract - Change Status')]");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By StatusOwner = By.xpath("//a[contains(.,'Status')]");

	By MonthFromDrpDwn = By.xpath("//select[@name='dobmonth']");
	By DateFromDrpDwn = By.xpath("//select[@name='dobday']");
	By YearFromDrpDwn = By.xpath("//select[@name='dobyear']");
	private By firstAvailableRow_RadioBtn = By
			.xpath("(//td[contains(text(),'Available')])[1]/..//input[@id='resaleContractIdRadio']");
	private By firstContractNumber = By.xpath("(//td[contains(text(),'Available')])[1]/../td[2]");
	private By holdBtn = By.xpath("//input[@id='holdBtn']");
	private By removeHoldBtn = By.xpath("//input[@id='removeHoldBtn']");
	private By proceedBtn = By.xpath("//input[@id='nextBtn']");
	private By locateCustomerPageTitle = By
			.xpath("//tr[@class='page_title']//div[contains(.,'Contract Processing - Locate Customer')]");
	private By memberNoTxtBx = By.xpath("//input[@name='memberNumber']");
	private By titleDrpdwn = By.xpath("//select[@name='title']");
	private By sellingSiteDrpdwn = By.xpath("//select[@name='resaleSellingSite']");
	private By firestnameTxtBx = By.xpath("//input[@name='first_name']");
	private By lastnameTxtBx = By.xpath("//input[@name='last_name']");
	private By address1TxtBx = By.xpath("//input[@name='address1']");
	private By cityTxtBx = By.xpath("//input[@name='city']");
	private By stateDrpdwn = By.xpath("//select[@name='state']");
	private By postalCodeTxtBx = By.xpath("//input[@name='postalCode']");
	private By countryTxtBx = By.xpath("//input[@name='country']");
	private By homePhnTxtBx = By.xpath("//input[@name='home_phone']");
	private By wrkPhnTxtBx = By.xpath("//input[@name='work_phone']");
	private By emailTxtBx = By.xpath("//input[@name='emailAddress']");
	private By inventorySite = By.xpath("//select[@name='inventorySite']");
	private By inventoryPhase = By.xpath("//select[@name='inventoryPhase']");
	private By udiClubSubtypeDrpdwn = By.xpath("//select[@name='udi_subtype']");
	private By udiBusinessModel = By.xpath("//select[@name='waam']");
	private By paymentMethod = By.xpath("//select[@name='down_payment_method0']");

	public void validateStatus_resaleContractSelection() {
		String corrspContractNumber = null;
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		// checking for new requirement
		List<WebElement> listAvailable = driver
				.findElements(By.xpath("(//td[contains(text(),'Available')])/..//input[@id='resaleContractIdRadio']"));

		if (listAvailable.size() > 0) {
			for (int i = 0; i < listAvailable.size(); i++) {
				listAvailable.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection",
							Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + i + 1 + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		/*
		 * driver.findElement(firstAvailableRow_RadioBtn).click();
		 * tcConfig.updateTestReporter("SalePointResaleContractSelection",
		 * "resaleContractSelection", Status.PASS,
		 * "Radio Button selected for the first Available Contract");
		 */

		// String
		// corrspContractNumber=driver.findElement(firstContractNumber).getText().trim();
		driver.findElement(holdBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		point = driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[12]")).getText()
				.trim().contains(testData.get("UserID"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Hold user is diplayed as " + testData.get("UserID"));
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Hold User not displayed");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		driver.findElement(removeHoldBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Available")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Remove Hold button and Status of the selected Contract changed to Available");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[12]")).getText()
				.trim().contains("")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Hold user is not present");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Hold User is displayed");
		}

	}

	public void sortingContract_ExpirationDate() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");

		ArrayList<String> obtainedList = new ArrayList<>();
		List<WebElement> expDt = driver.findElements(ExpDate);
		expDt.remove(0);

		for (WebElement we : expDt) {
			obtainedList.add(we.getText());
		}

		ArrayList<String> sortedList = new ArrayList<>();
		for (String s : obtainedList) {
			sortedList.add(s);
		}
		Collections.sort(sortedList);
		System.out.println(sortedList);
		if (sortedList.equals(obtainedList)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "sortingContract_ExpirationDate",
					Status.PASS, "Expiration Date is sorted in ascending order");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "sortingContract_ExpirationDate",
					Status.FAIL, "Expiration Date is not sorted in ascending order");
		}
	}

	public void resaleContractSelection() {
		String corrspContractNumber = null;
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver
				.findElements(By.xpath("(//td[contains(text(),'Available')])/..//input[@id='resaleContractIdRadio']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection",
							Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		/*
		 * driver.findElement(firstAvailableRow_RadioBtn).click();
		 * tcConfig.updateTestReporter("SalePointResaleContractSelection",
		 * "resaleContractSelection", Status.PASS,
		 * "Radio Button selected for the first Available Contract");
		 */

		// corrspContractNumber=driver.findElement(firstContractNumber).getText().trim();
		driver.findElement(holdBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		point = driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");

		driver.findElement(proceedBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(locateCustomerPageTitle)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Contract Processing - Locate Customer Page");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Contract Processing - Locate Customer Page");
		}
	}

	private By statusLink = By.xpath("(//a[contains(.,'Status')])[1]");

	public void contractFlow() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			clickElementJSWithWait(statusLink);
		} catch (Exception e) {
			System.out.println("No status link");
		}

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			String memberType = testData.get("MemberType");
			if (memberType.equalsIgnoreCase("Existing")) {

				driver.findElement(memberNoTxtBx).sendKeys(testData.get("MemberNo"));
			} else if (memberType.equalsIgnoreCase("New")) {
				System.out.println("Member number not required for New Owner");
			}

			// driver.findElement(ssnSelect).click();
			// driver.findElement(ssnSelect).clear();
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			new Select(driver.findElement(sellingSiteDrpdwn)).selectByValue(testData.get("SellingSite"));

			new Select(driver.findElement(titleDrpdwn)).selectByVisibleText("Mr.");

			driver.findElement(firestnameTxtBx).click();
			driver.findElement(firestnameTxtBx).clear();
			driver.findElement(firestnameTxtBx).sendKeys(testData.get("firstName"));

			driver.findElement(lastnameTxtBx).click();
			driver.findElement(lastnameTxtBx).clear();
			driver.findElement(lastnameTxtBx).sendKeys(testData.get("lastName"));

			driver.findElement(address1TxtBx).click();
			driver.findElement(address1TxtBx).clear();
			driver.findElement(address1TxtBx).sendKeys(testData.get("address1"));

			driver.findElement(cityTxtBx).click();
			driver.findElement(cityTxtBx).clear();
			driver.findElement(cityTxtBx).sendKeys(testData.get("city"));

			new Select(driver.findElement(stateDrpdwn)).selectByVisibleText(testData.get("state"));

			driver.findElement(postalCodeTxtBx).click();
			driver.findElement(postalCodeTxtBx).clear();
			driver.findElement(postalCodeTxtBx).sendKeys(testData.get("postalCode"));

			driver.findElement(countryTxtBx).click();
			driver.findElement(countryTxtBx).clear();
			driver.findElement(countryTxtBx).sendKeys(testData.get("country"));

			driver.findElement(homePhnTxtBx).click();
			driver.findElement(homePhnTxtBx).clear();
			driver.findElement(homePhnTxtBx).sendKeys(testData.get("homePhone"));

			driver.findElement(wrkPhnTxtBx).click();
			driver.findElement(wrkPhnTxtBx).clear();
			driver.findElement(wrkPhnTxtBx).sendKeys(testData.get("workPhone"));

			driver.findElement(emailTxtBx).click();
			driver.findElement(emailTxtBx).clear();
			driver.findElement(emailTxtBx).sendKeys(testData.get("emailAddress"));

			new Select(driver.findElement(DateFromDrpDwn)).selectByVisibleText("1");
			new Select(driver.findElement(MonthFromDrpDwn)).selectByVisibleText("Feb");
			new Select(driver.findElement(YearFromDrpDwn)).selectByVisibleText("1992");

			clickElementJS((genderSelect));

			try {
				clickElementJS(saleType);
			} catch (Exception e) {
				System.out.println("Sale type already selected");

			}
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = driver.findElements(deleteCheck);
				int count = 0;

				recDekl();
			} else {
				System.out.println("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			System.out.println("Owner Page Not Displayed");
		}

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLongWait");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.findElement(udiClubSubtypeDrpdwn).getAttribute("value").contains("clubwyndham_access")) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Default Value is Club Wyndham Access");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Default Value is not Club Wyndham Access");
			}

			if (driver.findElement(udiBusinessModel).getAttribute("value").equals("1")) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Default Value is WVO Standard");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Default Value is not WVO Standard");
			}

			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLongWait");

		if (driver.findElement(newPointsCheck).getAttribute("value").equals(point)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Buyer contracts inventory point is same as that of seller contract");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Buyer contracts inventory point is not same as that of seller contract");
		}

		if (driver.findElement(inventorySite).getAttribute("value").contains(testData.get("inventorySite"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Default value of inventory site is " + testData.get("inventorySite"));

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Default value of inventory site is not " + testData.get("inventorySite"));
		}

		if (driver.findElement(inventoryPhase).getAttribute("value").contains(testData.get("inventoryPhase"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Default value of inventory phase is " + testData.get("inventoryPhase"));

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Default value of inventory phase is not " + testData.get("inventoryPhase"));
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");

		clickElementJS(nextVal);

		try {
			new WebDriverWait(driver, 120).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");

		if (testData.get("LoanPaymentMethod").contains("NO")) {
			if (verifyObjectDisplayed(noSelect)) {
				clickElementJS(noSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"NO Selected");

				new Select(driver.findElement(paymentMethod)).selectByVisibleText(testData.get("Payment Method"));

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"No radio button not present");
			}
		} else if (testData.get("LoanPaymentMethod").contains("Cash")) {
			if (verifyObjectDisplayed(cashSelect)) {

				clickElementJSWithWait(cashSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Cash Selected");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Cash radio button not present");
			}
		} else if (testData.get("LoanPaymentMethod").contains("CC Auto Pay")) {
			if (verifyObjectDisplayed(ccAutoPaySelect)) {
				clickElementJS(ccAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ccName.sendKeys(testData.get("CreditCardHolder"));
				new Select(driver.findElement(creditCardcType)).selectByVisibleText(testData.get("CCType"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				CCAccountNo.sendKeys(testData.get("CCNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select(driver.findElement(creditCardcMonth)).selectByVisibleText(testData.get("CCMonth"));
				new Select(driver.findElement(ccExpYear)).selectByVisibleText(testData.get("CCYear"));
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"CC Auto Pay Selected");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"CC Auto Pay radio button not present");
			}
		} else if (testData.get("LoanPaymentMethod").contains("ACH Auto Pay")) {
			if (verifyObjectDisplayed(achAutoPaySelect)) {
				clickElementJS(achAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				bank_name.sendKeys(testData.get("BankName"));
				bank_city.sendKeys(testData.get("BankCity"));
				bank_postal_code.sendKeys(testData.get("BankPostalCode"));
				bank_account_number.sendKeys(testData.get("BankAccNumber"));
				account_holder_name.sendKeys(testData.get("AccountHolderName"));
				bank_routing_number.sendKeys(testData.get("BankRoutingNumber"));
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"ACH Auto Pay Selected");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"CC Auto Pay radio button not present");
			}

		}

		if (testData.get("PaymentMethod").equalsIgnoreCase("Other - VCC")) {
			String value = paymentAmount1.getAttribute("value");
			String valueleft = testData.get("PaymentAmount2");
			double newvalue = Double.parseDouble(value);
			double newvalueleft = Double.parseDouble(valueleft);
			newvalue = newvalue - newvalueleft;
			// newvalue=newvalue-"ExplicitLongWait"0.00;
			String amt = Double.toString(newvalue);
			System.out.println("Amount " + amt);
			paymentAmount1.click();
			paymentAmount1.clear();
			String selectAll = Keys.chord(Keys.SHIFT, Keys.HOME);
			paymentAmount1.sendKeys(selectAll);
			paymentAmount1.sendKeys(amt);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(paymentMethodDrpdwn2)).selectByVisibleText(testData.get("PaymentMethod"));
			paymentAmount2.click();
			paymentAmount2.clear();
			paymentAmount2.sendKeys(selectAll);
			paymentAmount2.sendKeys(testData.get("PaymentAmount2"));
		} else {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(payMethod)).selectByVisibleText(testData.get("PaymentMethod"));
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		if (testData.get("CheckThridPartyClosingFee").equalsIgnoreCase("Yes")) {
			if (thrid_party_closing_fee.getAttribute("value").equals(testData.get("ThridPartyClosingFeeValue"))) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Thrid Party Closing Fee Value is " + testData.get("ThridPartyClosingFeeValue"));
			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Thrid Party Closing Fee Value is not " + testData.get("ThridPartyClosingFeeValue"));
			}

		}
		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLongWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitLongWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLongWait");

		if (testData.get("validateDocStampFee").equalsIgnoreCase("Yes")) {
			checkDocStampFee();
		}
		if (testData.get("validateServiceFee").equalsIgnoreCase("Yes")) {
			checkServiceFee();
		}

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		if (testData.get("CheckForm").equalsIgnoreCase("Y")) {
			String formvalue = paymentGatewayForm.getAttribute("value");
			System.out.println("Form name " + formvalue);
			if (formvalue.equalsIgnoreCase(testData.get("PrintForm"))) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						testData.get("PrintForm") + " is present");
			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						testData.get("PrintForm") + " is not present");
			}

		}
		waitUntilElementVisibleIE(driver, printClick, "ExplicitLongWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(printOption)) {
				new Select(driver.findElement(printOption)).selectByIndex(2);
				tcConfig.updateTestReporter("SalepointContractPrintPage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLongWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();
			System.out.println("Contract no: " + strContractNo);

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

		if (strContractNo.startsWith("00395")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Contract No starts with 00395");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Contract No not starts with 00395");
		}

	}

	public void recDekl() {
		List<WebElement> e = driver.findElements(deleteCheck);
		if (e.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(e.get(0));
			try {
				new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception exc) {
				System.out.println("No Alert Present after Login");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			recDekl();
		}
	}

	public By contractNumber = By.xpath("//input[@name='contract_number']");
	public By searchValue = By.xpath("//input[@name='searchValue']");
	public By useYearEndDate = By.xpath("//select[@name='use_year_end_date']");
	public By paymentMethodDrpdwn2 = By.xpath("//select[@name='down_payment_method1']");
	public By payMethod = By.xpath("//select[@name='down_payment_method0']");
	public By ccExpYear = By.xpath("//select[@name='cc_payment_exp_year']");
	public By searchBtn = By.xpath("//input[@name='search']");
	public By searchBtn2 = By.xpath("//input[@id='SearchBtn']");
	public By radioOwnerSelect = By.xpath("//input[@name='radiosel']");
	public By authorizeBtn = By.xpath("//input[@id='AuthorizeBtn']");
	public By bckBtn = By.xpath("//input[@id='bckBtn']");
	public By cancelBtn = By.xpath("//input[@id='CancelBtn']");
	public By changeStatusBtn = By.xpath("//input[@value='Change Status']");
	public By contractNumberColumnValue = By.xpath("//td[text()='Contract Number']/following::tr[1]/td[1]");
	public By decisionValue = By.xpath("//td[text()='DECISION']/following::td[1]");

	public void defaultData_NotEditable() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("EditContractURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(contractNumber).sendKeys(strContractNo);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJS(searchBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, lotsClick, "ExplicitLongWait");

		if (verifyElementDisplayed(lotsSelect)) {
			clickElementJSWithWait(lotsSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			try {

				new WebDriverWait(driver, 120).until(ExpectedConditions.alertIsPresent());

				if (driver.switchTo().alert().getText()
						.contains("Cannot select another contract type during Edit mode.")) {
					tcConfig.updateTestReporterWithoutScreenshot("SalePointResaleContractSelection",
							"defaultData_NotEditable", Status.PASS,
							"User is not able to edit Sale type during edit mode");

				} else {
					tcConfig.updateTestReporterWithoutScreenshot("SalePointResaleContractSelection",
							"defaultData_NotEditable", Status.FAIL,
							"User is able to edit Sale type during edit mode");

				}
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "defaultData_NotEditable",
						Status.FAIL, "Sale type not displayed");
			}
		}

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(useYearEndDate).isDisplayed()) {
			new Select(driver.findElement(useYearEndDate)).selectByVisibleText(testData.get("ChangedUseYearEndDate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

			try {
				new WebDriverWait(driver, 120).until(ExpectedConditions.alertIsPresent());

				if (driver.switchTo().alert().getText()
						.contains("The Use Year End date must be same for both Seller and Buyer contracts")) {
					tcConfig.updateTestReporterWithoutScreenshot("SalePointResaleContractSelection",
							"defaultData_NotEditable", Status.PASS,
							"User is not able to edit Use Year End date during edit mode");

				} else {
					tcConfig.updateTestReporterWithoutScreenshot("SalePointResaleContractSelection",
							"defaultData_NotEditable", Status.FAIL,
							"User is able to edit Use Year End date during edit mode");

				}
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "defaultData_NotEditable",
						Status.FAIL, "Use Year End date not displayed");
			}
		}
	}

	public By voidBtn = By.xpath("//input[@class='button' and @value='Void']");
	public By transmitBtn = By.xpath("//input[@class='button' and @value='Transmit']");
	public By statusChangeMessage = By.xpath("//tr[@class='section_title']/following::tr[1]/td");

	public void voidContract() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("ViewContractURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(contractNumber).sendKeys(strContractNo);
		// driver.findElement(contractNumber).sendKeys("003951900049 ");

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementJS(searchBtn);
		if (verifyObjectDisplayed(headerContractSummary)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.PASS,
					"User navigated to Contract Summary Page");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.FAIL,
					"User unable to navigate to Contract Summary Page");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(changeStatusBtn);
		if (verifyObjectDisplayed(headerChangeStatus)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.PASS,
					"User navigated to Change status Page");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.FAIL,
					"User unable to navigate to Change status Page");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(voidBtn);

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String msg = driver.findElement(statusChangeMessage).getText();
		System.out.println("Message " + msg);
		if (msg.equalsIgnoreCase("Contract status changed successfully.")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.PASS,
					"Contract Status change to Void");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "voidContract", Status.FAIL,
					"Error in changing the status");
		}
	}

	public void editTotalNewPoints() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			clickElementJSWithWait(statusLink);
		} catch (Exception e) {
			System.out.println("No status link");
		}

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			String memberType = testData.get("MemberType");
			if (memberType.equalsIgnoreCase("Existing")) {

				driver.findElement(memberNoTxtBx).sendKeys(testData.get("MemberNo"));
			} else if (memberType.equalsIgnoreCase("New")) {
				System.out.println("Member number not required for New Owner");
			}

			// driver.findElement(ssnSelect).click();
			// driver.findElement(ssnSelect).clear();
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			new Select(driver.findElement(sellingSiteDrpdwn)).selectByValue(testData.get("SellingSite"));
			new Select(driver.findElement(titleDrpdwn)).selectByVisibleText("Mr.");

			driver.findElement(firestnameTxtBx).click();
			driver.findElement(firestnameTxtBx).clear();
			driver.findElement(firestnameTxtBx).sendKeys(testData.get("firstName"));

			driver.findElement(lastnameTxtBx).click();
			driver.findElement(lastnameTxtBx).clear();
			driver.findElement(lastnameTxtBx).sendKeys(testData.get("lastName"));

			driver.findElement(address1TxtBx).click();
			driver.findElement(address1TxtBx).clear();
			driver.findElement(address1TxtBx).sendKeys(testData.get("address1"));

			driver.findElement(cityTxtBx).click();
			driver.findElement(cityTxtBx).clear();
			driver.findElement(cityTxtBx).sendKeys(testData.get("city"));

			new Select(driver.findElement(stateDrpdwn)).selectByVisibleText(testData.get("state"));

			driver.findElement(postalCodeTxtBx).click();
			driver.findElement(postalCodeTxtBx).clear();
			driver.findElement(postalCodeTxtBx).sendKeys(testData.get("postalCode"));

			driver.findElement(countryTxtBx).click();
			driver.findElement(countryTxtBx).clear();
			driver.findElement(countryTxtBx).sendKeys(testData.get("country"));

			driver.findElement(homePhnTxtBx).click();
			driver.findElement(homePhnTxtBx).clear();
			driver.findElement(homePhnTxtBx).sendKeys(testData.get("homePhone"));

			driver.findElement(wrkPhnTxtBx).click();
			driver.findElement(wrkPhnTxtBx).clear();
			driver.findElement(wrkPhnTxtBx).sendKeys(testData.get("workPhone"));

			driver.findElement(emailTxtBx).click();
			driver.findElement(emailTxtBx).clear();
			driver.findElement(emailTxtBx).sendKeys(testData.get("emailAddress"));

			new Select(driver.findElement(DateFromDrpDwn)).selectByVisibleText("1");
			new Select(driver.findElement(MonthFromDrpDwn)).selectByVisibleText("Feb");
			new Select(driver.findElement(YearFromDrpDwn)).selectByVisibleText("1992");

			clickElementJS((genderSelect));

			try {
				clickElementJS(saleType);
			} catch (Exception e) {
				System.out.println("Sale type already selected");

			}
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = driver.findElements(deleteCheck);
				int count = 0;

				recDekl();
			} else {
				System.out.println("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			System.out.println("Owner Page Not Displayed");
		}

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLongWait");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.findElement(udiClubSubtypeDrpdwn).getAttribute("value").contains("clubwyndham_access")) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Default Value is Club Wyndham Access");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Default Value is not Club Wyndham Access");
			}

			if (driver.findElement(udiBusinessModel).getAttribute("value").equals("1")) {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
						"Default Value is WVO Standard");

			} else {
				tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
						"Default Value is not WVO Standard");
			}

			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLongWait");

		if (driver.findElement(newPointsCheck).getAttribute("value").equals(point)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Buyer contracts inventory point is same as that of seller contract");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Buyer contracts inventory point is not same as that of seller contract");
		}

		if (driver.findElement(inventorySite).getAttribute("value").contains(testData.get("inventorySite"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Default value of inventory site is " + testData.get("inventorySite"));

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Default value of inventory site is not " + testData.get("inventorySite"));
		}

		if (driver.findElement(inventoryPhase).getAttribute("value").contains(testData.get("inventoryPhase"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"Default value of inventory phase is " + testData.get("inventoryPhase"));

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Default value of inventory phase is not " + testData.get("inventoryPhase"));
		}

		if (verifyElementDisplayed(newPoints)) {
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "sp_New_contractFlow", Status.FAIL,
					"NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			new WebDriverWait(driver, 25).until(ExpectedConditions.alertIsPresent());

			Alert alert = driver.switchTo().alert();
			String alertMessage = driver.switchTo().alert().getText();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			alert.accept();

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "sp_New_contractFlow", Status.PASS,
					"User is not able to edit New Points value. Message displayed: " + alertMessage);

		} catch (Exception e) {
			System.out.println("No Alert Present");
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "sp_New_contractFlow", Status.FAIL,
					"User is not displayed with any Popup on editing New Points value");
		}
	}

	public By searchTxtBx = By.xpath("//input[@name='searchValue']");

	public void authorizeContract() {

		driver.navigate().to(testData.get("PaymentManagementURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(searchValue).sendKeys(strContractNo);
		// driver.findElement(searchTxtBx).sendKeys("003951900185 ");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(searchBtn2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(radioOwnerSelect);

		if (driver.findElement(authorizeBtn).isEnabled()) {
			clickElementJS(authorizeBtn);
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "authorizeContract", Status.PASS,
					"Authorize button is present and user clicked on Authorize button ");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "authorizeContract", Status.FAIL,
					"Authorize button not enable");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if ((driver.findElement(contractNumberColumnValue).getText().equals(strContractNo))
				&& (driver.findElement(decisionValue).getText().equalsIgnoreCase("Approved"))) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "authorizeContract", Status.PASS,
					"Contract Authorize Successfully");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "authorizeContract", Status.FAIL,
					"Error in Contract Authorization");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(bckBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(cancelBtn);

	}

	public void transmitContract() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("ViewContractURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(contractNumber).sendKeys(strContractNo);
		// driver.findElement(contractNumber).sendKeys("003951900049 ");

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementJS(searchBtn);
		if (verifyObjectDisplayed(headerContractSummary)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.PASS,
					"User navigated to Contract Summary Page");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.FAIL,
					"User unable to navigate to Contract Summary Page");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(changeStatusBtn);
		if (verifyObjectDisplayed(headerChangeStatus)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.PASS,
					"User navigated to Change status Page");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.FAIL,
					"User unable to navigate to Change status Page");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(transmitBtn);

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String msg = driver.findElement(statusChangeMessage).getText();
		System.out.println("Message " + msg);
		if (msg.contains("Contract status changed successfully")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.PASS,
					"Contract Status change to Transmit");

		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "transmitContract", Status.FAIL,
					"Error in changing the status");
		}
	}

	/*
	 * Method: checkDocStampFee Description:check Doc Stamp Fee Date: Nov/"ExplicitLongWait""ExplicitLongWait"
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	protected By docStampFeeValue = By.xpath("//td[contains(.,'DOC STAMP FEE')]/following-sibling::td[1]");
	protected By netPurchasePriceValue = By.xpath("//td[contains(.,'NET PURCHASE PRICE')]/following-sibling::td[1]");
	protected By serviceFeeValue = By.xpath("//td[contains(.,'SERVICE FEE')]/following-sibling::td[1]");

	public void checkDocStampFee() {
		String value = driver.findElement(netPurchasePriceValue).getText().replaceAll("\\D", "");
		if (Integer.parseInt(driver.findElement(docStampFeeValue).getText().replaceAll("\\D", "")) == 0.35
				* Integer.parseInt(value) / 100) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "checkDocStampFee", Status.PASS,
					"Doc Stamp Fee is $" + 0.35 * Integer.parseInt(value) / 10000);
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "checkDocStampFee", Status.FAIL,
					"Doc Stamp Fee is incorrect");
		}
	}

	/*
	 * Method: checkServiceFee Description:check service Fee Date: Nov/"ExplicitLongWait""ExplicitLongWait"
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void checkServiceFee() {
		String value = driver.findElement(serviceFeeValue).getText().trim().replaceAll("\\D", "");
		if (Integer.parseInt(value) == 8000) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "checkDocStampFee", Status.PASS,
					"Service Fee is $80");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "checkDocStampFee", Status.FAIL,
					"Service Fee is not $80.00");
		}
	}

	protected By memberNumber = By.name("memberNo");

	public void resaleContractSelection_MultiContract() {
		String corrspContractNumber = "";
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver.findElements(By.xpath(
				"//td[contains(text(),'Available')]/preceding-sibling::td[contains(text(),'Y')]/..//input[@id='resaleContractIdRadio']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection",
							Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver.findElement(
							By.xpath("(//td[contains(text(),'Available')]/preceding-sibling::td[contains(text(),'Y')])["
									+ (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElementJS(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");

		clickElementJS(proceedBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(customerNo)) {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Contract Processing - Locate Customer Page");
		} else {
			tcConfig.updateTestReporter("SalePointResaleContractSelection", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Contract Processing - Locate Customer Page");
		}

	}

	protected By splitIndicator = By.xpath("//td[contains(.,'Split Indicator')]");
	protected By thirdPartyIndicator = By.xpath("//td[contains(.,'Third Party Indicator')]");
	protected By tscField = By.xpath("//input[@name='tsc']");

	public void validateSplitAndThirdPartyIndicator() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointResaleContractSelection", "contractFlow", Status.FAIL,
					"Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			clickElementJSWithWait(statusLink);
		} catch (Exception e) {
			System.out.println("No status link");
		}

		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(getElementText(splitIndicator).trim().contains("Y"),
				"Eitehr Split Indicator is not present or value is incorrect");
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "validateSplitAndThirdPartyIndicator",
				Status.PASS, "Split Indicator value is Y");

		Assert.assertTrue(
				getElementText(thirdPartyIndicator).trim().contains("Y")
						|| getElementText(thirdPartyIndicator).contains(""),
				"Eitehr Third Party Indicator is not present or value is incorrect");
		tcConfig.updateTestReporter("SalePointResaleContractSelection", "validateSplitAndThirdPartyIndicator",
				Status.PASS, "Third Party Indicator value is correctly displayed");

	}

}
