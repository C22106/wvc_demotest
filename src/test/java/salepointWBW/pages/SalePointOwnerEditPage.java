package salepointWBW.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointOwnerEditPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);

	public SalePointOwnerEditPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//select[@name='relationship']")
	protected WebElement relationshipStatus;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleType;

	@FindBy(xpath = "//input[@name='ssn']")
	protected WebElement ssnField;

	@FindBy(xpath = "//select[@name='dobmonth']")
	protected WebElement MonthFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobday']")
	protected WebElement DateFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobyear']")
	protected WebElement YearFromDrpDwn;

	@FindBy(xpath = "//input[@class='button' and @id='primaryOwner']")
	protected WebElement primaryOwnerEdit;

	@FindBy(xpath = "//input[@class='button' and @id='loan']")
	protected WebElement PaymentEdit;

	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	protected WebElement home_Phone_fld;

	@FindBy(xpath = "//input[@name='sex' and @value='M']")
	protected WebElement genderSelect;

	@FindBy(xpath = "//input[@value='Update']")
	protected WebElement updateVal;

	/*
	 * Method: sp_WVR_Owner_Edit_Form Description:Edit Form Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */
	public void sp_WVR_Owner_Edit_Form() throws Exception {

		try {
			// select the relationship
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(relationshipStatus).selectByIndex(2);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// select the gender
			clickElementJS((genderSelect));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// select sale type
			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");
				clickElementJS(saleType);
				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				System.out.println("SaleType is not visible ");

			}

			// ssn field
			if (ssnField.isEnabled() == false) {
				System.out.println("SSN Not Required User Can Proceed");

			} else {
				if (ssnField.getAttribute("value").isEmpty() == true) {
					ssnField.clear();
					ssnField.sendKeys(testData.get("ssnNumber"));
				}
			}

			// DateOf Birth
			String dbMonth = "", dbDay = "", dbYear = "";

			String dob = testData.get("DOB");

			String arr[] = dob.split("/");

			// MonthFormation
			if (arr[0].length() == 1) {
				dbMonth = "0" + arr[0];
			} else {
				dbMonth = arr[0];
			}
			// DayFormation
			if (arr[1].length() == 1) {
				dbDay = "0" + arr[1];
			} else {
				dbDay = arr[1];
			}
			// Year
			dbYear = arr[2];

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (MonthFromDrpDwn.getAttribute("value").equals("")) {
				new Select(MonthFromDrpDwn).selectByValue(dbMonth);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (DateFromDrpDwn.getAttribute("value").equals("")) {
				new Select(DateFromDrpDwn).selectByValue(dbDay);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (YearFromDrpDwn.getAttribute("value").equals("")) {
				new Select(YearFromDrpDwn).selectByValue(dbYear);
			}

			// Home Phone Number
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (home_Phone_fld.getAttribute("value").equals("")) {
				home_Phone_fld.sendKeys(testData.get("homeNumber"));
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// Click on Next
			clickElementJS(updateVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			System.out.println("Wrong Value Entered");
		}

	}
}
