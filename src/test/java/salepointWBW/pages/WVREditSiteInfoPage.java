package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WVREditSiteInfoPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WVREditSiteInfoPage.class);

	public WVREditSiteInfoPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@value='Modify']")
	protected List<WebElement> editSiteModifyCTA;

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Split Information')]")
	protected WebElement splitInformation;

	@FindBy(xpath = "//select[@id='optOutEnabled_id']")
	protected WebElement optOutDropDown;

	@FindBy(xpath = "//select[@id='optOutTerm_id']")
	protected WebElement optOutTerm;

	@FindBy(xpath = "//input[@value='Save']")
	protected WebElement saveButton;

	@FindBy(xpath = "//div[@class='success_elem']")
	protected WebElement successMessage;

	// td[contains(.,'LIMITED EDITION')]//following-sibling::td[@value='Modify']

	/*
	 * Method: editSiteInfoNavigation Edit Site Information Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void editSiteInfoNavigation() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("EditSiteInfoURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisible(driver, editSiteModifyCTA.get(0), "ExplicitMedWait");
		Assert.assertTrue(verifyElementDisplayed(editSiteModifyCTA.get(0)), "Edit Site Info page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated To Edit Site Info Page");

	}

	/*
	 * Method: selectSite Description:Click Site to be modified Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectSite() {
		String getSiteName = testData.get("Location_Code");
		clickElementJSWithWait(By.xpath("//input[contains(@onclick,'"+getSiteName+"')]"));
		//clickElementJSWithWait(By.xpath("//table/tbody/tr/td[contains(.,'"+getSiteName+"')]//following-sibling::td/input[@value='Modify']"));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Clicked " + getSiteName + " to Modify");
	}

	/*
	 * Method: clickSaveButton Description:Click Save Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickSaveButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(saveButton);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present after Login");
		}
		waitUntilElementVisibleIE(driver, successMessage, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(successMessage), "Site Info couldn't be Saved Successfully");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Site Info Saved Successfully");
	}
	
	/*
	 * Method: selectOptOut Description:Click Opt Out and select  Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectOptOut() {
		waitUntilElementVisibleIE(driver, optOutDropDown, "ExplicitMedWait");
		String getOptOut = testData.get("selectOptOut");
		selectByText(optOutDropDown,getOptOut);
	}
	
	/*
	 * Method: selectSite Description:Click Opt Out term and selectDate: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectOptOutTerm() {
		String getOptOutTerm = testData.get("selectOptOutTerm");
		selectByText(optOutTerm,getOptOutTerm);
	}

	

}