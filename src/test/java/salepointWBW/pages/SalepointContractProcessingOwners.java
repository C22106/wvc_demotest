package salepointWBW.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointContractProcessingOwners extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractProcessingOwners.class);

	public SalepointContractProcessingOwners(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	protected WebElement fieldHomePhone;
	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Owners')]")
	protected WebElement headerContractProcessingOwner;
	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNoCheck;
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelect;
	@FindBy(xpath = "//td[contains(.,'CC Auto')]/input[@type='radio']")
	protected WebElement ccSelect;
	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;
	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Owners')]")
	protected WebElement headerContractProcessOwner;
	@FindBy(xpath = "//div[contains(text(),'Edit Primary Owner')]")
	protected WebElement editPrimaryOwner;
	@FindBy(xpath = "//input[@id='primaryOwner']")
	protected WebElement editPOwner;
	@FindBy(xpath = "//input[@id='fairshare']")
	protected WebElement fairshare;
	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteOwner;
	@FindBy(xpath = "//a[contains(.,'Status')]")
	protected WebElement StatusOwner;
	@FindBy(xpath = "//select[@name='dobmonth']")
	protected WebElement MonthFromDrpDwn;
	@FindBy(xpath = "//select[@name='dobday']")
	protected WebElement DateFromDrpDwn;
	@FindBy(xpath = "//select[@name='dobyear']")
	protected WebElement YearFromDrpDwn;
	@FindBy(xpath = "//input[@class='button' and @id='primaryOwner']")
	protected WebElement primaryOwnerEdit;
	@FindBy(xpath = "//input[@class='button' and @id='loan']")
	protected WebElement PaymentEdit;
	@FindBy(xpath = "//input[@class='button' and @value='Update']")
	protected WebElement updateButton;
	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	protected WebElement home_Phone;
	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	protected WebElement home_Phone_fld;
	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteListOfOwner;
	@FindBy(xpath = "(//a[contains(.,'Status')])[1]")
	protected WebElement statusListOfOwner;
	@FindBy(xpath = "//input[@name='ssn']")
	protected WebElement ssnSelect;
	@FindBy(xpath = "//select[@name='dobmonth']")
	protected WebElement monthFromDropDown;
	@FindBy(xpath = "//select[@name='dobday']")
	protected WebElement dayFromDropDown;
	@FindBy(xpath = "//select[@name='dobyear']")
	protected WebElement yearFromDropDown;
	@FindBy(xpath = "//input[@name='sex' and @value='M']")
	protected WebElement genderSelect;
	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow']//td[1]")
	protected WebElement pName;
	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleType;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;
	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Customer Information')]")
	protected WebElement headerCustomerInformation;
	@FindBy(xpath = "//form[@name = 'contractsListOwners']")
	protected WebElement chcekOwner;
	@FindBy(xpath = "//input[@name='memberNumber']")
	protected WebElement memberNoTxtBx;
	@FindBy(xpath = "//select[@name='title']")
	protected WebElement titleDrpdwn;
	@FindBy(xpath = "//input[@name='first_name']")
	protected WebElement firestnameTxtBx;
	@FindBy(xpath = "//input[@name='last_name']")
	protected WebElement lastnameTxtBx;
	@FindBy(xpath = "//input[@name='address1']")
	protected WebElement address1TxtBx;
	@FindBy(xpath = "//input[@name='city']")
	protected WebElement cityTxtBx;
	@FindBy(xpath = "//select[@name='state']")
	protected WebElement stateDrpdwn;
	@FindBy(xpath = "//input[@name='postalCode']")
	protected WebElement postalCodeTxtBx;
	@FindBy(xpath = "//input[@name='country']")
	protected WebElement countryTxtBx;
	@FindBy(xpath = "//input[@name='home_phone']")
	protected WebElement homePhnTxtBx;
	@FindBy(xpath = "//input[@name='work_phone']")
	protected WebElement wrkPhnTxtBx;
	@FindBy(xpath = "//input[@name='emailAddress']")
	protected WebElement emailTxtBx;
	@FindBy(xpath = "//div[@id = 'error']")
	protected WebElement errorMessage;
	@FindBy(xpath = "//div[contains(text(),'Processing - Owners']")
	protected WebElement pageTitleOwners;

	@FindBy(xpath = "(//a[contains(.,'Status')])[1]")
	protected WebElement secStatus;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwner;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteOwnerObj;

	/*
	 * Method: checkPrimaryandSecondaryOwner Description:Chck for Primary and
	 * Secondary Owner Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void checkPrimaryandSecondaryOwner() {
		if (verifyElementDisplayed(chcekOwner)) {
			try {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisible(driver, chcekOwner, "ExplicitMedWait");
				List<WebElement> secOwnlist = (deleteOwner);
				int noOfSecOwn = secOwnlist.size();
				if (verifyElementDisplayed(chcekOwner)) {
					String Pname = pName.getText().trim();
					if (Pname == null || Pname.isEmpty()) {
						if (noOfSecOwn == 1) {
							clickElementJSWithWait(statusListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							for (int i = 0; i < (noOfSecOwn - 1); i++) {
								clickElementJSWithWait(deleteListOfOwner);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.switchTo().alert().accept();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
							clickElementJSWithWait(statusListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
					} else {
						for (int i = 0; i < (noOfSecOwn); i++) {
							clickElementJSWithWait(deleteListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
					}
				}

			} catch (Exception e) {
				log.info(e);
			}
		}
	}

	/*
	 * Method: contractProcessingOwnersNew Description:Contract Processing Nav
	 * Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contractProcessingOwnersNew() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerCustomerInformation));
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String memberType = testData.get("ExistingOwner");
			if (memberType.equalsIgnoreCase("Yes")) {

				(memberNoTxtBx).sendKeys(testData.get("ExistingMemberNo"));
			} else {
				log.info("Member number not required for New Owner");
			}

			try {
				if (!(ssnSelect).isEnabled() == false) {
					(ssnSelect).sendKeys(testData.get("ssnNumber"));
				}
			} catch (Exception e) {
				log.info(e);
			}

			try {
				if (!(fieldHomePhone).isEnabled() == false) {
					fieldDataEnter(fieldHomePhone, testData.get("homePhone"));
				}
			} catch (Exception e) {
				log.info(e);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select((titleDrpdwn)).selectByVisibleText("Mr.");

			(firestnameTxtBx).click();
			(firestnameTxtBx).clear();
			(firestnameTxtBx).sendKeys(testData.get("firstName"));

			(lastnameTxtBx).click();
			(lastnameTxtBx).clear();
			(lastnameTxtBx).sendKeys(testData.get("lastName"));

			(address1TxtBx).click();
			(address1TxtBx).clear();
			(address1TxtBx).sendKeys(testData.get("address1"));

			(cityTxtBx).click();
			(cityTxtBx).clear();
			(cityTxtBx).sendKeys(testData.get("city"));

			new Select((stateDrpdwn)).selectByVisibleText(testData.get("state"));

			(postalCodeTxtBx).click();
			(postalCodeTxtBx).clear();
			(postalCodeTxtBx).sendKeys(testData.get("postalCode"));

			(countryTxtBx).click();
			(countryTxtBx).clear();
			(countryTxtBx).sendKeys(testData.get("country"));

			(wrkPhnTxtBx).click();
			(wrkPhnTxtBx).clear();
			(wrkPhnTxtBx).sendKeys(testData.get("workPhone"));

			(emailTxtBx).click();
			(emailTxtBx).clear();
			(emailTxtBx).sendKeys(testData.get("emailAddress"));

			if ((monthFromDropDown).isEnabled()) {
				selectByIndex(monthFromDropDown, 2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				selectByIndex(dayFromDropDown, 2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				selectByIndex(yearFromDropDown, 4);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait((genderSelect));
			clickElementJSWithWait((saleType));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextVal);
			try {
				WebDriverWait wait = new WebDriverWait(driver, 20);
				wait.until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();

			} catch (Exception e) {
				log.info("no Alert is displayed");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, headerContractProcessingOwner, "ExplicitLongWait");
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("SalepointContractProcessingOwners", "contractProcessingCustomerInformation",
					Status.PASS, "Customer Info page saved succesfully");

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalepointContractProcessingOwners", "contractProcessingCustomerInformation",
					Status.FAIL, "Error in filling Contract Processing - Customer Information");
		}
	}

	/*
	 * Method: errorMessageValidation Description:Validate Error Message Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void errorMessageValidation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkPrimaryandSecondaryOwner();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerCustomerInformation));
		try {
			(firestnameTxtBx).click();
			(firestnameTxtBx).clear();
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			try {
				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Error Alert displayed as Mandatory Field are not filled");
			} catch (Exception e) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Error Alert not displayed even if Mandatory Field are not filled");
			}
			waitUntilElementVisibleIE(driver, errorMessage, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(errorMessage), "Error Message not displayed");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Error Message Displayed");
		} catch (Exception e) {
			log.info("Error: " + e);
		}
	}

	/*
	 * Method: validateInvalidSSN_Error Description:Invalid SSN Error Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void validateInvalidSSN_Error() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkPrimaryandSecondaryOwner();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerCustomerInformation));
		try {
			if (!(ssnSelect).isEnabled() == false) {
				(ssnSelect).click();
				(ssnSelect).clear();
				(ssnSelect).sendKeys(testData.get("ssnNumber"));
			} else {
				tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
						Status.FAIL, "SSN Field Not Enabled");
			}

			try {
				if (!(fieldHomePhone).isEnabled() == false) {
					fieldDataEnter(fieldHomePhone, testData.get("homePhone"));
				}
			} catch (Exception e) {
				log.info(e);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select((titleDrpdwn)).selectByVisibleText("Mr.");

			(firestnameTxtBx).click();
			(firestnameTxtBx).clear();
			(firestnameTxtBx).sendKeys(testData.get("firstName"));

			(lastnameTxtBx).click();
			(lastnameTxtBx).clear();
			(lastnameTxtBx).sendKeys(testData.get("lastName"));

			(address1TxtBx).click();
			(address1TxtBx).clear();
			(address1TxtBx).sendKeys(testData.get("address1"));

			(cityTxtBx).click();
			(cityTxtBx).clear();
			(cityTxtBx).sendKeys(testData.get("city"));

			new Select((stateDrpdwn)).selectByVisibleText(testData.get("state"));

			(postalCodeTxtBx).click();
			(postalCodeTxtBx).clear();
			(postalCodeTxtBx).sendKeys(testData.get("postalCode"));

			(countryTxtBx).click();
			(countryTxtBx).clear();
			(countryTxtBx).sendKeys(testData.get("country"));

			(wrkPhnTxtBx).click();
			(wrkPhnTxtBx).clear();
			(wrkPhnTxtBx).sendKeys(testData.get("workPhone"));

			(emailTxtBx).click();
			(emailTxtBx).clear();
			(emailTxtBx).sendKeys(testData.get("emailAddress"));

			if ((monthFromDropDown).isEnabled()) {
				selectByIndex(monthFromDropDown, 2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				selectByIndex(dayFromDropDown, 2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				selectByIndex(yearFromDropDown, 4);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait((genderSelect));
			clickElementJSWithWait((saleType));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				if (driver.switchTo().alert().getText().contains("valid Social Security Number")) {
					driver.switchTo().alert().accept();
					tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
							Status.PASS, "Error Alert displayed as SSN Field is Invalid");
				} else {
					tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
							Status.FAIL, "Invalid message in alert displayed and message is: "
									+ driver.switchTo().alert().getText());
					driver.switchTo().alert().accept();

				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
						Status.FAIL, "Error Alert not displayed even if SSN Field is invalid");
			}
			waitUntilElementVisibleIE(driver, errorMessage, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(errorMessage), "Error Message not displayed");
			if (errorMessage.getText().contains("valid Social Security Number")) {
				tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
						Status.PASS, "Error Message Displayed for invalid SSN");
			} else {
				tcConfig.updateTestReporter("SalepointContractProcessingOwners", "validateInvalidSSN_Error",
						Status.FAIL, "Invalid Error Message not displayed for SSN");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Error in filling Contract Processing - Customer Information");
		}
	}

	public void changeStatus() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		int noOfSecOwn = 0;
		waitUntilElementVisibleIE(driver, deleteOwnerObj, "ExplicitLongWait");
		if (verifyElementDisplayed(deleteOwnerObj)) {
			noOfSecOwn = deleteOwner.size();
		}

		// check primary owner

		if (verifyElementDisplayed(pName)) {
			// strOwnerName=pName.getText();
			System.out.println("Primary Owner Exists");
			// delete all secondary owner
			for (int i = 1; i < noOfSecOwn; i++) {
				clickElementJSWithWait(deleteFirstOwner);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} else {

			log.info("No Primary Owner Exists");
			// delete all secondary owner except one

			for (int i = 0; i < (noOfSecOwn - 1); i++) {
				clickElementJSWithWait(deleteListOfOwner);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			// make one secondary to primary

			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, secStatus, "ExplicitLongWait");
		clickElementJS(secStatus);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {
				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Secondary Owner swap" + " message is getting displayed");

			} catch (Exception e) {
				log.info("no Alert is displayed");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
					"Secondary Owner Pop Up is not visible");
		}

	}

}
