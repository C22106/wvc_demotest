package salepointWBW.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointWVR extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointWVR.class);

	public SalePointWVR(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//select[@id='udi_subtype_id']")
	protected WebElement salesubtype;

	@FindBy(xpath = "//input[@name='ok']")
	protected WebElement okBtn;

	@FindBy(name = "tourID")
	protected WebElement tourID;

	@FindBy(name = "tourTime")
	protected WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement next1;

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteSec;

	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	protected WebElement generate;

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	protected WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNo;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(id = "customerNumber")
	protected WebElement customerNo;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiSelect;

	@FindBy(xpath = "//input[@name='sales_types' and @value='prestige_club']")
	protected WebElement CWPR;

	@FindBy(id = "Print")
	protected WebElement print;

	@FindBy(id = "newPoints")
	protected WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculate;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;

	@FindBy(id = "customerNumber")
	protected WebElement customerNoInput;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleTypeSelect;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	List<WebElement> deleteCheck;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiClick;

	@FindBy(id = "newPoints")
	protected WebElement newPointsCheck;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculateClick;

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement salePersonSelect;

	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement saveClick;

	@FindBy(id = "Print")
	protected WebElement printClick;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNoCheck;

	@FindBy(xpath = "//input[@name='ssn']")
	protected WebElement ssnSelect;

	@FindBy(xpath = "//input[@name='dobmonth']")
	protected WebElement dobMonth;

	@FindBy(xpath = "//input[@name='dobday']")
	protected WebElement dobDay;

	@FindBy(xpath = "//input[@name='dobyear']")
	protected WebElement dobYear;

	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Owners')]")
	protected WebElement headerContractProcessOwner;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Customer Information')]")
	protected WebElement headerCustInfo;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteOwner;

	@FindBy(xpath = "//a[contains(.,'Status')]")
	protected WebElement StatusOwner;

	@FindBy(xpath = "//select[@name='dobmonth']")
	protected WebElement MonthFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobday']")
	protected WebElement DateFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobyear']")
	protected WebElement YearFromDrpDwn;

	@FindBy(xpath = "//a[text() = 'Delete'][0]")
	protected WebElement textDelete;

	@FindBy(xpath = "//a[text() = 'Delete']")
	protected WebElement totalSecondaryOwner;

	@FindBy(xpath = "//a[text() = 'Status']")
	protected WebElement textStatus;

	@FindBy(xpath = "//input[@name='memberNumber']")
	protected WebElement existingMemberNumber;

	@FindBy(xpath = "//input[@value='rciWeek']")
	protected WebElement rciRadioBtn;

	@FindBy(xpath = "//input[@name='sex' and @value='F']")
	protected WebElement genderSelectRadio;

	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelectRadio;

	@FindBy(xpath = "//a[text()='Status']")
	protected List<WebElement> statusEle;

	@FindBy(xpath = "//input[@name='member_number']")
	protected WebElement discoveryMember;

	@FindBy(xpath = "//input[@name='search']")
	protected WebElement searchBtn;

	@FindBy(xpath = "//input[@value='Process Upgrade']")
	protected WebElement upgradeButton;

	@FindBy(xpath = "//input[@name='contract_number']")
	protected WebElement tradeContract;

	@FindBy(xpath = "//input[@value='View Traded Contracts']")
	protected WebElement viewtradeContracts;

	@FindBy(xpath = "//input[@value='Process Trades']")
	protected WebElement processTrade;

	@FindBy(xpath = "//input[@value='Experience']")
	protected WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	protected WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	protected WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	protected WebElement finalizeButton;

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	protected WebElement selectAllByDate;

	@FindBy(xpath = "//select[@name='startmonth']")
	protected WebElement startmonth;

	@FindBy(xpath = "//select[@name='startday']")
	protected WebElement startday;

	@FindBy(xpath = "//input[@name='getData']")
	protected WebElement getData;

	@FindBy(xpath = "//input[@value='Change Status']")
	protected WebElement changeStatus;

	@FindBy(xpath = "//input[@value='Transmit']")
	protected WebElement Transmit;

	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	protected WebElement returnBack;

	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	protected WebElement validateStatus;

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	protected WebElement searchFilter;

	@FindBy(xpath = "//input[@value='Change Status']")
	protected WebElement statusChangeClick;

	@FindBy(xpath = "//input[@value='Transmit']")
	protected WebElement transmitClick;

	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	protected WebElement reportBack;

	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	protected WebElement logStatusVali;

	@FindBy(id = "customerNumber")
	protected WebElement DiscoverycustomerNo;

	@FindBy(xpath = "//select[@name='relationship']")
	protected WebElement Title_to_be_taken;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement DiscoversaleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='Discovery']")
	protected WebElement DiscoversaleTypeselect;

	@FindBy(xpath = "//select[@name='invPkg']")
	protected WebElement Discoverpackageselect;

	@FindBy(xpath = "//input[@name='loan' and @value='loan_cc']")
	protected WebElement DiscoveryLoanPaymentMethod;

	@FindBy(xpath = "//input[@name='cc_payment_member_name1']")
	protected WebElement DiscoveryNameoncard;

	@FindBy(xpath = "//input[@id='cc_account_number1']")
	protected WebElement DiscoveryAccno;

	@FindBy(xpath = "//select[@name='cc_exp_month1']")
	protected WebElement DiscoveryEXPmonth;

	@FindBy(xpath = "//select[@name='cc_exp_year1']")
	protected WebElement DiscoveryEXPyear;

	@FindBy(xpath = "//input[@class='button' and @value='Save and Print']")
	protected WebElement DiscoverySaveandprint;

	@FindBy(xpath = "//button[@id='Print']")
	protected WebElement Discoveryprint;

	/*
	 * Method: sp_New_ContractFlow Description:New Contract Flow Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_New_ContractFlow() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"New Contract Page Navigation Error");

		}

		if (testData.get("Secondry_owner").contains("Y")) {

			List<WebElement> Stat = statusEle;
			log.info(Stat.size());
			clickElementJSWithWait(Stat.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().alert().accept();
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {
				if (testData.get("Secondry_owner").contains("N")) {

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					existingMemberNumber.click();
					existingMemberNumber.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					existingMemberNumber.sendKeys(testData.get("ExistingMemberNo"));
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					existingMemberNumber.click();
					existingMemberNumber.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					existingMemberNumber.sendKeys(testData.get("ExistingMemberNo"));
				}
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				log.info("Check if Page Down done");
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			// nextVal.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ssnSelect.click();
				ssnSelect.clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ssnSelect.sendKeys(testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(genderSelectRadio);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitMedWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = deleteCheck;

				clickElementJSWithWait(list.get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			log.info("Owner Page Not Displayed");
		}
		// clickElementJS(nextVal);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLowWait");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// added 7/20/2020

			new Select(salesubtype).selectByVisibleText(testData.get("ContractType"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (testData.get("RCI").contains("Y")) {
				if (verifyElementDisplayed(rciRadioBtn)) {
					clickElementJSWithWait(rciRadioBtn);
				} else {
					tcConfig.updateTestReporter("SalepointContractDataEntryPage",
							"contractProcessingDataEntrySelection", Status.FAIL, "RCI radioBtn not found");
				}
			}
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(newPoints)) {
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		if (verifyObjectDisplayed(cashSelectRadio)) {
			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLowWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitMedWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Select sel = new Select(driver.findElement(By.id("justification")));
			sel.selectByValue("System Issue");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: selectCheckBoxPIC Description:Select PIC radio Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */ public void sp_discovery_contractUpgrade() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, discoveryMember, "ExplicitLowWait");

		if (verifyObjectDisplayed(discoveryMember)) {

			discoveryMember.click();
			discoveryMember.sendKeys(testData.get("discoveryMember"));

			searchBtn.click();

			waitUntilElementVisibleIE(driver, upgradeButton, "ExplicitLowWait");

			upgradeButton.click();

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Discovery Page Navigation Error");
		}

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ssnSelect.click();
				ssnSelect.clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ssnSelect.sendKeys(testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(genderSelectRadio);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = deleteCheck;

				clickElementJSWithWait(list.get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			log.info("Owner Page Not Displayed");
		}
		// clickElementJS(nextVal);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLowWait");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(newPoints)) {
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		if (verifyObjectDisplayed(cashSelectRadio)) {
			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLowWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitMedWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: sp_trade_contract Description:Trade Contract Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_trade_contract() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, tradeContract, "ExplicitLowWait");

		if (verifyObjectDisplayed(tradeContract)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tradeContract.click();
			tradeContract.sendKeys(testData.get("tradeMember"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

			try {

				driver.switchTo().alert().accept();

				log.info("Alert Present Member Needs to be changed");

			} catch (Exception e) {
				log.info("No Alert");
			}

			waitUntilElementVisibleIE(driver, viewtradeContracts, "ExplicitLowWait");

			clickElementJS(viewtradeContracts);

			waitUntilElementVisibleIE(driver, processTrade, "ExplicitLowWait");

			clickElementJS(processTrade);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			try {
				driver.switchTo().alert().dismiss();

			} catch (Exception e) {
				log.info("No Alert");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "trade Page Navigation Error");

		}

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ssnSelect.click();
				ssnSelect.clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ssnSelect.sendKeys(testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(genderSelectRadio);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = deleteCheck;

				for (int i = 0; i < list.size(); i++) {

					clickElementJSWithWait(list.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} else {
				log.info("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			log.info("Owner Page Not Displayed");
		}
		// clickElementJS(nextVal);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLowWait");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(newPoints)) {
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		if (verifyObjectDisplayed(cashSelectRadio)) {
			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLowWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitMedWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: sp_New_contractSearch Description:New Contacrt Creation Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_New_contractSearch() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, searchFilter, "ExplicitLowWait");

		if (verifyElementDisplayed(selectAllByDate)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract in batch report Page Displayed");

			clickElementJS(selectAllByDate);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// LocalDate today= LocalDate.now();
			Calendar cal = Calendar.getInstance();
			int monthVal = (cal.get(Calendar.MONTH));

			String strMonth = String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();

			waitUntilElementVisibleIE(driver, By.name("startmonth"), "ExplicitLowWait");

			new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);

			new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(getData);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"),
					"ExplicitLowWait");

			if (verifyElementDisplayed(
					driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]")))) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Contract No Verified and is: " + strContractNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract No Not Verified");

			}

			waitUntilElementVisibleIE(driver, statusChangeClick, "ExplicitMedWait");

			if (verifyElementDisplayed(changeStatus)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Summary Page Navigation Successful and Verified");

				clickElementJSWithWait(changeStatus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleIE(driver, transmitClick, "ExplicitLowWait");

				if (verifyElementDisplayed(Transmit)) {

					clickElementJSWithWait(Transmit);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();

					waitUntilElementVisibleIE(driver, reportBack, "ExplicitLowWait");

					if (verifyElementDisplayed(returnBack)) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
								"Status Change Successful");
						clickElementJSWithWait(returnBack);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
								"Status Change UnSuccessful");
					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, logStatusVali, "ExplicitLowWait");

					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'T')]"));

					if (list2.get(0).getText().contains("T")) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
								"Contract Transmitted");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
								"Contract did not Transmitted");

					}

				}

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	/*
	 * Method: wvr_Discovercontract_Creation Description:Discovery Creation
	 * Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void wvr_Discovercontract_Creation() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, DiscoverycustomerNo, "ExplicitLowWait");
		if (verifyElementDisplayed(DiscoverycustomerNo)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Locate Customer  Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));
			clickElementJS(nextVal);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Error while navigating to Contract Processing - Locate Customer Page");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyElementDisplayed(Title_to_be_taken)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Customer Information  Navigation Successful");
			Title_to_be_taken.click();
			Select title = new Select(Title_to_be_taken);
			title.selectByIndex(6);
			DiscoversaleType.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Customer information entered");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Processing - Customer Information  Navigation Failed");
		}
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Owners   Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Processing - Owners   Navigation Failed");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(DiscoversaleTypeselect)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Data Entry Selection    Navigation Successful");
			DiscoversaleTypeselect.click();
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Processing - Data Entry Selection    Navigation Failed");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(Discoverpackageselect)) {
			log.info("Entered in package");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Discovery Package Selection Navigation Successful");
			Discoverpackageselect.click();
			Select discoverypackage = new Select(Discoverpackageselect);
			discoverypackage.selectByIndex(1);
			clickElementJS(nextVal);
		} else {
			log.info("package selection failed");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Processing - Discovery Package Selection Navigation Failed");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(DiscoveryLoanPaymentMethod)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Payment Options  Navigation Successful");
			DiscoveryLoanPaymentMethod.click();
			if (verifyElementDisplayed(DiscoveryNameoncard)) {
				DiscoveryNameoncard.click();
				DiscoveryNameoncard.sendKeys(testData.get("Account_holder_name"));
				DiscoveryAccno.click();
				DiscoveryAccno.sendKeys(testData.get("Card_Number"));
				DiscoveryEXPmonth.click();
				Select expmnth = new Select(DiscoveryEXPmonth);
				expmnth.selectByIndex(3);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				DiscoveryEXPyear.click();
				Select expyear = new Select(DiscoveryEXPyear);
				expyear.selectByIndex(5);
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Payment Details Entered");
				clickElementJS(nextVal);
				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

					Alert alert = driver.switchTo().alert();
					alert.accept();
				} catch (Exception e) {
					log.info("No Alert Prersent");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Error while entering Payment Details");
			}
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Payment Options  Navigation Failed");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Processing - Money Screen    Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Processing - Money Screen Navigation Failed");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);

		waitUntilElementVisibleIE(driver, DiscoverySaveandprint, "ExplicitLowWait");

		DiscoverySaveandprint.click();
		waitForSometime(tcConfig.getConfig().get("medWait"));

		waitUntilElementVisibleIE(driver, Discoveryprint, "ExplicitMedWait");

		if (verifyElementDisplayed(Discoveryprint)) {
			log.info("discovery print button clicked");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"SalePoint Print Summary Page navigated successfully");

			Discoveryprint.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"SalePoint Print Summary Page navigated failed");

		}

		if (verifyElementDisplayed(contractNo)) {

			strContractNo = contractNo.getText();
			log.info(strContractNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "strcontractNo");

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract number could not be retrieved");
		}

	}

	@FindBy(xpath = "//input[@name='conversionType' and @value='ev_club']")
	protected WebElement Convert_to_CWP;

	@FindBy(xpath = "//input[@name='conversionType' and @value='udi']")
	protected WebElement Convert_to_UDI;

	@FindBy(xpath = "//input[@name='contract_number']")
	protected WebElement Converted_contract;

	@FindBy(xpath = "//input[@name='partners' and @value='rciWeek']")
	protected WebElement RCI_information;

	@FindBy(xpath = "//input[@name='conversionFee']")
	protected WebElement Conversion_fee;

	@FindBy(xpath = "//input[@name='processingFee']")
	protected WebElement Processing_fee;

	@FindBy(xpath = "//input[@name='process' and @value='Process Conversions']")
	protected WebElement Process_conversion_btn;

	@FindBy(xpath = "//input[@class='button' and @value='Print']")
	protected WebElement print_btn;

	@FindBy(xpath = "//input[@id='Print']")
	protected WebElement print1;

	@FindBy(xpath = "//td[contains(.,'The Conversion Contract Number is')]")
	protected WebElement contractNo;

	/*
	 * Method: fixed_to_cwp_conversion Description:Fixed Week to CWP Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void fixed_to_cwp_conversion() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, Convert_to_CWP, "ExplicitLowWait");
		if (verifyElementDisplayed(Convert_to_CWP)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Page Navigation Successful");
			Convert_to_CWP.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Converted_contract.click();
			Converted_contract.sendKeys(testData.get("Converted_contract"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Page Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Fixed Week Conversions Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Fixed Week Conversions Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Primary Owner Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Primary Owner Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversion Contract Owners Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversion Contract Owners Navigation Failed");
		}
		if (verifyElementDisplayed(RCI_information)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Partners Information Navigation Successful");
			RCI_information.click();
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Partners Information Navigation Failed");
		}
		if (verifyElementDisplayed(Conversion_fee)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Convert Fixed Week to FairShare Plus  Navigation Successful");
			Conversion_fee.click();
			Conversion_fee.clear();
			Conversion_fee.sendKeys(testData.get("Conversion_fee"));
			Processing_fee.click();
			Processing_fee.clear();
			Processing_fee.sendKeys(testData.get("Processing_fee"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Fees Entered");
			Process_conversion_btn.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Convert Fixed Week to FairShare Plus  Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Fixed Week to FairShare Plus  Navigation Successful");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Fixed Week to FairShare Plus  Navigation Failed");
		}
		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");
		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(print_btn)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Summary Navigation Successful");
			print_btn.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Summary Navigation Failed");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Discoveryprint, "ExplicitMedWait");
		if (verifyElementDisplayed(Discoveryprint)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"SalePoint Print Summary Page navigated successfully");

			Discoveryprint.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"SalePoint Print Summary Page navigated failed");

		}

		if (verifyElementDisplayed(contractNo)) {

			strContractNo = contractNo.getText();
			log.info(strContractNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Number Is: " + strContractNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract number could not be retrieved");
		}

	}

	public By contractNumtxtfld = By.xpath("//input[@name='contract_number']");
	public By searchBtntrans = By.xpath("//input[@name='search']");
	public By ViewContrct_page = By.xpath("//div[contains(text(),'View Contract')]");
	public By ViewSummary_page = By.xpath("//div[contains(text(),'Contract Summary')]");
	public By Change_status = By.xpath("//input[contains(@value,'Change Status')]");
	public By Viewcontrctsummary_page = By.xpath("//div[contains(text(),'View Contract - Change Status')]");
	public By transmitBtn = By.xpath("//input[contains(@value,'Transmit')]");
	public By successmsg = By.xpath("//td[contains(text(),'Contract status changed successfully.')]");

	/*
	 * Method: transmit Description:Transmit Contract Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */
	public void transmit() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("Transmit_URL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, ViewContrct_page, "ExplicitLowWait");

		if (verifyObjectDisplayed(contractNumtxtfld)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					" Contract Page Navigation Successful");
			driver.findElement(contractNumtxtfld).click();
			driver.findElement(contractNumtxtfld).sendKeys(strContractNo);

			clickElementJS(searchBtntrans);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Page Navigation Error");

		}

		waitUntilElementVisibleIE(driver, ViewSummary_page, "ExplicitLowWait");

		if (verifyObjectDisplayed(Change_status)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					" Summary Page Navigation Successful");

			clickElementJS(Change_status);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Summary Page Navigation Error");

		}
		waitUntilElementVisibleIE(driver, Viewcontrctsummary_page, "ExplicitLowWait");

		if (verifyObjectDisplayed(transmitBtn)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					" Summary Page Navigation Successful");

			clickElementJS(transmitBtn);

			driver.switchTo().alert().accept();

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Summary Page Navigation Error");

		}

		waitUntilElementVisibleIE(driver, successmsg, "ExplicitLowWait");

		if (verifyObjectDisplayed(successmsg)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, " Transmition Successful");

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Transmition Error");

		}

	}

	public By crs_num = By.xpath("//input[@name='customerNumber']");
	public By Edit_Btn = By.xpath("(//a[text()='Edit'])[2]");
	@FindBy(xpath = "//input[@name='customerNumber']")
	protected WebElement TourNum;

	/*
	 * Method: sp_CWPR_contract Description:CWPR Contract Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_CWPR_contract() throws EncryptedDocumentException, InvalidFormatException, IOException {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (testData.get("Secondry_owner").contains("Y")) {
			List<WebElement> Stat = driver.findElements(By.xpath("//a[text()='Status']"));
			log.info(Stat.size());
			clickElementJSWithWait(Stat.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().alert().accept();
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {
				if (testData.get("Secondry_owner").contains("N")) {

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ssnSelect.click();
					ssnSelect.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ssnSelect.sendKeys(testData.get("ssnNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@name='home_phone']")).click();
					driver.findElement(By.xpath("//input[@name='home_phone']")).clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@name='home_phone']")).sendKeys(testData.get("homeNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobmon = new Select(driver.findElement(By.xpath("//select[@name='dobmonth']")));
					seldobmon.selectByValue("8");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobday = new Select(driver.findElement(By.xpath("//select[@name='dobday']")));
					seldobday.selectByValue("15");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobyear = new Select(driver.findElement(By.xpath("//select[@name='dobyear']")));
					seldobyear.selectByValue("1947");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(genderSelectRadio);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ssnSelect.click();
					ssnSelect.clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ssnSelect.sendKeys(testData.get("ssnNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@name='home_phone']")).click();
					driver.findElement(By.xpath("//input[@name='home_phone']")).clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@name='home_phone']")).sendKeys(testData.get("homeNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobmon = new Select(driver.findElement(By.xpath("//select[@name='dobmonth']")));
					seldobmon.selectByValue("8");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobday = new Select(driver.findElement(By.xpath("//select[@name='dobday']")));
					seldobday.selectByValue("15");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobyear = new Select(driver.findElement(By.xpath("//select[@name='dobyear']")));
					seldobyear.selectByValue("1947");
					clickElementJS(genderSelectRadio);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (testData.get("Secondry_owner").contains("Y")) {
				if (verifyElementDisplayed(driver.findElement(Edit_Btn))) {

					clickElementJSWithWait(Edit_Btn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					log.info("The ssn is" + ssnSelect.getAttribute("value"));
					if (testData.get("ssnReq").contains("Yes")) {
						ssnSelect.click();
						ssnSelect.clear();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						ssnSelect.sendKeys(testData.get("ssnNumber2"));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
					driver.findElement(By.xpath("//input[@name='home_phone']")).click();
					driver.findElement(By.xpath("//input[@name='home_phone']")).clear();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@name='home_phone']")).sendKeys(testData.get("homeNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobmon = new Select(driver.findElement(By.xpath("//select[@name='dobmonth']")));
					seldobmon.selectByValue("8");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobday = new Select(driver.findElement(By.xpath("//select[@name='dobday']")));
					seldobday.selectByValue("15");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Select seldobyear = new Select(driver.findElement(By.xpath("//select[@name='dobyear']")));
					seldobyear.selectByValue("1947");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(genderSelectRadio);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//input[@value='Update']")).click();

				} else {
					log.info("No Secondary owner good to go");
				}
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			log.info("Owner Page Not Displayed");
		}
		// clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLowWait");

		if (verifyElementDisplayed(udiClick)) {
			clickElementJSWithWait(udiClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Select SaleType = new Select(driver.findElement(By.xpath("//select[@name='udi_subtype']")));
			SaleType.selectByVisibleText(testData.get("ContractType"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(newPoints)) {
			Select sel = new Select(driver.findElement(By.xpath("//select[@name='inventorySite']")));
			sel.selectByIndex(Integer.parseInt(testData.get("InventoryIndex")));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		if (verifyObjectDisplayed(cashSelectRadio)) {
			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLowWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitMedWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Select sel = new Select(driver.findElement(By.id("justification")));
			sel.selectByValue("System Issue");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

		writeDataInExcel(testData.get("ContractType"), strMemberNo, strContractNo, testData.get("Type"));

		transmit();
	}

	/*
	 * Method: sp_MGPR_contract Description:MGPR Contrct Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_MGPR_contract() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (testData.get("Secondry_owner").contains("Y")) {
			List<WebElement> Stat = driver.findElements(By.xpath("//a[text()='Status']"));
			log.info(Stat.size());
			clickElementJSWithWait(Stat.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().alert().accept();
		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ssnSelect.click();
				ssnSelect.clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ssnSelect.sendKeys(testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(genderSelectRadio);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				log.info("SSN Cannot Be Entered");
			}

			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Customer Info page Displayed");

				// JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				log.info("MainLine  Not Present User Can Proceed");

			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("SSN Not Required User Can Proceed");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			if (verifyElementDisplayed(deleteSec)) {
				List<WebElement> list = deleteCheck;

				for (int i = 0; i < list.size(); i++) {

					clickElementJSWithWait(list.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			} else {
				log.info("No Secondary owner good to go");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);

		} else {
			log.info("Owner Page Not Displayed");
		}
		// clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLowWait");

		if (verifyElementDisplayed(udiClick)) {
			clickElementJSWithWait(udiClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Select SaleType = new Select(driver.findElement(By.xpath("//select[@name='udi_subtype']")));
			SaleType.selectByVisibleText(testData.get("ContractType"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(newPoints)) {
			Select sel = new Select(driver.findElement(By.xpath("//select[@name='inventorySite']")));
			sel.selectByIndex(Integer.parseInt(testData.get("InventoryIndex")));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		if (verifyObjectDisplayed(cashSelectRadio)) {
			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashSelectRadio);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLowWait");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitMedWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: fwToUDIConversion Description:FW to UDI Conversion Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void fwToUDIConversion() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, Convert_to_UDI, "ExplicitLowWait");
		if (verifyElementDisplayed(Convert_to_UDI)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Page Navigation Successful");
			Convert_to_UDI.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Converted_contract.click();
			Converted_contract.sendKeys(testData.get("Converted_contract"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Page Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Fixed Week Conversions Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Fixed Week Conversions Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Primary Owner Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Primary Owner Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversion Contract Owners Navigation Successful");
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversion Contract Owners Navigation Failed");
		}
		if (verifyElementDisplayed(RCI_information)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Partners Information Navigation Successful");
			RCI_information.click();
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Partners Information Navigation Failed");
		}
		if (verifyElementDisplayed(Conversion_fee)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Convert Fixed Week to FairShare Plus  Navigation Successful");
			Conversion_fee.click();
			Conversion_fee.clear();
			Conversion_fee.sendKeys(testData.get("Conversion_fee"));
			Processing_fee.click();
			Processing_fee.clear();
			Processing_fee.sendKeys(testData.get("Processing_fee"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Fees Entered");
			Process_conversion_btn.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Convert Fixed Week to FairShare Plus  Navigation Failed");
		}
		if (verifyElementDisplayed(nextVal)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Only Fixed Week to FairShare Plus  Navigation Successful");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Only Fixed Week to FairShare Plus  Navigation Failed");
		}
		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");
		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(print_btn)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Conversions Summary Navigation Successful");
			print_btn.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Conversions Summary Navigation Failed");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Discoveryprint, "ExplicitMedWait");
		if (verifyElementDisplayed(Discoveryprint)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"SalePoint Print Summary Page navigated successfully");

			Discoveryprint.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"SalePoint Print Summary Page navigated failed");

		}

		if (verifyElementDisplayed(contractNo)) {

			strContractNo = contractNo.getText();
			log.info(strContractNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Number Is: " + strContractNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract number could not be retrieved");
		}

	}

	public static void writeDataInExcel(String Contarct_Type, String Member_Num, String Contaract_Num, String Type)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		String ExcelPath = System.getProperty("user.dir") + "\\resources\\TestData_New.xls";
		File file = new File(ExcelPath);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workbook = WorkbookFactory.create(inputStream);
		Sheet sheet = workbook.getSheetAt(0);
		Cell cell = null;
		// return rowIndex;

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String time = dateFormat.format(new Date());
			log.info(time);

			Object[][] bookData = { { Contarct_Type, Member_Num, Contaract_Num, Type, time },

			};

			int rowCount = sheet.getLastRowNum();

			for (Object[] aBook : bookData) {
				Row row = sheet.createRow(++rowCount);

				int columnCount = 0;

				for (Object field : aBook) {
					cell = row.createCell(++columnCount);
					if (field instanceof String) {
						cell.setCellValue((String) field);
					} else if (field instanceof Integer) {
						cell.setCellValue((Integer) field);
					}
				}

			}

			log.info("Excel updated ");
			if (inputStream != null) {
				inputStream.close();
			}
			FileOutputStream outFile = new FileOutputStream(file);
			workbook.write(outFile);
			outFile.close();
		} catch (FileNotFoundException fn) {
			fn.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * Method: sp_WVR_ValidateAutoPopUp Description:Validate Auto Pop up Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WVR_ValidateAutoPopUp() throws Exception {

		// Enter the MENu URL to Navigate to Tour Search page
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Enter Customer/Tour Number
		waitUntilElementVisibleIE(driver, customerNo, "ExplicitLongWait");
		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Page Navigation Error");
		}

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {
				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Automation Pop Up message");

			} catch (Exception e) {
				log.info("no Alert is displayed");
			}

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
					"Automation Pop Up is not available");
		}
	}
	

	public void sp_WVR_DismissAutoPopUp() throws Exception {

		// Enter the MENu URL to Navigate to Tour Search page
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Enter Customer/Tour Number
		waitUntilElementVisibleIE(driver, customerNo, "ExplicitLongWait");
		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Page Navigation Error");
		}

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {
				driver.switchTo().alert().dismiss();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Automation Pop Up message is displayed and dismissed");

			} catch (Exception e) {
				log.info("no Alert is displayed");
			}

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
					"Automation Pop Up is not available");
		}
	}


}
