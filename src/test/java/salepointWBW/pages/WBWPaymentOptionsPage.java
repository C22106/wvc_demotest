package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWPaymentOptionsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWPaymentOptionsPage.class);

	public WBWPaymentOptionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Payment')]")
	protected WebElement paymentOptionsHeader;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Payment')]")
	protected WebElement paymentOptionsHeaderDiv;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[contains(@id,'pymtTypeId')]")
	protected List<WebElement> selectDownPayment;

	@FindBy(name = "useSameAutoPay")
	protected WebElement useSameAutoPay;

	@FindBy(xpath = "//select[@id='duesPACId']")
	protected WebElement selectPaymentMethod;

	@FindBy(xpath = "//input[contains(@name,'Token')]")
	protected WebElement tokenInputBox;

	@FindBy(xpath = "//select[@name='duesCCPACInfo.cardType' or @name='loanCCPACInfo.cardType']")
	protected WebElement selectCardType;
	
	@FindBy(xpath = "//select[@name='loanCCPACInfo.cardType']")
	protected WebElement selectCardTypeLoan;
	
	
	//@FindBy(xpath = "//input[@name='duesCCPACInfo.cardMemberName' or @name='loanCCPACInfo.cardMemberName']")
	protected By nameOnCard=By.xpath("//input[@name='duesCCPACInfo.cardMemberName']");	
	protected By nameOnCardLoan=By.xpath("//input[@name='loanCCPACInfo.cardMemberName']");
	protected By enterCardNumber=By.xpath("//input[@name='duesCCPACInfo.cardNumber' or @name='loanCCPACInfo.cardNumber']");
	protected By enterCardNumberLoan=By.xpath("//input[@name='loanCCPACInfo.cardNumber']");
	//@FindBy(xpath = "//select[@name='duesCCPACInfo.expireMonth' or @name='loanCCPACInfo.expireMonth']")
	protected By selectExpiryMonth=By.xpath("//select[@name='duesCCPACInfo.expireMonth' or @name='loanCCPACInfo.expireMonth']");
	protected By selectExpiryMonthLoan=By.xpath("//select[@name='loanCCPACInfo.expireMonth']");
	//@FindBy(xpath = "//select[@name='duesCCPACInfo.expireYear' or @name='loanCCPACInfo.expireYear']")
	protected By selectExpiryYear=By.xpath("//select[@name='duesCCPACInfo.expireYear' or @name='loanCCPACInfo.expireYear']");
	protected By selectExpiryYearLoan=By.xpath("//select[@name='loanCCPACInfo.expireYear']");
	@FindBy(xpath = "//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']")
	protected WebElement editPaymentWBW;
	
	@FindBy(xpath= "//a[contains(.,'Loan')]")
	protected WebElement linkLoanTab;

	/*
	 * Method: downPaymentPageNavigation Description: Payments Options Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void paymentOptionsNavigation() throws Exception {
		waitUntilElementVisible(driver, paymentOptionsHeaderDiv, "ExplicitLongWait");
		Assert.assertTrue(
				verifyElementDisplayed(paymentOptionsHeader) || verifyElementDisplayed(paymentOptionsHeaderDiv),
				"Payment Options page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Payment Options Page");

	}

	/*
	 * Method: selectPaymentMethod Description:Select Payment Method Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectDownPayment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectDownPayment.get(0), testData.get("downPayment"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: selectPaymentMethod Description:Select Payment Method Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPaymentMethod() {
		waitUntilElementVisibleIE(driver, selectPaymentMethod, "ExplicitMedWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectPaymentMethod, testData.get("PaymentBy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: enterPaymentDetails Description:enter Payment Details Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterPaymentDetails() {
		if (testData.get("PaymentBy").toLowerCase().contains("cc")) {
			waitUntilElementVisibleIE(driver, tokenInputBox, "ExplicitMedWait");
			sendKeyboardKeys(Keys.TAB);
			enterCCAutoPayDetails();
		} else if (testData.get("PaymentBy").toLowerCase().contains("no")) {
			log.info("No Auto Pay Selected");
		} else {
			waitUntilElementVisibleIE(driver, tokenInputBox, "ExplicitMedWait");
			// write code for Auto Pay
		}

	}

	/*
	 * Method: enterCcAutoPayDetails Description:enter Auto Pay Payment Details
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterCCAutoPayDetails() {
		selectByText(selectCardType, testData.get("CCType"));
		getObject(nameOnCard).clear();
		getObject(nameOnCard).sendKeys(testData.get("CCHolder"));
		getObject(enterCardNumber).clear();
		getObject(enterCardNumber).sendKeys(testData.get("CCNumber"));
		selectByText(selectExpiryMonth, testData.get("CCMonth"));
		selectByText(selectExpiryYear, testData.get("CCYear"));
	}
	
	
	

	/*
	 * Method: enterLoanCCAutoPayDetails Description:enter Auto Pay Payment Details
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterLoanCCAutoPayDetails() {
		selectByText(selectCardTypeLoan, testData.get("CCType"));
		getObject(nameOnCardLoan).clear();
		getObject(nameOnCardLoan).sendKeys(testData.get("CCHolder"));
		getObject(enterCardNumberLoan).clear();
		getObject(enterCardNumberLoan).sendKeys(testData.get("CCNumber"));
		selectByText(selectExpiryMonthLoan, testData.get("CCMonth"));
		selectByText(selectExpiryYearLoan, testData.get("CCYear"));
	}

	/*
	 * Method: selectSameAutoPay Description:SelectSameAuto Pay Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectSameAutoPay() {
		if (verifyElementDisplayed(useSameAutoPay)) {
			clickElementJS(useSameAutoPay);
		}

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			log.info("no Alert is displayed");
		}
		

	}
	
	
	public void editPayment(){
		waitUntilElementVisibleIE(driver, editPaymentWBW, "ExplicitLongWait");
		clickElementJS((editPaymentWBW));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	public void clickLoanTab(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS((linkLoanTab));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
}