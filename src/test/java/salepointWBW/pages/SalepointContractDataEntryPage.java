package salepointWBW.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointContractDataEntryPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractDataEntryPage.class);

	public SalepointContractDataEntryPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiClick;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//select[@id='udi_subtype_id']")
	protected WebElement salesubtype;

	@FindBy(xpath = "//input[@value='rciWeek']")
	protected WebElement rciRadioBtn;

	@FindBy(xpath = "//input[@name='pic_info']")
	protected WebElement chkBoxPIC;

	@FindBy(xpath = "//input[@name='bonus_pts']")
	protected WebElement chkBoxBonus;

	/*
	 * Method: contractProcessingDataEntrySelection Description:Select Contract
	 * Processing Data Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingDataEntrySelection() throws Exception {

		waitUntilElementVisibleIE(driver, udiClick, "ExplicitLongWait");

		if (verifyElementDisplayed(udiSelect)) {
			String salestype = testData.get("SalesType");
			WebElement type_sales = driver
					.findElement(By.xpath("//input[@name='sales_types' and @value='" + salestype + "']"));
			clickElementJSWithWait(type_sales);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (salestype.equalsIgnoreCase("UDI")) {
				new Select((salesubtype)).selectByVisibleText(testData.get("ContractType"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(),
					Status.FAIL, "UDI not present");
		}

	}

	/*
	 * Method: selectRCIRadio Description:Select RCI radio Date: 2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void selectRCIRadio() {
		Assert.assertTrue(verifyElementDisplayed(rciRadioBtn), "RCI Radio Button not present");
		clickElementJSWithWait(rciRadioBtn);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Clicked on RCI radio button");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present after Login");
		}
	}
	
	
	 /* Method: selectCheckBoxPIC Description:Select PIC radio Date: 2020 Author:Abhijeet Roy Changes By: NA*/

	/*
	 * Method: selectCheckBoxPIC Description:Select PIC radio Date: 2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void selectCheckBoxPIC() {
		Assert.assertTrue(verifyElementDisplayed(chkBoxPIC), "PIC checkbox not present");
		clickElementJSWithWait(chkBoxPIC);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Clicked on PIC CheckBox");
	}

	/*
	 * Method: clickNextButton Description:Select Next Button Date: 2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			System.out.println("No Alert Present after Login");
		}
	}

	/*
	 * Method: selectcheckoxBONUS Description:Select Bonus Check Box Date: 2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckBoxBonus() {
		Assert.assertTrue(verifyElementDisplayed(chkBoxBonus), "BONUS checkbox not present");
		clickElementJSWithWait(chkBoxBonus);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Clicked on BONUS CheckBox");
	}

	/*
	 * Method: selectRadioDiscovery Description:Select Discovery Radio Date:
	 * 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectRadioDiscovery() {
		WebElement type_sales = driver.findElement(By.xpath("//input[@name='sales_types' and @value='Discovery']"));
		clickElementJSWithWait(type_sales);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Discovery Radio Button Clicked Successfully");
	}
}
