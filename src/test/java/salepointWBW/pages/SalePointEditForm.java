package salepointWBW.pages;



import java.awt.AWTException;
import java.awt.Robot;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointEditForm extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointEditForm.class);

	public SalePointEditForm(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public By printForm=By.xpath("//SELECT[@name='printForm']");
	private By nextBtn=By.xpath("//input[@name='Next']");
	private By resaleRadioBtn=By.xpath("(//input[@name='resale'])[3]");
	@FindBy(xpath = "//input[@name='Save']")
	WebElement save;
	
	@FindBy(xpath = "(//input[@name='resale'])[3]")
	WebElement resaleRadioBtn1;
	
	public void checkAlwaysPrintForForms() throws AWTException{
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		driver.navigate().to(testData.get("EditFormURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointEditForm", "checkAlwaysPrintForForms", 
				Status.PASS, "User navigated to Select Print Form Page");
		
		new Select(driver.findElement(printForm)).selectByVisibleText(testData.get("PrintForm"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		clickElementJS(nextBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", resaleRadioBtn1);
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		boolean resaleRadioBtnIsDisplayed  =driver.findElement(resaleRadioBtn).isDisplayed();
		if(resaleRadioBtnIsDisplayed){
			tcConfig.updateTestReporter("SalePointEditForm", "checkAlwaysPrintForForms", Status.PASS,
					"Resale Radio Button is displayed");
			
		}else{
			tcConfig.updateTestReporter("SalePointEditForm", "checkAlwaysPrintForForms", Status.FAIL,
					"Resale Radio Button is not displayed");
		}
		
		js.executeScript("arguments[0].scrollIntoView();", resaleRadioBtn1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String resaleRadioBtnValue =driver.findElement(resaleRadioBtn).getAttribute("value");
		
		if(resaleRadioBtnValue.equalsIgnoreCase("A")){
			tcConfig.updateTestReporter("SalePointEditForm", "checkAlwaysPrintForForms", Status.PASS,
					"Resale Radio Button is selected for Always Print");
			
		}else{
			tcConfig.updateTestReporter("SalePointEditForm", "checkAlwaysPrintForForms", Status.FAIL,
					"Resale Radio Button is not selected");
		}
		
		clickElementJS(save);
	}

}
