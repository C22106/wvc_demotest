package salepointWBW.pages;
/*package com.wvo.UIScripts.SalePoint.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import org.openqa.selenium.support.PageFactory;


import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalePointWBWOwnerDetailsPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalePointWBWOwnerDetailsPage.class);

	public SalePointWBWOwnerDetailsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	By textDelete = By.xpath("//a[text() = 'Delete']");
	By totalSecondaryOwner = By.xpath("//a[text() = 'Delete']");
	By textStatus = By.xpath("//a[text() = 'Status']");
	By next1 = By.xpath("//input[@name='next']");
	By customerNo = By.id("customerNumber");
	

	public void deleteSecondaryOwner() throws Exception {
		if (verifyObjectDisplayed(textDelete)) {
			if (getList(totalSecondaryOwner).size() > 0) {
				if (getList(totalSecondaryOwner).size() == 1) {
					clickElementJSWithWait(textStatus);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					Alert alert = driver.switchTo().alert();					
					alert.accept();
				} else {
					for (int i = 0; i < getList(totalSecondaryOwner).size() - 1; i++) {
						clickElementJSWithWait(getList(totalSecondaryOwner).get(0));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						Alert alert = driver.switchTo().alert();
						alert.accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					}
					clickElementJSWithWait(textStatus);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					Alert alert = driver.switchTo().alert();
					alert.accept();
				}
			}
		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(next1);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		// alert.accept();

	}
}
*/