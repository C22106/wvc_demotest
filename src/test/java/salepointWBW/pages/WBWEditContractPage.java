package salepointWBW.pages;

import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWEditContractPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWEditContractPage.class);

	public WBWEditContractPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Edit')]")
	protected WebElement editContractHeader;

	@FindBy(name = "next")
	protected WebElement nextButton;

	@FindBy(name = "member_number")
	protected WebElement memberNumber;
	
	/*
	 * Method: editContractPageNavigation Description:Edit Contract Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void editContractPageNavigation() throws Exception {
		driver.navigate().to(testData.get("EditContract"));
		waitUntilElementVisible(driver, editContractHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(editContractHeader), "Edit Contract page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated To Edit Contract Page");
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}
	
	/*
	 * Method: enterMemberNumber Description:enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		if (verifyElementDisplayed(memberNumber)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member Number Field Displayed");
			memberNumber.click();
			memberNumber.sendKeys(testData.get("strMemberNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member Number Field not Displayed");
		}
	}

}
