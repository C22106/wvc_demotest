package salepointWBW.pages;

import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointResalePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointResalePage.class);
	public static String point;
	public static String strContractNo;
	public static String strMemberNo;
	public static String corrspContractNumber;

	public SalePointResalePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By okBtn = By.xpath("//input[@name='ok']");
	protected By tourID = By.name("tourID");
	protected By tourTime = By.name("tourTime");
	protected By nextBtn = By.xpath("//input[@name='next']");
	protected By searchBtn = By.xpath("//input[@name='search']");
	protected By tscField = By.xpath("//input[@name='tsc']");
	protected By saleper = By.name("salesMan");
	protected By deleteSec = By.xpath("//a[contains(.,'Delete')]");
	protected By savecontract = By.id("saveContract");
	protected By generate = By.xpath("//input[@name='print']");
	protected By conNo = By.xpath("//td[contains(.,'The Contract Number for this contract is:')]/b");
	protected By memberNo = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");
	protected By contracts = By.xpath("//span[contains(text(), 'Contracts')]");
	protected By tourIdEnter = By.xpath("tourID");
	protected By deleteOwner = By.xpath("//a[contains(.,'Delete')]");
	protected By paymentSelect = By.name("paymentType");
	protected By nextClick = By.xpath("//button[@name='Next']");
	protected By salePersonId = By.id("wbwcommissions_salesMan");
	protected By saveContractClick = By.id("saveContract");
	protected By printClick = By.xpath("//input[@name='print']");
	protected By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");
	protected By ssnSelect = By.xpath("//input[@name='socialSecurityNumber']");
	protected By homePhoneSelect = By.xpath("//input[@name='homePhone']");
	protected By primaryOwnerPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	protected By ownerDetailsPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");
	protected By MonthFromDrpDwn = By.xpath("//select[@name='beginDateMonth']");
	protected By DateFromDrpDwn = By.xpath("//select[@name='beginDateDay']");
	protected By YearFromDrpDwn = By.xpath("//select[@name='beginDateYear']");
	protected By ssnWBWSelect = By.xpath("//input[@name='socialSecurityNumber']");
	protected By pNameWBW = By.xpath("//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]");
	protected By editPOwnerWBW = By.xpath("//a[contains(.,'Edit')][1]");
	protected By deleteOwnerWBW = By.xpath("//a[contains(.,'Delete')]");
	protected By deleteFirstOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[1]");
	protected By deleteListOfOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[2]");
	protected By MakePrimaryWBW = By.xpath("//a[contains(.,'Make Primary')]");
	protected By holdBtn = By.xpath("//input[@id='holdBtn']");
	protected By removeHoldBtn = By.xpath("//input[@id='removeHoldBtn']");
	protected By proceedBtn = By.xpath("//input[@id='nextBtn']");
	protected By memberNumber = By.name("memberNo");
	public By sellingSiteDrwpdwn = By.id("sellingSite");
	public By financedCheckBox = By.xpath("//input[@name='paymentType' and @value='finance']");
	public By cashCheckBox = By.xpath("//input[@name='paymentType' and @value='cash']");
	public By procFeeCollected = By.name("procFeeCollected");
	protected By ownerInfoScreen = By.xpath("//div[contains(text(),'Owner Info Screen')]");
	protected By deleteCheck = By.xpath("//a[contains(.,'Delete')]");
	protected By duesPaymentMethods = By.id("duesPACId");
	protected By cardtype = By.id("duesCCPACInfo.cardType");
	protected By cardMemberName = By.id("duesCCPACInfo.cardMemberName");
	protected By cardNumber = By.id("duesCCPACInfo.cardNumber");
	protected By cardExpireMonth = By.id("duesCCPACInfo.expireMonth");
	protected By cardExpireYear = By.id("duesCCPACInfo.expireYear");
	protected By sameAutoPay = By.id("useSameAutoPayId");
	protected By nextBtnPaymentPage = By.xpath("//input[@value='Next']");
	protected By printOption = By.id("justification");
	protected By upgradeRadioBtn = By.xpath("//input[@name='contractTypes' and @value='Upgrade']");
	protected By finalizeBtn = By.id("T");
	protected By voidBtn = By.id("V");
	protected By signatureDrpdwn = By.name("signatureDate");
	protected By docStampFeeValue = By.xpath("//td[contains(.,'Doc Stamp Fee')]/following-sibling::td[1]");
	protected By grossPurchasePrice = By.xpath("//td[contains(.,'Gross Purchase Price')]/following-sibling::td");
	protected By serviceFee = By.xpath("(//td[contains(.,'Service Fee')]/following-sibling::td)[1]");
	public By firstAmtTxtBx = By.id("pymtAmt0");
	public By secondAmtTxtBx = By.id("pymtAmt1");
	public By secondTypeDrpdwn = By.id("pymtTypeId1");
	protected By splitIndicator = By.xpath("//td[contains(.,'Split Indicator')]/following-sibling::td");
	protected By thirdPartyIndicator = By.xpath("//td[contains(.,'Third Party Indicator')]/following-sibling::td");
	protected By netPurchasePriceValue = By.xpath("//td[contains(.,'Net Purchase Price')]/following-sibling::td[1]");
	protected By contractDownPymt = By.name("contractDownPymt");
	public By bandSecondaryOwner = By.xpath("//a[contains(@href,'SecondaryOwner')]/following-sibling::b");
	public By chngePrimaryOwnerLink = By.xpath("//a[contains(text(),'Change Primary Owner')]");
	
	
	
	/*
	 * Method: resaleContractSelection Description: resale Contract Selection
	 * Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void resaleContractSelection() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver
				.findElements(By.xpath("(//td[contains(text(),'Available')])/..//input[@id='resaleContractIdRadio']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElement(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		point = driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");

		clickElement(proceedBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(memberNumber)) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Upgrade-Purchase Information Page");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Upgrade-Purchase Information Page");
		}

	}

	/*
	 * Method: resaleContractSelectionAndRelease Description: resale Contract
	 * Selection and then releaseing the same Date: Nov/2020 Author: Kamalesh
	 * Gupta Changes By: NA
	 */

	public void resaleContractSelectionAndRelease() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver
				.findElements(By.xpath("(//td[contains(text(),'Available')])/..//input[@id='resaleContractIdRadio']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElement(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");
		clickElement(removeHoldBtn);

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().equalsIgnoreCase("Available")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Remove Hold button and Status of the selected Contract changed to Available");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[12]")).getText()
				.equals(" ")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Hold User Column is Empty");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Hold User Column is not Empty");
		}

	}

	/*
	 * Method: resaleContractSelection_MultiContract Description: resale
	 * Contract Selection having multiple contact as Yes Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void resaleContractSelection_MultiContract() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver.findElements(By.xpath(
				"//td[contains(text(),'Available')]/preceding-sibling::td[contains(text(),'Y')]/..//input[@id='resaleContractIdRadio']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver.findElement(
							By.xpath("(//td[contains(text(),'Available')]/preceding-sibling::td[contains(text(),'Y')])["
									+ (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElement(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		point = driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");

		clickElement(proceedBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(memberNumber)) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Upgrade-Purchase Information Page");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Upgrade-Purchase Information Page");
		}

	}

	/*
	 * Method: resaleContractSelection_MultiContract Description: resale
	 * Contract Selection having multiple contact as Yes Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void validateRowColor_ResaleContract() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver
				.findElements(By.xpath("//*[@id='resaleContractIdRadio']/../following-sibling::td[3]"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {

				corrspContractNumber = driver
						.findElement(By.xpath(
								"(//*[@id='resaleContractIdRadio'])[" + (i + 1) + "]/../following-sibling::td[1]"))
						.getText().trim();

				String ContractDate = getElementText(a.get(i)).trim();
				ContractDate = ContractDate.replace("-", "/");
				float daysBetween = 0;
				SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date(System.currentTimeMillis());
				try {
					Date UIDate = myFormat.parse(ContractDate);
					Date currentDate = myFormat.parse(myFormat.format(date).toString());
					long difference = UIDate.getTime() - currentDate.getTime();
					daysBetween = (difference / (1000 * 60 * 60 * 24));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (daysBetween <= 5) {
					if (Color.fromString(a.get(i).getCssValue("background-color")).asHex()
							.equalsIgnoreCase("#FF0000")) {
						tcConfig.updateTestReporter("SalePointResalePage", "validateRowColor_ResaleContract",
								Status.PASS, "Row is Red for contract number- " + corrspContractNumber);
					} else {
						tcConfig.updateTestReporter("SalePointResalePage", "validateRowColor_ResaleContract",
								Status.FAIL, "Row is not Red for contract number- " + corrspContractNumber);
					}

				} else if (daysBetween > 5 && daysBetween <= 15) {

					if (Color.fromString(a.get(i).getCssValue("background-color")).asHex()
							.equalsIgnoreCase("#FFFF00")) {
						tcConfig.updateTestReporter("SalePointResalePage", "validateRowColor_ResaleContract",
								Status.PASS, "Row is Yellow for contract number- " + corrspContractNumber);
					} else {
						tcConfig.updateTestReporter("SalePointResalePage", "validateRowColor_ResaleContract",
								Status.FAIL, "Row is not Yellow for contract number- " + corrspContractNumber);
					}

				}

			}
		}

	}

	/*
	 * Method: enterMemberNumberAndTourId Description: resale
	 * enterMemberNumberAndTourId Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void enterMemberNumberAndTourId(String type) {

		sendKeys(memberNumber, testData.get("MemberNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElement(searchBtn);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		waitUntilElementVisible(driver, tscField, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(tscField),
				"Something wrong with member number. Please enter correct member number");
		try {
			if (getElementText(bandSecondaryOwner).trim().contains("(A1)")
					|| getElementText(bandSecondaryOwner).trim().contains("(B1)")
					|| getElementText(bandSecondaryOwner).trim().contains("(A)")
					|| getElementText(bandSecondaryOwner).trim().contains("(B)")) {
				clickElementJSWithWait(chngePrimaryOwnerLink);
			}
		} catch (Exception e) {
			log.info("No Secondary Owner with band A1, B1, A or B");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys(tscField, testData.get("strTourId"));

		selectByText(sellingSiteDrwpdwn, testData.get("SellingSite"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (type.equalsIgnoreCase("CASH")) {
			clickElement(cashCheckBox);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String value = getElementText(netPurchasePriceValue).trim().replace("$", "").replace(",", "");
			// value=String.valueOf(Integer.parseInt(value)/100);
			clickElement(contractDownPymt);
			for (int i = 0; i < 10; i++) {
				sendKeysBy(contractDownPymt, Keys.BACK_SPACE);
				sendKeysBy(contractDownPymt, Keys.DELETE);
			}
			driver.findElement(contractDownPymt).sendKeys(value);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElement(procFeeCollected);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}
			sendKeys(procFeeCollected, testData.get("ProcessingFeeCollected"));

		} else if (type.equalsIgnoreCase("Financed")) {
			clickElement(financedCheckBox);
		}
		clickElement(nextBtn);

	}

	/*
	 * Method: enterMemberNumberAndTourId Description: resale
	 * enterMemberNumberAndTourId Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void removeAdditionalSecondaryOwner() {
		waitUntilElementVisible(driver, nextClick, "ExplicitWaitLongerTime");
		assertTrue(verifyObjectDisplayed(ownerInfoScreen), "Owner info Screen not Displayed");
		if (verifyObjectDisplayed(deleteSec)) {
			recDeleteSecondaryOwner();
		} else {
			System.out.println("No Secondary owner good to go");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElement(nextClick);

	}

	/*
	 * Method: recDeleteSecondaryOwner Description: recursive method to delete
	 * secondary owner Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void recDeleteSecondaryOwner() {
		List<WebElement> e = driver.findElements(deleteCheck);
		if (e.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(e.get(0));
			try {
				new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception exc) {
				log.info("No Alert Present after Login");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			recDeleteSecondaryOwner();
		}
	}

	/*
	 * Method: worldmakePayment_VCC Description: make payment on payment page
	 * spilting the amount in VCC Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void worldmakePayment_VCC() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElement(nextBtn);
		waitUntilElementVisible(driver, duesPaymentMethods, "ExplicitWaitLongerTime");

		double firstValue = Double.parseDouble(getElementAttribute(firstAmtTxtBx, "value"));
		firstValue = firstValue - 500.00;
		clearElement(firstAmtTxtBx);
		sendKeys(firstAmtTxtBx, String.valueOf(firstValue));

		selectByText(secondTypeDrpdwn, "Other - VCC");
		clearElement(secondAmtTxtBx);
		sendKeys(secondAmtTxtBx, "500.00");

		selectByText(duesPaymentMethods, testData.get("PaymentMethod"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(cardtype, testData.get("CardType"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clearElement(cardMemberName);
		sendKeys(cardMemberName, testData.get("CreditCardHolder"));
		clearElement(cardNumber);
		sendKeys(cardNumber, testData.get("CCNumber"));
		selectByText(cardExpireMonth, testData.get("CCMonth"));
		selectByText(cardExpireYear, testData.get("CCYear"));
		if (verifyObjectDisplayed(sameAutoPay)) {
			clickElement(sameAutoPay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		clickElement(nextBtnPaymentPage);
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception exc) {
			log.info("No Alert Present");
		}

	}

	/*
	 * Method: worldmakePayment Description: make payment on payment page Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void worldmakePayment(String type) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (type.equalsIgnoreCase("CASH")) {
			clickElement(cashCheckBox);
			clearElement(procFeeCollected);
			sendKeys(procFeeCollected, testData.get("ProcessingFeeCollected"));

		}
		clickElement(nextBtn);
		waitUntilElementVisible(driver, duesPaymentMethods, "ExplicitWaitLongerTime");

		String paymentType = testData.get("PaymentMethod");

		selectByText(duesPaymentMethods, paymentType);
		if (paymentType.equalsIgnoreCase("CC Auto Pay")) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(cardtype, testData.get("CardType"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElement(cardMemberName);
			sendKeys(cardMemberName, testData.get("CreditCardHolder"));
			clearElement(cardNumber);
			sendKeys(cardNumber, testData.get("CCNumber"));
			selectByText(cardExpireMonth, testData.get("CCMonth"));
			selectByText(cardExpireYear, testData.get("CCYear"));
		} else if (paymentType.equalsIgnoreCase("No Auto Pay")) {

		}
		if (verifyObjectDisplayed(sameAutoPay)) {
			clickElement(sameAutoPay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		clickElement(nextBtnPaymentPage);
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception exc) {
			log.info("No Alert Present");
		}

	}

	/*
	 * Method: contractConfirmation Description: final step for contract
	 * creation Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void contractConfirmation(String type) {
		waitUntilElementVisible(driver, saleper, "ExplicitWaitLongerTime");
		sendKeys(saleper, testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElement(nextBtn);

		waitUntilElementVisible(driver, savecontract, "ExplicitWaitLongerTime");
		if (testData.get("validateDocStampFee").equalsIgnoreCase("Yes")) {
			checkDocStampFee(type);
		}
		if (testData.get("validateServiceFee").equalsIgnoreCase("Yes")) {
			checkServiceFee();
		}
		if (verifyObjectDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointResalePage", "contractConfirmation", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");
			clickElement(savecontract);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitUntilElementVisible(driver, printClick, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(printOption)) {
				new Select(driver.findElement(printOption)).selectByIndex(2);
				tcConfig.updateTestReporter("SalePointResalePage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}

		waitUntilElementVisible(driver, memberNoCheck, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(conNo)) {
			strContractNo = getElementText(conNo).trim();
			System.out.println("Contract no: " + strContractNo);

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.PASS,
					"Contract Created with number: " + getElementText(conNo));
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyObjectDisplayed(memberNo)) {

			strMemberNo = getElementText(memberNo).split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

		log.info("For Selling Site " + testData.get("SellingSite") + " and Tour No " + testData.get("strTourId")
				+ " having credit bandscore " + testData.get("TourBandScore") + " and Member Number " + strMemberNo
				+ ", Contract Number is " + strContractNo);
	}

	/*
	 * Method: finalizeContract Description:finalize Contract Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void finalizeContract() {

		driver.navigate().to(testData.get("ContractManagement"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElement(upgradeRadioBtn);
		clickElement(nextBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElement(By.xpath("//a[contains(text(),'" + strMemberNo + "')]"));
		waitUntilElementVisible(driver, finalizeBtn, "ExplicitWaitLongerTime");
		selectByIndex(signatureDrpdwn, 1);
		clickElement(finalizeBtn);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		waitUntilElementVisible(driver,
				By.xpath("//a[contains(text(),'" + strMemberNo + "')]/../following-sibling::td[4]"),
				"ExplicitWaitLongerTime");
		if (getElementText(By.xpath("//a[contains(text(),'" + strMemberNo + "')]/../following-sibling::td[4]"))
				.equalsIgnoreCase("Finalize")) {
			tcConfig.updateTestReporter("SalePointResalePage", "finalizeContract", Status.PASS,
					"Contract Finalize successfully");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "finalizeContract", Status.FAIL,
					"Contract not Finalize ");
		}

	}

	/*
	 * Method: voidContract Description: mark void Contract Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void voidContract() {

		driver.navigate().to(testData.get("ContractManagement"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElement(upgradeRadioBtn);
		clickElement(nextBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElement(By.xpath("//a[contains(text(),'" + strMemberNo + "')]"));
		waitUntilElementVisible(driver, finalizeBtn, "ExplicitWaitLongerTime");
		selectByIndex(signatureDrpdwn, 1);
		clickElement(voidBtn);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		Assert.assertEquals(
				getElementText(By.xpath("//a[contains(text(),'" + strMemberNo + "')]/../following-sibling::td[4]"))
						.trim(),
				"Void");
		tcConfig.updateTestReporter("SalePointResalePage", "finalizeContract", Status.PASS,
				"Contract marked void successfully");
	}

	/*
	 * Method: checkStatus_VoidContract Description: check Status on resale
	 * contract selection screen after Voiding Contract Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void checkStatus_VoidContract() {
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().equalsIgnoreCase("Available")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Remove Hold button and Status of the selected Contract changed to Available");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}
	}

	/*
	 * Method: checkDocStampFee Description:check Doc Stamp Fee Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	public void checkDocStampFee(String type) {
		if (type.equalsIgnoreCase("CASH")) {
			if (getElementText(docStampFeeValue).contains("$0.00")) {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee for Cash type contract is $0.00");
			} else {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee for Cash type contract is not $0.00");
			}

		} else if (type.equalsIgnoreCase("Financed")) {
			String value = getElementText(grossPurchasePrice).trim().replaceAll("\\D", "");
			if (Integer.parseInt(getElementText(docStampFeeValue).replaceAll("\\D", "")) == 0.35
					* Integer.parseInt(value) / 100) {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee for Financed type contract is " + 0.35 * Integer.parseInt(value) / 10000);
			} else {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee for Financed type contract is incorrect");
			}
		}
	}

	/*
	 * Method: checkServiceFee Description:check service Fee Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void checkServiceFee() {
		String value = getElementText(serviceFee).trim().replaceAll("\\D", "");
		if (Integer.parseInt(value) / 100 == 80) {
			tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
					"Service Fee for Financed type contract is $80");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
					"Service Fee for Financed type contract is not $80.00");
		}
	}

	/*
	 * Method: enterMemberNumberAndTourId Description: resale
	 * enterMemberNumberAndTourId Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void validateSplitAndThirdPartyIndicator() {

		sendKeys(memberNumber, testData.get("MemberNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElement(searchBtn);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		waitUntilElementVisible(driver, tscField, "ExplicitWaitLongerTime");
		Assert.assertTrue(getElementText(splitIndicator).trim().equalsIgnoreCase("Y"),
				"Split Indicator value is not Y");
		tcConfig.updateTestReporter("SalePointResalePage", "validateSplitAndThirdPartyIndicator", Status.PASS,
				"Split Indicator value is Y");

		Assert.assertTrue(getElementText(thirdPartyIndicator).trim().equalsIgnoreCase("Y")
				|| getElementText(thirdPartyIndicator).contains(""), "Third Party value is incorrect");
		tcConfig.updateTestReporter("SalePointResalePage", "validateSplitAndThirdPartyIndicator", Status.PASS,
				"Third Party Indicator value is correctly displayed");
	}
}