package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointContractPriceOverridePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractPriceOverridePage.class);

	public SalepointContractPriceOverridePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;

	public void clicknextButton() {
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");
		clickElementJS(nextClick);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info(e);
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Next button clicked in Price Override Screen");
	}

}
