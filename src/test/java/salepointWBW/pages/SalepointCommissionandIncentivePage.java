package salepointWBW.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointCommissionandIncentivePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointCommissionandIncentivePage.class);

	public SalepointCommissionandIncentivePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement salePersonSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement saleper;

	@FindBy(xpath = "//div[contains(text(),'Commissions and Incentives')]")
	protected WebElement pagetitle;
	

	/*
	 * Method: selectSalesperson Description:Select Salesperson
	 * Date: 2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void selectSalesperson() throws Exception {
		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitLongWait");
		if (verifyElementDisplayed(pagetitle)) {
			waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitMedWait");
			saleper.click();
			saleper.sendKeys(testData.get("strSalePer"));
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			tcConfig.updateTestReporter(getClassName(), getMethodName(),
					Status.PASS, "Commissions and Incentive screen displayed and SalesPerson Number is : "+testData.get("strSalePer"));
			waitUntilElementVisibleIE(driver, nextClick, "ExplicitLowWait");
			
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(),
					Status.FAIL, "Commissions and Incentive screen not displayed");
		}
	}
}
