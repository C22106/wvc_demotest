package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWCombinePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWCombinePage.class);

	public WBWCombinePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Combine')]")
	protected WebElement combineHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(name = "member_number")
	protected WebElement memberNumber;

	@FindBy(xpath = "//td[@class='warning_text']")
	protected List<WebElement> totalContracts;

	@FindBy(name = "trade")
	protected WebElement addContract;

	@FindBy(name = "process")
	protected WebElement processCombine;

	/*
	 * Method: combineNavigation Description:Combine Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void combineNavigation() throws Exception {
		waitUntilElementVisible(driver, combineHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(combineHeader), "Combine page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated To Combine Page");

	}

	/*
	 * Method: enterMemberNumber Description:Enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterMemberNumber() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		memberNumber.clear();
		memberNumber.sendKeys(testData.get("strMemberNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: enterCombineMemberNumber Description:Enter Combine Member Number
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterCombineMemberNumber() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		memberNumber.clear();
		memberNumber.sendKeys(testData.get("strCombineMemberNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}

	/*
	 * Method: checkTotalContracts Description:Check Total Contracts Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void checkTotalContracts(int totalContractList) {
		waitUntilElementVisibleIE(driver, totalContracts.get(0), "ExplicitLongWait");
		Assert.assertTrue(totalContracts.size() == totalContractList,
				totalContractList + " contract(s) displayed incorrectly");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				totalContractList + " contract(s) displayed correctly");

	}

	/*
	 * Method: clickAddContracts Description:Click ADD cONTRACT Button Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void clickAddContracts() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(addContract), "Add Contract Button Not Displayed");
		clickElementJS(addContract);

	}

	/*
	 * Method: clickProcessCombine Description:Click Process Combine Button
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void clickProcessCombine() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(processCombine), "Process Combine Button Not Displayed");
		clickElementJS(processCombine);

	}

}