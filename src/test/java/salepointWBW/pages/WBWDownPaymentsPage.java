package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWDownPaymentsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWDownPaymentsPage.class);

	public WBWDownPaymentsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Down Payments')]")
	protected WebElement downPaymentsHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[@name='downPymtType']")
	protected List<WebElement> selectDownPayment;

	/*
	 * Method: downPaymentPageNavigation Description:Down Payments Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void downPaymentPageNavigation() throws Exception {
		waitUntilElementVisible(driver, downPaymentsHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(downPaymentsHeader), "Purchase Information page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Down Payments Page");

	}

	/*
	 * Method: selectFullDownPayment Description:Select Full Down Payment Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void selectFullDownPayment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectDownPayment.get(0), testData.get("downPayment"));

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}
}