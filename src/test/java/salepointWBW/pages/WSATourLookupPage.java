package salepointWBW.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WSATourLookupPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WSATourLookupPage.class);
	public String selectedCustName;

	public WSATourLookupPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "tourNumber")
	protected WebElement txtTourNumber;

	@FindBy(xpath = "//button[@name='cn-submit']")
	protected WebElement submitBtn;

	@FindBy(xpath = "//button[@name='btn-continue']")
	protected WebElement continueBtn;

	@FindBy(xpath = "//button[@name='btn-existingContinue']")
	protected WebElement existingContinueBtn;

	@FindBy(xpath = "//button[@name='btn-newOwnerContinue']")
	protected WebElement newOwnerContinueBtn;

	@FindBy(xpath = "//input[@name='memberNumber']")
	protected WebElement txtMemberNumber;

	@FindBy(xpath = "//h1[contains(text(),'Co-owners Selection')]")
	protected WebElement coOwnerSelectionPageHeader;

	@FindBy(xpath = "//h1[contains(text(),'Add New Owners')]")
	protected WebElement addOwnersHeader;

	@FindBy(xpath = "(//form[@id='resultsForm']//p)[1]")
	protected WebElement customerNametxt;

	@FindBy(xpath = "(//form[@id='resultsForm']//input[@type='radio'])[1]")
	protected WebElement selectCustRadio;

	/*
	 * Method: tourLookUpWSA Description:Look Up Tour Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */
	public void tourLookUpWSA() throws Exception {
		waitUntilElementVisible(driver, txtTourNumber, "ExplicitLongWait");
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")
				|| testData.get("SubPitchType").equalsIgnoreCase("Fixed Week")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("strTourId"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, "ExplicitLongWait");

			// select the

			String MemberName = testData.get("selectUser");

			// p[contains(.,'"+MemberName+"')]

			// selectedCustName=driver.findElement(customerNametxt).getText();
			// driver.findElement(selectCustRadio).click();

			selectedCustName = MemberName;
			List<WebElement> listogelem = driver
					.findElements(By.xpath("//p[contains(.,'" + MemberName + "')]/../../..//input[@type='radio']"));
			listogelem.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(listogelem.get(0))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member is Selected");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Memebr is not Selected");
			}

			continueBtn.click();

			waitUntilElementVisible(driver, txtMemberNumber, "ExplicitLongWait");

			continueBtn.click();
			waitUntilElementVisible(driver, coOwnerSelectionPageHeader, "ExplicitLongWait");
			existingContinueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, "ExplicitLongWait");
			if (verifyElementDisplayed(addOwnersHeader)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Add Owner Page is DisPlayed");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Add Owner Page is not getting DisPlayed");
			}
			newOwnerContinueBtn.click();

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("strTourId"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")
					&& testData.get("PitchType").equalsIgnoreCase("Upgrade/Conversion")) {
				enterMemberNumber();
			}
			waitUntilElementVisible(driver, continueBtn, "ExplicitLongWait");

			String MemberName = testData.get("selectUser");

			selectedCustName = MemberName;
			List<WebElement> listogelemwbw = driver
					.findElements(By.xpath("//p[contains(.,'" + MemberName + "')]/../../..//input[@type='radio']"));
			listogelemwbw.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(listogelemwbw.get(0))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member is Selected");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Memebr is not Selected");
			}

			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, "ExplicitLowWait");
			if (verifyElementDisplayed(addOwnersHeader)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Add Owner Page is DisPlayed");
				newOwnerContinueBtn.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
						"Add Owner Page is not getting DisPlayed");
			}

			if (verifyObjectDisplayed(closeWarning)) {
				closeWarning.click();
			}

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("strTourId"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, "ExplicitLongWait");
			selectCustRadio.click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, "ExplicitLongWait");
			newOwnerContinueBtn.click();
		}
	}

	@FindBy(xpath = "//select[@id='cust-lookup-type']")
	protected WebElement selectLookUpType;
	@FindBy(name = "cn-submit")
	protected WebElement submitButton;
	@FindBy(name = "memberNumber")
	protected WebElement enterMemberNumber;
	@FindBy(xpath = "//h2[contains(.,'Delinquencies')]/ancestor::div[@id='errorPopup']//img[@class='modalClose']")
	protected WebElement closeDelinquencies;
	@FindBy(xpath = "// h2[contains(.,'warning')]/ancestor::div[@id='errorPopup']//img[@class='modalClose']")
	protected WebElement closeWarning;

	/*
	 * Method: enterMemberNumber Description:Enter Member Number: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */
	public void enterMemberNumber() {
		waitUntilElementVisible(driver, selectLookUpType, "ExplicitLongWait");
		selectByText(selectLookUpType, testData.get("selectLookUpType"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		enterMemberNumber.clear();
		enterMemberNumber.sendKeys(testData.get("strMemberNumber"));
		submitBtn.click();
		waitUntilElementVisible(driver, closeDelinquencies, "ExplicitLowWait");
		if (verifyObjectDisplayed(closeDelinquencies)) {
			closeDelinquencies.click();
		}
	}
}
