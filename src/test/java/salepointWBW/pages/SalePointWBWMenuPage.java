package salepointWBW.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointWBWMenuPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointWBWMenuPage.class);

	public SalePointWBWMenuPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next2;

	// socialSecurityNumber
	@FindBy(id = "wbwcommissions_salesMan")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(id = "saveContract")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	//@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	@FindBy(xpath = "//span[contains(text(), 'Contracts')]")
	WebElement contracts;

	String strContractNo = "";
	String strMemberNo = "";

	By tourIdEnter = By.name("tourID");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By paymentSelect = By.name("paymentType");

	By nextClick = By.xpath("//input[@name='next']");

	By salePersonId = By.id("wbwcommissions_salesMan");

	By saveContractClick = By.id("saveContract");

	By printClick = By.xpath("//input[@name='print']");

	By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");
	
	By ssnSelect =By.xpath("//input[@name='socialSecurityNumber']");
	
	By homePhoneSelect = By.xpath("//input[@name='homePhone']");
	
	By primaryOwnerPage= By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	
	By ownerDetailsPage =By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");
	
	/* changes By Utsav*/
	By MonthFromDrpDwn=By.xpath("//select[@name='beginDateMonth']");
	By DateFromDrpDwn=By.xpath("//select[@name='beginDateDay']");
	By YearFromDrpDwn=By.xpath("//select[@name='beginDateYear']");
	/* changes By Utsav*/
	
	@FindBy(xpath = "//input[@name='socialSecurityNumber']")
	WebElement ssnWBWSelect;
	@FindBy(xpath="//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	WebElement pNameWBW;
	By editPOwnerWBW = By.xpath("//a[contains(.,'Edit')][1]");
	By deleteOwnerWBW = By.xpath("//a[contains(.,'Delete')]");
	By deleteFirstOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[1]");
	By deleteListOfOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[2]");
	
	By MakePrimaryWBW = By.xpath("//a[contains(.,'Make Primary')]");

	public void sp_Ex_ContractFlow() throws Exception {
		
		int noOfSecOwnWBW =0;
		List<WebElement> secOwnlistWBW =null;
		

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisible(driver, tourIdEnter, "ExplicitLongWait");// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));
			//try{
			clickElementJS(next1);
			//}catch(Exception e){
				
			//}
			
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisible(driver, next1, "ExplicitLongWait");
		
		//if(verifyElementDisplayed(driver.findElement(primaryOwnerPage))){
		if(verifyObjectDisplayed(primaryOwnerPage)){	
			
//			driver.findElement(ssnSelect).click();
//			driver.findElement(ssnSelect).clear();
//			waitForSometime(tcConfig.getConfig().get("LowWait"));
//			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			
			//ssn Field 					
			
			if (ssnWBWSelect.isEnabled()==false){
				
				System.out.println("SSN Not Required User Can Proceed");

			}else{
				if (ssnWBWSelect.getAttribute("value").isEmpty()==true){
					ssnWBWSelect.clear();
					ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
				}
			}
			
			driver.findElement(homePhoneSelect).click();
			driver.findElement(homePhoneSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(homePhoneSelect).sendKeys(testData.get("homeNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			
			
			clickElementJS(next1);
			

				
			//delete all secondary owner except one
			
	
			
			
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				List<WebElement> list = driver.findElements(deleteOwner);
//
//				
//				clickElementJSWithWait(list.get(0));
//				try{
//					clickElementJSWithWait(list.get(0));
//				}catch(Exception e){
//					
//				}
//				
//
//				waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//				try {
//					new WebDriverWait(driver, "ExplicitLongWait").until(ExpectedConditions.alertIsPresent());
//
//					Alert alert = driver.switchTo().alert();
//					alert.accept();
//				} catch (Exception e) {
//					System.out.println("No Alert Prersent");
//				}
//
//				waitForSometime(tcConfig.getConfig().get("LowWait"));

//			} else {
//				System.out.println("No Secondary owner good to go");
//			}

			
			
			
		}else if(verifyElementDisplayed(driver.findElement(ownerDetailsPage))){
			System.out.println("OwnerDetails Page Directly Opened Owner Detail Page");
			
			
			//check the secondary owner count with delete buttons 
			
			if (verifyObjectDisplayed(deleteOwnerWBW)) {
				secOwnlistWBW = driver.findElements(deleteOwnerWBW);
				
				noOfSecOwnWBW= secOwnlistWBW.size();
				
			}
			
				
			//check primary owner
			
			if (verifyElementDisplayed(pNameWBW)){
			
			//System.out.println("Primary Owner Exists");
			
			//delete all secondary owner
			for (int i=0;i<noOfSecOwnWBW;i++){
				
				clickElementJSWithWait(deleteFirstOwnerWBW);
				waitForSometime(tcConfig.getConfig().get("MedWait"));	
			
				if (ExpectedConditions.alertIsPresent() != null) {
				try {
	
					driver.switchTo().alert().accept();
	
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			}			
		}else {
				System.out.println("Primary Owner Does not exist");
				
					
				//delete all secondary owner except one
				
				
				for (int i=0;i<(noOfSecOwnWBW-1);i++){
					
					//clickElementJSWithWait(secOwnlist.get(0));
					clickElementJSWithWait(deleteListOfOwnerWBW);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
						
					}	
				
				//make one secondary to primary
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(MakePrimaryWBW)){
				clickElementJSWithWait(MakePrimaryWBW);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				
			}

			if (verifyObjectDisplayed(editPOwnerWBW)) {

				clickElementJSWithWait(editPOwnerWBW);					
			
			if(verifyObjectDisplayed(primaryOwnerPage)){
				if (ssnWBWSelect.isEnabled()==false){
					
					System.out.println("SSN Not Required User Can Proceed");

				}else{
					if (ssnWBWSelect.getAttribute("value").isEmpty()==true){
						ssnWBWSelect.clear();
						ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
					}
				}
				
				driver.findElement(homePhoneSelect).click();
				driver.findElement(homePhoneSelect).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(homePhoneSelect).sendKeys(testData.get("homeNumber"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				try{
					boolean nextCheck=false;
					clickElementJS(next1);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				//	nextCheck=driver.findElement(paymentSelect).isDisplayed();
					nextCheck=driver.findElement(ownerDetailsPage).isDisplayed();

					}
				catch(Exception e){
					next1.click();
				}
				//next1.click();
				
			}
				
			}
			
			noOfSecOwnWBW =0;
			
			
		}else {
				System.out.println("Unhandled Error fow WBW Page");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		
		waitUntilElementInVisible(driver, ownerDetailsPage, "ExplicitLongWait");

		
		if (verifyObjectDisplayed(deleteOwnerWBW)) {
			secOwnlistWBW = driver.findElements(deleteOwnerWBW);
			
			noOfSecOwnWBW= secOwnlistWBW.size();
			
			
			for (int i=0;i<(noOfSecOwnWBW-1);i++){
				
				//clickElementJSWithWait(secOwnlist.get(0));
				clickElementJSWithWait(deleteListOfOwnerWBW);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
					
				}
			
		}
		else
		{
			System.out.println("No Secondary owner good to go");
		}
		
		noOfSecOwnWBW =0;

		
		//clickElementJS(next1);
		next1.click();


		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		//do payment

		waitUntilElementVisibleIE(driver, paymentSelect, "ExplicitLongWait");
		List<WebElement> list1 = driver.findElements(paymentSelect);

		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		new Select(driver.findElement(By.name("explID"))).selectByVisibleText(testData.get("strPoints"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitUntilElementInVisible(driver, nextClick, "ExplicitLongWait");

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextClick);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saleper, "ExplicitLongWait");

		saleper.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleper.sendKeys(testData.get("strSalePer"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementInVisible(driver, saveContractClick, "ExplicitLongWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		//waitUntilElementVisibleIE(driver, printClick, "ExplicitLongWait");
		
		waitUntilElementInVisible(driver, printClick, "ExplicitLongWait");

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			//clickElementJSWithWait(generate);
			generate.click();
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		//waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitLongWait");
		waitUntilElementVisible(driver, conNo, "ExplicitLongWait");

		
		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	@FindBy(xpath = "//input[@value='Experience']")
	WebElement experienceClick;
	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	By experienceSelect = By.xpath("//input[@value='Experience']");

	By summaryPage = By.xpath("//tr[@class='page_title']//div");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By nextValue = By.xpath("//input[@value='Next']");

	public void sp_Ex_contractSearch() throws Exception {
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-2);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementInVisible(driver, experienceSelect, "ExplicitLongWait");

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			
			/* changes By Utsav*/
			
			new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwn)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwn)).selectByValue(strYearFromDrpDwn);
			
			/* changes By Utsav*/

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(experienceClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementInVisible(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), "ExplicitLongWait");
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementInVisible(driver, summaryPage, "ExplicitLongWait");

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), "ExplicitLongWait");
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
								"Contract did not finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	public void sp_WM_ContractFlow() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, "ExplicitLongWait");// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, next1, "ExplicitLongWait");
		
		if(verifyElementDisplayed(driver.findElement(primaryOwnerPage))){
			
			driver.findElement(ssnSelect).click();
			driver.findElement(ssnSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			
			driver.findElement(homePhoneSelect).click();
			driver.findElement(homePhoneSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(homePhoneSelect).sendKeys(testData.get("homeNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			clickElementJS(next1);
			
			
			
		}else if(verifyElementDisplayed(driver.findElement(ownerDetailsPage))){
			System.out.println("OwnerDetails Page Directly Opened GTG");
		}else {
				System.out.println("Unhandled Error fow WBW Page");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, ownerDetailsPage, "ExplicitLongWait");

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			clickElementJSWithWait(list.get(0));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				//driver.navigate().refresh();

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				System.out.println("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			System.out.println("No Secondary owner good to go");
		}

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, paymentSelect, "ExplicitLongWait");
		List<WebElement> list1 = driver.findElements(paymentSelect);

		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(list1.get(1));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitUntilElementVisibleIE(driver, nextValue, "ExplicitLongWait");

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextValue);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, salePersonId, "ExplicitLongWait");

		saleper.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleper.sendKeys(testData.get("strSalePer"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitLongWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, printClick, "ExplicitLongWait");

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJSWithWait(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitLongWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}
	

	
	public void sp_WM_contractSearch() throws Exception {
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-2);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, worldMarkSelect, "ExplicitLongWait");
		

		if (verifyElementDisplayed(worldmarkClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			/* changes By Utsav*/
						
			new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwn)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwn)).selectByValue(strYearFromDrpDwn);
			
			/* changes By Utsav*/
			
			
			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(worldmarkClick);

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), "ExplicitLongWait");
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, "ExplicitLongWait");

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), "ExplicitLongWait");
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}
	
	
	

}
