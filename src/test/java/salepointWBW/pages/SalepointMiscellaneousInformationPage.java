package salepointWBW.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointMiscellaneousInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointMiscellaneousInformationPage.class);

	public SalepointMiscellaneousInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//div[contains(text(),'Miscellaneous Information ')]")
	protected WebElement pageMiscellaneousTitle;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	/*
	 * Method: contrctProcessingMiscellaneousInformation Description:Contract
	 * Processing radio Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contrctProcessingMiscellaneousInformation() {
		waitUntilElementVisibleIE(driver, pageMiscellaneousTitle, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(pageMiscellaneousTitle));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Miscellaneous Information Screen present");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
}