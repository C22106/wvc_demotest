package salepointWBW.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWOwnerInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWOwnerInformationPage.class);

	public WBWOwnerInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Owner Information')]")
	protected WebElement ownerInformationHeader;

	@FindBy(name = "socialSecurityNumber")
	protected WebElement socialSecurityNumber;

	@FindBy(name = "homePhone")
	protected WebElement homePhone;

	@FindBy(name = "workPhone")
	protected WebElement workPhone;

	@FindBy(name = "emailAddress")
	protected WebElement emailAddress;

	@FindBy(name = "firstName")
	protected WebElement firstName;

	@FindBy(name = "lastName")
	protected WebElement lastName;

	@FindBy(name = "address1")
	protected WebElement address1;

	@FindBy(name = "city")
	protected WebElement city;

	@FindBy(xpath = "//select[@name='stateAbbreviation']")
	protected WebElement stateSelect;

	@FindBy(xpath = "//select[@name='country']")
	protected WebElement countrySelect;

	@FindBy(name = "postalCode")
	protected WebElement postalCode;

	@FindBy(name = "next")
	protected WebElement nextButton;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Owners')]")
	protected WebElement ownerDetailsPage;

	// Owner Deatils page

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteSecondaryOwner;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement secondaryOwners;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	protected WebElement primaryOwner;

	/*
	 * Method: enterSecurityNumber Description:Enter SSN Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterSecurityNumber() {
		if (socialSecurityNumber.isEnabled() == false) {
			log.info("SSN Not Required User Can Proceed");
		} else if (socialSecurityNumber.getAttribute("value").isEmpty() == true|| socialSecurityNumber.getAttribute("value").equalsIgnoreCase("0")) {
			socialSecurityNumber.clear();
			socialSecurityNumber.sendKeys(testData.get("ssnNumber"));
		}
	}

	/*
	 * Method: enterFirstName Description:Enter First Name Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterFirstName() {
		if (firstName.isEnabled() == false) {
			log.info("First Name Not Required User Can Proceed");
		} else if (firstName.getAttribute("value").isEmpty() == true) {
			firstName.clear();
			firstName.sendKeys(testData.get("firstName"));
		}
	}

	/*
	 * Method: enterLastName Description:Enter Last Name Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void enterLastName() {
		if (lastName.isEnabled() == false) {
			log.info("Last Name Not Required User Can Proceed");
		} else if (lastName.getAttribute("value").isEmpty() == true) {
			lastName.clear();
			lastName.sendKeys(testData.get("lastname"));
		}
	}

	/*
	 * Method: enterHomePhone Description:Enter Home Phone Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterHomePhone() {
		if (homePhone.isEnabled() == false) {
			log.info("Homephone Not Required User Can Proceed");
		} else if (homePhone.getAttribute("value").isEmpty() == true) {
			homePhone.clear();
			homePhone.sendKeys(testData.get("homePhone"));
		}
	}

	/*
	 * Method: enterWorkPhone Description:Enter Work Phone Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterWorkPhone() {
		if (workPhone.isEnabled() == false) {
			log.info("Work Phone Not Required User Can Proceed");
		} else if (workPhone.getAttribute("value").isEmpty() == true) {
			workPhone.clear();
			workPhone.sendKeys(testData.get("workPhone"));
		}
	}

	/*
	 * Method: enterEmailAddress Description:Enter Email Address Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterEmailAddress() {
		if (emailAddress.isEnabled() == false) {
			log.info("Email Address Not Required User Can Proceed");
		} else if (emailAddress.getAttribute("value").isEmpty() == true) {
			emailAddress.clear();
			emailAddress.sendKeys(testData.get("emailAddress"));
		}
	}

	/*
	 * Method: enterAddressLine1 Description:Enter Address Line 1 Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterAddressLine1() {
		if (address1.isEnabled() == false) {
			log.info("Address Line 1 Not Required User Can Proceed");
		} else if (address1.getAttribute("value").isEmpty() == true) {
			address1.clear();
			address1.sendKeys(testData.get("address1"));
		}
	}

	/*
	 * Method: enterCity Description:Enter City Date: 2020 Author:Unnat Jain
	 * Changes By: NA
	 */
	public void enterCity() {
		if (city.isEnabled() == false) {
			log.info("City Not Required User Can Proceed");
		} else if (city.getAttribute("value").isEmpty() == true) {
			city.clear();
			city.sendKeys(testData.get("city"));
		}
	}

	/*
	 * Method: enterZipCode Description:Enter Zip Code Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void enterZipCode() {
		if (postalCode.isEnabled() == false) {
			log.info("Postal Code Not Required User Can Proceed");
		} else if (postalCode.getAttribute("value").isEmpty() == true) {
			postalCode.clear();
			postalCode.sendKeys(testData.get("postalCode"));
		}
	}

	/*
	 * Method: enterState Description:Enter State Date: 2020 Author:Unnat Jain
	 * Changes By: NA
	 */
	public void enterState() {
		if (stateSelect.isEnabled() == false) {
			log.info("State Not Required User Can Proceed");
		} else {
			selectByText(stateSelect, testData.get("state"));
		}
	}

	/*
	 * Method: enterCountry Description:Enter Country Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void enterCountry() {
		if (countrySelect.isEnabled() == false) {
			log.info("State Not Required User Can Proceed");
		} else {
			selectByText(countrySelect, testData.get("country"));
		}
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}

	/*
	 * Method: enterOwnerInformation Description:Enter Owner Information Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterOwnerInformation() {
		waitUntilElementVisible(driver, ownerInformationHeader, "ExplicitMedWait");
		enterSecurityNumber();
		enterFirstName();
		enterLastName();
		enterHomePhone();
		enterWorkPhone();
		enterEmailAddress();
		enterAddressLine1();
		enterCountry();
		enterState();
		enterCity();
		enterZipCode();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Entered All Details", true);
		clickNextButton();
	}

	/*
	 * Method: updateEmailAddress Description:Enter Owner Information Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void updateEmailAddress() {
		emailAddress.clear();
		emailAddress.sendKeys(testData.get("updatedEmailAddress"));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Email Updated", true);
		clickNextButton();
	}

}