package salepointWBW.pages;
/*package com.wvo.UIScripts.SalePoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalepointContractMoneyScreenPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalepointContractMoneyScreenPage.class);

	public SalepointContractMoneyScreenPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@value='Calculate']")
	WebElement calculate;

	By calculateClick = By.xpath("//input[@value='Calculate']");

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	public void calculateClick() throws Exception {
		waitUntilElementVisibleIE(driver, calculateClick, 120);

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalepointContractMoneyScreenPage", "calculateClick", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalepointContractMoneyScreenPage", "calculateClick", Status.FAIL,
					"Money Screen not present");
		}
	}

}
*/