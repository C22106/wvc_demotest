package salepointWBW.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointEditUserInformation extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointEditUserInformation.class);

	public SalepointEditUserInformation(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//table[@class='client_table']")
	protected WebElement User_table;
	protected String Location_URL = "http://salepointtest1.corproot.com/webapp/ccis/SearchUserServlet?action=currentUser";

	/*
	 * Method: Update_DefaultLocation Description:Update Deafult Location Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void Update_DefaultLocation() {
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().to(testData.get("Location_URL"));
		waitUntilElementVisibleIE(driver, User_table, "ExplicitMedWait");
		if (verifyElementDisplayed(User_table)) {

			Select sel = new Select(driver.findElement(By.xpath("//select[@name='location']")));
			sel.selectByValue(testData.get("Location_Code"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}
}
