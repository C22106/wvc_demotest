package salepointWBW.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointBasePage extends FunctionalComponents {

	public SalePointBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	
	
	/*
	 * *************************Method: selectByIndex ***
	 * *************Description: Drop Down Select By Index**
	 */

	public void selectByIndex(By by, int index) {
		try {
			new Select(getObject(by)).selectByIndex(index);
			;
		} catch (Exception e) {
			log.info("The Drop Down Value is not present");
		}
	}
	
	/*
	 * *************************Method: sendKeysBy ***
	 * ***************Description: Send data to Field**
	 */

	public void sendKeys(By by, String strData) {
		try {
			getObject(by).sendKeys(strData);
		} catch (Exception e) {
			log.info("Could Not Send Keys");
		}
	}
	
	/*
	 * *************************Method: sendKeysBy ***
	 * ***************Description: Send data to Field**
	 */
	public void sendKeys(WebElement wb, String strData) {
		try {
			wb.sendKeys(strData);
		} catch (Exception e) {
			log.info("Could Not Send Keys");
		}

	}
	
	public void sendKeysBy(By by, Keys keys) {
		try {
			getObject(by).sendKeys(keys);
		} catch (Exception e) {
			log.info("Could Not Send Keys");
		}
	}
	
	/*
	 * *************************Method: clearElementBy ***
	 * *************Description: Clear Field Text**
	 */

	public void clearElement(By by) {
		try {
			getObject(by).clear();
		} catch (Exception e) {
			log.info("Could Not Clear Element");
		}
	}

	
	/*
	 * *************************Method: clickElementBy ***
	 * *************Description: Click a Element with By **
	 */

	public void clickElement(By by) {
		try {
			getObject(by).click();
		} catch (Exception e) {
			log.error("Could Not Click The Element");
		}
	}
	
	/*
	 * *************************Method: selectByIndex ***
	 * *************Description: Drop Down Select By Index**
	 */

	public void selectByIndex(WebElement by, int index) {
		try {
			new Select((by)).selectByIndex(index);
			;
		} catch (Exception e) {
			log.info("The Drop Down Value is not present");
		}
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/**
	 * Check If any extra tabs Present
	 * 
	 * @param applicationURl
	 */
	public void navigateToAppURL(String applicationURl) {
		driver.navigate().to(applicationURl);
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}

	/*
	 * Click Element with Java Script Executor
	 * 
	 * @param element
	 * 
	 * @throws Exception
	 */
	public void clickByElementJS(By by) {

		if (getObject(by).isEnabled() && getObject(by).isDisplayed()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", getObject(by));
		} else {
			log.info("Unable to click on element");
		}
	}
	
	public void waitUntilElementVisibleIE(WebDriver driver, WebElement locator, String string) {
		String strValue = tcConfig.getConfig().get(string);
		Long timeOut = Long.parseLong(strValue);
		for (long i = 0; i < timeOut / 2; i++) {

			if (locator.isDisplayed()) {

				waitForSometime("2");
				break;
			} else {

				log.info(locator + " Element not visible");
			}

		}

	}

	public void waitUntilElementVisibleIE(WebDriver driver, By by, String timeOutInSeconds) {
		String strValue = tcConfig.getConfig().get(timeOutInSeconds);
		Long timeOut = Long.parseLong(strValue);
		for (long i = 0; i < timeOut / 2; i++) {
			try {

				if (driver.findElement(by).isDisplayed()) {
					waitForSometime("2");
					break;
				} else {

					System.out.println(by + " Element not visible");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println(by + " Element not found");
				waitForSometime("2");
			}

		}

	}
	/*
	 * *************************Method: selectByText ***
	 * *************Description: Drop Down Select By Text**
	 */
	public void selectByText(WebElement ele, String strData) {
		try {
			new Select(ele).selectByVisibleText(strData);
		} catch (Exception e) {
			log.info("The Drop Down Text is not present");
		}
	}
	
	/* *************************Method: selectByText ***
	 * *************Description: Drop Down Select By Text**
	 */
	public void selectByValue(WebElement ele, String strData) {
		try {
			new Select(ele).selectByValue(strData);
		} catch (Exception e) {
			log.info("The Drop Down Text is not present");
		}
	}
	
	/*
	 * Function to wait until the specified element is visible
	 * 
	 * @param by The {@link WebDriver} locator used to identify the element
	 * 
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(WebDriver driver, By by, String timeOutInSeconds) {
		try {
			String strValue = "";
			strValue = tcConfig.getConfig().get(timeOutInSeconds);
			Long timeOut = Long.parseLong(strValue);

			(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.info("Loading Element: " + by);
		}

	}
	
	/*
	 * Function to wait until the specified element is visible
	 * 
	 * @param by The {@link WebDriver} locator used to identify the element
	 * 
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(WebDriver driver, WebElement locator, String timeOutInSeconds) {
		try {
			String strValue = "";
			strValue = tcConfig.getConfig().get(timeOutInSeconds);
			Long timeOut = Long.parseLong(strValue);
			(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			log.info("Loading Element: " + locator);
		}

	}
}
