package salepointWBW.scripts;

import java.util.Map;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import salepointWBW.pages.SalePointEditForm;
import salepointWBW.pages.SalePointResaleContractSelection;
import salepointWBW.pages.SalePointUpgradeContract;
import salepointWBW.pages.SalePointResalePage;
import salepointWBW.pages.SalePointAdminPage;
//import salepointWBW.pages.SalePointWBWMenuPage;
import salepointWBW.pages.SalePointHomePage;
import salepointWBW.pages.SalePointLoginPage;
import salepointWBW.pages.SalePointOwnerPage;
import salepointWBW.pages.SalePointPaymentScreen;
import salepointWBW.pages.SalePointWBW;
import salepointWBW.pages.SalePointWBWMenuPage;
import salepointWBW.pages.SalePointWVR;
import salepointWBW.pages.SalepointCommissionandIncentivePage;
import salepointWBW.pages.SalepointCompanySelect;
import salepointWBW.pages.SalepointContractDataEntryPage;
import salepointWBW.pages.SalepointContractFeesPage;
import salepointWBW.pages.SalepointContractManagementPage;
import salepointWBW.pages.SalepointContractPaymentPage;
import salepointWBW.pages.SalepointContractPriceOverridePage;
import salepointWBW.pages.SalepointContractPrintPage;
import salepointWBW.pages.SalepointContractProcessingOwners;
import salepointWBW.pages.SalepointContractSummaryPage;
import salepointWBW.pages.SalepointDiscUpgradePage;
import salepointWBW.pages.SalepointDiscoveryPackagePage;
import salepointWBW.pages.SalepointEditContractPage;
import salepointWBW.pages.SalepointEditUserInformation;
import salepointWBW.pages.SalepointInventorySelectionPage;
import salepointWBW.pages.SalepointLocateCustomerPage;
import salepointWBW.pages.SalepointMiscellaneousInformationPage;
import salepointWBW.pages.SalepointTradesPage;
import salepointWBW.pages.WBWCombinePage;
import salepointWBW.pages.WBWComissionsPage;
import salepointWBW.pages.WBWContractManagementPage;
import salepointWBW.pages.WBWDownPaymentsPage;
import salepointWBW.pages.WBWEditContractPage;
import salepointWBW.pages.WBWFinanceInformationPage;
import salepointWBW.pages.WBWOwnerInformationPage;
import salepointWBW.pages.WBWOwnersListPage;
import salepointWBW.pages.WBWPaymentOptionsPage;
import salepointWBW.pages.WBWPurchaseInformationPage;
import salepointWBW.pages.WBWSplitPage;
import salepointWBW.pages.WBWSplitProcessPage;
import salepointWBW.pages.WBWSummaryPage;
import salepointWBW.pages.WBWUpgradeContractPage;
import salepointWBW.pages.WSAHomePage;
import salepointWBW.pages.WSALoginPage;
import salepointWBW.pages.WSANewProposalPage;
import salepointWBW.pages.WSARetrieveTourPage;
import salepointWBW.pages.WSATourLookupPage;
import salepointWBW.pages.WVRAPCombinePage;
import salepointWBW.pages.WVRAPComissionsPage;
import salepointWBW.pages.WVRAPContractManagementPage;
import salepointWBW.pages.WVRAPDownPaymentsPage;
import salepointWBW.pages.WVRAPFinanceInformationPage;
import salepointWBW.pages.WVRAPOwnerInformationPage;
import salepointWBW.pages.WVRAPOwnersListPage;
import salepointWBW.pages.WVRAPPaymentOptionsPage;
import salepointWBW.pages.WVRAPPurchaseInformationPage;
import salepointWBW.pages.WVRAPSplitPage;
import salepointWBW.pages.WVRAPSplitProcessPage;
import salepointWBW.pages.WVRAPSummaryPage;
import salepointWBW.pages.WVRAPTourInformationPage;
import salepointWBW.pages.WVRAPUpgradeContractPage;
import salepointWBW.pages.WVREditSiteInfoPage;
import automation.core.TestBase;

public class SalePointScripts extends TestBase {
	public static final Logger log = Logger.getLogger(SalePointScripts.class);

	/*
	
	/*
	 * Name: Abhijeet Function/event:
	 * tc_034_SP_WBW_Worldmark_Contaract_AutomationFlow Description: WBW
	 * Worldmark Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_034_SP_WBW_Worldmark_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalePointWBW spWBW = new SalePointWBW(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			wsaLogin.launchApplication();
			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();
			spLogin.loginToSP("IE");			
			spHome.companySelectSalePoint();
			spEditUser.Update_DefaultLocation();
			spWBW.navigateToMenuURL();
			spWBW.enterTourID();
			spWBW.acceptOrDeclineAutomationFlow(true);
			spWBW.sp_WBW_edit_PrimaryOwner();
			wbwPaymentOptions.editPayment();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.clickLoanTab();
			wbwPaymentOptions.enterLoanCCAutoPayDetails();
			wbwPaymentOptions.clickNextButton();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.voidContract();
			spLogin.Logout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_001_WBW_Experience_Contract_Finalize Test Case:
	 * TC020_SalePoint_WBW_ Experience Contract_Payment_Financed_CC
	 * Autopay_Discover Description: WBW Experience Contract Creation with CC
	 * Auto Pay and Discovery, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_001_WBW_Experience_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWDownPaymentsPage wbwDownPayment = new WBWDownPaymentsPage(tcconfig);
		WBWFinanceInformationPage wbwFinanceInfo = new WBWFinanceInformationPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.selectPointsPackage();
			wbwPurchaseInfo.clickNextButton();
			wbwDownPayment.downPaymentPageNavigation();
			wbwDownPayment.selectFullDownPayment();
			wbwDownPayment.clickNextButton();
			wbwFinanceInfo.financeInformationPageNavigation();
			wbwFinanceInfo.selectPaymentMethod();
			wbwFinanceInfo.enterPaymentDetails();
			wbwFinanceInfo.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	
	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_004_WBW_Worldmark_EditContract_Finalize Test Case:
	 * TC030_SalePoint_WBW_Verify user can edit the existing WBW Worldmark
	 * contract and update the primary owner Description: WBW WorldMark Contract
	 * Creation, and then edit any info and finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_004_WBW_Worldmark_EditContract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWEditContractPage editContract = new WBWEditContractPage(tcconfig);

		try {
			spLogin.launchApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			editContract.editContractPageNavigation();
			editContract.enterMemberNumber();
			editContract.clickNextButton();
			wbwOwnersList.editPrimaryOwnerandUpdate();
			wbwOwnersList.actionOnSecondaryOwner(false);
			wbwOwnersList.clickNextButton();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_005_WBW_Upgrade_Contract_Finalize Test Case:
	 * TC023_SalePoint_WBW_Upgrade_Verify that user is able to Upgrade existing
	 * Contract_ payment CC AMEX Description: WBW Upgrade Contract Creation with
	 * CC Autopay and AMEX, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_005_WBW_Upgrade_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);

		try {
			spLogin.launchApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.selectSignature();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_007_WBW_Upgrade_Contract_Void Test Case:
	 * TC023_SalePoint_WBW_Upgrade_Verify that user is able to Upgrade existing
	 * Contract_ payment CC AMEX Description: WBW Upgrade Contract Creation with
	 * CC Autopay and AMEX, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_007_WBW_Upgrade_Contract_Void(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);

		try {
			spLogin.launchApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spEditUser.Update_DefaultLocation();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.selectSignature();
			wbwSummary.voidContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_009_WVRAP_Worldmark_Contract_Void Test Case:
	 * TC043_SalePoint_WVRAP_Verify that User is able to create Worldmark
	 * Contract_Financed_CC autopay_VISA down payment type Description: WVRAP
	 * Worldmark Contract Creation with CC Auto Pay and Visa, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_009_WVRAP_Worldmark_Contract_Void(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPOwnersListPage wvrapOwnersList = new WVRAPOwnersListPage(tcconfig);
		WVRAPPurchaseInformationPage wvrapPurchaseInfo = new WVRAPPurchaseInformationPage(tcconfig);
		WVRAPPaymentOptionsPage wvrapPaymentOptions = new WVRAPPaymentOptionsPage(tcconfig);
		WVRAPComissionsPage wvrapComission = new WVRAPComissionsPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapTourPage.enterTourID();
			wvrapTourPage.acceptOrDeclineAutomationFlow(false);
			wvrapOwnersList.ownerInformationPageNavigation();
			wvrapPurchaseInfo.purchaseInformationNavigation();
			wvrapPurchaseInfo.selectPaymentType();
			wvrapPurchaseInfo.clickNextButton();
			wvrapPaymentOptions.paymentOptionsNavigation();
			wvrapPaymentOptions.selectDownPayment();
			wvrapPaymentOptions.selectPaymentMethod();
			wvrapPaymentOptions.enterPaymentDetails();
			wvrapPaymentOptions.selectSameAutoPay();
			wvrapPaymentOptions.clickNextButton();
			wvrapComission.comissionNavigation();
			wvrapComission.enterSalePerson();
			wvrapComission.clickNextButton();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.voidContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

		/*
	 * Name: Utsav Function/event: Description:
	 * TC_309_WD_SaleRep_Edit_Assign_Site_Multiple_WBW
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_309_WD_SaleRep_Edit_Assign_Site_Multiple_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();
			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			spAdminpage.Select_Mutliple_Site_Assign();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_310_WD_SaleRep_Edit_Assign_Site_Multiple_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_310_WD_SaleRep_Edit_Assign_Site_Multiple_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			spAdminpage.Select_Mutliple_Site_Assign();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_311_WD_SaleRep_Role_Based_Test_Access_WBW
	 */

}